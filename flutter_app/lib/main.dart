import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app/screens/GetStarted.dart';
import 'package:flutter_app/screens/login/Signin.dart';
import 'package:flutter_app/models/UserBean.dart';
import 'package:flutter_app/utils/ColorUtils.dart';
import 'package:flutter_app/utils/SessionTimeoutHandler.dart';
import 'package:flutter_app/utils/SessionTimeoutHandler.dart';
import 'package:flutter_app/utils/my_globals.dart';
import 'package:local_auth/local_auth.dart';
import 'screens/Home.dart';
import 'utils/SessionTimeoutHandler.dart';
import 'utils/UtilsImporter.dart';
import 'package:local_auth/error_codes.dart' as auth_error;

void main() async {
  runApp(MyApp());
}


class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) => AppRoot();
}

class AppRoot extends StatefulWidget {
  @override
  AppRootState createState() => AppRootState();
}

class AppRootState extends State<AppRoot> {

  //MyGlobals myGlobals = MyGlobals();

  @override
  void initState() {
    print("initState _initializeTimer==> ");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      // onTap: myGlobals.handleUserInteraction(),
      // onPanDown: myGlobals.handleUserInteraction(),
      // onScaleStart: myGlobals.handleUserInteraction(),
      // ... repeat this for all gesture events
      child: MaterialApp(
          color: getColorFromHex("#2e67b8"),
          debugShowCheckedModeBanner: false,
          title: 'HeartSend',
          theme: ThemeData(
            backgroundColor: getColorFromHex("#D9DDE5"),
          ),
          home: SplashScreen(),
          routes:  <String, WidgetBuilder> {
            '/screen1': (BuildContext context) => new Signin(),
            // '/screen2' : (BuildContext context) => new Screen2(),
          },
       // navigatorKey: SessionTimeoutHandler().navigatorKey,
      ),
    );
  }
}

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<double> animation;
  bool isUserLogin = false;
  Future<List<String>> userProfile;
  UserBean userBean;
  bool isSwitchEnable = false;

  bool isError = false;
  String errorString =  "";
  //MyGlobals myGlobals = MyGlobals();


  startTime() async {
    var _duration = new Duration(seconds: 5);
    return new Timer(_duration, navigationPage);
  }

  @override
  void initState() {
    super.initState();
    Future<bool> isLogin = UtilsImporter().preferencesUtils.getisLogin();
    isLogin.then((data) {
      isUserLogin = data;
    });
    Future<bool> isEnable = UtilsImporter().preferencesUtils.getisTouchEnable();
    isEnable.then((data) {
      isSwitchEnable = data;
    });

    controller = new AnimationController(
      duration: const Duration(seconds: 2),
      vsync: this,
    );

    animation = CurvedAnimation(parent: controller, curve: Curves.easeIn);

    controller.forward();

    startTime();
  }




  final LocalAuthentication _localAuthentication = LocalAuthentication();

  Future<bool> _isBiometricAvailable() async {
    bool isAvailable = false;
    try {
      isAvailable = await _localAuthentication.canCheckBiometrics;
    } on PlatformException catch (e) {
      print("==error occur==>" + e.toString());

      print(e);
    }
    if (!mounted) return isAvailable;
    isAvailable ? print('Biometric is available!') : print('Biometric is unavailable.');
    return isAvailable;
  }

  Future<void> _getListOfBiometricTypes() async {
    List<BiometricType> listOfBiometrics;
    try {
      listOfBiometrics = await _localAuthentication.getAvailableBiometrics();
    } on PlatformException catch (e) {
      print("==error occur==>" + e.toString());
      print(e);
    }
    if (!mounted) return;
    print(listOfBiometrics);
  }

  Future<void> _authenticateUser() async {
    bool isAuthenticated = false;
    String Error = "";
    String ErrorCode = "";

    try {
      isAuthenticated = await _localAuthentication.authenticateWithBiometrics(
        localizedReason: "",
        useErrorDialogs: true,
        stickyAuth: true,
      );
    } on PlatformException catch (e) {
      print("==error occur==>" + e.toString());

      if (e.code == auth_error.lockedOut) {
        // Handle this exception here.
        ErrorCode = e.code;
        Error = e.message;
      }
      if (e.code == auth_error.permanentlyLockedOut) {
        // Handle this exception here.
        ErrorCode = e.code;
        Error = e.message;
      }

    }
    if (!mounted) return;
    isAuthenticated ? print('User is authenticated!') : print('User is not authenticated.');

    if (isAuthenticated) {
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) {
        return Home();
      }));
      //Navigator.of(context).push( MaterialPageRoute( builder: (context) => TransactionScreen(), ), );
    }else{

      setState(() {
        isError = true;
        if (Error.isNotEmpty) {
          if( ErrorCode ==  auth_error.lockedOut) {
            errorString =  Error;
          }
          if( ErrorCode == auth_error.permanentlyLockedOut) {
            errorString =  "The operation was canceled because ERROR_LOCKOUT occurred too many times. Biometric authentication is disabled until Strong authentication like PIN/Pattern/Password is required to unlock.";
          }
        }else {
          SystemNavigator.pop();
        }
      });



    }

  }



  void navigationPage() {
   if (isUserLogin) {
     Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) {
         // return Home();
         return Signin();
       }));




     // if(isSwitchEnable ) {
     //     startAuthentication();
     // }else {
     //     Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) {
     //       return Home();
     //     }));
     // }
   } else {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context) => GetStarted()),
          (Route<dynamic> route) => false);
    }
  }

  Future<void> startAuthentication() async {
    if (await _isBiometricAvailable()) {
      await _getListOfBiometricTypes();
      await _authenticateUser();
    }else {
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) {
        return Home();
      }));
    }

  }

  @override
  Widget build(BuildContext context) {
    myGlobals.myGlobleContext = context;

    return Scaffold(
      backgroundColor: getColorFromHex("#FFFFFF"),
      body: Container(
        color: getColorFromHex("#FFFFFF"),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            FadeTransition(
              opacity: animation,
              child: Center(
                child : Column(
                  children: [


                    Padding(
                        padding: const EdgeInsets.only(
                            left: 40, bottom: 4, top: 20),
                        child: Visibility(
                            maintainSize: true,
                            maintainAnimation: true,
                            maintainState: true,
                            visible: isError,
                            child: Text(
                                errorString,
                                //"Please fill in the required fields*",
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                    color: getRedColor(),
                                    fontWeight: FontWeight.bold,
                                    fontSize: 13,
                                    fontFamily: getThemeFontName())))),

                    Container(
                      padding: EdgeInsets.all(20),
                      child: Image.asset(
                        UtilsImporter().stringUtils.logo,
                        height: 120,
                        fit: BoxFit.fitHeight,
                      ),
                    )


                  ],

                )

                  // child : Container(
                  //   padding: EdgeInsets.all(20),
                  //   child: Image.asset(
                  //     UtilsImporter().stringUtils.logo,
                  //     height: 120,
                  //     fit: BoxFit.fitHeight,
                  //   ),
                  // )
              ),
            ),
          ],
        ),
      ),
    );
  }

}



header(String token) =>
    {"Authorization": token ?? "", "Content-Type": 'application/json'};


//Future<Event> getSentryEnvEvent(dynamic exception, dynamic stackTrace) async {
//  final DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
//  /// return Event with IOS extra information to send it to Sentry
//  if (Platform.isIOS) {
//
//    final IosDeviceInfo iosDeviceInfo = await deviceInfo.iosInfo;
//    return Event(
//      release: '0.0.2',
//      environment: 'development', // replace it as it's desired
//      extra: <String, dynamic>{
//        'name': iosDeviceInfo.name,
//        'model': iosDeviceInfo.model,
//        'systemName': iosDeviceInfo.systemName,
//        'systemVersion': iosDeviceInfo.systemVersion,
//        'localizedModel': iosDeviceInfo.localizedModel,
//        'utsname': iosDeviceInfo.utsname.sysname,
//        'identifierForVendor': iosDeviceInfo.identifierForVendor,
//        'isPhysicalDevice': iosDeviceInfo.isPhysicalDevice,
//      },
//      exception: exception,
//      stackTrace: stackTrace,
//    );
//  }
//
//  /// return Event with Andriod extra information to send it to Sentry
//  if (Platform.isAndroid) {
//    final AndroidDeviceInfo androidDeviceInfo = await deviceInfo.androidInfo;
//    return Event(
//      release: '0.0.2',
//      environment: 'development', // replace it as it's desired
//      extra: <String, dynamic>{
//        'type': androidDeviceInfo.type,
//        'model': androidDeviceInfo.model,
//        'device': androidDeviceInfo.device,
//        'id': androidDeviceInfo.id,
//        'androidId': androidDeviceInfo.androidId,
//        'brand': androidDeviceInfo.brand,
//        'display': androidDeviceInfo.display,
//        'hardware': androidDeviceInfo.hardware,
//        'manufacturer': androidDeviceInfo.manufacturer,
//        'product': androidDeviceInfo.product,
//        'version': androidDeviceInfo.version.release,
//        'supported32BitAbis': androidDeviceInfo.supported32BitAbis,
//        'supported64BitAbis': androidDeviceInfo.supported64BitAbis,
//        'supportedAbis': androidDeviceInfo.supportedAbis,
//        'isPhysicalDevice': androidDeviceInfo.isPhysicalDevice,
//      },
//      exception: exception,
//      stackTrace: stackTrace,
//    );
//  }
//
//  /// Return standard Error in case of non-specifed paltform
//  ///
//  /// if there is no detected platform,
//  /// just return a normal event with no extra information
//  return Event(
//    release: '0.0.2',
//    environment: 'production',
//    exception: exception,
//    stackTrace: stackTrace,
//  );
//}
