import 'dart:convert';

class PainFeelItem {
  String name;
  bool isSelected=false;
  PainFeelItem({
    this.name,
     this.isSelected,
   });

  factory PainFeelItem.fromJson(Map<String, dynamic> json) {
    return PainFeelItem(
      name: json['name'],
      isSelected: false,
    );
  }

}
