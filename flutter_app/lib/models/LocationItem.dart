import 'dart:convert';

//LocationItem imgFromJson(String str) => LocationItem.fromJson(json.decode(str));

class LocationItem {
  String country;
  int geonameid;
  String name;
  String subcountry;

  LocationItem({
    this.country,
    this.geonameid,
    this.name,
    this.subcountry
  });

  factory LocationItem.fromJson(Map<String, dynamic> json) {
    return LocationItem(
      country: json['country'],
      geonameid: json['geonameid'],
      name: json['name'],
      subcountry: json['subcountry'],
    );
  }
}
