import 'package:flutter_app/models/UserBean.dart';

class RegisterResponse {
  String status;
  String message;
  UserBean user;

  RegisterResponse.fromJson(Map<String, dynamic> parsedJson) {
    status = parsedJson['status'];
    message = parsedJson['message'];
    user = UserBean(parsedJson['user']);
  }
}
