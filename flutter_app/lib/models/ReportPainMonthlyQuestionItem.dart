import 'dart:convert';

import 'AnswerItem.dart';

class ReportPainMonthlyQuestionItem {
  String id;
  String question;
  List<AnswerItem> answers = new List();
  int type = 0;

  ReportPainMonthlyQuestionItem({this.question, this.answers});

  ReportPainMonthlyQuestionItem.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    question = json['question'];
    type = json['type'];
    if (json['answers'] != null) {
      answers = new List<AnswerItem>();
      json['answers'].forEach((v) {
        answers.add(new AnswerItem.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['question'] = this.question;
    data['type'] = this.type;
    if (this.answers != null) {
      data['answers'] = this.answers.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
