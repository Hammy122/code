import 'dart:convert';

IndustryItem imgFromJson(String str) => IndustryItem.fromJson(json.decode(str));

class IndustryItem {
  dynamic name;

  IndustryItem({this.name});

//  factory IndustryItem.fromJson(Map<dynamic,dynamic> json) {
//
//    IndustryItem p = new IndustryItem();
//    p.name =  json['name'];
//    return p;
//
////    return new IndustryItem(
////      name: json['name'] as String,
////    );
//  }

//     factory IndustryItem.fromJson(Map<String, dynamic> json) : name = json['name'] as String;

  IndustryItem.fromJson(Map<String, dynamic> json) : name = json['name'];
}
