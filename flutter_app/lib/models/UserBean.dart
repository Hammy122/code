class UserBean {
  String _First_name;
  String _email;
  String _Last_name;
  String _BOD;
  String _Provider_id;
  String _updated_at;
  String _created_at;
  String _id;

  UserBean(parsedJson) {
    _First_name = parsedJson['First_name'];
    _email = parsedJson['email'];
    _Last_name = parsedJson['Last_name'];
    _BOD = parsedJson['BOD'];
    _Provider_id = parsedJson['Provider_id'];
    _updated_at = parsedJson['updated_at'];
    _created_at = parsedJson['created_at'];
    _id = parsedJson['id'];
  }

  String get email => _email;

  set emailAddress(String value) {
    _email = value;
  }

  String get First_name => _First_name;

  set First_name(String value) {
    _First_name = value;
  }

  String get Last_name => _Last_name;

  set Last_name(String value) {
    _Last_name = value;
  }

  String get BOD => _BOD;

  set BOD(String value) {
    _BOD = value;
  }

  String get Provider_id => _Provider_id;

  set Provider_id(String value) {
    _Provider_id = value;
  }

  String get updated_at => _updated_at;

  set updated_at(String value) {
    _updated_at = value;
  }

  String get created_at => _created_at;

  set created_at(String value) {
    _created_at = value;
  }

  String get id => _id;

  set id(String value) {
    _id = value;
  }
}
