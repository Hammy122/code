import 'dart:convert';

import 'AnswerItem.dart';

class ReportWellnessQuestionItem {
  String id;
  String question;
  String sub_text="";
   List<AnswerItem> answers = new List();

  ReportWellnessQuestionItem({
    this.question,
      this.answers
   });

  ReportWellnessQuestionItem.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    question = json['question'];
    sub_text = json['sub_text'];
    if (json['answers'] != null) {
      answers = new List<AnswerItem>();
      json['answers'].forEach((v) {
        answers.add(new AnswerItem.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['question'] = this.question;
    if (this.answers != null) {
      data['answers'] = this.answers.map((v) => v.toJson()).toList();
    }
    return data;
  }

}
