import 'dart:core';


class ReachBean {
  int _id;
  String _name;
  String _handle;
  String _description;
  String _type;
  String _category;
  String _interests;
  double _price;
  bool _is_visible;
  bool _isRead;
  String _photo;
  String _creator_username;
  String _creator_first_name;
  String _creator_last_name;
  String _location;
  String _role;

  ReachBean(
      this._id,
      this._name,
      this._handle,
      this._description,
      this._category,
      this._type,
      this._price,
      this._is_visible,
      this._interests,
      this._photo,
      this._isRead,
      this._creator_username,
      this._creator_first_name,
      this._creator_last_name,
      this._location,
      this._role);

  set emailAddress(int value) {
    _id = value;
  }

  set description(String value) {
    _description = value;
  }

  set name(String value) {
    _name = value;
  }

  set handle(String value) {
    _handle = value;
  }

  set type(String value) {
    _type = value;
  }

  set category(String value) {
    _category = value;
  }

  set interests(String value) {
    _interests = value;
  }

  set price(double value) {
    _price = value;
  }

  set is_visible(bool value) {
    _is_visible = value;
  }

  set isRead(bool value) {
    _isRead = value;
  }

  set picture(String value) {
    _photo = value;
  }

  set creator_username(String value) {
    _creator_username = value;
  }

  set creator_first_name(String value) {
    _creator_first_name = value;
  }

  set creator_last_name(String value) {
    _creator_last_name = value;
  }
  set location(String value) {
    _location = value;
  }
  set role(String value) {
    _role = value;
  }

  int get id => _id;

  String get name => _name;

  String get handle => _handle;

  String get description => _description;

  String get type => _type;

  String get category => _category;

  String get interests => _interests;

  double get price => _price;

  bool get is_visible => _is_visible;

  bool get is_read => _isRead;

  String get photo => _photo;

  String get creator_username => _creator_username;

  String get creator_first_name => _creator_first_name;

  String get creator_last_name => _creator_last_name;
  String get location => _location;
  String get role => _role;

  factory ReachBean.fromJson(Map<String, dynamic> json) {
    return ReachBean(
        json['id'],
        json['name'],
        json['handle'],
        json['description'],
        json['category'],
        json['type'],
        json['price'],
        json['is_visible'],
        json['interests'],
        json['photo'],
        false,
        json['creator']['username'],
        json['creator']['first_name'],
        json['creator']['last_name'],
        json['location'],
        json['creator']['role']
    );
  }
}
