class AnswerItem {
  int id;
  String text;
  bool isSelected;

  AnswerItem({this.id, this.text, this.isSelected});

  AnswerItem.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    text = json['text'];
    isSelected = json['isSelected'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['text'] = this.text;
    data['isSelected'] = this.isSelected;
    return data;
  }
}
