import 'dart:async';

import 'package:dio/dio.dart';
import 'package:flutter_app/utils/UtilsImporter.dart';
import 'package:flutter_app/utils/app_exceptions.dart';
import 'package:flutter_app/utils/network_util.dart';

class RestDatasource {
  NetworkUtil _netUtil = new NetworkUtil();

  Future<dynamic> login(String email, String password) {
    return _netUtil.post(
        UtilsImporter().apiUtils.baseURL + UtilsImporter().apiUtils.signin,
        body: {"email": email, "password": password}).then((dynamic res) {
      print(res.toString());
      if (res.toString().contains("message"))
        throw new FetchDataException(res["message"]);
      return res;
    });
  }

}
