import 'package:flutter/material.dart';


  ///
  /// Convert a color hex-string to a Color object.
  ///
  Color getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll('#', '');

    if (hexColor.length == 6) {
      hexColor = 'FF' + hexColor;
    }

    return Color(int.parse(hexColor, radix: 16));
  }

  Color getThemeBlackColor() {
    return getColorFromHex("#232020");
  }

Color getThemeGrayColor() {
  return getColorFromHex("#505359");
}

String getThemeFontName() {
  return  "Mulish";
}

  Color getRedColor() {
    return getColorFromHex("#c60000");
  }

  Color getInputHintTextColor() {
    return getColorFromHex("#DBDDE2");
  }
  Color getBlackColor() {
    return getColorFromHex("#000000");
  }

  Color getThemeGreenColor() {
    return getColorFromHex("#00A79D");
  }

  Color getThemeBlueColor() {
    return getColorFromHex("#1B76BB");
  }
Color getThemePinkColor() {
  return getColorFromHex("#f26b6b");
}
Color getThemeYellowColor() {
  return getColorFromHex("#ffb650");
}
Color getWhiteColor() {
  return getColorFromHex("#FFFFFF");
}
  Color getInputBoxFillColor() {
    return getColorFromHex("#efeff3");
  }

  Color getInputBoxHintColor() {
    return getColorFromHex("#8c8c8c");
  }
  Color getAnatomicalColor() {
    return getColorFromHex("#FF7765");
  }

