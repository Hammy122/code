import 'package:flutter/material.dart';
import 'package:flutter_app/utils/app_widgets.dart';

import '../models/ChatMessageItem.dart';
import 'ColorUtils.dart';

class ReceivedMessagesWidget extends StatelessWidget {
  final int i;

  const ReceivedMessagesWidget({
    Key key,
    @required this.i,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 7.0),
      child: Row(
        children: <Widget>[
          CircleAvatar(
            backgroundImage: ExactAssetImage('assets/ic_messages_pcp.png'),
          ),
          SizedBox(width: 15),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                constraints: BoxConstraints(
                    maxWidth: MediaQuery.of(context).size.width * .6),
                padding: const EdgeInsets.all(15.0),
                decoration: BoxDecoration(
                  color: getInputBoxFillColor(),
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(25),
                    bottomLeft: Radius.circular(25),
                    bottomRight: Radius.circular(25),
                  ),
                ),
                child:
                Text("${messages[i]['message']}",
                style: TextStyle(
                fontSize: 15,
                color: Colors.black,
                fontWeight: FontWeight.normal))

              ),
              SizedBox(
                height: 5,
              ),
              Text(
                "${messages[i]['time']}",
                style:
                    Theme.of(context).textTheme.body2.apply(color: Colors.grey),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
