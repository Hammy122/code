import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_app/screens/login/Signin.dart';

GlobalKey<ScaffoldState> sessionTimer_scaffoldKey ;
class SessionTimeoutHandler {

  GlobalKey<NavigatorState> navigatorKey = new GlobalKey<NavigatorState>();

  Timer session_timer;

  void session_startTimer() {
    print("====> start timer");
    const time = const Duration(seconds: 10);
    session_timer = Timer.periodic(time, (Timer t) => session_logOutUser());
  }

  session_logOutUser() {
    print("====> stop timer") ;
    if(sessionTimer_scaffoldKey.currentContext != null)
      {
        print("====> NOT NULL ") ;
        Navigator.pushReplacement(sessionTimer_scaffoldKey.currentContext, MaterialPageRoute(builder: (context) {
          return Signin();
        }));
      }
    session_timer.cancel();
  }

//var sessionTimer_scaffoldKey ;
///  GlobalKey<ScaffoldState> sessionTimer_scaffoldKey = new GlobalKey<ScaffoldState>() ;

  // static const MAIN_CONTAINER_ID = 'myapp-main-content';
  // GlobalKey<NavigatorState> _navigator ;
  // //var _navigator ;
  // Timer _sessionTimer;
  // int   _timeoutInSeconds;
  // static DateTime _timeLeft;
  // Timer _timer;
  // //SessionTimeoutHandler(this._navigator, this._timeoutInSeconds );
  // SessionTimeoutHandler(this._scaffoldKey);
  // void installLogoutHandler() {
  //   const time = const Duration(seconds: 10);
  //   print("SESSION_initializeTimer==> ");
  //   _timer = Timer.periodic(time, (Timer t) => _logout());
  // }
  // void _logout() {
  //   if (_scaffoldKey.currentContext != null) {
  //     print("NNNNNOOOOTTTTT_NNUUULLLL.....");
  //   }
  //   if (_scaffoldKey.currentState != null) {
  //     print("NNNNNOOOOTTTTT_NNUUULLLL.....");
  //   }
  //   print("SESSION LOGOUT NOW ===> ");
  //   // _timer.cancel();
  //   // if(_navigator.currentContext != null) {
  //   //     print("NOT NULL ===> " );
  //   // }
  //   // _navigator.currentState.pushNamedAndRemoveUntil( '/screen1', (Route<dynamic> route) => false);
  // }
  // handleUserInteraction(GlobalKey<NavigatorState> _navigatortest) {
  //   _navigator = _navigatortest;
  //   if( _navigatortest != null) {
  //     print("testtexsts ===> ");
  //   }
  //   print("userInteraction ===> ");
  //   if (_timer == null)
  //     return;
  //   if (!_timer.isActive) {
  //     // This means the user has been logged out
  //     print("IF ===> " + _timer.isActive.toString());
  //     return;
  //   }
  //   _timer.cancel();
  //   installLogoutHandler();
  // }
  // handleUserInteractionFine() {
  //   if (_timer == null)
  //     return;
  //   if (!_timer.isActive) {
  //     // This means the user has been logged out
  //     print("IF ===> " + _timer.isActive.toString());
  //     return;
  //   }
  //   _timer.cancel();
  //   installLogoutHandler();
  // }


}

