import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app/screens/GetStarted.dart';
import 'package:flutter_app/screens/login/Signin.dart';
import 'package:intl/intl.dart'; //for date format
import 'package:intl/date_symbol_data_local.dart';
import 'package:toast/toast.dart';

import 'UtilsImporter.dart'; //for date

BoxDecoration myBoxDecoration() {
  return BoxDecoration(
    border: Border.all(
      color: Colors.black,
      width: 2,
    ),
  );
}

DateTime convertToDate(String input) {
  try {
    var d = new DateFormat("yyyy-MM-dd").parseStrict(input);
    return d;
  } catch (e) {
    return null;
  }
}

String convertToDateFull(String input) {
  try {
    var d = new DateFormat("yyyy-MM-dd").parseStrict(input);
    var formatter = new DateFormat('dd MMM yyyy');
    return formatter.format(d);
  } catch (e) {
    return null;
  }
}

String convertToDateFullDt(DateTime input) {
  try {
    var formatter = new DateFormat('dd MMM yyyy');
    return formatter.format(input);
  } catch (e) {
    return null;
  }
}

bool isDate(String dt) {
  try {
    var d = new DateFormat("yyyy-MM-dd").parseStrict(dt);
    return true;
  } catch (e) {
    return false;
  }
}

bool isValidDate(String dt) {
  if (dt.isEmpty || !dt.contains("-") || dt.length < 10) return false;
  List<String> dtItems = dt.split("-");
  var d = DateTime(
      int.parse(dtItems[0]), int.parse(dtItems[1]), int.parse(dtItems[2]));
  return d != null && isDate(dt) && d.isAfter(new DateTime.now());
}

// String functions
String daysAheadAsStr(int daysAhead) {
  var now = new DateTime.now();
  DateTime ft = now.add(new Duration(days: daysAhead));
  return ftDateAsStr(ft);
}

String ftDateAsStr(DateTime ft) {
  // return ft.day.toString().padLeft(2, "0") +
  //     "/" +
  //     ft.month.toString().padLeft(2, "0") +
  //     "/" +
  //     ft.year.toString();

  return ft.year.toString().padLeft(2, "0") +
      "-" +
      ft.month.toString().padLeft(2, "0") +
      "-" +
      ft.day.toString();
}


String symptomsftDateAsStr(DateTime ft) {
  // return ft.day.toString().padLeft(2, "0") +
  //     "/" +
  //     ft.month.toString().padLeft(2, "0") +
  //     "/" +
  //     ft.year.toString();

  return ft.month.toString().padLeft(2, "0") +
      "-" +
      ft.day.toString().padLeft(2, "0") +
      "- " +
      ft.year.toString();
}

String symptomsDateAsStr(DateTime ft) {
  return ft.month.toString().padLeft(2, "0") +
      "/" +
      ft.day.toString().padLeft(2, "0") +
      "/" +
      ft.year.toString();
}




String TrimDate(String dt) {
  if (dt.contains(" ")) {
    List<String> p = dt.split(" ");
    return p[0];
  } else
    return dt;
}

goBack(BuildContext context) {
  if (Navigator.canPop(context)) {
    Navigator.pop(context);
  } else {
    SystemNavigator.pop();
  }
}

doLogout(BuildContext context,String message) {
  Toast.show(message.replaceFirst(RegExp('Unauthorised:'), ' '), context, duration: 7, gravity:  Toast.BOTTOM);
  UtilsImporter()
      .preferencesUtils
      .clearSharedPreferences();

  Navigator.of(context).pushAndRemoveUntil(
      MaterialPageRoute(builder: (context) => GetStarted()),
      (Route<dynamic> route) => false);
}

Future<String> getDeviceId() async {
  var deviceInfo = DeviceInfoPlugin();
  if (Platform.isIOS) {
    // import 'dart:io'
    var iosDeviceInfo = await deviceInfo.iosInfo;
    return iosDeviceInfo.identifierForVendor; // unique ID on iOS
  } else {
    var androidDeviceInfo = await deviceInfo.androidInfo;
    return androidDeviceInfo.androidId; // unique ID on Android
  }
}