class StringUtils {
  //Font
  String fontRoboto = 'roboto';
  String fontLogin = 'login';

  //Labels
  String lableLoginAccount = 'Login to your Account.';
  String lableCreateAccount = 'Create your Account.';
  String lableLoginMobile = 'Login with Mobile Number';
  String lableEmail = 'Email';
  String labelPassword = 'Password';
  String btnLogin = 'Login';
  String btnSendOTP = 'Verify Phone Number';
  String btnReSendOTP = 'Resend Code';
  String btnSubmitOTP = 'Verify OTP';
  String btnCreateAccount = 'Create Account';
  String txtForgetPassword = 'Forgot password?';
  String labelSociaLogin = 'Or login using social media.';
  String labelSociaAccount = 'Or signup using social media.';
  String lableNoaccount = "Don't have an account? ";
  String labelCreate = 'Create';

  String returnEmail = "Enter valid Email";
  String returnName = "Name required minimum 4 characters";

  String returnNumber = "Please enter valid Number";
  String lableEnterOPT = 'Enter OTP';
  String messageSuccess =
      'Your email is registred succesfully, Please check your inbox for create password come back to app. We are waitng for you';
  String messageTitle = 'Thank you!';
  String messageCong = 'Congratulations';
  String userPlaceHolder =
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSQmNZxADT7Tc0oznG7e55bA-WvkdkV7CLNXtHs9QB1jwJdb-gt';
  String bottomMenuHome = 'Home';
  String bottomMenuCategory = 'Category';
  String bottomMenuFavorite = 'Bookmark';
  String bottomMenuSetting = 'Profile';
  String btnBookmark = 'Bookmark';
  String btnRemoveBookmark = 'Remove';
  String btnCopy = 'Copy';
  String btnShare = 'Share';
  String strQuote = 'Quote';
  String messgeNoInternet = 'Please check your internet connection';

  //hint
  String hintEmail = 'Enter email';
  String hintPassword = 'Password';
  String hintFullname = 'Enter fullname';
  String hintMobile = 'Enter mobile number';
  String hintOTP = 'Enter OTP';

  //Images

  String icFacebook = 'images/ic_facebook.png';
  String icGoogle = 'images/ic_google.png';
  String icPhone = 'images/ic_phone.png';
  String logo = 'assets/logo.png';


  String tab_thyreach = 'assets/ic_group.png';
  String getStartedMiddleLogo = 'assets/getstarted_logo.png';


  String reportYourPainLogo = 'assets/reportPain.png';
  String reportYourWellnessLogo = 'assets/ReportWellness.png';
  String reportYourAnxietyLogo = 'assets/ReportAnxiety.png';


  // Privacy
  String privacy = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. \n\n Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.';

  // Password info text
  String returnPasswordTitle = "Your password must include:";
  String returnPasswordDescription = " -At least 8 characters-the more characters, the better. \n - A mixture of both uppercase and lowercase letters. \n - A mixture of letters and numbers.";
  String returnForgotPasswordDescription = " • At least 8 characters-the more \n  characters, the better.  \n • A mixture of both uppercase and lowercase letters. \n • A mixture of letters and numbers.";

  // ProviderCode info text
  String providerCodeTitle = "Provider Code";
  //String ProviderCodeDescription = " (information about the provider’s code here)";
  String ProviderCodeDescription = " (Please enter the code provided by your provider. Should start with an PROxxx)";

  String toolbarLogo= "assets/logoHeartsendToolbar.png";
  String reportSubmittedIcon= "assets/report_submitted.png";

  // Anatomy front
  String front_abdomen = 'assets/abdomin.png';
  String front_head = 'assets/front_head.png';
  String front_throat = 'assets/front_throat.png';
  String front_chest = 'assets/chest.png';
  String front_pelvis = 'assets/pelvis.png';
  String front_left_soulder = 'assets/front_left_soulder.png';
  String front_right_soulder = 'assets/front_right_soulder.png';
  String front_left_upper_arm = 'assets/front_left_upper_arm.png';
  String front_right_upper_arm = 'assets/front_right_upper_arm.png';


  String front_left_elbow_one = 'assets/front_left_elbow_one.png';
  String front_left_elbow_two = 'assets/front_left_elbow_two.png';

  String front_right_elbow_one = 'assets/front_right_elbow_one.png';
  String front_right_elbow_two = 'assets/front_right_elbow_two.png';


  String front_left_elbow_pit = 'assets/front_left_elbow_pit.png';
  String front_right_elbow_pit = 'assets/front_right_elbow_pit.png';
  String front_left_forearm = 'assets/front_left_forearm.png';
  String front_right_forearm = 'assets/front_rightforearm.png';
  String front_left_hand = 'assets/front_left_hand.png';
  String front_right_hand = 'assets/front_right_hand.png';
  String front_left_upper_leg = 'assets/front_left_upper_leg.png';
  String front_right_upper_leg = 'assets/front_right_upper_leg.png';
  String front_left_knee = 'assets/front_left_knee.png';
  String front_right_knee = 'assets/front_right_knee.png';
  String front_left_lower_leg = 'assets/front_left_lower_leg.png';
  String front_right_lower_leg = 'assets/front_right_lower_leg.png';
  String front_left_foot = 'assets/front_left_foot.png';
  String front_right_foot = 'assets/front_right_foot.png';
  String front_left_hip = 'assets/front_left_hip.png';
  String front_right_hip = 'assets/front_right_hip.png';


  // Anatomy back
  String back_head = 'assets/back_head.png';
  String back_neck = 'assets/back_neck.png';
 String back_upper_back = 'assets/back_upper_back.png';
  String back_lower_back = 'assets/back_lower_back.png';
  String back_left_buttock = 'assets/back_left_buttock.png';
  String back_right_buttock = 'assets/back_right_buttock.png';
  String back_left_upper_leg = 'assets/back_left_upper_leg.png';
  String back_right_upper_leg = 'assets/back_right_upper_leg.png';
  String back_left_upper_arm = 'assets/back_left_upper_arm.png';
  String back_right_upper_arm = 'assets/back_right_upper_arm.png';
  String back_left_elbow = 'assets/back_left_elbow.png';
  String back_right_elbow = 'assets/back_right_elbow.png';
  String back_left_lower_arm = 'assets/back_left_lower_arm.png';
  String back_right_lower_arm = 'assets/back_right_lower_arm.png';
  String back_left_hand = 'assets/back_left_hand.png';
  String back_right_hand = 'assets/back_right_hand.png';
//  String front_left_upper_leg = 'assets/front_left_upper_leg.png';
//  String front_right_upper_leg = 'assets/front_right_upper_leg.png';
  String back_left_knee = 'assets/back_left_knee.png';
  String back_right_knee = 'assets/back_right_knee.png';
  String back_left_lower_leg = 'assets/back_left_lower_leg.png';
  String back_right_lower_leg = 'assets/back_right_lower_leg.png';
  String back_left_foot = 'assets/back_left_foot.png';
  String back_right_foot = 'assets/back_right_foot.png';
//  String front_left_hip = 'assets/front_left_hip.png';
//  String front_right_hip = 'assets/front_right_hip.png';

}
