import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/apiHelper/login_response.dart';
import 'package:toast/toast.dart';

import '../models/UserBean.dart';

class CommanUtils {
  static User _user;
  var subscription;

  bool validateEmail(String value) {
    print(value);
    String p = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';

    RegExp regExp = new RegExp(p);

    return regExp.hasMatch(value);
  }

  bool isTextEmpty(String value) {
    print(value);
    if (value.isEmpty)
      return true;
    else
      return false;
  }

  bool validatePassword(String value) {
    if (value.length < 6) {
      return false;
    } else {
      return true;
    }
  }

  bool validateTextField(String value) {
    if (value.length <= 0) {
      return false;
    } else {
      return true;
    }
  }

  bool validateOTP(String value) {
    if (value.length != 6) {
      return false;
    } else {
      return true;
    }
  }

  bool validateMobile(String value) {
    String patttern = r'([+]?\d{1,2}[.-\s]?)?(\d{3}[.-]?){2}\d{4}';
    RegExp regExp = new RegExp(patttern);
    if (value.length == 0) {
      return false;
    } else if (!regExp.hasMatch(value)) {
      return false;
    } else {
      return true;
    }
  }

  void showToast(String message, BuildContext context) {
    Toast.show(message, context,
        duration: Toast.LENGTH_LONG,
        gravity: Toast.BOTTOM,
        backgroundColor: Color.fromRGBO(78, 78, 85, 1),
        textColor: Colors.white);
  }

  void showAlertDialogWithOkButton(String message, BuildContext context) {
    // set up the button
    Widget okButton = FlatButton(
      child: Text("OK",
          style: TextStyle(
              color: Colors.black,
              fontSize: 15,
              fontWeight: FontWeight.w600,
              fontFamily: 'Orkney')),
      onPressed: () {
        //Navigator.pop(context);
        Navigator.of(context, rootNavigator: true).pop('dialog');
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
//      title: Text("My title"),
      content: Text(message,
          style: TextStyle(
              color: Colors.black,
              fontSize: 15,
              fontWeight: FontWeight.normal,
              fontFamily: 'Orkney')),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
  void showAlertDialogWithTitleOkButton(String title,String message, BuildContext context) {
    // set up the button
    Widget okButton = FlatButton(
      child: Text("OK",
          style: TextStyle(
              color: Colors.black,
              fontSize: 15,
              fontWeight: FontWeight.w600,
              fontFamily: 'Orkney')),
      onPressed: () {
        //Navigator.pop(context);
        Navigator.of(context, rootNavigator: true).pop('dialog');
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text(title),
      content: Text(message,
          style: TextStyle(
              color: Colors.black,
              fontSize: 15,
              fontWeight: FontWeight.normal,
              fontFamily: 'Orkney')),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }


  void setCurrentUser(User user) {
    _user = user;
  }

  User getCurrentUser() {
    return _user;
  }

  Future<bool> checkConnection() async {
    return await DataConnectionChecker().hasConnection;
  }
}
