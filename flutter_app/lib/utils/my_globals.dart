import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_app/screens/login/Signin.dart';
import 'package:toast/toast.dart';

MyGlobals myGlobals = MyGlobals();
class MyGlobals {

  GlobalKey<ScaffoldState> _scaffoldKey;
  MyGlobals() {
    _scaffoldKey = GlobalKey<ScaffoldState>();
  }
  Timer _timer;
  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  BuildContext myGlobleContext;

  void initializeTimer() {
    const time = const Duration(seconds: 10);
    print("_initializeTimer==> ");
    // _timer = Timer.periodic(const Duration(seconds: 10), (_) => _logOutUser);
   // _timer = Timer.periodic(time, (Timer t) => logOutUser());
    _timer = Timer.periodic(time, (Timer t) => logOutUser());
  }

  void logOutUser() {
    // Log out the user if they're logged in, then cancel the timer.
    // You'll have to make sure to cancel the timer if the user manually logs out
    //   and to call _initializeTimer once the user logs in
    print("GLOBLE LOGOUT NOW ===> ");
    _timer.cancel();
    // // doLogout(context, UtilsImporter().apiUtils.sessionTimeOut);
    // if (myGlobleContext == null)
    //   print("Context is null $myGlobleContext" );
    // else
    //   print("Context is not null");
    //
    Toast.show( "Session time out!!!!!", myGlobleContext, duration: 7, gravity: Toast.BOTTOM);
    //
    // //Navigator.pop(myGlobleContext);
    // Navigator.pushReplacement(myGlobleContext, MaterialPageRoute(builder: (myGlobleContext) {
    //   // return Home();
    //   return Signin();
    // }));
  }

  handleUserInteraction() {
    print("userInteraction ===> ");
    if (_timer == null)
      return;
    if (!_timer.isActive) {
      // This means the user has been logged out
      print("IF ===> " + _timer.isActive.toString());
      return;
    }
    _timer.cancel();
    initializeTimer();
  }


}