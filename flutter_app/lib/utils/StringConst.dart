class StringConst {
  //Table
  static const TBL_USER = 'quote_user';

  //User
  static const KEY_NAME = 'fullname';
  static const KEY_EMAIL = 'emailAddress';
  static const KEY_NUMBER = 'phoneNumber';
  static const KEY_PROFILE_PIC = 'profilPic';
  static const KEY_UID = 'uid';
  static const KEY_PROVIDER = 'loginProvide';
  static const KEY_NICK_NAME = 'nickname';

  //Preference
  static const String KEY_ISLOGIN = 'isLogin';
  static const String KEY_TOKEN = 'token';
  static const String KEY_ISVERFIED = 'isVerified';
  static const String KEY_USERDATA = 'userdata';
  static const String KEY_ISTOUCHENABLE = 'isTouchEnable';

  // Home screen
  static const double tabFontSize = 12;
  static const double tabIconWidth = 30;
  static const double tabIconHeight = 30;
}
