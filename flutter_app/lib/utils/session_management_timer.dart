import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:toast/toast.dart';

class SessionManagementTimer {
  BuildContext context;
  Timer _timer;

  SessionManagementTimer(BuildContext _context) {
    if (this.context == null)
      this.context = _context;
  }




  void initializeTimer() {
    const time = const Duration(seconds: 15);
    print("_initializeTimer==> ");
    // _timer = Timer.periodic(const Duration(seconds: 10), (_) => _logOutUser);
    _timer = Timer.periodic(time, (Timer t) => logOutUser());
  }

  void logOutUser() {
    // Log out the user if they're logged in, then cancel the timer.
    // You'll have to make sure to cancel the timer if the user manually logs out
    //   and to call _initializeTimer once the user logs in
    print("TEST LOGOUT NOW ===> ");
    _timer.cancel();
    // doLogout(context, UtilsImporter().apiUtils.sessionTimeOut);
    if (context == null)
      print("Context is null");
    else
      print("Context is not null");

    Toast.show(
        "Session time out!!!!!", context, duration: 7, gravity: Toast.BOTTOM);
  }

  handleUserInteraction() {
    print("userInteraction ===> ");
    if (_timer == null)
      return;
    if (!_timer.isActive) {
      // This means the user has been logged out
      print("IF ===> " + _timer.isActive.toString());
      return;
    }

    _timer.cancel();
    initializeTimer();
  }
}