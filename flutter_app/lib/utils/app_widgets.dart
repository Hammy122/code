
import 'package:flutter/widgets.dart';
import 'package:flutter_app/utils/ColorUtils.dart';

TextStyle style = TextStyle(fontFamily: getThemeFontName(), fontSize: 14.0);

Text getTextWidget(String text,double fontSize,Color fontColor,FontWeight weight){
  return Text(text,
      textAlign: TextAlign.center,
      style: style.copyWith(
          fontSize: fontSize,
          color: fontColor,
          fontWeight: weight));
}

Text getTextWidgetWithAlign(String text,double fontSize,Color fontColor,FontWeight weight,TextAlign textAlign){
  return Text(text,
      textAlign: textAlign,
      style: style.copyWith(
          fontSize: fontSize,
          color: fontColor,
          fontWeight: weight));
}

