import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

import '../models/UserBean.dart';

class APIUtils {
  var subscription;

//  var baseURL = "http://heartsend.io/api/";
  var baseURL = "https://heartsend.io/api/";
  var signin = "login";
  var register = "register";
  var allquestion = "allquestion";
  var symptomslist = "symptomslist";
  var addsymptom = "addsymptom";
  var addReport = "addreport";
  var weeklyReport = "weeklyreport";
  var getdatereport = "getdatereport";
  var forgotPassword = "forgotPassword";
  var forgotPasswordOtp = "forgotPasswordotp";
  var updateResetPassword = "updateResetPassword";

  var profile = "profile/";
  var photo = "photo/";
  var logout = "logout";
  var change_password = "auth/password/change/";
  var reset_password = "/auth/password/reset/?email=";
  var reset_password1 = "/auth/password/reset/";

  // Create Reach
  var create_reach = "/reach/";

  //Contact Us
  static final contactEmail = "contact@heartsend.io";
  static final contactEmailSubject = "Help with app";
  static final contactEmailBody =
      "Heartsend team, \n I'm having trouble with your app ";


  //Send Feedback
  static final feedbackEmail = "feedback@heartsend.io";
  static final feedbackSubject = "Heartsend:Feedback Report for App";
  static final feedbackBody = "Tell us whats on your mind : ";

  var DAILY = "daily";
  var WEEKLY = "weekly";
  var SYMPTOMS = "symptoms";
  var WELLNESS = "wellness";
  var ANXIETY = "anxiety";
  var PAIN = "pain";

  // RESPONSE CODES
  static final codePatientDeactivated = 333;

   // Session Time out alert
     var sessionTimeOut= "Session expired, please Sign In again!";

}
