import 'package:flutter_app/models/UserBean.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:async';
import 'package:flutter_app/utils/StringConst.dart' as Const;
import 'UtilsImporter.dart';

class PreferencesUtils {
//  UserBean userBean;

  Future setisLogin() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setBool(Const.StringConst.KEY_ISLOGIN, true);
  }

  Future<bool> getisLogin() async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getBool(Const.StringConst.KEY_ISLOGIN) ?? false;
  }

  Future setisUserVerified(bool isverified) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setBool(Const.StringConst.KEY_ISVERFIED, isverified);
  }

  Future setUserData(List<String> listUserdata) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setStringList(Const.StringConst.KEY_USERDATA, listUserdata);
  }

  Future<List<String>> getUserData() async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getStringList(Const.StringConst.KEY_USERDATA) ?? [];
  }


  Future setToken(String token) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(Const.StringConst.KEY_TOKEN, token);
  }


  Future<String> getToken() async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getString(Const.StringConst.KEY_TOKEN) ?? "";
  }

  Future clearSharedPreferences() async {
    final pref = await SharedPreferences.getInstance();
    await pref.clear();
  }


  Future setisTouchEnable(bool isSet) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setBool(Const.StringConst.KEY_ISTOUCHENABLE, isSet);
  }

  Future<bool> getisTouchEnable() async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getBool(Const.StringConst.KEY_ISTOUCHENABLE) ?? false;
  }



}
