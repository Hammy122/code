

import 'package:flutter_app/utils/APIUtils.dart';
import 'package:flutter_app/utils/PreferencesUtils.dart';

import 'ColorUtils.dart';
import 'CommanUtils.dart';
import 'StringUtils.dart';

class UtilsImporter {
  StringUtils stringUtils = new StringUtils();
//  StyleUtils styleUtils = new StyleUtils();
//  WidgetUtils widgetUtils = new WidgetUtils();
  CommanUtils commanUtils = new CommanUtils();
//  FirebaseAuthUtils firebaseAuthUtils = new FirebaseAuthUtils();
//  ColorUtils colorUtils = new ColorUtils();
//  FirebaseDatabaseUtils firebaseDatabaseUtils = new FirebaseDatabaseUtils();
  PreferencesUtils preferencesUtils = new PreferencesUtils();
  APIUtils apiUtils = new APIUtils();
}
