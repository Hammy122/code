import 'dart:convert';
import 'dart:io';

import 'package:flutter_app/apiHelper/allquestions_response.dart';
import 'package:flutter_app/apiHelper/api_base_helper.dart';
import 'package:flutter_app/apiHelper/getSymptomlist_response.dart';
import 'package:flutter_app/apiHelper/get_report_date_response.dart';
import 'package:flutter_app/apiHelper/login_response.dart';
import 'package:flutter_app/apiHelper/logout_response.dart';
import 'package:flutter_app/models/UserBean.dart';
import 'package:flutter_app/utils/UtilsImporter.dart';

class APICallRepository {
  ApiBaseHelper _helper = ApiBaseHelper();

  Future<LoginResponse> signIn(
      String email, String password, String deviceId) async {
    final response = await _helper.post(
        UtilsImporter().apiUtils.baseURL + UtilsImporter().apiUtils.signin,
        body: {"email": email, "password": password, "device_id": deviceId});
    return LoginResponse.fromJson(response);
  }

  Future<LogoutResponse> signOut(String device_id, String token) async {
    final response = await _helper.postWithHeaders(
        UtilsImporter().apiUtils.baseURL + UtilsImporter().apiUtils.logout,
        headerToken(token),
        body: json.encode({"device_id": device_id}));
    return LogoutResponse.fromJson(response);
  }

  //
  // Future<String> forgotPassword(String email) async {
  //   final response = await _helper.get(UtilsImporter().apiUtils.baseURL + UtilsImporter().apiUtils.reset_password+email);
  //   return response.toString();
  // }
  //
  // Future<UserBean> fetchProfileFromServer(String token) async {
  //   final response = await _helper.getWithHeaders(UtilsImporter().apiUtils.baseURL + UtilsImporter().apiUtils.profile,token);
  //   print("fetchProfile=>"+response.toString());
  //   return UserBean.fromJson(response);
  // }
  //
  // Future<UserBean> callEditProfilePhotoAPI(String token,File image,String email,String first_name,String last_name) async {
  //   final response = await _helper.postPhotoDio(UtilsImporter().apiUtils.baseURL +
  //       UtilsImporter().apiUtils.profile +
  //       UtilsImporter().apiUtils.photo,image,token,email,first_name,last_name);
  //   print("response==>"+response);
  //   return UserBean.fromJson(json.decode(response));
  // }

  Future<String> signUp(String email, String firstname, String lastname,
      String password, String DOB, String Provider_id) async {
    print("email ==####==> " + email.toString());
    final response = await _helper.postWithHeaders(
        UtilsImporter().apiUtils.baseURL + UtilsImporter().apiUtils.register,
        header(),
        body: json.encode({
          "email": email,
          "password": password,
          "First_name": firstname,
          "Last_name": lastname,
          "BOD": DOB,
          "Provider_id": Provider_id
        }));
    return response.toString();
  }

  Future<String> forgotPassword(String email) async {
    print("email ==####==> " + email);
    final response = await _helper.postWithHeaders(
        UtilsImporter().apiUtils.baseURL +
            UtilsImporter().apiUtils.forgotPassword,
        header(),
        body: json.encode({
          "email": email,
        }));
    return response.toString();
  }

  Future<String> forgotPasswordOtp(String email, String otp) async {
    print("otp ==####==> " + otp);
    final response = await _helper.postWithHeaders(
        UtilsImporter().apiUtils.baseURL +
            UtilsImporter().apiUtils.forgotPasswordOtp,
        header(),
        body: json.encode({
          "email": email,
          "otp": otp,
        }));
    return response.toString();
  }

  Future<String> updateResetPassword(String email, String password) async {
    print("emai ==####==> " + email);
    print("password ==####==> " + password);
    final response = await _helper.postWithHeaders(
        UtilsImporter().apiUtils.baseURL +
            UtilsImporter().apiUtils.updateResetPassword,
        header(),
        body: json.encode({
          "email": email,
          "password": password,
        }));
    return response.toString();
  }

  Future<QuestionsResponse> allquestion(String device_id, String Question_type,
      String Question_section, String token) async {
    final response = await _helper.postWithHeaders(
        UtilsImporter().apiUtils.baseURL + UtilsImporter().apiUtils.allquestion,
        headerToken(token),
        body: json.encode({
          "device_id": device_id,
          "Question_type": Question_type,
          "Question_section": Question_section
        }));
    return QuestionsResponse.fromJson(response);
  }

  Future<GetReport> getdatereport(
      String device_id, String Question_section, String token) async {
    final response = await _helper.postWithHeaders(
        UtilsImporter().apiUtils.baseURL +
            UtilsImporter().apiUtils.getdatereport,
        headerToken(token),
        body: json.encode(
            {"device_id": device_id, "Question_section": Question_section}));
    return GetReport.fromJson(response);
  }

  // Future<String> addDailyReport(
  //       String token
  //     , String Question_section
  //     , String Question_value
  //     , String Questionrange
  //     , String device_id
  //     , String flag
  //     , String Report_type
  //     , int Question_id
  //     , String Question_ans
  //
  //     ) async {
  //
  //   final response = await _helper.postWithHeaders(UtilsImporter().apiUtils.baseURL + UtilsImporter().apiUtils.addReport,
  //       headerToken(token),
  //       body: json.encode({
  //         "Question_section": Question_section,
  //         "Question_value": Question_value,
  //         "Questionrange": Questionrange,
  //         "device_id": device_id,
  //         "flag": flag,
  //         "Report_type": Report_type,
  //         "Question_id": Question_id,
  //         "Question_ans": Question_ans
  //
  //       })
  //   );
  //
  //   return response.toString();
  // }

  // Future<String> addWeeklyReport(
  //       String token
  //     , String device_id
  //     , String bodyData
  //
  //     ) async {
  //
  //   final response = await _helper.postWithHeaders(UtilsImporter().apiUtils.baseURL + UtilsImporter().apiUtils.weeklyReport,
  //       headerToken(token),
  //       // body: json.encode({
  //       //   "device_id": device_id,
  //       //   "data": bodyData,
  //       // })
  //       body: json.encode({
  //         "data": bodyData,
  //       })
  //   );
  //   return response.toString();
  // }

  // Future<String> addWeeklyReport(
  //     String token
  //     , String device_id
  //     , String bodyData
  //     ,File file ) async {
  //
  //   final response = await _helper.postWithHeadersAndFile(
  //       UtilsImporter().apiUtils.baseURL + UtilsImporter().apiUtils.weeklyReport,
  //       headerToken(token) ,
  //       file,
  //       device_id);
  //
  //   return response.toString();
  // }

  Future<String> addWeeklyReport(
      String token, String deviceID, File file) async {
    final response = await _helper.postWithHeadersAndFile(
        UtilsImporter().apiUtils.baseURL +
            UtilsImporter().apiUtils.weeklyReport,
        headerToken(token),
        file,
        deviceID);

    return response.toString();
  }

  // Future<String> dailyReportApi(
  //     String token
  //     , String device_id
  //     ,File file ) async {
  //
  //   final response = await _helper.postWithHeadersAndFile(
  //       UtilsImporter().apiUtils.baseURL + UtilsImporter().apiUtils.addReport,
  //       headerToken(token) ,
  //       file,
  //       device_id);
  //
  //   return response.toString();
  // }

  Future<String> addSymptom(
      String device_id, String Symptoms_name, String token) async {
    final response = await _helper.postWithHeaders(
        UtilsImporter().apiUtils.baseURL + UtilsImporter().apiUtils.addsymptom,
        headerToken(token),
        body: json
            .encode({"device_id": device_id, "Symptoms_name": Symptoms_name}));
    return response.toString();
  }

  Future<GetSymptomsResponse> symptomslist(
      String device_id, String token) async {
    final response = await _helper.postWithHeaders(
        UtilsImporter().apiUtils.baseURL +
            UtilsImporter().apiUtils.symptomslist,
        headerToken(token),
        body: json.encode({"device_id": device_id}));
    return GetSymptomsResponse.fromJson(response);
  }

  Future<String> signout(String token) async {
    final response = await _helper.postWithHeaders(
        UtilsImporter().apiUtils.baseURL + UtilsImporter().apiUtils.logout,
        headerToken("Token " + token),
        body: null);
    return response.toString();
  }

  // Future<UserBean> callEditProfileAPI(String token,
  //     String first_name,
  //     String last_name,
  //     String number,
  //     String company_name,
  //     String company_info,
  //     String role,
  //     String about,
  //     String linkedin,
  //     String twitter,
  //     String website,
  //     String photo,
  //     String verified,
  //     String type) async {
  //
  //   final Map<String, dynamic> data = new Map<String, dynamic>();
  //   data['first_name'] = first_name;
  //   data['last_name'] = last_name;
  //
  //   data['profile'] = {
  //     "number": first_name,
  //     "company_name": company_name,
  //     "company_info": company_info,
  //     "role": role,
  //     "about": about,
  //     "linkedin": linkedin,
  //     "twitter": twitter,
  //     "website": website,
  //     "verified": verified,
  //     "type": type,
  //   };
  //   final response = await _helper.postWithHeaders(UtilsImporter().apiUtils.baseURL + UtilsImporter().apiUtils.profile,headerToken("Token "+token),
  //       body: json.encode(data)
  //   );
  //   return UserBean.fromJson(response);
  // }

  // Future<String> confirm(String email, String code) async {
  //   final response = await _helper.postWithHeaders(UtilsImporter().apiUtils.baseURL + UtilsImporter().apiUtils.confirm,header(),
  //       body: json.encode({
  //         "username": email,
  //         "code": code
  //       })
  //   );
  //   return response.toString();
  // }
  //header() => {"Content-Type": 'application/x-www-form-urlencoded', "Authorization": 'Bearer', "Accept": 'application/json'};
  header() => {"Content-Type": 'application/json'};

  headerToken(String token) => {
        "Authorization": "Bearer " + token ?? "",
        "Content-Type": "application/json"
      };

// headerTokenURLEncoded(String token) => {
//       "Authorization": "Bearer " + token ?? "",
//       "Content-Type": "application/x-www-form-urlencoded"
//     };
}
