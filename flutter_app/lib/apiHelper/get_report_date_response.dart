// To parse this JSON data, do
//
//     final getReport = getReportFromJson(jsonString);

import 'dart:convert';

GetReport getReportFromJson(String str) => GetReport.fromJson(json.decode(str));

String getReportToJson(GetReport data) => json.encode(data.toJson());

class GetReport {
  GetReport({
    this.response,
  });

  Response response;

  factory GetReport.fromJson(Map<String, dynamic> json) => GetReport(
    response: Response.fromJson(json["response"]),
  );

  Map<String, dynamic> toJson() => {
    "response": response.toJson(),
  };
}

class Response {
  Response({
    this.status,
    this.message,
    this.submittedTime,
    this.nextIterationTime,
    this.nextSubmissionDate,
    this.daysLeft,
    this.isAllow,
  });

  String status;
  String message;
  int submittedTime;
  int nextIterationTime;
  String nextSubmissionDate;
  int daysLeft;
  String isAllow;

  factory Response.fromJson(Map<String, dynamic> json) => Response(
    status: json["status"],
    message: json["message"],
    submittedTime: json["submitted_time"],
    nextIterationTime: json["next_iteration_time"],
    nextSubmissionDate:  json["next_submission_date"],
    daysLeft: json["daysLeft"],
    isAllow: json["is_allow"],
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "submitted_time": submittedTime,
    "next_iteration_time": nextIterationTime,
    "next_submission_date":  nextSubmissionDate,
    "daysLeft": daysLeft,
    "is_allow": isAllow,
  };
}
