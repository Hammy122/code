import 'dart:io';
import 'package:dio/dio.dart';
import 'package:flutter_app/utils/APIUtils.dart';
import 'package:flutter_app/utils/UtilsImporter.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';
import 'package:path/path.dart';
import 'app_exceptions.dart';

class ApiBaseHelper {
  Future<dynamic> get(String url) async {
    print('Api Get, url $url');
    var responseJson;
    try {
      final response = await http.get(url);
      responseJson = _returnResponse(response);
    } on BadRequestException catch (e) {
      throw BadRequestException(e);
    } on SocketException {
      print('No net');
      throw FetchDataException('No Internet connection');
    }

    return responseJson;
  }

  Future<dynamic> getWithHeaders(String url, String token) async {
    print('Api Get, url $url');
    var responseJson;
    try {
      header(String token) =>
          {"Authorization": token ?? "", "Content-Type": "application/json"};

      final response = await http.get(url, headers: header("Token " + token));

      responseJson = _returnResponse(response);
    } on BadRequestException catch (e) {
      throw BadRequestException(e);
    } on SocketException {
      print('No net');
      throw FetchDataException('No Internet connection');
    }

    return responseJson;
  }

  Future<dynamic> post(String url, {Map<String, String> body}) async {
    print('Api Post, url $url');
    var responseJson;
    try {
      final response = await http.post(url, body: body);
      responseJson = _returnResponse(response);
    } on SocketException {
      print('No net');
      throw FetchDataException('No Internet connection');
    } on BadRequestException catch (e) {
      throw BadRequestException(e);
    }
    print('api post.');
    return responseJson;
  }

  Future<dynamic> postPhotoDio(String url, File photo, String token,
      String email, String first_name, String last_name) async {
    print('Api Post, url ==>  $url');
    print('Api Post, token ==> $token');
    print('Api Post, username ==>  $email');
    print('Api Post, first_name ==>  $first_name');
    print('Api Post, last_name ==>  $last_name');
    print('Api Post, email  ==> $email');
    print('Api Post, image ==>  ' + photo.path);

    var responseJson;
    try {
      FormData formData = new FormData.fromMap({
        "username": email,
        "first_name": first_name,
        "last_name": last_name,
        "email": email,
        "photo": photo == null
            ? ""
            : await MultipartFile.fromFile(photo.path,
                filename: basename(photo.path)),
      });
      Dio dio = new Dio();
      dio.options.headers = header("Token " + token);
      final response = await dio.post(url, data: formData);
      responseJson = _returnDaoResponse(response);
//        return json.decode(json.encode(response.data.toString()));
    } on DioError catch (e) {
      print("==DioError==>" + e.response.data.toString());
      throw BadRequestException(e.response.data.toString());
    } on SocketException {
      print('No net');
      throw FetchDataException('No Internet connection');
    } on BadRequestException catch (e) {
      print("==BadRequestException==>" + e.toString());
      throw BadRequestException(e);
    }
    print('api post.');
    return responseJson;
  }

  Future<dynamic> postWithHeaders(String url, Map<String, String> header,
      {String body}) async {
    print('Api Post, url $url');
    print('Api Post, headers $header');
    print('Api Post, body $body');

    var responseJson;
    try {
      final response = await http.post(url, body: body, headers: header);
      responseJson = _returnResponse(response);
    } on SocketException {
      print('No net');
      throw FetchDataException('No Internet connection');
    } on BadRequestException catch (e) {
      throw BadRequestException(e);
    }on UnauthorisedException catch (e) {
      throw UnauthorisedException(e);
    }
    print('api post.');
    return responseJson;
  }

  Future<dynamic> postWithHeadersAndFile(String url, Map<String, String> header,File json, String device_id) async {
    print('Api Post, url $url');
    print('Api Post, headers $header');
    print('Api Post, file path' +  json.path);


    var responseJson;
    try {
      FormData formData = new FormData.fromMap({
        "device_id": device_id,
        "jsonfileupload": json == null
            ? ""
            : await MultipartFile.fromFile(json.path,
            filename: basename(json.path)),
      });
      Dio dio = new Dio();
      dio.options.headers = header;
      final response = await dio.post(url, data: formData);
      responseJson = _returnDaoResponse(response);
//        return json.decode(json.encode(response.data.toString()));
    } on DioError catch (e) {
      print("==DioError==>" + e.response.data.toString());
      throw BadRequestException(e.response.data.toString());
    } on SocketException {
      print('No net');
      throw FetchDataException('No Internet connection');
    } on BadRequestException catch (e) {
      print("==BadRequestException==>" + e.toString());
      throw BadRequestException(e);
    }
    print('api post.');
    return responseJson;
  }

  Future<dynamic> put(String url, dynamic body) async {
    print('Api Put, url $url');
    var responseJson;
    try {
      final response = await http.put(url, body: body);
      responseJson = _returnResponse(response);
    } on SocketException {
      print('No net');
      throw FetchDataException('No Internet connection');
    }
    print('api put.');
    print(responseJson.toString());
    return responseJson;
  }

  Future<dynamic> delete(String url) async {
    print('Api delete, url $url');
    var apiResponse;
    try {
      final response = await http.delete(url);
      apiResponse = _returnResponse(response);
    } on SocketException {
      print('No net');
      throw FetchDataException('No Internet connection');
    }
    print('api delete.');
    return apiResponse;
  }
}

dynamic _returnResponse(http.Response response) {
  print("response code ====>>>>>" + response.statusCode.toString());
  print("response body ====>>>>>" + response.body.toString());
  switch (response.statusCode) {
    case 201:
    case 200:
      var responseJson = json.decode(response.body.toString());
      print(responseJson);
      return responseJson;
    case 400:
      var responseJson = json.decode(response.body.toString());
      //throw UnauthorisedException(responseJson['response']['message']);
      throw FetchDataException(responseJson['response']['message']);
      /// case 333 for patient deactivated by provider
    case 333:
      var responseJson = json.decode(response.body.toString());
      throw PatientDeactivatedException(responseJson['response']['message']);
  /// case 555 for provider deactivated by super admin
    case 555:
      var responseJson = json.decode(response.body.toString());
      throw ProviderDeactivatedException(responseJson['response']['message']);
    case 515:
      var responseJson = json.decode(response.body.toString());
      throw UnauthorisedException(responseJson['response']['message']);
    case 401:
    case 403:
    case 410:
    var responseJson = json.decode(response.body.toString());
      throw UnauthorisedException(responseJson['response']['message']);
    case 101:
      throw UnauthorisedException(response.body.toString());
    case 404:
      throw UnauthorisedException(response.body.toString());
    case 500:
    default:
      throw FetchDataException(
          'Error occurred while Communication with Server with StatusCode : ${response.statusCode}');
  }
}

dynamic _returnDaoResponse(Response response) {
  print("response code ====>>>>>" + response.statusCode.toString());
  print("response body ====>>>>>" +
      json.decode(json.encode(response.toString())));
  switch (response.statusCode) {
    case 200:
    case 201:
      print("200 --> " + json.decode(json.encode(response.toString())));
      var responseJson = json.decode(json.encode(response.toString()));
      return responseJson;
    case 400:
      var responseJson = json.decode(response.data.toString());
      throw BadRequestException(responseJson['message']);
    case 401:
    case 403:
      throw BadRequestException(response.data.toString());
    case 500:
    default:
      throw FetchDataException(
          'Error occurred while Communication with Server with StatusCode : ${response.statusCode}');
  }
}

header(String token) => {"Authorization": token ?? "", "Content-Type": "application/json"};
