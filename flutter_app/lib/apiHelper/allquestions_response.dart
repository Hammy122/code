// To parse this JSON data, do
//
//     final questionsResponse = questionsResponseFromJson(jsonString);

import 'dart:convert';

QuestionsResponse questionsResponseFromJson(String str) =>
    QuestionsResponse.fromJson(json.decode(str));

String questionsResponseToJson(QuestionsResponse data) =>
    json.encode(data.toJson());

class QuestionsResponse {
  QuestionsResponse({
    this.response,
  });

  Response response;

  factory QuestionsResponse.fromJson(Map<String, dynamic> json) =>
      QuestionsResponse(
        response: Response.fromJson(json["response"]),
      );

  Map<String, dynamic> toJson() => {
        "response": response.toJson(),
      };
}

class Response {
  Response({
    this.status,
    this.message,
    this.data,
  });

  String status;
  String message;
  List<Datum> data;

  factory Response.fromJson(Map<String, dynamic> json) => Response(
        status: json["status"],
        message: json["message"],
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    this.id,
    this.question,
    this.questionValue,
    this.questionDetails,
    this.questionSection,
    this.questionType,
    this.questionrange,
    this.Questionsubtitle,
    this.isAdded,
  });

  int id;
  String question;
  dynamic questionValue;
  //dynamic questionDetails;
  List<AnswerItem> questionDetails;
  String questionSection;
  String questionType;
  String questionrange;
  String Questionsubtitle = "";
  bool isAdded = false;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        question: json["Question"],
        //questionValue: json["Question_value"],
        questionValue: json["Question_value"] == "Null" ? "" : json["Question_value"],
        // questionValue: json["Question_value"],
        // questionValue: json["Question_value"]
        //     .map((e) => AnswerItem.fromJson(e))
        //     .toList()
        //     .cast<AnswerItem>(),
        questionDetails:
            json["Question_details"] == "Null"
                ? null
                : json["Question_details"]
                    .map((e) => AnswerItem.fromJson(e))
                    .toList()
                    .cast<AnswerItem>(),

        questionSection: json["Question_section"],
        questionType: json["Question_type"],
        questionrange: json["Questionrange"],
        Questionsubtitle: json["Questionsubtitle"] == null ? "" : json["Questionsubtitle"],
        isAdded : false
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "Question": question,
        "Question_value": questionValue,
        //"Question_value": List<dynamic>.from(questionValue.map((x) => x)),
        "Question_details": List<dynamic>.from(questionDetails.map((x) => x)),
        "Question_section": questionSection,
        "Question_type": questionType,
        "Questionrange": questionrange,
        "Questionsubtitle": Questionsubtitle,
      };
}

class AnswerItem {
  String text;
  bool isSelected = false;

  AnswerItem(this.text, this.isSelected);

  factory AnswerItem.fromJson(json) => AnswerItem(json, false);
}
