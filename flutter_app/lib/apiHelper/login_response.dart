// To parse this JSON data, do
//
//     final loginResponse = loginResponseFromJson(jsonString);

import 'dart:convert';

LoginResponse loginResponseFromJson(String str) => LoginResponse.fromJson(json.decode(str));

String loginResponseToJson(LoginResponse data) => json.encode(data.toJson());

class LoginResponse {
  LoginResponse({
    this.response,
  });

  Response response;

  factory LoginResponse.fromJson(Map<String, dynamic> json) => LoginResponse(
    response: Response.fromJson(json["response"]),
  );

  Map<String, dynamic> toJson() => {
    "response": response.toJson(),
  };
}

class Response {
  Response({
    this.status,
    this.message,
    this.token,
    this.user,
  });

  String status;
  String message;
  String token;
  User user;

  factory Response.fromJson(Map<String, dynamic> json) => Response(
    status: json["status"],
    message: json["message"],
    token: json["token"],
    user: User.fromJson(json["user"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "token": token,
    "user": user.toJson(),
  };
}

class User {
  User({
    this.id,
    this.firstName,
    this.lastName,
    this.bod,
    this.providerId,
    this.email,
  });

  int id;
  String firstName;
  String lastName;
 // DateTime bod;
  String bod;
  String providerId;
  String email;

  factory User.fromJson(Map<String, dynamic> json) => User(
    id: json["id"],
    firstName: json["First_name"],
    lastName: json["Last_name"],
   // bod: DateTime.parse(json["BOD"]),
    bod:json["BOD"],
    providerId: json["Provider_id"],
    email: json["email"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "First_name": firstName,
    "Last_name": lastName,
   // "BOD": "${bod.year.toString().padLeft(4, '0')}-${bod.month.toString().padLeft(2, '0')}-${bod.day.toString().padLeft(2, '0')}",
    "BOD": bod,
    "Provider_id": providerId,
    "email": email,
  };
}
