// To parse this JSON data, do
//
//     final getSymptomsResponse = getSymptomsResponseFromJson(jsonString);

import 'dart:convert';

GetSymptomsResponse getSymptomsResponseFromJson(String str) => GetSymptomsResponse.fromJson(json.decode(str));

String getSymptomsResponseToJson(GetSymptomsResponse data) => json.encode(data.toJson());

class GetSymptomsResponse {
  GetSymptomsResponse({
    this.response,
  });

  Response response;

  factory GetSymptomsResponse.fromJson(Map<String, dynamic> json) => GetSymptomsResponse(
    response: Response.fromJson(json["response"]),
  );

  Map<String, dynamic> toJson() => {
    "response": response.toJson(),
  };
}

class Response {
  Response({
    this.status,
    this.message,
    this.data,
  });

  String status;
  String message;
  List<SymptomItemModel> data;

  factory Response.fromJson(Map<String, dynamic> json) => Response(
    status: json["status"],
    message: json["message"],
    data: List<SymptomItemModel>.from(json["data"].map((x) => SymptomItemModel.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class SymptomItemModel {

  SymptomItemModel({
    this.symptomsName,
    this.isSelected,
    this.symptomsDate,
    this.symptomsStarted,
    this.isNormalActivities,
  });

  String symptomsName;
  bool isSelected = false;

  String symptomsDate = "Select Date";
  String symptomsStarted = "Select Date";
  bool isNormalActivities = false;


  factory SymptomItemModel.fromJson(Map<String, dynamic> json) => SymptomItemModel(
    symptomsName: json["Symptoms_name"],
    isSelected: false,
    symptomsDate: "Select Date",
    symptomsStarted: "started",
    isNormalActivities: false,
  );

  Map<String, dynamic> toJson() => {
    "Symptoms_name": symptomsName
  };
}
