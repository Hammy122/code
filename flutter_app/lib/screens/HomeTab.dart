import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_app/screens/Anxiety/ReportYourAnxietyQuestionOne.dart';
import 'package:flutter_app/screens/ChangePassword.dart';
import 'package:flutter_app/screens/ReportYourPain/ReportYourPainQuestionOne.dart';
import 'package:flutter_app/screens/ReportYourSymptoms/ReportYourSymptoms.dart';
import 'package:flutter_app/screens/ReportYourWellness/ReportYourWellness.dart';
import 'package:flutter_app/utils/ColorUtils.dart';
import 'package:flutter_app/utils/CustomPopupMenu.dart';
import 'package:flutter_app/models/ReachBean.dart';
import 'package:flutter_app/screens/login/Signin.dart';
import 'package:flutter_app/utils/app_widgets.dart';
import 'package:flutter_app/utils/utils.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:toast/toast.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:url_launcher/url_launcher.dart';
import '../models/UserBean.dart';
import '../utils/UtilsImporter.dart';
import 'package:http/http.dart' as http;

import 'Anxiety/ReportYourAnxiety.dart';
import 'ReportYourPain/ReportYourPain.dart';

class HomeTab extends StatefulWidget {
  @override
  _HomeTabState createState() => _HomeTabState();
}

class _HomeTabState extends State<HomeTab> {
  //ProgressDialog pr;
  TextStyle style = TextStyle(fontFamily: 'Orkney', fontSize: 14.0);
  bool isPTSelected = true, isPSYCHSelected = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future<String> token = UtilsImporter().preferencesUtils.getToken();
    token.then((data) {
       print("TOKEN ==> "+data);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
            new Padding(
              padding: const EdgeInsetsDirectional.only(top: 25.0, bottom: 10),
              child: Image.asset(
                UtilsImporter().stringUtils.toolbarLogo,
                width: 18,
                height: 18,
              ),
            ),
            new Container(
              margin: const EdgeInsets.only(top: 1),
              height: 1,
              color: getColorFromHex('#CCCCCC'),
            ),
            SizedBox(height: 10),
            getTextWidget('Your active providers', 14, getThemeGrayColor(),
                FontWeight.w600),
            SizedBox(
              height: 15,
            ),
            Expanded(
                child: Stack(
              children: <Widget>[
                Positioned(
                  top: 30,
                  height: 160,
                  child: Container(
                      padding: const EdgeInsets.only(
                          left: 0, right: 0, top: 0, bottom: 0),
                      width: MediaQuery.of(context).size.width,
                      child: Image.asset(
                        "assets/homeRoundedBg.png",
                        width: MediaQuery.of(context).size.width,
                      )),
                ),
                Visibility(
                    visible: false,
                    child: Positioned(
                      width: MediaQuery.of(context).size.width,
                      top: 100,
                      child: Center(
                          child: getTextWidget("Pain & anxiety", 15,
                              getThemeGrayColor(), FontWeight.normal)),
                    )),
                Positioned(
                  child: Container(
                    child: Row(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          SizedBox(
                            width: 10,
                          ),
                          Expanded(
                            child: GestureDetector(
                                onTap: () => setState(() {
                                      isPTSelected = true;
                                    }),
                                child: Material(
                                    elevation: 10.0,
                                    borderRadius: BorderRadius.circular(5.0),
                                    color: Colors.white,
                                    child: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          Align(
                                              alignment: Alignment.center,
                                              child: Image.asset(
                                                "assets/homeActiveProvidersProviderOn.png",
                                              )),
                                        ]))),
                            flex: 1,
                          ),
                          SizedBox(
                            width: 11,
                          ),
                          Expanded(
                            child: GestureDetector(
                                onTap: () => {},
                                child: Material(
                                    elevation: 0,
                                    borderRadius: BorderRadius.circular(5.0),
                                    color: getInputBoxFillColor(),
                                    child: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          Align(
                                              alignment: Alignment.center,
                                              child: Image.asset(
                                                "assets/homeActiveProvidersProviderOff.png",
                                              )),
                                        ]))),
                            flex: 1,
                          ),
                          SizedBox(
                            width: 11,
                          ),
                          Expanded(
                            child: GestureDetector(
                                onTap: () => setState(() {}),
                                child: Material(
                                    elevation: 0,
                                    borderRadius: BorderRadius.circular(5.0),
                                    color: getInputBoxFillColor(),
                                    child: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          Align(
                                              alignment: Alignment.center,
                                              child: Image.asset(
                                                "assets/ic_spec.png",
                                              )),
                                        ]))),
                            flex: 1,
                          ),
                          SizedBox(
                            width: 11,
                          ),
                          Expanded(
                            child: GestureDetector(
                                onTap: () => setState(() {}),
                                child: Material(
                                    elevation: 10.0,
                                    borderRadius: BorderRadius.circular(5.0),
                                    color: Colors.white,
                                    child: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          Align(
                                              alignment: Alignment.center,
                                              child: Image.asset(
                                                "assets/ic_psych.png",
                                              )),
                                        ]))),
                            flex: 1,
                          ),
                          SizedBox(
                            width: 10,
                          ),
                        ]),
                  ),
                ),
              ],
            )),
            SingleChildScrollView(
                padding: const EdgeInsets.symmetric(horizontal: 30.0),
                child: Column(children: <Widget>[
                  getTextWidget("How are you feeling today?", 20,
                      getThemeBlackColor(), FontWeight.normal),
                  SizedBox(
                    height: 25,
                  ),
                  Container(
                      margin: const EdgeInsets.only(left: 15, right: 15),
                      color: Colors.white,
                      child: Material(
                        borderRadius: BorderRadius.circular(6.0),
                        color: getThemeGreenColor(),
                        child: InkWell(
                          splashColor: Colors.white,
                          child: Padding(
                              padding:
                                  EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 15.0),
                              child: Center(
                                  child: Row(
                                      mainAxisSize: MainAxisSize.min,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                    Align(
                                        alignment: Alignment.center,
                                        child: Image.asset(
                                          "assets/homeBtnPainIcon.png",
                                          width: 45,
                                          height: 45,
                                        )),
                                    SizedBox(
                                      width: 10,
                                      height: 30,
                                    ),
                                    getTextWidget("Report your pain", 15,
                                        getWhiteColor(), FontWeight.bold),
                                  ]))),
                          onTap: () {
//                            openReportPage();

                            Navigator.push(context,
                                MaterialPageRoute(builder: (context) {
//                              return ReportYourPainQuestionOne();
                              return ReportYourPain();
//                        return HumanAnatomy();
                            }));
                          },
                        ),
                      )),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                      margin: const EdgeInsets.only(left: 15, right: 15),
                      color: Colors.white,
                      child: Material(
                          borderRadius: BorderRadius.circular(6.0),
                          color: getThemeBlueColor(),
                          child: InkWell(
                            splashColor: Colors.white,
                            child: Padding(
                              padding:
                                  EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 15.0),
                              child: Center(
                                  child: Row(
                                      mainAxisSize: MainAxisSize.min,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                    Align(
                                        alignment: Alignment.center,
                                        child: Image.asset(
                                          "assets/homeBtnSymptomsIcon.png",
                                          width: 45,
                                          height: 45,
                                        )),
                                    SizedBox(
                                      width: 10,
                                      height: 30,
                                    ),
                                    getTextWidget("Report your symptoms", 15,
                                        getWhiteColor(), FontWeight.bold),
                                  ])),
                            ),
                            onTap: () {
                              Navigator.push(context,
                                  MaterialPageRoute(builder: (context) {
                                return ReportYourSymptoms();
//                        return HumanAnatomy();
                              }));
                            },
                          ))),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                      margin: const EdgeInsets.only(left: 15, right: 15),
                      color: Colors.white,
                      child: Material(
                          borderRadius: BorderRadius.circular(6.0),
                          color: getThemePinkColor(),
                          child: InkWell(
                            splashColor: Colors.white,
                            child: Padding(
                              padding:
                                  EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 15.0),
                              child: Center(
                                  child: Row(
                                      mainAxisSize: MainAxisSize.min,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                    Align(
                                        alignment: Alignment.center,
                                        child: Image.asset(
                                          "assets/homeBtnAnxietyIcon.png",
                                          width: 45,
                                          height: 45,
                                        )),
                                    SizedBox(
                                      width: 10,
                                      height: 30,
                                    ),
                                    getTextWidget("Report your anxiety", 15,
                                        getWhiteColor(), FontWeight.bold),
                                  ])),
                            ),
                            onTap: () {
                              Navigator.push(context,
                                  MaterialPageRoute(builder: (context) {
                                return ReportYourAnxiety();
//                        return HumanAnatomy();
                              }));
                            },
                          ))),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                      margin: const EdgeInsets.only(left: 15, right: 15),
                      color: Colors.white,
                      child: Material(
                          borderRadius: BorderRadius.circular(6.0),
                          color: getThemeYellowColor(),
                          child: InkWell(
                            child: Padding(
                              padding:
                                  EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 15.0),
                              child: Center(
                                  child: Row(
                                      mainAxisSize: MainAxisSize.min,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                    Align(
                                        alignment: Alignment.center,
                                        child: Image.asset(
                                          "assets/homeBtnWellnessIcon.png",
                                          width: 45,
                                          height: 45,
                                        )),
                                    SizedBox(
                                      width: 10,
                                      height: 30,
                                    ),
                                    getTextWidget("Report your wellness", 15,
                                        getWhiteColor(), FontWeight.bold),
                                  ])),
                            ),
                            onTap: () {
                              Navigator.push(context,
                                  MaterialPageRoute(builder: (context) {
                                return ReportYourWellness();
//                        return HumanAnatomy();
                              }));
                            },
                          )))
                ]))
          ])),
    );
  }

  Route openReportPage() {
    return PageRouteBuilder(
      pageBuilder: (context, animation, secondaryAnimation) =>
          ReportYourPainQuestionOne(),
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        var begin = Offset(0.0, 1.0);
        var end = Offset.zero;
        var curve = Curves.ease;

        var tween =
            Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

        return SlideTransition(
          position: animation.drive(tween),
          child: child,
        );
      },
    );
  }

  BoxDecoration myBoxDecoration() {
    return BoxDecoration(
      border: Border.all(
        color: Colors.black,
        width: 2,
      ),
    );
  }
}
