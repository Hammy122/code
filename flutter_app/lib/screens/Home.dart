import 'dart:async';
import 'dart:io';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_app/screens/ExercisesTab.dart';
import 'package:flutter_app/screens/MessagesTab.dart';
import 'package:flutter_app/screens/SettingsTab.dart';

import 'package:flutter_app/screens/login/Signin.dart';
import 'package:flutter_app/screens/ActivateAccount.dart';
import 'package:flutter_app/screens/HomeTab.dart';
import 'package:flutter_app/models/UserBean.dart';
import 'package:flutter_app/utils/ColorUtils.dart';
import 'package:flutter_app/utils/SessionTimeoutHandler.dart';
import 'package:flutter_app/utils/StringConst.dart';
import 'package:flutter_app/utils/app_widgets.dart';
import 'package:flutter_app/utils/my_globals.dart';
import 'package:flutter_app/utils/session_management_timer.dart';
import 'package:flutter_app/utils/utils.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import '../utils/placeholder_widget.dart';
import '../utils/UtilsImporter.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int _currentIndex = 0;
  Widget widgetForBody = HomeTab();
  // SessionManagementTimer sessionManagementTimer;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
 // MyGlobals myGlobals = new MyGlobals();
 // GlobalKey<NavigatorState> _navigator = new  GlobalKey<NavigatorState>();


  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    sessionTimer_scaffoldKey = new GlobalKey<ScaffoldState>();

    _currentIndex = 0;
    // new Future.delayed(const Duration(milliseconds: 1000),() {
    //   sessionManagementTimer= new SessionManagementTimer(_scaffoldKey.currentContext);
    // });
    // SchedulerBinding.instance.addPostFrameCallback((_) {
    //   sessionManagementTimer= new SessionManagementTimer(context);
    // });
    //SessionTimeoutHandler(_navigator, 10 , _scaffoldKey ).handleUserInteraction(_navigator);
 //   SessionTimeoutHandler(_scaffoldKey).handleUserInteractionFine();
//
//    SessionTimeoutHandler().session_startTimer();

  }


  @override
  Widget build(BuildContext context) {
    myGlobals.myGlobleContext = context;

    return Scaffold(
        body: widgetForBody,
        key: _scaffoldKey,
        bottomNavigationBar: new Theme(
          data: Theme.of(context).copyWith(
              // sets the background color of the `BottomNavigationBar`
              canvasColor: Colors.white,
              // sets the active color of the `BottomNavigationBar` if `Brightness` is light
              primaryColor: Colors.black,
              textTheme: Theme.of(context).textTheme.copyWith(
                  caption: new TextStyle(color: getColorFromHex("#BFC5CC")))),
          // sets the inactive color of the `BottomNavigationBar`
          child: new BottomNavigationBar(
            elevation: 10,
            type: BottomNavigationBarType.fixed,
            onTap: onTabTapped,
            currentIndex: _currentIndex,
            items: [
              BottomNavigationBarItem(
                icon: _currentIndex == 0
                    ? new Image.asset(
                        'assets/navHomeOn.png',
                        height: StringConst.tabIconWidth,
                        width: StringConst.tabIconWidth,
                      )
                    : Image.asset(
                        'assets/navHomeOff.png',
                        height: StringConst.tabIconWidth,
                        width: StringConst.tabIconWidth,
                      ),
                title: getTextWidget(
                    "Home",
                    StringConst.tabFontSize,
                    _currentIndex == 0
                        ? getThemeGreenColor()
                        : getThemeBlackColor(),
                    FontWeight.bold),
              ),
              BottomNavigationBarItem(
                icon: _currentIndex == 1
                    ? new Image.asset(
                        'assets/navMessagesOn.png',
                        height: StringConst.tabIconWidth,
                        width: StringConst.tabIconWidth,
                      )
                    : new Image.asset(
                        'assets/navMessagesOff.png',
                        height: StringConst.tabIconWidth,
                        width: StringConst.tabIconWidth,
                      ),
                title: getTextWidget(
                    "Message",
                    StringConst.tabFontSize,
                    _currentIndex == 1
                        ? getThemeGreenColor()
                        : getThemeBlackColor(),
                    FontWeight.bold),
              ),
              BottomNavigationBarItem(
                  icon: _currentIndex == 2
                      ? new Image.asset(
                          'assets/navExercisesOn.png',
                          height: StringConst.tabIconWidth,
                          width: StringConst.tabIconWidth,
                        )
                      : new Image.asset(
                          'assets/navExercisesOff.png',
                          height: StringConst.tabIconWidth,
                          width: StringConst.tabIconWidth,
                        ),
                  title: getTextWidget(
                      "Exercises",
                      StringConst.tabFontSize,
                      _currentIndex == 2
                          ? getThemeGreenColor()
                          : getThemeBlackColor(),
                      FontWeight.bold)),
              BottomNavigationBarItem(
                  icon: _currentIndex == 3
                      ? new Image.asset(
                          'assets/navResourcesOn.png',
                          height: StringConst.tabIconWidth,
                          width: StringConst.tabIconWidth,
                        )
                      : new Image.asset(
                          'assets/navResourcesOff.png',
                          height: StringConst.tabIconWidth,
                          width: StringConst.tabIconWidth,
                        ),
                  title: getTextWidget(
                      "Resources",
                      StringConst.tabFontSize,
                      _currentIndex == 3
                          ? getThemeGreenColor()
                          : getThemeBlackColor(),
                      FontWeight.bold)),
              BottomNavigationBarItem(
                  icon: _currentIndex == 4
                      ? new Image.asset(
                          'assets/navSettingsOn.png',
                          height: StringConst.tabIconWidth,
                          width: StringConst.tabIconWidth,
                        )
                      : new Image.asset(
                          'assets/navSettingsOff.png',
                          height: StringConst.tabIconWidth,
                          width: StringConst.tabIconWidth,
                        ),
                  title: getTextWidget(
                      "Settings",
                      StringConst.tabFontSize,
                      _currentIndex == 4
                          ? getThemeGreenColor()
                          : getThemeBlackColor(),
                      FontWeight.bold)),
            ],
          ),
        ));
  }

  void onTabTapped(int index) {
    //myGlobals.handleUserInteraction();
    setState(() {
      _currentIndex = index;
      clickBottomMenu(_currentIndex);
    });
  }

  void clickBottomMenu(int index) {
    switch (_currentIndex) {
      case 0:
        widgetForBody = HomeTab();
        break;
      case 1 :
        widgetForBody= MessagesTab();
        break;
      case 2:
         widgetForBody = ExercisesTab();
         break;
      case 4:
        widgetForBody = SettingsTab();
        break;
    }
  }



  // Timer _timer;
  // void startTimer() {
  //   print("====> start timer");
  //   const time = const Duration(seconds: 10);
  //   _timer = Timer.periodic(time, (Timer t) => exit(1));
  // }
  //
  // logOutUser() {
  //   print("====> stop timer");
  //   _timer.cancel();
  //   Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) {
  //     return Signin();
  //   }));
  // }
  //

}
