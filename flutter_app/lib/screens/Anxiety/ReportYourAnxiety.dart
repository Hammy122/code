import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_app/screens/Anxiety/Daily/ReportYourAnxietyDaily.dart';
import 'package:flutter_app/screens/Anxiety/Weekly/ReportYourAnxietyWeekly.dart';
import 'package:flutter_app/screens/ChangePassword.dart';
import 'package:flutter_app/screens/ReportYourPain/ReportYourPainQuestionOne.dart';
import 'package:flutter_app/screens/ReportYourPain/ReportYourPainQuestionTwo.dart';
import 'package:flutter_app/screens/ReportYourWellness/Weekly/ReportYourWellnessWeekly.dart';
import 'package:flutter_app/utils/ColorUtils.dart';
import 'package:flutter_app/utils/CustomPopupMenu.dart';
import 'package:flutter_app/models/ReachBean.dart';
import 'package:flutter_app/screens/login/Signin.dart';
import 'package:flutter_app/utils/app_widgets.dart';
import 'package:flutter_app/utils/utils.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:toast/toast.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../models/UserBean.dart';
import '../../utils/UtilsImporter.dart';
import 'package:http/http.dart' as http;


class ReportYourAnxiety extends StatefulWidget {
  @override
  _ReportYourAnxietyState createState() => _ReportYourAnxietyState();
}

class _ReportYourAnxietyState extends State<ReportYourAnxiety> {
  TextStyle style = TextStyle(fontFamily: getThemeFontName(), fontSize: 14.0);
  String selectedAnswer = "XYZ";
  bool isWeeklySubmitted = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
            createToolBar(false, 'Report Your Anxiety'),
            new Container(
              margin: const EdgeInsets.only(top: 10),
              height: 1,
              color: getColorFromHex('#CCCCCC'),
            ),
            SizedBox(height: 20),
            Expanded(
                child: SingleChildScrollView(
                    padding: const EdgeInsets.symmetric(horizontal: 30.0),
                    child: getLayout()))
          ])),
    );
  }

  Widget createToolBar(bool isBackButton, String title) {
    return Stack(
      children: [
        SizedBox(
          width: 8,
        ),
        Positioned(
            left: 10,
            top: isBackButton ? 20 : 10,
            child: Align(
                alignment: Alignment.center,
                child: InkWell(
                    child: isBackButton
                        ? Image.asset(
                            "assets/ic_back.png",
                            height: 25,
                            width: 40,
                          )
                        : Image.asset(
                            "assets/buttonsTopBarClose.png",
                            height: 40,
                            width: 40,
                          ),
                    onTap: () {
                      goBack(context);
                    }))),
        Center(
            child: Column(
          children: <Widget>[
            new Padding(
              padding: const EdgeInsetsDirectional.only(top: 15.0, bottom: 5),
              child: getTextWidget(title, 18, Colors.black, FontWeight.bold),
            ),
          ],
        )),
        SizedBox(
          width: 10,
        ),
      ],
    );
  }

  void goToNext() {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return ReportYourPainQuestionOne();
    }));
  }

  Widget getLayout() {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Center(
              child: Image.asset(
            UtilsImporter().stringUtils.reportYourAnxietyLogo,
            height: 180,
            fit: BoxFit.fitHeight,
          )),
          SizedBox(
            height: 45,
          ),
          GestureDetector(
            child: Card(
                margin: EdgeInsets.symmetric(vertical: 10),
                color: Colors.white,
                elevation: 5,
                child: Padding(
                  padding: const EdgeInsetsDirectional.only(
                      top: 25.0, bottom: 25, start: 20, end: 5),
                  child: Row(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                            child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            getTextWidget("Submit a Daily Report", 15,
                                getThemeGreenColor(), FontWeight.bold),
                            SizedBox(
                              height: 8,
                            ),
                            getTextWidgetWithAlign(
                                "Submit anytime time a day and as many times as you need!",
                                12,
                                getThemeGrayColor(),
                                FontWeight.w600,
                                TextAlign.left),
                            SizedBox(
                              height: 18,
                            ),
                          ],
                        )),
                        Image.asset(
                          "assets/right_arrow.png",
                          height: 15,
                          width: 25,
                        )
                      ]),
                )),
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return ReportYourAnxietyDaily();
                //                        return HumanAnatomy();
              }));
            },
          ),
          SizedBox(
            height: 15,
          ),
//          GestureDetector(
//            child: Card(
//                margin: EdgeInsets.symmetric(vertical: 10),
//                color: Colors.white,
//                elevation: 5,
//                child: Padding(
//                  padding: const EdgeInsetsDirectional.only(
//                      top: 25.0, bottom: 25, start: 20, end: 5),
//                  child: Row(
//                      mainAxisSize: MainAxisSize.min,
//                      mainAxisAlignment: MainAxisAlignment.start,
//                      children: <Widget>[
//                        Expanded(
//                            child: Column(
//                          crossAxisAlignment: CrossAxisAlignment.start,
//                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                          children: <Widget>[
//                            isWeeklySubmitted
//                                ? Row(
//                                    children: <Widget>[
//                                      getTextWidget(
//                                          "Weekly Report",
//                                          15,
//                                          getThemeGreenColor(),
//                                          FontWeight.bold),
//                                      SizedBox(
//                                        width: 5,
//                                      ),
//                                      Image.asset(
//                                        "assets/lockedBadge.png",
//                                        height: 22,
//                                        width: 60,
//                                      )
//                                    ],
//                                  )
//                                : getTextWidget("Submit a Weekly Report", 15,
//                                    getThemeGreenColor(), FontWeight.bold),
//                            SizedBox(
//                              height: 8,
//                            ),
//                            isWeeklySubmitted
//                                ? getTextWidgetWithAlign(
//                                    "You presented your report two days ago. The next date you can submit is 11/01/2020.",
//                                    12,
//                                    getThemeGrayColor(),
//                                    FontWeight.w600,
//                                    TextAlign.left)
//                                : getTextWidgetWithAlign(
//                                    "Submit your first weekly report.",
//                                    12,
//                                    getThemeGrayColor(),
//                                    FontWeight.w600,
//                                    TextAlign.left),
//                            SizedBox(
//                              height: isWeeklySubmitted ? 10 : 30,
//                            ),
//                            isWeeklySubmitted
//                                ? Row(
//                                    children: <Widget>[
//                                      getTextWidget(
//                                          "Why is locked?",
//                                          11,
//                                          getThemeGreenColor(),
//                                          FontWeight.normal),
//                                      SizedBox(
//                                        width: 5,
//                                      ),
//                                      Image.asset(
//                                        "assets/ic_info.png",
//                                        height: 15,
//                                        width: 15,
//                                      )
//                                    ],
//                                  )
//                                : Container()
//                          ],
//                        )),
//                        isWeeklySubmitted
//                            ? Container(
//                                margin:
//                                    const EdgeInsets.only(right: 10, left: 10),
//                                child: CircularPercentIndicator(
//                                  radius: 90.0,
//                                  lineWidth: 5.0,
//                                  percent: 0.5,
//                                  circularStrokeCap: CircularStrokeCap.round,
//                                  animation: true,
//                                  animationDuration: 1200,
//                                  center: getTextWidget("5 \n Days left", 15,
//                                      getThemeBlackColor(), FontWeight.normal),
//                                  progressColor: getThemeBlueColor(),
//                                ),
//                              )
//                            : Image.asset(
//                                "assets/right_arrow.png",
//                                height: 15,
//                                width: 25,
//                              )
//                      ]),
//                )),
//            onTap: () {
//              isWeeklySubmitted
//                  ? UtilsImporter().commanUtils.showAlertDialogWithTitleOkButton(
//                      "Your Weekly Anxiety Report",
//                      "To ensure accuracy and quality, the anxiety scale weekly report can be submitted once per week. The countdown will display the time remaining in order to submit your next report.",
//                      context)
//                  : Navigator.push(context,
//                      MaterialPageRoute(builder: (context) {
//                      return ReportYourAnxietyWeekly();
//                      //                        return HumanAnatomy();
//                    }));
//            },
//          ),
        ]);
  }
}
