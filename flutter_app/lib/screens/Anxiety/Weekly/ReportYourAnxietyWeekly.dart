import 'dart:convert';

import 'package:circular_check_box/circular_check_box.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app/models/AnswerItem.dart';
import 'package:flutter_app/models/PainFeelItem.dart';
import 'package:flutter_app/models/ReportPainMonthlyQuestionItem.dart';
import 'package:flutter_app/models/ReportWellnessQuestionItem.dart';
import 'package:flutter_app/screens/ChangePassword.dart';
import 'package:flutter_app/screens/ReportSubmitted.dart';
import 'package:flutter_app/screens/ReportYourPain/ReportYourPainQuestionTwo.dart';
import 'package:flutter_app/utils/ColorUtils.dart';
import 'package:flutter_app/utils/CustomPopupMenu.dart';
import 'package:flutter_app/models/ReachBean.dart';
import 'package:flutter_app/screens/login/Signin.dart';
import 'package:flutter_app/utils/app_widgets.dart';
import 'package:flutter_app/utils/utils.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:toast/toast.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:http/http.dart' as http;

class ReportYourAnxietyWeekly extends StatefulWidget {
  @override
  _ReportYourAnxietyWeeklyState createState() =>
      _ReportYourAnxietyWeeklyState();
}

class _ReportYourAnxietyWeeklyState extends State<ReportYourAnxietyWeekly> {
  TextStyle style = TextStyle(fontFamily: getThemeFontName(), fontSize: 18.0);
  List<ReportPainMonthlyQuestionItem> weeklyAnxietyQuestions = new List();
  int currentQuestionIndex = 0;
  bool isQuestionsLoaded = false;

  loadWeeklyAnxietyQuestions() async {
    String data =
        await rootBundle.loadString('assets/weeklyAnxietyQuestions.json');
    var tagObjsJson = jsonDecode(data)['response']['data'] as List;
    // print("tagObjsJson  ==>"+tagObjsJson.toString());
    weeklyAnxietyQuestions = tagObjsJson
        .map((tagJson) => ReportPainMonthlyQuestionItem.fromJson(tagJson))
        .toList();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    loadWeeklyAnxietyQuestions().then((result) {
      setState(() {
        isQuestionsLoaded = true;
        print("SIZE ==> " + weeklyAnxietyQuestions.length.toString());
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
          child: isQuestionsLoaded
              ? Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                      createToolBar(
                          currentQuestionIndex >= 1,
                          'Report your Anxiety',
                          "Question " +
                              (currentQuestionIndex + 1).toString() +
                              " of " +
                              weeklyAnxietyQuestions.length.toString(),
                          currentQuestionIndex <
                                  (weeklyAnxietyQuestions.length - 1)
                              ? 'Next'
                              : 'Finish'),
                      new Container(
                        margin: const EdgeInsets.only(top: 10),
                        height: 1,
                        color: getColorFromHex('#CCCCCC'),
                      ),
                      Expanded(
                        child: Container(
                            margin: const EdgeInsets.all(20),
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  getTextWidget(
                                      weeklyAnxietyQuestions[
                                              currentQuestionIndex]
                                          .question,
                                      20,
                                      getThemeBlackColor(),
                                      FontWeight.normal),
                                  SizedBox(
                                    height: 45,
                                  ),
                                  new Expanded(
                                    child: getAnswersLayout(
                                        weeklyAnxietyQuestions[
                                            currentQuestionIndex]),
                                  )
                                ])),
                      )
                    ])
              : Center(
                  child: SizedBox(
                    child: Padding(
                        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                        child: CircularProgressIndicator()),
                    height: 40.0,
                    width: 40.0,
                  ),
                )),
    );
  }

  Widget createToolBar(
      bool isBackButton, String title, String subTitle, String buttonLabel) {
    return Stack(
      children: [
        SizedBox(
          width: 8,
        ),
        Positioned(
            left: 10,
            top: 10,
            child: Align(
                alignment: Alignment.center,
                child: InkWell(
                    child: isBackButton
                        ? Image.asset(
                            "assets/ic_back.png",
                            height: 25,
                            width: 40,
                          )
                        : Image.asset(
                            "assets/buttonsTopBarClose.png",
                            height: 40,
                            width: 40,
                          ),
                    onTap: () {
                      if (currentQuestionIndex <= 0)
                        goBack(context);
                      else {
                        setState(() {
                          if (currentQuestionIndex <= 0)
                            currentQuestionIndex = 0;
                          else
                            currentQuestionIndex--;
                        });
                      }
                    }))),
        Center(
            child: Column(
          children: <Widget>[
            new Padding(
              padding: const EdgeInsetsDirectional.only(top: 15.0, bottom: 0),
              child: getTextWidget(title, 17, Colors.black, FontWeight.bold),
            ),
            getTextWidget(subTitle, 13, getThemeGreenColor(), FontWeight.w600),
          ],
        )),
        Positioned(
            right: 10,
            top: 10,
            child: Center(
                child: Container(
                    decoration: new BoxDecoration(
                      borderRadius: new BorderRadius.circular(4.0),
                      color: getThemeGreenColor(),
                    ),
                    child: InkWell(
                      child: Visibility(
                          visible: getSelected(),
                          child: Padding(
                              padding: EdgeInsets.only(
                                  left: 8, top: 6, right: 8, bottom: 6),
                              child: getTextWidget(buttonLabel, 17,
                                  Colors.white, FontWeight.bold))),
                      onTap: () {
                        goToNext(buttonLabel);
                      },
                    )))),
        SizedBox(
          width: 10,
        ),
      ],
    );
  }

  Widget getAnswersLayout(ReportPainMonthlyQuestionItem monthlyPainQuestion) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
              child: Container(
                  margin: const EdgeInsets.only(left: 15, right: 15),
                  color: Colors.white,
                  child: ListView.builder(
                    itemCount: monthlyPainQuestion.answers.length,
                    itemBuilder: (context, index) {
                      return _getItemCell(
                          index: index,
                          title: monthlyPainQuestion.answers[index].text,
                          isSelected:
                              monthlyPainQuestion.answers[index].isSelected);
                    },
                    padding: EdgeInsets.all(0.0),
                  ))),
        ]);
  }

  void goToNext(String buttonLabel) {
    if (buttonLabel == 'Finish') {
      Navigator.push(context, MaterialPageRoute(builder: (context) {
        return ReportSubmitted();
      }));
    } else {
      setState(() {
        currentQuestionIndex++;
      });
    }
  }

  void unSelectAll() {
    for (int i = 0; i < weeklyAnxietyQuestions.length; i++) {
      for (int j = 0; j < weeklyAnxietyQuestions[i].answers.length; j++) {
        weeklyAnxietyQuestions[i].answers[j].isSelected = false;
      }
    }
  }

  void setSelectedAnswer(int answerIndex) {
    weeklyAnxietyQuestions[currentQuestionIndex]
        .answers[answerIndex]
        .isSelected = true;
  }

  bool getSelected() {
    for (int j = 0;
        j < weeklyAnxietyQuestions[currentQuestionIndex].answers.length;
        j++) {
      if (weeklyAnxietyQuestions[currentQuestionIndex].answers[j].isSelected)
        return true;
    }
    return false;
  }

  Widget _getItemCell({int index, String title, bool isSelected}) {
    return new GestureDetector(
      child: Card(
          margin: EdgeInsets.symmetric(vertical: 10),
          color: isSelected
              ? getColorFromHex('#1a75ba')
              : getColorFromHex("#ededf2"),
          child: Padding(
              padding: const EdgeInsetsDirectional.only(
                top: 25.0,
                bottom: 25,
              ),
              child: getTextWidget(title, 15,
                  isSelected ? Colors.white : Colors.black, FontWeight.bold))),
      onTap: () {
        setState(() {
          unSelectAll();
          isSelected = true;
          setSelectedAnswer(index);
        });
      },
    );
  }
}
