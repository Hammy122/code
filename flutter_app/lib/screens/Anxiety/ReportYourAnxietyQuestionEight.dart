import 'dart:convert';

import 'package:circular_check_box/circular_check_box.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app/models/PainFeelItem.dart';
import 'package:flutter_app/screens/ChangePassword.dart';
import 'package:flutter_app/screens/ReportSubmitted.dart';
import 'package:flutter_app/screens/ReportYourPain/ReportYourPainQuestionTwo.dart';
import 'package:flutter_app/utils/ColorUtils.dart';
import 'package:flutter_app/utils/CustomPopupMenu.dart';
import 'package:flutter_app/models/ReachBean.dart';
import 'package:flutter_app/screens/login/Signin.dart';
import 'package:flutter_app/utils/app_widgets.dart';
import 'package:flutter_app/utils/utils.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:toast/toast.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:http/http.dart' as http;


class ReportYourAnxietyQuestionEight extends StatefulWidget {
  @override
  _ReportYourAnxietyState createState() => _ReportYourAnxietyState();
}

class _ReportYourAnxietyState extends State<ReportYourAnxietyQuestionEight> {
  TextStyle style = TextStyle(fontFamily: getThemeFontName(), fontSize: 18.0);
  bool isSelected = false;
  List<BoxSelection> array = new List();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    array.add(new BoxSelection("Very often indeed", false));
    array.add(new BoxSelection("Quite often", false));
    array.add(new BoxSelection("Not very often", false));
    array.add(new BoxSelection("Not at all", false));
   }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                createToolBar(true, 'Report your Anxiety', 'Question 8 of 8', 'Finish'),
            new Container(
              margin: const EdgeInsets.only(top: 10),
              height: 1,
              color: getColorFromHex('#CCCCCC'),
            ),
            Expanded(
              child: Container(
                  margin: const EdgeInsets.all(20),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        getTextWidget(
                            "I get sudden feelings of panic",
                            20,
                            getThemeBlackColor(),
                            FontWeight.normal),
                        SizedBox(
                          height: 3,
                        ),
                        SizedBox(
                          height: 43,
                        ),
                        new Expanded(
                          child: getAnswersLayout(),
                        )
                      ])),
            )
          ])),
    );
  }

  Widget getAnswersLayout() {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
              child: Container(
                  margin: const EdgeInsets.only(left: 15, right: 15),
                  color: Colors.white,
                  child: ListView.builder(
                    itemCount: array.length,
                    itemBuilder: _getItemCell,
                    padding: EdgeInsets.all(0.0),
                  ))),
        ]);
  }

  Widget createToolBar(
      bool isBackButton, String title, String subTitle, String buttonLabel) {
    return Stack(
      children: [
        SizedBox(
          width: 8,
        ),
        Positioned(
            left: 10,
            top: isBackButton ? 20 :  10,
            child: Align(
                alignment: Alignment.center,
                child: InkWell(
                    child: isBackButton
                        ? Image.asset(
                      "assets/ic_back.png",
                      height: 25,
                      width: 40,
                    )
                        : Image.asset(
                      "assets/buttonsTopBarClose.png",
                      height: 40,
                      width: 40,
                    ),
                    onTap: () {
                      goBack(context);
                    }))),
        Center(
            child: Column(
              children: <Widget>[
                new Padding(
                  padding: const EdgeInsetsDirectional.only(top: 15.0, bottom: 0),
                  child: getTextWidget(title, 17, Colors.black, FontWeight.bold),
                ),
                getTextWidget(subTitle, 13, getThemeGreenColor(), FontWeight.w600),
              ],
            )),
        Positioned(
            right: 10,
            top: 10,
            child: Center(
                child: Container(
                    decoration: new BoxDecoration(
                      borderRadius: new BorderRadius.circular(4.0),
                      color: getThemeGreenColor(),
                    ),
                    child: InkWell(
                      child: Visibility(
                          visible: isSelected,
                          child: Padding(
                              padding: EdgeInsets.only(
                                  left: 8, top: 6, right: 8, bottom: 6),
                              child: getTextWidget(buttonLabel, 17,
                                  Colors.white, FontWeight.bold))),
                      onTap: () {
                        goToNext();
                      },
                    )))),
        SizedBox(
          width: 10,
        ),
      ],
    );
  }
  void goToNext() {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return ReportSubmitted();
    }));
  }

  Widget _getItemCell(BuildContext context, int index) {
    return new GestureDetector(
      child: Card(
          margin: EdgeInsets.symmetric(vertical: 10),
          color: array[index].isSelected
              ? getColorFromHex('#1a75ba')
              : getColorFromHex("#ededf2"),
          child: Padding(
              padding: const EdgeInsetsDirectional.only(
                top: 25.0,
                bottom: 25,
              ),
              child: getTextWidget(
                  array[index].title,
                  15,
                  array[index].isSelected ? Colors.white : Colors.black,
                  FontWeight.bold))),
      onTap: () {
        setState(() {
          unSelectAll();
          isSelected = true;
          array[index].isSelected = true;
        });
      },
    );
  }

  void unSelectAll() {
    for (int i = 0; i < array.length; i++) {
      array[i].isSelected = false;
    }
  }
}

class BoxSelection {
  bool isSelected;
  String title;

  BoxSelection(this.title, this.isSelected);
}
