import 'dart:convert';
import 'dart:io';

import 'package:circular_check_box/circular_check_box.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app/apiHelper/allquestions_response.dart';
import 'package:flutter_app/apiHelper/app_exceptions.dart';
import 'package:flutter_app/models/AnswerItem.dart';
import 'package:flutter_app/models/PainFeelItem.dart';
import 'package:flutter_app/models/ReportPainMonthlyQuestionItem.dart';
import 'package:flutter_app/persistance/api_repository.dart';
import 'package:flutter_app/screens/ChangePassword.dart';
import 'package:flutter_app/screens/ReportSubmitted.dart';
import 'package:flutter_app/screens/ReportYourPain/ReportYourPainQuestionTwo.dart';
import 'package:flutter_app/utils/ColorUtils.dart';
import 'package:flutter_app/utils/CustomPopupMenu.dart';
import 'package:flutter_app/models/ReachBean.dart';
import 'package:flutter_app/screens/login/Signin.dart';
import 'package:flutter_app/utils/UtilsImporter.dart';
import 'package:flutter_app/utils/app_widgets.dart';
import 'package:flutter_app/utils/utils.dart';
import 'package:path_provider/path_provider.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:toast/toast.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:http/http.dart' as http;

class ReportYourAnxietyDaily extends StatefulWidget {
  @override
  _ReportYourAnxietyDailyState createState() => _ReportYourAnxietyDailyState();
}

class _ReportYourAnxietyDailyState extends State<ReportYourAnxietyDaily> {
  TextStyle style = TextStyle(fontFamily: getThemeFontName(), fontSize: 18.0);
  List<Datum> dailyAnxietyQuestions = new List();
  int currentQuestionIndex = 0;
  bool isQuestionsLoaded = false;
  String token = "";
  String deviceId = "12345678";
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool isSubmissionInProgress = false;
  String selected_Question_details = "";

  // loadDailyAnxietyQuestions() async {
  //   String data =
  //       await rootBundle.loadString('assets/dailyAnxietyQuestions.json');
  //   var tagObjsJson = jsonDecode(data)['response']['data'] as List;
  //   // print("tagObjsJson  ==>"+tagObjsJson.toString());
  //   dailyAnxietyQuestions = tagObjsJson
  //       .map((tagJson) => ReportPainMonthlyQuestionItem.fromJson(tagJson))
  //       .toList();
  //   // print("total records  ==>"+monthlyPainQuestions.length.toString());
  // }
  void getAllQuestions() {
    setState(() => isQuestionsLoaded = false);
    var api = new APICallRepository();
    api
        .allquestion(deviceId, UtilsImporter().apiUtils.DAILY,
            UtilsImporter().apiUtils.ANXIETY, token)
        .then((response) {
      setState(() {
        setState(() => isQuestionsLoaded = true);
        dailyAnxietyQuestions = response.response.data;
      });
    }, onError: (error) {
      setState(() {
        print("ERROR FOUND FINALLY HERE ===> " + error.toString());
        // setState(() => isLoading = false);
        if (error.toString().contains('Unauthorised')) {
          doLogout(context, error.toString());
        }
        if (error is ProviderDeactivatedException) {
          doLogout(context, error.toString());
        } else if (error is PatientDeactivatedException) {
          doLogout(context, error.toString());
        } else {
          _scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text(error.toString(),
                textAlign: TextAlign.center,
                style: style.copyWith(
                    color: Colors.white, fontWeight: FontWeight.bold)),
            duration: Duration(seconds: 5),
          ));
        }
      });
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // loadDailyAnxietyQuestions().then((result) {
    //   setState(() {
    //     isQuestionsLoaded = true;
    //     print("SIZE ==> " + dailyAnxietyQuestions.length.toString());
    //   });
    // });
    Future<String> token = UtilsImporter().preferencesUtils.getToken();
    Future<String> device = getDeviceId();
    token.then((data) {
      print("token ==> " + data);
      this.token = data;
      device.then((deviceId) {
        print("deviceID ==> " + deviceId);
        this.deviceId = deviceId;
        getAllQuestions();
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      body: SafeArea(
          child: isQuestionsLoaded
              ? Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                      createToolBar(
                          currentQuestionIndex >= 1,
                          'Report your Anxiety',
                          "Questions " +
                              (currentQuestionIndex + 1).toString() +
                              " of " +
                              dailyAnxietyQuestions.length.toString(),
                          currentQuestionIndex <
                                  (dailyAnxietyQuestions.length - 1)
                              ? 'Next'
                              : 'Finish'),
                      new Container(
                        margin: const EdgeInsets.only(top: 10),
                        height: 1,
                        color: getColorFromHex('#CCCCCC'),
                      ),
                      Expanded(
                        child: Container(
                            margin: const EdgeInsets.all(20),
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  getTextWidget(
                                      dailyAnxietyQuestions[
                                              currentQuestionIndex]
                                          .question,
                                      20,
                                      getThemeBlackColor(),
                                      FontWeight.normal),
                                  SizedBox(
                                    height: 45,
                                  ),
                                  new Expanded(
                                    child: getAnswersLayout(
                                        dailyAnxietyQuestions[
                                            currentQuestionIndex]),
                                  )
                                ])),
                      )
                    ])
              : Center(
                  child: SizedBox(
                    child: Padding(
                        padding: EdgeInsets.fromLTRB(15.0, 11.0, 15.0, 11.0),
                        child: CircularProgressIndicator(
                            valueColor: AlwaysStoppedAnimation<Color>(
                                getThemeGreenColor()))),
                    height: 50.0,
                    width: 60.0,
                  ),
                )),
    );
  }

  Widget createToolBar(
      bool isBackButton, String title, String subTitle, String buttonLabel) {
    return Stack(
      children: [
        SizedBox(
          width: 8,
        ),
        Positioned(
            left: 10,
            top: 10,
            child: Align(
                alignment: Alignment.center,
                child: InkWell(
                    child: isBackButton
                        ? Image.asset(
                            "assets/ic_back.png",
                            height: 25,
                            width: 40,
                          )
                        : Image.asset(
                            "assets/buttonsTopBarClose.png",
                            height: 40,
                            width: 40,
                          ),
                    onTap: () {
                      if (currentQuestionIndex <= 0)
                        goBack(context);
                      else {
                        setState(() {
                          if (currentQuestionIndex <= 0)
                            currentQuestionIndex = 0;
                          else
                            currentQuestionIndex--;
                        });
                      }
                    }))),
        Center(
            child: Column(
          children: <Widget>[
            new Padding(
              padding: const EdgeInsetsDirectional.only(top: 15.0, bottom: 0),
              child: getTextWidget(title, 17, Colors.black, FontWeight.bold),
            ),
            getTextWidget(subTitle, 13, getThemeGreenColor(), FontWeight.w600),
          ],
        )),
        Positioned(
            right: 10,
            top: 10,
            child: Center(
                child: isSubmissionInProgress
                    ? Center(
                        child: SizedBox(
                          child: Padding(
                              padding:
                                  EdgeInsets.fromLTRB(15.0, 11.0, 15.0, 11.0),
                              child: CircularProgressIndicator(
                                  valueColor: AlwaysStoppedAnimation<Color>(
                                      getThemeGreenColor()))),
                          height: 50.0,
                          width: 60.0,
                        ),
                      )
                    : Container(
                        decoration: new BoxDecoration(
                          borderRadius: new BorderRadius.circular(4.0),
                          color: getThemeGreenColor(),
                        ),
                        child: InkWell(
                          child: Visibility(
                              visible: getSelected(),
                              child: Padding(
                                  padding: EdgeInsets.only(
                                      left: 8, top: 6, right: 8, bottom: 6),
                                  child: getTextWidget(buttonLabel, 17,
                                      Colors.white, FontWeight.bold))),
                          onTap: () {
                            goToNext(buttonLabel);
                          },
                        )))),
        SizedBox(
          width: 10,
        ),
      ],
    );
  }

  Widget getAnswersLayout(Datum monthlyPainQuestion) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
              child: Container(
                  margin: const EdgeInsets.only(left: 15, right: 15),
                  color: Colors.white,
                  child: ListView.builder(
                    itemCount: monthlyPainQuestion.questionDetails.length,
                    itemBuilder: (context, index) {
                      return _getItemCell(
                          index: index,
                          title:
                              monthlyPainQuestion.questionDetails[index].text,
                          isSelected: monthlyPainQuestion
                              .questionDetails[index].isSelected);
                    },
                    padding: EdgeInsets.all(0.0),
                  ))),
        ]);
  }

  void unSelectAll() {
    for (int i = 0; i < dailyAnxietyQuestions.length; i++) {
      for (int j = 0;
          j < dailyAnxietyQuestions[i].questionDetails.length;
          j++) {
        dailyAnxietyQuestions[i].questionDetails[j].isSelected = false;
      }
    }
  }

  void setSelectedAnswer(int answerIndex) {
    dailyAnxietyQuestions[currentQuestionIndex]
        .questionDetails[answerIndex]
        .isSelected = true;
  }

  bool getSelected() {
    for (int j = 0;
        j < dailyAnxietyQuestions[currentQuestionIndex].questionDetails.length;
        j++) {
      if (dailyAnxietyQuestions[currentQuestionIndex]
          .questionDetails[j]
          .isSelected) {
        selected_Question_details =
            dailyAnxietyQuestions[currentQuestionIndex].questionDetails[j].text;
        return true;
      }
    }
    return false;
  }

  Widget _getItemCell({int index, String title, bool isSelected}) {
    return new GestureDetector(
      child: Card(
          margin: EdgeInsets.symmetric(vertical: 10),
          color: isSelected
              ? getColorFromHex('#1a75ba')
              : getColorFromHex("#ededf2"),
          child: Padding(
              padding: const EdgeInsetsDirectional.only(
                top: 25.0,
                bottom: 25,
              ),
              child: getTextWidget(title, 15,
                  isSelected ? Colors.white : Colors.black, FontWeight.bold))),
      onTap: () {
        setState(() {
          unSelectAll();
          isSelected = true;
          setSelectedAnswer(index);
        });
      },
    );
  }

  void goToNext(String buttonLabel) {
    submitData(buttonLabel);
    //dailyAddReport(buttonLabel);

    // if (buttonLabel == 'Finish') {
    //   Navigator.push(context, MaterialPageRoute(builder: (context) {
    //     return ReportSubmitted();
    //   }));
    // } else {
    //   setState(() {
    //     currentQuestionIndex++;
    //   });
    // }
  }

  void dailyAddReport(String buttonLabel) {
    if (buttonLabel == 'Finish') {
      Navigator.push(context, MaterialPageRoute(builder: (context) {
        return ReportSubmitted();
      }));
    } else {
      setState(() {
        currentQuestionIndex++;
        selected_Question_details = "";
      });
    }

    // var isFlag = "";
    // if(dailyAnxietyQuestions[currentQuestionIndex].isAdded)
    // {
    //   isFlag = "1";
    // }else{
    //   isFlag = "0";
    // }
    // var api = new APICallRepository();
    // api.addDailyReport(
    //     token
    //     ,dailyAnxietyQuestions[currentQuestionIndex].questionSection
    //     ,dailyAnxietyQuestions[currentQuestionIndex].questionValue == "" ? "Null" : dailyAnxietyQuestions[currentQuestionIndex].questionValue
    //     ,dailyAnxietyQuestions[currentQuestionIndex].questionrange  == "" ? "Null" : dailyAnxietyQuestions[currentQuestionIndex].questionrange
    //     ,deviceId
    //     ,isFlag   // 0 == add and 1 == update data
    //     ,UtilsImporter().apiUtils.DAILY
    //     ,dailyAnxietyQuestions[currentQuestionIndex].id
    //     ,selected_Question_details
    //
    // ).then((response) {
    //   setState(() {
    //     print("Upload ===> " + response.toString());
    //     dailyAnxietyQuestions[currentQuestionIndex].isAdded = true;
    //
    //     if (buttonLabel == 'Finish') {
    //
    //       Navigator.push(context, MaterialPageRoute(builder: (context) {
    //         return ReportSubmitted();
    //       }));
    //
    //     } else {
    //
    //       setState(() {
    //         currentQuestionIndex++;
    //         selected_Question_details = "";
    //       });
    //
    //     }
    //
    //
    //   });
    // }, onError: (error) {
    //   setState(() {
    //     print("ERROR FOUND FINALLY HERE ===> " + error.toString());
    //     if(error.toString().contains('Unauthorised')){
    //       doLogout(context,error.toString());
    //     } else {
    //
    //       _scaffoldKey.currentState.showSnackBar(SnackBar(
    //         content: Text(error.toString(),
    //             textAlign: TextAlign.center,
    //             style: style.copyWith(
    //                 color: Colors.white, fontWeight: FontWeight.bold)),
    //         duration: Duration(seconds: 5),
    //       ));
    //
    //     }
    //   });
    // });
  }

  var dailyQueAnsList = new List<Map<String, String>>();
  File file;

  _writeJson(String text) async {
    final Directory directory = await getApplicationDocumentsDirectory();
    file = File('${directory.path}/report_data.json');
    await file.writeAsString(text);
  }

  void submitData(String buttonLabel) {
    Map map = new Map<String, String>();
    map["Question_section"] = UtilsImporter().apiUtils.ANXIETY;
    map["Question"] = dailyAnxietyQuestions[currentQuestionIndex].id.toString();
    map["Question_value"] = selected_Question_details;
    map["report_type"] = UtilsImporter().apiUtils.DAILY;
    dailyQueAnsList.add(map);

    if (buttonLabel == 'Finish') {
      setState(() {
        isSubmissionInProgress = true;
      });

      print("MapList==> " + json.encode(dailyQueAnsList));
      _writeJson(json.encode(dailyQueAnsList)).then((result) {
        var api = new APICallRepository();
        api.addWeeklyReport(token, deviceId, file).then((response) {
          setState(() {
            isSubmissionInProgress = false;

            print("Success ===> " + response.toString());
            Navigator.push(context, MaterialPageRoute(builder: (context) {
              return ReportSubmitted();
            }));
          });
        }, onError: (error) {
          setState(() {
            isSubmissionInProgress = false;

            print("ERROR FOUND FINALLY HERE ===> " + error.toString());
            if (error.toString().contains('Unauthorised')) {
              doLogout(context, error.toString());
            } else if (error is ProviderDeactivatedException) {
              doLogout(context, error.toString());
            } else if (error is PatientDeactivatedException) {
              doLogout(context, error.toString());
            } else {
              _scaffoldKey.currentState.showSnackBar(SnackBar(
                content: Text(error.toString(),
                    textAlign: TextAlign.center,
                    style: style.copyWith(
                        color: Colors.white, fontWeight: FontWeight.bold)),
                duration: Duration(seconds: 5),
              ));
            }
          });
        });
      });
    } else {
      setState(() {
        currentQuestionIndex++;
        selected_Question_details = "";
      });
    }
  }
}
