import 'dart:convert';
import 'dart:io';

import 'package:circular_check_box/circular_check_box.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app/apiHelper/allquestions_response.dart';
import 'package:flutter_app/apiHelper/app_exceptions.dart';
import 'package:flutter_app/models/AnswerItem.dart';
import 'package:flutter_app/models/PainFeelItem.dart';
import 'package:flutter_app/models/ReportPainMonthlyQuestionItem.dart';
import 'package:flutter_app/persistance/api_repository.dart';
import 'package:flutter_app/screens/ChangePassword.dart';
import 'package:flutter_app/screens/ReportSubmitted.dart';
import 'package:flutter_app/screens/ReportYourPain/ReportYourPainQuestionTwo.dart';
import 'package:flutter_app/utils/ColorUtils.dart';
import 'package:flutter_app/utils/CustomPopupMenu.dart';
import 'package:flutter_app/models/ReachBean.dart';
import 'package:flutter_app/screens/login/Signin.dart';
import 'package:flutter_app/utils/UtilsImporter.dart';
import 'package:flutter_app/utils/app_widgets.dart';
import 'package:flutter_app/utils/utils.dart';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';

class ReportYourPainWeekly extends StatefulWidget {
  @override
  _ReportYourPainWeeklyState createState() => _ReportYourPainWeeklyState();
}

class _ReportYourPainWeeklyState extends State<ReportYourPainWeekly> {
  TextStyle style = TextStyle(fontFamily: getThemeFontName(), fontSize: 18.0);
  List<Datum> weeklyPainQuestions = new List();
  int currentQuestionIndex = 0;
  bool isQuestionsLoaded = false;
  bool isNextFinishVisible = false;
  TextEditingController _textEditingController = new TextEditingController();
  bool isContentFilled = false;
  String token = "";
  String deviceId = "12345678";
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool isSubmissionInProgress = false;
  String selected_Question_details = "";

  // loadMonthlyPainQuestions() async {
  //   String data =
  //       await rootBundle.loadString('assets/monthlyPainQuestions.json');
  //   var tagObjsJson = jsonDecode(data)['response']['data'] as List;
  //   // print("tagObjsJson  ==>"+tagObjsJson.toString());
  //   monthlyPainQuestions = tagObjsJson
  //       .map((tagJson) => ReportPainMonthlyQuestionItem.fromJson(tagJson))
  //       .toList();
  //   // print("total records  ==>"+monthlyPainQuestions.length.toString());
  // }

  void getAllQuestions() {
    setState(() => isQuestionsLoaded = false);
    var api = new APICallRepository();
    api
        .allquestion(deviceId, UtilsImporter().apiUtils.WEEKLY,
            UtilsImporter().apiUtils.PAIN, token)
        .then((response) {
      setState(() {
        setState(() => isQuestionsLoaded = true);
        weeklyPainQuestions = response.response.data;
      });
    }, onError: (error) {
      setState(() {
        print("ERROR FOUND FINALLY HERE ===> " + error.toString());
        // setState(() => isLoading = false);
        if (error.toString().contains('Unauthorised')) {
          doLogout(context, error.toString());
        }
        if (error is ProviderDeactivatedException) {
          doLogout(context, error.toString());
        } else if (error is PatientDeactivatedException) {
          doLogout(context, error.toString());
        } else {
          _scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text(error.toString(),
                textAlign: TextAlign.center,
                style: style.copyWith(
                    color: Colors.white, fontWeight: FontWeight.bold)),
            duration: Duration(seconds: 5),
          ));
        }
      });
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // loadMonthlyPainQuestions().then((result) {
    //   setState(() {
    //     isQuestionsLoaded = true;
    //     print("SIZE ==> " + monthlyPainQuestions.length.toString());
    //   });
    // });
    Future<String> token = UtilsImporter().preferencesUtils.getToken();
    Future<String> device = getDeviceId();
    token.then((data) {
      print("token ==> " + data);
      this.token = data;
      device.then((deviceId) {
        print("deviceID ==> " + deviceId);
        this.deviceId = deviceId;
        getAllQuestions();
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      body: SafeArea(
          child: isQuestionsLoaded
              ? Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                      createToolBar(
                          currentQuestionIndex >= 1,
                          'Report your Pain',
                          "Question " +
                              (currentQuestionIndex + 1).toString() +
                              " of " +
                              weeklyPainQuestions.length.toString(),
                          currentQuestionIndex <
                                  (weeklyPainQuestions.length - 1)
                              ? 'Next'
                              : 'Finish'),
                      new Container(
                        margin: const EdgeInsets.only(top: 10),
                        height: 1,
                        color: getColorFromHex('#CCCCCC'),
                      ),
                      Expanded(
                        child: Container(
                            margin: const EdgeInsets.all(20),
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  getTextWidget(
                                      weeklyPainQuestions[currentQuestionIndex]
                                          .question,
                                      20,
                                      getThemeBlackColor(),
                                      FontWeight.normal),
                                  SizedBox(
                                    height: 45,
                                  ),
                                  new Expanded(
                                    child: getAnswersLayout(weeklyPainQuestions[
                                        currentQuestionIndex]),
                                  )
                                ])),
                      )
                    ])
              : Center(
                  child: SizedBox(
                    child: Padding(
                        padding: EdgeInsets.fromLTRB(15.0, 11.0, 15.0, 11.0),
                        child: CircularProgressIndicator(
                            valueColor: AlwaysStoppedAnimation<Color>(
                                getThemeGreenColor()))),
                    height: 50.0,
                    width: 60.0,
                  ),
                )),
    );
  }

  Widget createToolBar( bool isBackButton, String title, String subTitle, String buttonLabel) {
    return Stack(
      children: [
        SizedBox(
          width: 8,
        ),
        Positioned(
            left: 10,
            top: 10,
            child: Align(
                alignment: Alignment.center,
                child: InkWell(
                    child: isBackButton
                        ? Image.asset(
                            "assets/ic_back.png",
                            height: 25,
                            width: 40,
                          )
                        : Image.asset(
                            "assets/buttonsTopBarClose.png",
                            height: 40,
                            width: 40,
                          ),
                    onTap: () {
                      if (currentQuestionIndex <= 0)
                        goBack(context);
                      else {
                        setState(() {
                          if (currentQuestionIndex <= 0)
                            currentQuestionIndex = 0;
                          else
                            currentQuestionIndex--;
                        });
                      }
                    }))),
        Center(
            child: Column(
          children: <Widget>[
            new Padding(
              padding: const EdgeInsetsDirectional.only(top: 15.0, bottom: 0),
              child: getTextWidget(title, 17, Colors.black, FontWeight.bold),
            ),
            getTextWidget(subTitle, 13, getThemeGreenColor(), FontWeight.w600),
          ],
        )),
        Positioned(
            right: 10,
            top: 10,
            child: Center(
                child:isSubmissionInProgress ?
                Center(
                  child: SizedBox(
                    child: Padding(
                        padding: EdgeInsets.fromLTRB(15.0, 11.0, 15.0, 11.0),
                        child: CircularProgressIndicator(
                            valueColor: AlwaysStoppedAnimation<Color>(
                                getThemeGreenColor()))),
                    height: 50.0,
                    width: 60.0,
                  ),
                )
               : Container(
                    decoration: new BoxDecoration(
                      borderRadius: new BorderRadius.circular(4.0),
                      color: getThemeGreenColor(),
                    ),
                    child: InkWell(
                      child: Visibility(
                          visible: getSelected(),
                          child: Padding(
                              padding: EdgeInsets.only(
                                  left: 8, top: 6, right: 8, bottom: 6),
                              child: getTextWidget(buttonLabel, 17,
                                  Colors.white, FontWeight.bold))),
                      onTap: () {
                        goToNext(buttonLabel);
                      },
                    )))),
        SizedBox(
          width: 10,
        ),
      ],
    );
  }

  Widget getAnswersLayout(Datum weeklyPainQuestion) {
    if (weeklyPainQuestion.questionrange == "text") {
      return Container(
        child: TextField(
          maxLines: null,
          expands: true,
          keyboardType: TextInputType.multiline,
          maxLength: 500,
          controller: _textEditingController,
          cursorColor: getThemeBlackColor(),
          onChanged: (text) {
            if (text.length > 0) {
              setState(() {
                getSelected();
                //isNextFinishVisible=true;
              });
              setState(() => isContentFilled = true);
            } else {
              setState(() => isContentFilled = false);
            }
          },
          decoration: InputDecoration(
            border: InputBorder.none,
            contentPadding: EdgeInsets.fromLTRB(10.0, 2.0, 10.0, 2.0),
            hintText: "Describe the event in here",
          ),
          obscureText: false,
          style: style,
        ),
        decoration: BoxDecoration(
          color: getInputBoxFillColor(),
          border: Border.all(
            color: getInputBoxFillColor(),
            width: 1,
          ),
          borderRadius: BorderRadius.circular(2),
        ),
      );
    } else {
      return Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Expanded(
                child: Container(
                    margin: const EdgeInsets.only(left: 15, right: 15),
                    color: Colors.white,
                    child: ListView.builder(
                      itemCount: weeklyPainQuestion.questionDetails.length,
                      itemBuilder: (context, index) {
                        return _getItemCell(
                            index: index,
                            title:
                                weeklyPainQuestion.questionDetails[index].text,
                            isSelected: weeklyPainQuestion
                                .questionDetails[index].isSelected);
                      },
                      padding: EdgeInsets.all(0.0),
                    ))),
          ]);
    }
  }

  void goToNext(String buttonLabel) {
    // if (buttonLabel == 'Finish') {
    //   Navigator.push(context, MaterialPageRoute(builder: (context) {
    //     return ReportSubmitted();
    //   }));
    // } else {
    //   setState(() {
    //     currentQuestionIndex++;
    //   });
    // }

   addWeeklyReport(buttonLabel);
  }

  File file;
  _writeJson(String text) async {
    final Directory directory = await getApplicationDocumentsDirectory();
    file = File('${directory.path}/report_data.json');
    await file.writeAsString(text);
  }


  var weeklyQueAnsList = new List<Map<dynamic, dynamic>>();
  //List<Map<dynamic, dynamic>> weeklyQueAnsList = [];


  void addWeeklyReport(String buttonLabel) {
    if (weeklyPainQuestions[currentQuestionIndex].questionrange == "text") {
      selected_Question_details = _textEditingController.text;
    }
    print("selected ==> " + selected_Question_details);
    Map map = new Map<String, String>();
    //var map = {};
    // map[ "Question_section" ] =weeklyPainQuestions[currentQuestionIndex].questionSection ;
    // map[ "Question" ] = weeklyPainQuestions[currentQuestionIndex].question;
    // map[ "Question_value" ] = weeklyPainQuestions[currentQuestionIndex].questionValue;
    // map[ "Question_type" ] = weeklyPainQuestions[currentQuestionIndex].questionType;
    // map[ "Questionrange" ] = weeklyPainQuestions[currentQuestionIndex].questionrange;
    // map["device_id"] = deviceId;

    map["Question_section"] = UtilsImporter().apiUtils.PAIN;
    map["Question"] = weeklyPainQuestions[currentQuestionIndex].id.toString();
    map["Question_value"] = selected_Question_details;
    map["report_type"] = UtilsImporter().apiUtils.WEEKLY;
    weeklyQueAnsList.add(map);

    if (buttonLabel == 'Finish') {
      setState(() {
        isSubmissionInProgress = true;
      });
      print("MapList==> " + json.encode(weeklyQueAnsList));
      _writeJson(json.encode(weeklyQueAnsList)).then((result)
      {
        // print("readData==> ");
        // readCounter();
        var api = new APICallRepository();
        api.addWeeklyReport(token, deviceId, file).then((response) {
              setState(() {

                isSubmissionInProgress = false;
                print("Success ===> " + response.toString());
                _textEditingController.text = "";
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return ReportSubmitted();
                }));
              });
        }, onError: (error) {
          setState(() {

            isSubmissionInProgress = false;
            print("ERROR FOUND FINALLY HERE ===> " + error.toString());
            if (error.toString().contains('Unauthorised')) {
              doLogout(context, error.toString());
            } else if (error is ProviderDeactivatedException) {
              doLogout(context, error.toString());
            } else if (error is PatientDeactivatedException) {
              doLogout(context, error.toString());
            } else {
              _scaffoldKey.currentState.showSnackBar(SnackBar(
                content: Text(error.toString(),
                    textAlign: TextAlign.center,
                    style: style.copyWith(
                        color: Colors.white, fontWeight: FontWeight.bold)),
                duration: Duration(seconds: 5),
              ));
            }
          });
        });

      });

      // api.addWeeklyReport(
      //     token
      //     ,deviceId
      //     ,weeklyQueAnsList.toString()
      // ).then((response) {

    } else {
      setState(() {
        currentQuestionIndex++;
      });
    }
  }

  Future<int> readCounter() async {
    try {
      final file_ = await file;
      // Read the file.
      String contents = await file_.readAsString();
      print("read file ===> $contents");

      return int.parse(contents);
    } catch (e) {
      // If encountering an error, return 0.
      return 0;
    }
  }



  void unSelectAll() {
    for (int i = 0; i < weeklyPainQuestions.length; i++) {
      for (int j = 0; j < weeklyPainQuestions[i].questionDetails.length; j++) {
        weeklyPainQuestions[i].questionDetails[j].isSelected = false;
      }
    }
  }

  void setSelectedAnswer(int answerIndex) {
    weeklyPainQuestions[currentQuestionIndex]
        .questionDetails[answerIndex]
        .isSelected = true;
    selected_Question_details = weeklyPainQuestions[currentQuestionIndex]
        .questionDetails[answerIndex]
        .text;
  }

  bool getSelected() {
    isNextFinishVisible = false;
    if (weeklyPainQuestions[currentQuestionIndex].questionrange == 'text') {
      if (isContentFilled) {
        isNextFinishVisible = true;
      }
    } else {
      for (int j = 0;
          j < weeklyPainQuestions[currentQuestionIndex].questionDetails.length;
          j++) {
        if (weeklyPainQuestions[currentQuestionIndex]
            .questionDetails[j]
            .isSelected) isNextFinishVisible = true;
      }
    }

    return isNextFinishVisible;
  }

  Widget _getItemCell({int index, String title, bool isSelected}) {
    return new GestureDetector(
      child: Card(
          margin: EdgeInsets.symmetric(vertical: 10),
          color: isSelected
              ? getColorFromHex('#1a75ba')
              : getColorFromHex("#ededf2"),
          child: Padding(
              padding: const EdgeInsetsDirectional.only(
                top: 25.0,
                bottom: 25,
              ),
              child: getTextWidget(title, 15,
                  isSelected ? Colors.white : Colors.black, FontWeight.bold))),
      onTap: () {
        setState(() {
          unSelectAll();
          isSelected = true;
          setSelectedAnswer(index);
        });
      },
    );
  }
}




