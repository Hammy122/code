import 'dart:convert';
import 'dart:io';

import 'package:circular_check_box/circular_check_box.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app/apiHelper/allquestions_response.dart';
import 'package:flutter_app/apiHelper/app_exceptions.dart';
import 'package:flutter_app/models/PainFeelItem.dart';
import 'package:flutter_app/persistance/api_repository.dart';
import 'package:flutter_app/screens/ChangePassword.dart';
import 'package:flutter_app/screens/ReportYourPain/ReportYourPainQuestionTwo.dart';
import 'package:flutter_app/utils/ColorUtils.dart';
import 'package:flutter_app/utils/CustomPopupMenu.dart';
import 'package:flutter_app/models/ReachBean.dart';
import 'package:flutter_app/screens/login/Signin.dart';
import 'package:flutter_app/utils/UtilsImporter.dart';
import 'package:flutter_app/utils/app_widgets.dart';
import 'package:flutter_app/utils/utils.dart';
import 'package:path_provider/path_provider.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:toast/toast.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter_app/screens/ReportSubmitted.dart';

import 'package:http/http.dart' as http;

class ReportYourPainDaily extends StatefulWidget {
  final ValueChanged<List> onChanged;

  const ReportYourPainDaily({Key key, this.onChanged}) : super(key: key);

  @override
  _ReportYourPainDailyState createState() => _ReportYourPainDailyState();
}

class _ReportYourPainDailyState extends State<ReportYourPainDaily> {
  TextStyle style = TextStyle(fontFamily: getThemeFontName(), fontSize: 14.0);
  String token = "";
  String deviceId = "12345678";
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool isSubmissionInProgress = false;
  TextEditingController _textEditingController = new TextEditingController();
  bool isContentFilled = false;

  @override
  void initState() {
    super.initState();

    Future<String> token = UtilsImporter().preferencesUtils.getToken();
    Future<String> device = getDeviceId();
    token.then((data) {
      print("token ==> " + data);
      this.token = data;
      device.then((deviceId) {
        print("deviceID ==> " + deviceId);
        this.deviceId = deviceId;
        getAllQuestions();
      });
    });

    //using for body
    if (mounted) {
      _publishSelection(_bodyPartList);
    }

    //using for radio

    loadPainFeel().then((result) {
      setState(() {
        isPainFeelValuesLoaded = true;
        print("SIZE ==> " + painfeelItems.length.toString());
      });
    });
  }

  void getAllQuestions() {
    setState(() => isQuestionsLoaded = false);
    var api = new APICallRepository();
    api
        .allquestion(deviceId, UtilsImporter().apiUtils.DAILY,
            UtilsImporter().apiUtils.PAIN, token)
        .then((response) {
      setState(() {
        setState(() => isQuestionsLoaded = true);

        dailyPainQuestionsList = response.response.data;

        // temp remove same body value....
        bool isExist = false;
        dailyPainQuestionsList.forEach((item) {
          if (item.questionrange.contains("body")) {
            if (isExist) {
              dailyPainQuestionsList.remove(item);
            } else {
              isExist = true;
            }
          }
        });
        //.....
      });
    }, onError: (error) {
      setState(() {
        print("ERROR FOUND FINALLY HERE ===> " + error.toString());
        // setState(() => isLoading = false);
        if (error.toString().contains('Unauthorised')) {
          doLogout(context, error.toString());
        }
        if (error is ProviderDeactivatedException) {
          doLogout(context, error.toString());
        } else if (error is PatientDeactivatedException) {
          doLogout(context, error.toString());
        }   else {
          _scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text(error.toString(),
                textAlign: TextAlign.center,
                style: style.copyWith(
                    color: Colors.white, fontWeight: FontWeight.bold)),
            duration: Duration(seconds: 5),
          ));
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    final myTextField = Container(
      child: TextField(
        maxLines: null,
        expands: true,
        keyboardType: TextInputType.multiline,
        maxLength: 500,
        controller: _textEditingController,
        cursorColor: getThemeBlackColor(),
        onChanged: (text) {
          if (text.length > 0)
            setState(() => isContentFilled = true);
          else
            setState(() => isContentFilled = false);
        },
        decoration: InputDecoration(
          border: InputBorder.none,
          contentPadding: EdgeInsets.fromLTRB(10.0, 2.0, 10.0, 2.0),
          hintText: "Describe the event in here",
        ),
        obscureText: false,
        style: style,
      ),
      decoration: BoxDecoration(
        color: getInputBoxFillColor(),
        border: Border.all(
          color: getInputBoxFillColor(),
          width: 1,
        ),
        borderRadius: BorderRadius.circular(2),
      ),
    );

    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      body: SafeArea(
          child: isQuestionsLoaded
              ? Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                      createToolBar(
                          currentQuestionIndex >= 1,
                          'Report your Pain',
                          "Questions " +
                              (currentQuestionIndex + 1).toString() +
                              " of " +
                              dailyPainQuestionsList.length.toString(),
                          currentQuestionIndex <
                                  (dailyPainQuestionsList.length - 1)
                              ? 'Next'
                              : 'Finish'),
                      new Container(
                        margin: const EdgeInsets.only(top: 10),
                        height: 1,
                        color: getColorFromHex('#CCCCCC'),
                      ),
                      SizedBox(height: 20),
                      Expanded(
                          child: dailyPainQuestionsList[currentQuestionIndex]
                                      .questionrange ==
                                  "text"
                              ? Container(
                                  margin: const EdgeInsets.all(20),
                                  child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        getTextWidget(
                                            dailyPainQuestionsList[
                                                    currentQuestionIndex]
                                                .question,
                                            20,
                                            getThemeBlackColor(),
                                            FontWeight.normal),
                                        SizedBox(
                                          height: 3,
                                        ),
                                        getTextWidget(
                                            dailyPainQuestionsList[
                                                    currentQuestionIndex]
                                                .Questionsubtitle,
                                            13,
                                            getThemeBlackColor(),
                                            FontWeight.w600),
                                        SizedBox(
                                          height: 43,
                                        ),
                                        new Expanded(
                                          child: myTextField,
                                        )
                                      ]))
                              : getWidgetFromQuestionRange()
                          //child: getQuestionBodyLayout()
                          // child: getQuestionOneLayout()
                          )
                    ])
              : Center(
                  child: SizedBox(
                    child: Padding(
                        padding: EdgeInsets.fromLTRB(15.0, 11.0, 15.0, 11.0),
                        child: CircularProgressIndicator(
                            valueColor: AlwaysStoppedAnimation<Color>(
                                getThemeGreenColor()))),
                    height: 50.0,
                    width: 60.0,
                  ),
                )),
    );
  }

  Widget createToolBar(
      bool isBackButton, String title, String subTitle, String buttonLabel) {
    return Stack(
      children: [
        SizedBox(
          width: 8,
        ),
        Positioned(
            left: 10,
            top: isBackButton ? 20 : 10,
            //top: 10,
            child: Align(
                alignment: Alignment.center,
                child: InkWell(
                    child: isBackButton
                        ? Image.asset(
                            "assets/ic_back.png",
                            height: 25,
                            width: 40,
                          )
                        : Image.asset(
                            "assets/buttonsTopBarClose.png",
                            height: 40,
                            width: 40,
                          ),
                    onTap: () {
                      //goBack(context);
                      if (currentQuestionIndex <= 0)
                        goBack(context);
                      else {
                        setState(() {
                          //selectedAnswer = "XYZ";
                          _closeModalBottomSheet();
                          if (currentQuestionIndex <= 0)
                            currentQuestionIndex = 0;
                          else
                            currentQuestionIndex--;
                        });
                      }
                    }))),
        Center(
            child: Column(
          children: <Widget>[
            new Padding(
              padding: const EdgeInsetsDirectional.only(top: 15.0, bottom: 0),
              child: getTextWidget(title, 17, Colors.black, FontWeight.bold),
            ),
            getTextWidget(subTitle, 13, getThemeGreenColor(), FontWeight.w600),
          ],
        )),
        Positioned(
            right: 10,
            top: 10,
            child:
            Center(
                child:isSubmissionInProgress ?
                Center(
                    child: SizedBox(
                      child: Padding(
                          padding: EdgeInsets.fromLTRB(15.0, 11.0, 15.0, 11.0),
                          child: CircularProgressIndicator(
                              valueColor: AlwaysStoppedAnimation<Color>(
                                  getThemeGreenColor()))),
                      height: 50.0,
                      width: 60.0,
                    ),
                  )
                : Container(
                    decoration: new BoxDecoration(
                      borderRadius: new BorderRadius.circular(4.0),
                      color: getThemeGreenColor(),
                    ),
                    child: InkWell(
                      child: Visibility(
                          visible: isCheckVisibility(),
                          child: Padding(
                              padding: EdgeInsets.only(
                                  left: 8, top: 6, right: 8, bottom: 6),
                              child: getTextWidget(buttonLabel, 17,
                                  Colors.white, FontWeight.bold))),
                      onTap: () {
                        //goToNext();
                        goToNext(buttonLabel);
                      },
                    )))),
        SizedBox(
          width: 10,
        ),
      ],
    );
  }

//  SET WIDGET FROM QUESTION RANGE
  Widget getWidgetFromQuestionRange() {
    if (dailyPainQuestionsList[currentQuestionIndex].questionrange ==
        "number") {
      return SingleChildScrollView(
          padding: const EdgeInsets.symmetric(horizontal: 30.0),
          child: getQuestionNumberLayout());
    } else if (dailyPainQuestionsList[currentQuestionIndex].questionrange ==
        "body") {
      return SingleChildScrollView(
          padding: const EdgeInsets.symmetric(horizontal: 30.0),
          child: getQuestionBodyLayout());
    } else if (dailyPainQuestionsList[currentQuestionIndex].questionrange ==
        "radio") {
//      return getQuestionRadioLayout();

      return SingleChildScrollView(
          padding: const EdgeInsets.symmetric(horizontal: 30.0),
          child: getQuestionRadioLayout());
    } else {
      return Container(
        margin: const EdgeInsets.all(20),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              getTextWidget(
                  dailyPainQuestionsList[currentQuestionIndex].question,
                  20,
                  getThemeBlackColor(),
                  FontWeight.normal),
              SizedBox(
                height: 45,
              ),
              new Expanded(
                child: getAnswersLayout(
                    dailyPainQuestionsList[currentQuestionIndex]),
              )
            ]),
      );
      // return Expanded(child : getAnswersLayout(dailyPainQuestionsList[currentQuestionIndex]));
    }
    // else
    // {
    //   return getQuestionTextLayout();
    // }
  }

  Widget getAnswersLayout(Datum monthlyPainQuestion) {
    print(
        "LENGTH ==> " + monthlyPainQuestion.questionDetails.length.toString());
    return Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
              child: Container(
                  margin: const EdgeInsets.only(left: 15, right: 15),
                  color: Colors.white,
                  child: ListView.builder(
                    itemCount: monthlyPainQuestion.questionDetails.length,
                    itemBuilder: (context, index) {
                      return _getItemCell(
                          index: index,
                          title:
                              monthlyPainQuestion.questionDetails[index].text,
                          isSelected: monthlyPainQuestion
                              .questionDetails[index].isSelected);
                    },
                    padding: EdgeInsets.all(0.0),
                  ))),
        ]);
  }

  Widget _getItemCell({int index, String title, bool isSelected}) {
    return new GestureDetector(
      child: Card(
          margin: EdgeInsets.symmetric(vertical: 10),
          color: isSelected
              ? getColorFromHex('#1a75ba')
              : getColorFromHex("#ededf2"),
          child: Padding(
              padding: const EdgeInsetsDirectional.only(
                top: 25.0,
                bottom: 25,
              ),
              child: getTextWidget(title, 15,
                  isSelected ? Colors.white : Colors.black, FontWeight.bold))),
      onTap: () {
        setState(() {
          unSelectAll();
          isSelected = true;
          setSelectedAnswer(index);
        });
      },
    );
  }

  void setSelectedAnswer(int answerIndex) {
    dailyPainQuestionsList[currentQuestionIndex]
        .questionDetails[answerIndex]
        .isSelected = true;
    selected_Question_details = dailyPainQuestionsList[currentQuestionIndex]
        .questionDetails[answerIndex]
        .text;
  }

  void unSelectAll() {
    for (int i = 0; i < dailyPainQuestionsList.length; i++) {
      for (int j = 0;
          j < dailyPainQuestionsList[i].questionDetails.length;
          j++) {
        dailyPainQuestionsList[i].questionDetails[j].isSelected = false;
      }
    }
  }

//  NUMBER WIDGET....

  int colorIndex = 0;
  String selectedAnswer = "XYZ";
  int currentQuestionIndex = 0;
  bool isQuestionsLoaded = false;
  List<Datum> dailyPainQuestionsList = new List();
  List<String> colorList = [
    '#c60000',
    '#7a2565',
    '#b01d47',
    '#f77118',
    '#ffab03',
    '#86c433',
    '#00abb0',
    '#1a75ba',
    '#134f7d',
    '#133c52',
    '#999999'
  ];

  Widget getQuestionNumberLayout() {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          getTextWidget(dailyPainQuestionsList[currentQuestionIndex].question,
              20, getThemeBlackColor(), FontWeight.normal),
          SizedBox(
            height: 3,
          ),
          getTextWidget(
              dailyPainQuestionsList[currentQuestionIndex].Questionsubtitle,
              13,
              getThemeBlackColor(),
              FontWeight.w600),
          SizedBox(
            height: 35,
          ),
          Column(
            children: [
              for (int j = 0;
                  j <
                      dailyPainQuestionsList[currentQuestionIndex]
                          .questionDetails
                          .length;
                  j++)
                getSelectedRoundedCornerWidget(j)
            ],
          ),
        ]);
  }

  Widget getSelectedRoundedCornerWidget(int index) {
    if (index > colorList.length - 1)
      colorIndex = 0;
    else
      colorIndex = index;

    if (index == 0) {
      return getRoundedCornerWidget(
          getColorFromHex(colorList[colorIndex]),
          dailyPainQuestionsList[currentQuestionIndex].questionValue[index],
          dailyPainQuestionsList[currentQuestionIndex]
              .questionDetails[index]
              .text,
          8,
          8,
          0,
          0);
    } else if (index ==
        dailyPainQuestionsList[currentQuestionIndex].questionDetails.length -
            1) {
      return getRoundedCornerWidget(
          getColorFromHex(colorList[colorIndex]),
          dailyPainQuestionsList[currentQuestionIndex].questionValue[index],
          dailyPainQuestionsList[currentQuestionIndex]
              .questionDetails[index]
              .text,
          0,
          0,
          8,
          8);
    } else {
      return getRoundedCornerWidget(
          getColorFromHex(colorList[colorIndex]),
          dailyPainQuestionsList[currentQuestionIndex].questionValue[index],
          dailyPainQuestionsList[currentQuestionIndex]
              .questionDetails[index]
              .text
              .replaceAll("heart_send", ""),
          0,
          0,
          0,
          0);
    }
  }

  Widget getRoundedCornerWidget(
      Color fillColor,
      String textInRectangle,
      String label,
      double cornerTopLeft,
      double cornerTopRight,
      double cornerBottomLeft,
      double cornerBottomRight) {
    return Center(
        child: Row(
      children: [
        new Flexible(
          child: InkWell(
              onTap: () {
                setState(() {
                  selectedAnswer = textInRectangle;
                  // selectedAnswer = label;
                });
              },
              child: new Container(
                margin: EdgeInsets.only(top: 3, left: 25),
                height: 50,
                width: 90,
                decoration: new BoxDecoration(
                  color: getBoxColor(textInRectangle, fillColor),
                  border: new Border.all(color: fillColor, width: 1.0),
                  borderRadius: new BorderRadius.only(
                    topLeft: Radius.circular(cornerTopLeft),
                    topRight: Radius.circular(cornerTopRight),
                    bottomLeft: Radius.circular(cornerBottomLeft),
                    bottomRight: Radius.circular(cornerBottomRight),
                  ),
                ),
                child: Center(
                  child: getTextWidget(
                      textInRectangle,
                      30,
                      getRectFontColor(textInRectangle),
                      getRectFontWeight(textInRectangle)),
                ),
              )),
          flex: 1,
        ),
        SizedBox(
          width: 15,
        ),
        new Flexible(
          child: getTextWidget(
              label,
              15,
              getLabelFontColor(textInRectangle, fillColor),
              getRectFontWeight(textInRectangle)),
          flex: 2,
        )
      ],
    ));
  }

  Color getBoxColor(String label, Color fillColor) {
    if (selectedAnswer == label)
      return fillColor;
    else
      return Colors.white;
  }

  Color getRectFontColor(String label) {
    if (selectedAnswer == label)
      return Colors.white;
    else
      return getInputBoxHintColor();
  }

  FontWeight getRectFontWeight(String label) {
    if (selectedAnswer == label)
      return FontWeight.bold;
    else
      return FontWeight.normal;
  }

  Color getLabelFontColor(String label, Color fillColor) {
    if (selectedAnswer == label)
      return fillColor;
    else
      return getInputBoxHintColor();
  }

// BODY WIDGET....

  var _bodyPartList = [];
  var _radioPartList = [];
  bool isLogoutLoading = false;
  int segmentedControlValue = 0;
  PersistentBottomSheetController controller;

  Widget getQuestionBodyLayout() {
    return Center(
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            getTextWidget(dailyPainQuestionsList[currentQuestionIndex].question,
                20, getThemeBlackColor(), FontWeight.normal),
            SizedBox(
              height: 3,
            ),
            getTextWidget(
                dailyPainQuestionsList[currentQuestionIndex].Questionsubtitle,
                13,
                getThemeBlackColor(),
                FontWeight.w600),
            SizedBox(
              height: 20,
            ),
            Container(
                child: CupertinoSlidingSegmentedControl(
                    children: {
                  0: Container(
                      padding: EdgeInsets.symmetric(vertical: 9.0),
                      child: getTextWidget(
                          "FRONT", 13, Colors.white, FontWeight.bold)),
                  1: Container(
                      padding: EdgeInsets.symmetric(vertical: 9.0),
                      child: getTextWidget(
                          "BACK", 13, Colors.white, FontWeight.bold)),
                },
                    backgroundColor: getInputBoxFillColor(),
                    thumbColor: getThemeGreenColor(),
                    groupValue: segmentedControlValue,
                    onValueChanged: (newValue) {
                      setState(() {
                        segmentedControlValue = newValue;
                      });
                    })),
            segmentedControlValue == 1
                ? humanAnatomyBack()
                : humanAnatomyFront(),
            new Container(
              // margin: const EdgeInsets.only(  left: 30.0, right: 30, top: 5, bottom: 20),
              padding: const EdgeInsets.all(3.0),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  getTextWidget(
                      'Spots selected: ' + _bodyPartList.length.toString(),
                      16,
                      getThemeGrayColor(),
                      FontWeight.w600),
                ],
              ),
            )
          ]),
    );
  }

  Widget humanAnatomyFront() {
    return Container(
      margin: EdgeInsets.only(top: 40),
      color: Colors.white,
      width: 440.0,
      height: 750,
      child: SizedBox(
        child: Stack(
          children: <Widget>[
            bodyPart(UtilsImporter().stringUtils.front_head, "Head", 0.0, 0.0,
                0.0, 100.0),
            bodyPart(UtilsImporter().stringUtils.front_throat, "Throat", 91.0,
                0.0, 0.0, 38.0),
            bodyPart(UtilsImporter().stringUtils.front_chest, "Chest", 102.0,
                0.0, 0.0, 135.0),
            bodyPart(UtilsImporter().stringUtils.front_abdomen, "Abdomen",
                229.0, 0.0, 0.0, 119.0),
            bodyPart(UtilsImporter().stringUtils.front_pelvis, "Pelvis", 342.0,
                0.0, 0.0, 70.0),
            bodyPart(UtilsImporter().stringUtils.front_left_hip, "Left Hip",
                330.0, 95.0, 0.0, 62.0),
            bodyPart(UtilsImporter().stringUtils.front_right_hip, "Right Hip",
                330.0, 0.0, 95.0, 62.0),
            bodyPart(UtilsImporter().stringUtils.front_left_soulder,
                "Left Shoulder", 122.0, 175.0, 0.0, 96.0),
            bodyPart(UtilsImporter().stringUtils.front_right_soulder,
                "Right Shoulder", 122.0, 0.0, 174.0, 96.0),
            bodyPart(UtilsImporter().stringUtils.front_left_upper_arm,
                "Left Arm", 203.0, 192.0, 0.0, 85.0),
            bodyPart(UtilsImporter().stringUtils.front_right_upper_arm,
                "Right Arm", 206.0, 0.0, 191.0, 80.0),
            bodyPart(UtilsImporter().stringUtils.front_left_elbow_one,
                "LeftElbow_One", 283.0, 230.0, 0.0, 9.0),
            bodyPart(UtilsImporter().stringUtils.front_left_elbow_two,
                "LeftElbow_Two", 289.0, 177.0, 0.0, 9.0),
            bodyPart(UtilsImporter().stringUtils.front_right_elbow_one,
                "RightElbow_One", 285.0, 0.0, 177.0, 9.0),
            bodyPart(UtilsImporter().stringUtils.front_right_elbow_two,
                "RightElbow_Two", 285.0, 0.0, 225.0, 9.0),
            bodyPart(UtilsImporter().stringUtils.front_left_forearm,
                "Left Forearm", 292.0, 210.0, 0.0, 105.0),
            bodyPart(UtilsImporter().stringUtils.front_right_forearm,
                "Right Forearm", 290.0, 0.0, 200.0, 100.0),
            bodyPart(UtilsImporter().stringUtils.front_left_hand, "Left Hand",
                395.0, 210.0, 0.0, 55.0),
            showLeftRightButton("Right", 485.0, 240.0, 0.0, 55.0),
            // showLeftRightButton("Right", 485.0, 260.0, 0.0, 55.0),
            bodyPart(UtilsImporter().stringUtils.front_right_hand, "Right Hand",
                388.0, 0.0, 200.0, 55.0),
            showLeftRightButton(" Left ", 485.0, 0.0, 240.0, 55.0),
            // showLeftRightButton(" Left ", 485.0, 0.0, 260.0, 55.0),
            bodyPart(UtilsImporter().stringUtils.front_left_upper_leg,
                "Left Upper Leg", 390.0, 68.0, 0.0, 156.0),
            bodyPart(UtilsImporter().stringUtils.front_right_upper_leg,
                "Right Upper Leg", 390.0, 0.0, 68.0, 156.0),
            bodyPart(UtilsImporter().stringUtils.front_left_knee, "Left Knee",
                528.0, 57.0, 0.0, 36.0),
            bodyPart(UtilsImporter().stringUtils.front_right_knee, "Right Knee",
                528.0, 0.0, 57.0, 36.0),
            bodyPart(UtilsImporter().stringUtils.front_left_lower_leg,
                "Left Lower Leg", 548.0, 62.0, 0.0, 160.0),
            bodyPart(UtilsImporter().stringUtils.front_right_lower_leg,
                "Right Lower Leg", 548.0, 0.0, 62.0, 160.0),
            bodyPart(UtilsImporter().stringUtils.front_left_foot, "Left Foot",
                705.0, 55.0, 0.0, 32.0),
            bodyPart(UtilsImporter().stringUtils.front_right_foot, "Right Foot",
                705.0, 0.0, 55.0, 32.0),
          ],
        ),
      ),
    );
  }

  Widget humanAnatomyBack() {
    return Container(
      margin: EdgeInsets.only(top: 50),
      color: Colors.white,
      width: 400.0,
      height: 820,
      child: SizedBox(
        child: Stack(
          children: <Widget>[
            bodyPart(UtilsImporter().stringUtils.back_head, "Back of Head", 0.0,
                0.0, 0.0, 85.0),
            bodyPart(UtilsImporter().stringUtils.back_neck, "Neck", 59.0, 0.0,
                0.0, 57.0),
            bodyPart(UtilsImporter().stringUtils.back_upper_back, "Upper Back",
                100.0, 0.0, 0.0, 142.0),
            bodyPart(UtilsImporter().stringUtils.back_lower_back, "Lower Back",
                233.0, 0.0, 0.0, 105.0),
            bodyPart(UtilsImporter().stringUtils.back_right_buttock,
                "Right Buttock", 330.0, 0.0, 78.0, 94.0),
            bodyPart(UtilsImporter().stringUtils.back_left_buttock,
                "Left Buttock", 330.0, 78.0, 0.0, 94.0),
            bodyPart(UtilsImporter().stringUtils.back_left_upper_leg,
                "Left Upper Leg Back", 413.0, 78.0, 0.0, 134.0),
            bodyPart(UtilsImporter().stringUtils.back_right_upper_leg,
                "Right Upper Leg Back", 413.0, 0.0, 78.0, 134.0),
            bodyPart(UtilsImporter().stringUtils.back_left_upper_arm,
                "Left Arm Back", 122.0, 179.0, 0.0, 150.0),
            bodyPart(UtilsImporter().stringUtils.back_right_upper_arm,
                "Right Arm Back", 122.0, 0.0, 179.0, 150.0),
            bodyPart(UtilsImporter().stringUtils.back_left_elbow,
                "LeftElbow_back", 255.0, 190.0, 0.0, 32.0),
            bodyPart(UtilsImporter().stringUtils.back_right_elbow,
                "RightElbow_back", 255.0, 0, 190.0, 32.0),
            bodyPart(UtilsImporter().stringUtils.back_left_lower_arm,
                "Left Lower Arm", 270.0, 197.0, 0.0, 115.0),
            bodyPart(UtilsImporter().stringUtils.back_right_lower_arm,
                "Right Forearm Back", 270.0, 0.0, 197.0, 115.0),
            bodyPart(UtilsImporter().stringUtils.back_left_hand,
                "Left Hand Back", 382.0, 196.0, 0.0, 55.0),
            showLeftRightButton(" Left ", 485.0, 240.0, 0.0, 55.0),
            showLeftRightButton("Right", 485.0, 0.0, 240.0, 55.0),

            // showLeftRightButton(" Left ", 485.0, 260.0, 0.0, 55.0),
            // showLeftRightButton("Right", 485.0, 0.0, 260.0, 55.0),
            bodyPart(UtilsImporter().stringUtils.front_right_hand,
                "Right Hand Back", 382.0, 0.0, 196.0, 55.0),
            bodyPart(UtilsImporter().stringUtils.back_left_knee,
                "Left Knee Back", 542.0, 70.0, 0.0, 44.0),
            bodyPart(UtilsImporter().stringUtils.back_right_knee,
                "Right Knee Back", 542.0, 0.0, 70.0, 44.0),
            bodyPart(UtilsImporter().stringUtils.back_left_lower_leg,
                "Left Leg Back", 572.0, 74.0, 0.0, 180.0),
            bodyPart(UtilsImporter().stringUtils.back_right_lower_leg,
                "Right Leg Back", 572.0, 0.0, 74.0, 180.0),
            bodyPart(UtilsImporter().stringUtils.back_left_foot,
                "Left Foot Back", 750.0, 60.0, 0.0, 30.0),
            bodyPart(UtilsImporter().stringUtils.back_right_foot,
                "Right Foot Back", 750.0, 0.0, 60.0, 30.0),
          ],
        ),
      ),
    );
  }

  Widget bodyPart(String svgPath, String svgName, double marginTop,
      double marginRight, double marginLeft, double height) {
    Color _svgColor =
        _bodyPartList.contains(svgName) ? getAnatomicalColor() : null;

//    final Widget bodyPartSvg = new SvgPicture.asset(svgPath,
//        semanticsLabel: svgName, color: _svgColor,allowDrawingOutsideViewBox: true);
    final Widget bodyPartSvg = new Image.asset(svgPath, color: _svgColor);
    return Container(
      margin:
          EdgeInsets.only(top: marginTop, right: marginRight, left: marginLeft),
      height: height,
      alignment: Alignment.topCenter,
      child: GestureDetector(
          onTap: () {
            setState(() {
              if (_bodyPartList.contains(svgName)) {
                _bodyPartList.remove(svgName);
              } else {
                _settingModalBottomSheet(context);
                _bodyPartList.add(svgName);
                // _bodyPartList.forEach((e) => print(e));
              }
            });
          },
          child: bodyPartSvg),
    );
  }

  Widget showLeftRightButton(String label, double marginTop, double marginRight,
      double marginLeft, double height) {
    return Container(
      margin:
          EdgeInsets.only(top: marginTop, right: marginRight, left: marginLeft),
      height: height,
      alignment: Alignment.topCenter,
      child: GestureDetector(onTap: () {}, child: addButton(label)),
    );
  }

  Widget addButton(String label) {
    return Container(
        padding: EdgeInsets.only(left: 15.0, right: 15.0, top: 8, bottom: 8),
        color: getThemeGrayColor(),
        child: getTextWidget(label, 12, Colors.white, FontWeight.normal));
  }

  void _publishSelection(List _bodyPartList) {
    if (widget.onChanged != null) {
      widget.onChanged(_bodyPartList);
    }
  }

  void _settingModalBottomSheet(context) {
    _closeModalBottomSheet();
    controller =
        _scaffoldKey.currentState.showBottomSheet((BuildContext context) {
      return Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(10),
              topRight: Radius.circular(10),
              bottomLeft: Radius.circular(10),
              bottomRight: Radius.circular(10)),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 8,
              blurRadius: 7,
              offset: Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        height: 300,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Stack(
                children: [
                  SizedBox(
                    width: 8,
                  ),
                  Center(
                      child: Column(
                    children: <Widget>[
                      new Padding(
                        padding: const EdgeInsetsDirectional.only(
                            top: 15.0, bottom: 20),
                        child: getTextWidget(
                            'Spots selected: ' +
                                _bodyPartList.length.toString(),
                            17,
                            Colors.black,
                            FontWeight.bold),
                      ),
                      Container()
                    ],
                  )),
                  Positioned(
                      right: 10,
                      top: 10,
                      child: Center(
                          child: Container(
                              decoration: new BoxDecoration(
                                borderRadius: new BorderRadius.circular(4.0),
                                color: getThemeGreenColor(),
                              ),
                              child: InkWell(
                                child: Visibility(
                                    visible: true,
                                    child: Padding(
                                        padding: EdgeInsets.only(
                                            left: 8,
                                            top: 6,
                                            right: 8,
                                            bottom: 6),
                                        child: getTextWidget('Done', 17,
                                            Colors.white, FontWeight.bold))),
                                onTap: () {
                                  _closeModalBottomSheet();
                                },
                              )))),
                  SizedBox(
                    width: 10,
                  ),
                ],
              ),
              Expanded(
                  child: SingleChildScrollView(
                      child: Container(
                          padding: const EdgeInsets.only(
                              left: 20, right: 20, bottom: 20, top: 20),
                          child: getTextWidget(
                              _bodyPartList
                                  .toString()
                                  .replaceAll("[", "")
                                  .replaceAll("]", ""),
                              18,
                              getThemeBlackColor(),
                              FontWeight.normal))))
            ],
          ),
        ),
      );
    });
  }

  void _closeModalBottomSheet() {
    if (controller != null) {
      controller.close();
      controller = null;
    }
  }

// RADIO WIDGET....

  List<PainFeelItem> painfeelItems = List();
  bool isPainFeelValuesLoaded = false;

  loadPainFeel() async {
    String data = await rootBundle.loadString('assets/painfeel.json');
    List<dynamic> list = json.decode(data);
    painfeelItems = list.map((i) => PainFeelItem.fromJson(i)).toList();
  }

  bool isValueSelected() {
    bool isSelected = false;
    for (int i = 0; i < painfeelItems.length; i++) {
      if (painfeelItems[i].isSelected) {
        isSelected = true;
        break;
      }
    }
    return isSelected;
  }

  Widget getQuestionRadioLayout() {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          // getTextWidget("How does your pain feel?", 20, getThemeBlackColor(), FontWeight.normal),
          getTextWidget(dailyPainQuestionsList[currentQuestionIndex].question,
              20, getThemeBlackColor(), FontWeight.normal),

          SizedBox(
            height: 3,
          ),

          getTextWidget(
              dailyPainQuestionsList[currentQuestionIndex].Questionsubtitle,
              13,
              getThemeBlackColor(),
              FontWeight.w600),

          SizedBox(
            height: 43,
          ),

          GridView.count(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            crossAxisCount: 2,
            childAspectRatio: 4.0,
            children: List.generate(painfeelItems.length, (index) {
              return checkbox(painfeelItems[index].name,
                  painfeelItems[index].isSelected, index);
            }),
          )
        ]);
  }

  Widget checkbox(String title, bool boolValue, int index) {
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        CircularCheckBox(
          activeColor: getThemeGreenColor(),
          value: boolValue,
          materialTapTargetSize: MaterialTapTargetSize.padded,
          onChanged: (bool value) {
            setState(() {
              painfeelItems[index].isSelected = value;
            });
          },
        ),
        getTextWidget(
            title,
            15,
            painfeelItems[index].isSelected
                ? getThemeGreenColor()
                : Colors.black,
            painfeelItems[index].isSelected
                ? FontWeight.bold
                : FontWeight.normal)
      ],
    );
  }

// TEXT WIDGET....

  /*

  Widget  getQuestionTextLayout(){

    return Container(

      child:  Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[

            // getTextWidget("How does your pain feel?", 20, getThemeBlackColor(), FontWeight.normal),
            getTextWidget(dailyPainQuestionsList[currentQuestionIndex].question, 20, getThemeBlackColor(), FontWeight.normal),

            SizedBox(  height: 3, ),

            getTextWidget(dailyPainQuestionsList[currentQuestionIndex].Questionsubtitle, 13, getThemeBlackColor(), FontWeight.w600),

            SizedBox( height: 43, ),

            Expanded(
                child : Container(
                  child: TextField(
                    maxLines: null,
                    expands: true,
                    keyboardType: TextInputType.multiline,
                    maxLength: 500,
                    controller: _textEditingController,
                    cursorColor: getThemeBlackColor(),
                    onChanged: (text) {
                      if (text.length > 0)
                        setState(() => isContentFilled = true);
                      else
                        setState(() => isContentFilled = false);
                    },

                    decoration: InputDecoration(
                      border: InputBorder.none,
                      contentPadding: EdgeInsets.fromLTRB(10.0, 2.0, 10.0, 2.0),
                      hintText: "Describe the event in here",
                    ),
                    obscureText: false,
                    style: style,
                  ),
                  decoration: BoxDecoration(
                    color: getInputBoxFillColor(),
                    border: Border.all(
                      color: getInputBoxFillColor(),
                      width: 1,
                    ),
                    borderRadius: BorderRadius.circular(2),
                  ),
                )
            ),



          ]
      ),

    );

  }

*/

/*
  Widget getQuestionOneLayout() {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[

          getTextWidget("How is your physical pain?", 20, getThemeBlackColor(), FontWeight.normal),

          SizedBox( height: 3, ),

          getTextWidget("Select a number from the scale.", 13, getThemeBlackColor(), FontWeight.w600),

          SizedBox(  height: 45, ),

          getRoundedCornerWidget( getRedColor(), "10", 'Unable to Move', 8, 8, 0, 0),

          getRoundedCornerWidget( getColorFromHex('#7a2565'), "9", 'Unbearable', 0, 0, 0, 0),

          getRoundedCornerWidget( getColorFromHex('#b01d47'), "8", 'Severe', 0, 0, 0, 0),

          getRoundedCornerWidget( getColorFromHex('#f77118'), "7", 'Intense', 0, 0, 0, 0),

          getRoundedCornerWidget( getColorFromHex('#ffab03'), "6", 'Distressing', 0, 0, 0, 0),

          getRoundedCornerWidget( getColorFromHex('#86c433'), "5", 'Distracting', 0, 0, 0, 0),

          getRoundedCornerWidget( getColorFromHex('#00abb0'), "4", 'Moderate', 0, 0, 0, 0),

          getRoundedCornerWidget( getColorFromHex('#1a75ba'), "3", 'Uncomfortable', 0, 0, 0, 0),

          getRoundedCornerWidget( getColorFromHex('#134f7d'), "2", 'Minor', 0, 0, 0, 0),

          getRoundedCornerWidget( getColorFromHex('#133c52'), "1", 'Mild', 0, 0, 0, 0),

          getRoundedCornerWidget( getColorFromHex('#999999'), "0", 'Pain free', 0, 0, 8, 8)

        ]
    );

  }
  Widget getQuestionTwoLayout() {

    return Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[

          getTextWidget("How emotionally distressing is your pain?", 20,
              getThemeBlackColor(), FontWeight.normal),

          SizedBox( height: 45, ),

          getRoundedCornerWidget(
              getColorFromHex('#1b76bb'), "10", 'Most distressing', 8, 8, 0, 0),

          getRoundedCornerWidget(
              getColorFromHex('#1b76bb'), "9", '9', 0, 0, 0, 0),

          getRoundedCornerWidget(
              getColorFromHex('#1b76bb'), "8", '8', 0, 0, 0, 0),

          getRoundedCornerWidget(
              getColorFromHex('#1b76bb'), "7", '7', 0, 0, 0, 0),

          getRoundedCornerWidget(
              getColorFromHex('#1b76bb'), "6", '6', 0, 0, 0, 0),

          getRoundedCornerWidget(
              getColorFromHex('#1b76bb'), "5", '5', 0, 0, 0, 0),

          getRoundedCornerWidget(
              getColorFromHex('#1b76bb'), "4", '4', 0, 0, 0, 0),

          getRoundedCornerWidget(
              getColorFromHex('#1b76bb'), "3", '3', 0, 0, 0, 0),

          getRoundedCornerWidget(
              getColorFromHex('#1b76bb'), "2", '2', 0, 0, 0, 0),

          getRoundedCornerWidget(
              getColorFromHex('#1b76bb'), "1", '1', 0, 0, 0, 0),

          getRoundedCornerWidget(
              getColorFromHex('#1b76bb'), "0", 'Not distressing', 0, 0, 8, 8)

        ]
    );

  }

*/

  void goToNext(String buttonLabel) {
    // Navigator.push(context, MaterialPageRoute(builder: (context) {
    //   return ReportYourPainQuestionTwo();
    // }));
    _closeModalBottomSheet();

    submitData(buttonLabel);
    //dailyAddReport(buttonLabel);

//     if (buttonLabel == 'Finish') {
//       Navigator.push(context, MaterialPageRoute(builder: (context) {
//         return ReportSubmitted();
//       }));
//     } else {
//       setState(() {
//         currentQuestionIndex++;
//         colorIndex = 0;
//         selectedAnswer = "XYZ";
//         // _bodyPartList.clear();
//         //isLogoutLoading = false;
//         //segmentedControlValue = 0;
//         //isPainFeelValuesLoaded = false;
// //        _textEditingController.text = "";
// //        isContentFilled = false;
//       });
//     }
  }

  bool isCheckVisibility() {
    if (dailyPainQuestionsList[currentQuestionIndex].questionrange == "number") {
      return selectedAnswer != "XYZ";
    } else if (dailyPainQuestionsList[currentQuestionIndex].questionrange ==  "body") {
      return _bodyPartList.length > 0;
    } else if (dailyPainQuestionsList[currentQuestionIndex].questionrange ==  "radio") {
      return isPainFeelValuesLoaded ? (isValueSelected() ? true : false) : false;
    } else {
      return isContentFilled;
    }
  }

  String selected_Question_details = "";
  String selected_Question_value = "";

  getRadioSelectedItemName() {
    for (int i = 0; i < painfeelItems.length; i++) {
      if (painfeelItems[i].isSelected) {
        _radioPartList.add(painfeelItems[i].name.trim());
      }
    }
  }

  void dailyAddReport(String buttonLabel) {
    if (buttonLabel == 'Finish') {
      Navigator.push(context, MaterialPageRoute(builder: (context) {
        return ReportSubmitted();
      }));
    } else {
      setState(() {
        currentQuestionIndex++;
        colorIndex = 0;
        selectedAnswer = "XYZ";
        selected_Question_details = "";

        _textEditingController.text = "";
      });
    }

    // if (dailyPainQuestionsList[currentQuestionIndex].questionrange == "number")
    // {
    //   selected_Question_value = selectedAnswer;
    //   selected_Question_details = selectedAnswer ;
    // }
    // else if (dailyPainQuestionsList[currentQuestionIndex].questionrange ==  "body")
    // {
    //   selected_Question_details = _bodyPartList.toString().replaceAll("[", "").replaceAll("]", "");
    // }
    // else if (dailyPainQuestionsList[currentQuestionIndex].questionrange ==  "radio")
    // {
    //   getRadioSelectedItemName();
    //   selected_Question_details = _radioPartList.toString().replaceAll("[", "").replaceAll("]", "");
    // }
    // else if (dailyPainQuestionsList[currentQuestionIndex].questionrange == "text") {
    //     selected_Question_details = _textEditingController.text;
    // }
    // if (dailyPainQuestionsList[currentQuestionIndex].questionrange != "number")
    //   {
    //     selected_Question_value = dailyPainQuestionsList[currentQuestionIndex].questionValue == "" ? "Null" : dailyPainQuestionsList[currentQuestionIndex].questionValue;
    //   }
    // var isFlag = "";
    // if(dailyPainQuestionsList[currentQuestionIndex].isAdded)
    // {
    //   isFlag = "1";
    // }else{
    //   isFlag = "0";
    // }
    // var api = new APICallRepository();
    // api.addDailyReport(
    //     token
    //     ,dailyPainQuestionsList[currentQuestionIndex].questionSection
    //     ,selected_Question_value
    //     ,dailyPainQuestionsList[currentQuestionIndex].questionrange  == "" ? "Null" : dailyPainQuestionsList[currentQuestionIndex].questionrange
    //     ,deviceId
    //     ,isFlag   // 0 == add and 1 == update data
    //     ,UtilsImporter().apiUtils.DAILY
    //     ,dailyPainQuestionsList[currentQuestionIndex].id
    //     ,selected_Question_details
    // ).then((response) {
    //   setState(() {
    //     print("Upload ===> " + response.toString());
    //     dailyPainQuestionsList[currentQuestionIndex].isAdded = true;
    //       if (buttonLabel == 'Finish') {
    //         Navigator.push(context, MaterialPageRoute(builder: (context) {
    //           return ReportSubmitted();
    //         }));
    //       } else {
    //         setState(() {
    //           currentQuestionIndex++;
    //           colorIndex = 0;
    //           selectedAnswer = "XYZ";
    //           selected_Question_details = "";
    //           _textEditingController.text = "";
    //         });
    //       }
    //   });
    // }, onError: (error) {
    //   setState(() {
    //     print("ERROR FOUND FINALLY HERE ===> " + error.toString());
    //     if(error.toString().contains('Unauthorised')){
    //       doLogout(context,error.toString());
    //     } else {
    //       _scaffoldKey.currentState.showSnackBar(SnackBar(
    //         content: Text(error.toString(),
    //             textAlign: TextAlign.center,
    //             style: style.copyWith(
    //                 color: Colors.white, fontWeight: FontWeight.bold)),
    //         duration: Duration(seconds: 5),
    //       ));
    //     }
    //   });
    // });
  }


  var dailyQueAnsList = new List<Map<String, String>>();
  File file;

  _writeJson(String text) async {
    final Directory directory = await getApplicationDocumentsDirectory();
    file = File('${directory.path}/report_data.json');
    await file.writeAsString(text);
  }

  void submitData(String buttonLabel)
  {
    if (dailyPainQuestionsList[currentQuestionIndex].questionrange == "number")
    {
      selected_Question_details = selectedAnswer ;
    }
    else if (dailyPainQuestionsList[currentQuestionIndex].questionrange ==  "body")
    {
      selected_Question_details = _bodyPartList.toString().replaceAll("[", "").replaceAll("]", "");
    }
    else if (dailyPainQuestionsList[currentQuestionIndex].questionrange ==  "radio")
    {
      getRadioSelectedItemName();
      selected_Question_details = _radioPartList.toString().replaceAll("[", "").replaceAll("]", "");
    }
    else if (dailyPainQuestionsList[currentQuestionIndex].questionrange == "text") {
        selected_Question_details = _textEditingController.text;
    }

    Map map = new Map<String, String>();
    map["Question_section"] = UtilsImporter().apiUtils.PAIN;
    map["Question"] = dailyPainQuestionsList[currentQuestionIndex].id.toString();
    map["Question_value"] = selected_Question_details;
    map["report_type"] = UtilsImporter().apiUtils.DAILY;
    dailyQueAnsList.add(map);

    if (buttonLabel == 'Finish') {

      setState(() {
        isSubmissionInProgress = true;
      });
      print("MapList==> " + json.encode(dailyQueAnsList));
      _writeJson(json.encode(dailyQueAnsList)).then((result)
      {
        var api = new APICallRepository();
        api.addWeeklyReport(token, deviceId, file).then((response) {
          setState(() {
            isSubmissionInProgress = false;
            print("Success ===> " + response.toString());
            Navigator.push(context, MaterialPageRoute(builder: (context) {
              return ReportSubmitted();
            }));
          });
        }, onError: (error) {
          setState(() {
            isSubmissionInProgress = false;
            print("ERROR FOUND FINALLY HERE ===> " + error.toString());
            if (error.toString().contains('Unauthorised')) {
              doLogout(context, error.toString());
            } else if (error is ProviderDeactivatedException) {
              doLogout(context, error.toString());
            } else if (error is PatientDeactivatedException) {
              doLogout(context, error.toString());
            } else {
              _scaffoldKey.currentState.showSnackBar(SnackBar(
                content: Text(error.toString(),
                    textAlign: TextAlign.center,
                    style: style.copyWith(
                        color: Colors.white, fontWeight: FontWeight.bold)),
                duration: Duration(seconds: 5),
              ));
            }
          });
        });
      });
    } else {
      setState(() {
        currentQuestionIndex++;
        colorIndex = 0;
        selectedAnswer = "XYZ";
        selected_Question_details = "";
        _textEditingController.text = "";
      });
    }
  }

}
