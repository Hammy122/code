import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_app/screens/ChangePassword.dart';
import 'package:flutter_app/utils/ColorUtils.dart';
import 'package:flutter_app/utils/CustomPopupMenu.dart';
import 'package:flutter_app/models/ReachBean.dart';
import 'package:flutter_app/screens/login/Signin.dart';
import 'package:flutter_app/utils/app_widgets.dart';
import 'package:flutter_app/utils/utils.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:toast/toast.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../models/UserBean.dart';
import '../../utils/UtilsImporter.dart';
import 'package:http/http.dart' as http;

import '../body/ReportYourPainQuestionThree.dart';

class ReportYourPainQuestionTwo extends StatefulWidget {
  @override
  _ReportYourPainState createState() => _ReportYourPainState();
}

class _ReportYourPainState extends State<ReportYourPainQuestionTwo> {
  //ProgressDialog pr;
  TextStyle style = TextStyle(fontFamily: 'Orkney', fontSize: 14.0);
  String selectedAnswer = "XYZ";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                createToolBar(true, 'Report your Pain', 'Question 2 of 5', 'Next'),
            new Container(
              margin: const EdgeInsets.only(top: 10),
              height: 1,
              color: getColorFromHex('#CCCCCC'),
            ),
            SizedBox(height: 20),
            Expanded(
                child: SingleChildScrollView(
                    padding: const EdgeInsets.symmetric(horizontal: 30.0),
                    child: getQuestionTwoLayout()))
          ])),
    );
  }
  Widget createToolBar(
      bool isBackButton, String title, String subTitle, String buttonLabel) {
    return Stack(
      children: [
        SizedBox(
          width: 8,
        ),
        Positioned(
            left: 10,
            top: isBackButton ? 20 :  10,
            child: Align(
                alignment: Alignment.center,
                child: InkWell(
                    child: isBackButton
                        ? Image.asset(
                      "assets/ic_back.png",
                      height: 25,
                      width: 40,
                    )
                        : Image.asset(
                      "assets/buttonsTopBarClose.png",
                      height: 40,
                      width: 40,
                    ),
                    onTap: () {
                      goBack(context);
                    }))),
        Center(
            child: Column(
              children: <Widget>[
                new Padding(
                  padding: const EdgeInsetsDirectional.only(top: 15.0, bottom: 0),
                  child: getTextWidget(title, 17, Colors.black, FontWeight.bold),
                ),
                getTextWidget(subTitle, 13, getThemeGreenColor(), FontWeight.w600),
              ],
            )),
        Positioned(
            right: 10,
            top: 10,
            child: Center(
                child: Container(
                    decoration: new BoxDecoration(
                      borderRadius: new BorderRadius.circular(4.0),
                      color: getThemeGreenColor(),
                    ),
                    child: InkWell(
                      child: Visibility(
                          visible: selectedAnswer != "XYZ",
                          child: Padding(
                              padding: EdgeInsets.only(
                                  left: 8, top: 6, right: 8, bottom: 6),
                              child: getTextWidget(buttonLabel, 17,
                                  Colors.white, FontWeight.bold))),
                      onTap: () {
                        goToNext();
                      },
                    )))),
        SizedBox(
          width: 10,
        ),
      ],
    );
  }

  void goToNext() {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return ReportYourPainQuestionThree();
    }));
  }

  Widget getQuestionTwoLayout() {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          getTextWidget("How emotionally distressing is your pain?", 20,
              getThemeBlackColor(), FontWeight.normal),
          SizedBox(
            height: 45,
          ),
          getRoundedCornerWidget(
              getColorFromHex('#1b76bb'), "10", 'Most distressing', 8, 8, 0, 0),
          getRoundedCornerWidget(
              getColorFromHex('#1b76bb'), "9", '9', 0, 0, 0, 0),
          getRoundedCornerWidget(
              getColorFromHex('#1b76bb'), "8", '8', 0, 0, 0, 0),
          getRoundedCornerWidget(
              getColorFromHex('#1b76bb'), "7", '7', 0, 0, 0, 0),
          getRoundedCornerWidget(
              getColorFromHex('#1b76bb'), "6", '6', 0, 0, 0, 0),
          getRoundedCornerWidget(
              getColorFromHex('#1b76bb'), "5", '5', 0, 0, 0, 0),
          getRoundedCornerWidget(
              getColorFromHex('#1b76bb'), "4", '4', 0, 0, 0, 0),
          getRoundedCornerWidget(
              getColorFromHex('#1b76bb'), "3", '3', 0, 0, 0, 0),
          getRoundedCornerWidget(
              getColorFromHex('#1b76bb'), "2", '2', 0, 0, 0, 0),
          getRoundedCornerWidget(
              getColorFromHex('#1b76bb'), "1", '1', 0, 0, 0, 0),
          getRoundedCornerWidget(
              getColorFromHex('#1b76bb'), "0", 'Not distressing', 0, 0, 8, 8)
        ]);
  }

  Widget getRoundedCornerWidget(
      Color fillColor,
      String textInRectangle,
      String label,
      double cornerTopLeft,
      double cornerTopRight,
      double cornerBottomLeft,
      double cornerBottomRight) {
    return Center(
        child: Row(
      children: [
        new Flexible(
          child: InkWell(
              onTap: () {
                setState(() {
                  selectedAnswer = label;
                });
              },
              child: new Container(
                margin: EdgeInsets.only(top: 3, left: 25),
                height: 50,
                width: 90,
                decoration: new BoxDecoration(
                  color: getBoxColor(label, fillColor),
                  border: new Border.all(color: fillColor, width: 1.0),
                  borderRadius: new BorderRadius.only(
                    topLeft: Radius.circular(cornerTopLeft),
                    topRight: Radius.circular(cornerTopRight),
                    bottomLeft: Radius.circular(cornerBottomLeft),
                    bottomRight: Radius.circular(cornerBottomRight),
                  ),
                ),
                child: Center(
                  child: getTextWidget(textInRectangle, 30,
                      getRectFontColor(label), getRectFontWeight(label)),
                ),
              )),
          flex: 1,
        ),
        SizedBox(
          width: 15,
        ),
        new Flexible(
          child: getTextWidget(label, 15, getLabelFontColor(label, fillColor),
              getRectFontWeight(label)),
          flex: 2,
        )
      ],
    ));
  }

  Color getBoxColor(String label, Color fillColor) {
    if (selectedAnswer == label)
      return fillColor;
    else
      return Colors.white;
  }

  Color getRectFontColor(String label) {
    if (selectedAnswer == label)
      return Colors.white;
    else
      return getInputBoxHintColor();
  }

  FontWeight getRectFontWeight(String label) {
    if (selectedAnswer == label)
      return FontWeight.bold;
    else
      return FontWeight.normal;
  }

  Color getLabelFontColor(String label, Color fillColor) {
    if (label == '1' ||
        label == '2' ||
        label == '3' ||
        label == '4' ||
        label == '5' ||
        label == '6' ||
        label == '7' ||
        label == '8' ||
        label == '9') {
      return Colors.white;
    } else if (selectedAnswer == label)
      return getThemeBlackColor();
    else
      return getThemeBlackColor();
  }
}
