import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_app/persistance/api_repository.dart';
import 'package:flutter_app/screens/ChangePassword.dart';
import 'package:flutter_app/screens/ReportYourPain/ReportYourPainQuestionOne.dart';
import 'package:flutter_app/screens/ReportYourPain/ReportYourPainQuestionTwo.dart';
import 'package:flutter_app/screens/ReportYourPain/Weekly/ReportYourPainWeekly.dart';
import 'package:flutter_app/utils/ColorUtils.dart';
import 'package:flutter_app/utils/CustomPopupMenu.dart';
import 'package:flutter_app/models/ReachBean.dart';
import 'package:flutter_app/screens/login/Signin.dart';
import 'package:flutter_app/utils/app_widgets.dart';
import 'package:flutter_app/utils/utils.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:toast/toast.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../models/UserBean.dart';
import '../../utils/UtilsImporter.dart';
import 'package:http/http.dart' as http;

import 'Daily/ReportYourPainDaily.dart';

class ReportYourPain extends StatefulWidget {
  @override
  _ReportYourPainState createState() => _ReportYourPainState();
}

class _ReportYourPainState extends State<ReportYourPain> {
  //ProgressDialog pr;
  TextStyle style = TextStyle(fontFamily: getThemeFontName(), fontSize: 14.0);
  String selectedAnswer = "XYZ";
  bool isWeeklySubmitted = false;
  String next_submission_date =  "";
  int daysLeft=1;
  String token = "";
  String deviceId = "12345678";
  bool isLoading=false;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    isLoading=true;
    Future<String> token = UtilsImporter().preferencesUtils.getToken();
    Future<String> device = getDeviceId();
    token.then((data) {
      print("token ==> " + data);
      this.token = data;
      device.then((deviceId) {
        print("deviceID ==> " + deviceId);
        this.deviceId = deviceId;
        getdatereport();
      });
    });
  }
  void getdatereport() {
    setState(() => isLoading = true);
    var api = new APICallRepository();
    api
        .getdatereport(deviceId, UtilsImporter().apiUtils.PAIN,
          token)
        .then((response) {
      setState(() {
        setState(() => isLoading = false);
        if(response.response.isAllow=="true"){
          setState(() {
            daysLeft=response.response.daysLeft;
            isWeeklySubmitted=false;
          });
        }else{
          setState(() {
            next_submission_date=response.response.nextSubmissionDate.toString();
            daysLeft=response.response.daysLeft;
            isWeeklySubmitted=true;
          });
        }

      //  print("Response ==> "+ respons.toString());

        //.....
      });
    }, onError: (error) {
      setState(() {
        print("ERROR FOUND FINALLY HERE ===> " + error.toString());
        setState(() => isLoading = false);
        if (error.toString().contains('Unauthorised')) {
          doLogout(context, error.toString());
        } else {
          _scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text(error.toString(),
                textAlign: TextAlign.center,
                style: style.copyWith(
                    color: Colors.white, fontWeight: FontWeight.bold)),
            duration: Duration(seconds: 5),
          ));
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      body:  isLoading
          ? Center(
        child: SizedBox(
          child: Padding(
              padding: EdgeInsets.fromLTRB(15.0, 11.0, 15.0, 11.0),
              child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(
                      getThemeGreenColor()))),
          height: 50.0,
          width: 60.0,
        ),
      )
          :SafeArea(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
            createToolBar(false, 'Report your Pain'),
            new Container(
              margin: const EdgeInsets.only(top: 10),
              height: 1,
              color: getColorFromHex('#CCCCCC'),
            ),
            SizedBox(height: 20),
            Expanded(
                child: SingleChildScrollView(
                    padding: const EdgeInsets.symmetric(horizontal: 30.0),
                    child: getLayout()))
          ])),
    );
  }

  Widget createToolBar(bool isBackButton, String title) {
    return Stack(
      children: [
        SizedBox(
          width: 8,
        ),
        Positioned(
            left: 10,
            top: isBackButton ? 20 : 10,
            child: Align(
                alignment: Alignment.center,
                child: InkWell(
                    child: isBackButton
                        ? Image.asset(
                            "assets/ic_back.png",
                            height: 25,
                            width: 40,
                          )
                        : Image.asset(
                            "assets/buttonsTopBarClose.png",
                            height: 40,
                            width: 40,
                          ),
                    onTap: () {
                      goBack(context);
                    }))),
        Center(
            child: Column(
          children: <Widget>[
            new Padding(
              padding: const EdgeInsetsDirectional.only(top: 15.0, bottom: 5),
              child: getTextWidget(title, 18, Colors.black, FontWeight.bold),
            ),
          ],
        )),
        SizedBox(
          width: 10,
        ),
      ],
    );
  }

  void goToNext() {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return ReportYourPainQuestionOne();
    }));
  }

  Widget getLayout() {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Center(
              child: Image.asset(
            UtilsImporter().stringUtils.reportYourPainLogo,
            height: 180,
            fit: BoxFit.fitHeight,
          )),
          SizedBox(
            height: 45,
          ),
          GestureDetector(
            child: Card(
                margin: EdgeInsets.symmetric(vertical: 10),
                color: Colors.white,
                elevation: 5,
                child: Padding(
                  padding: const EdgeInsetsDirectional.only(
                      top: 25.0, bottom: 25, start: 20, end: 5),
                  child: Row(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                            child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            getTextWidget("Submit a Daily Report", 15,
                                getThemeGreenColor(), FontWeight.bold),
                            SizedBox(
                              height: 8,
                            ),
                            getTextWidgetWithAlign(
                                "Submit anytime time a day and as many times as you need!",
                                12,
                                getThemeGrayColor(),
                                FontWeight.w600,
                                TextAlign.left),
                            SizedBox(
                              height: 18,
                            ),
                          ],
                        )),
                        Image.asset(
                          "assets/right_arrow.png",
                          height: 15,
                          width: 25,
                        )
                      ]),
                )),
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                // return ReportYourPainQuestionOne();
                return ReportYourPainDaily();
                //                        return HumanAnatomy();
              }));
            },
          ),
          SizedBox(
            height: 15,
          ),
          GestureDetector(
            child: Card(
                margin: EdgeInsets.symmetric(vertical: 10),
                color: Colors.white,
                elevation: 5,
                child: Padding(
                  padding: const EdgeInsetsDirectional.only(
                      top: 25.0, bottom: 25, start: 20, end: 5),
                  child: Row(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                            child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            isWeeklySubmitted
                                ? Row(
                                    children: <Widget>[
                                      getTextWidget(
                                          "Weekly Report",
                                          15,
                                          getThemeGreenColor(),
                                          FontWeight.bold),
                                      SizedBox(
                                        width: 5,
                                      ),
                                      Image.asset(
                                        "assets/lockedBadge.png",
                                        height: 22,
                                        width: 60,
                                      )
                                    ],
                                  )
                                : getTextWidget("Submit a Weekly Report", 15,
                                    getThemeGreenColor(), FontWeight.bold),
                            SizedBox(
                              height: 8,
                            ),
                            isWeeklySubmitted
                                ? getTextWidgetWithAlign(
                                    "You presented your report one day ago. The next date you can submit is "+next_submission_date +".",
                                    12,
                                    getThemeGrayColor(),
                                    FontWeight.w600,
                                    TextAlign.left)
                                : getTextWidgetWithAlign(
                                    "Submit your first weekly report.",
                                    12,
                                    getThemeGrayColor(),
                                    FontWeight.w600,
                                    TextAlign.left),
                            SizedBox(
                              height: isWeeklySubmitted ? 10 : 30,
                            ),
                            isWeeklySubmitted
                                ? Row(
                                    children: <Widget>[
                                      getTextWidget(
                                          "Why is locked?",
                                          11,
                                          getThemeGreenColor(),
                                          FontWeight.normal),
                                      SizedBox(
                                        width: 5,
                                      ),
                                      Image.asset(
                                        "assets/ic_info.png",
                                        height: 15,
                                        width: 15,
                                      )
                                    ],
                                  )
                                : Container()
                          ],
                        )),
                        isWeeklySubmitted
                            ? Container(
                                margin: const EdgeInsets.only(right: 10,left: 10),
                                child: CircularPercentIndicator(

                                  radius: 90.0,
                                  lineWidth: 5.0,
                                  percent: 0.5,
                                  circularStrokeCap: CircularStrokeCap.round,
                                  animation: true,
                                  animationDuration: 1000,
                                  center: getTextWidget(daysLeft.toString()+ "  \n Days left", 15,
                                      getThemeBlackColor(), FontWeight.normal),
                                  progressColor: getThemeBlueColor(),
                                ),
                              )
                            : Image.asset(
                                "assets/right_arrow.png",
                                height: 15,
                                width: 25,
                              )
                      ]),
                )),
            onTap: () {
              isWeeklySubmitted
                  ? UtilsImporter().commanUtils.showAlertDialogWithTitleOkButton(
                      "Your Weekly Pain Report",
                      "To ensure accuracy and quality, the pain catastrophizing monthly report can be submitted once per week. The countdown will display will help you monitor when you can submit your next report.",
                      context)
                  : Navigator.push(context,
                      MaterialPageRoute(builder: (context) {
                      return ReportYourPainWeekly();
                      //                        return HumanAnatomy();
                    }));
            },
          ),
        ]);
  }
}
