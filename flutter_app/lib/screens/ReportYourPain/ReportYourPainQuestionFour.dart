import 'dart:convert';

import 'package:circular_check_box/circular_check_box.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app/models/PainFeelItem.dart';
import 'package:flutter_app/screens/ChangePassword.dart';
import 'package:flutter_app/screens/ReportYourPain/ReportYourPainQuestionFive.dart';
import 'package:flutter_app/screens/ReportYourPain/ReportYourPainQuestionTwo.dart';
import 'package:flutter_app/utils/ColorUtils.dart';
import 'package:flutter_app/utils/CustomPopupMenu.dart';
import 'package:flutter_app/models/ReachBean.dart';
import 'package:flutter_app/screens/login/Signin.dart';
import 'package:flutter_app/utils/app_widgets.dart';
import 'package:flutter_app/utils/utils.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:toast/toast.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../models/UserBean.dart';
import '../../utils/UtilsImporter.dart';
import 'package:http/http.dart' as http;

class ReportYourPainQuestionFour extends StatefulWidget {
  @override
  _ReportYourPainState createState() => _ReportYourPainState();
}


class _ReportYourPainState extends State<ReportYourPainQuestionFour> {
  //ProgressDialog pr;
  List<PainFeelItem> painfeelItems = List();
  bool isPainFeelValuesLoaded = false;
  TextStyle style = TextStyle(fontFamily: getThemeFontName(), fontSize: 14.0);

  loadPainFeel() async {
    String data = await rootBundle.loadString('assets/painfeel.json');
    List<dynamic> list = json.decode(data);
    painfeelItems = list.map((i) => PainFeelItem.fromJson(i)).toList();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    loadPainFeel().then((result) {
      setState(() {
        isPainFeelValuesLoaded = true;
        print("SIZE ==> " + painfeelItems.length.toString());
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[

        createToolBar(true, 'Report your Pain', 'Question 4 of 5', 'Next'),
            new Container(
              margin: const EdgeInsets.only(top: 10),
              height: 1,
              color: getColorFromHex('#CCCCCC'),
            ),
            SizedBox(height: 20),
            Expanded(child: getQuestionFourLayout())
          ])),
    );
  }

  Widget createToolBar( bool isBackButton, String title, String subTitle, String buttonLabel) {
    return Stack(
      children: [
        SizedBox(
          width: 8,
        ),
        Positioned(
            left: 10,
            top: isBackButton ? 20 :  10,
            child: Align(
                alignment: Alignment.center,
                child: InkWell(
                    child: isBackButton
                        ? Image.asset(
                      "assets/ic_back.png",
                      height: 25,
                      width: 40,
                    )
                        : Image.asset(
                      "assets/buttonsTopBarClose.png",
                      height: 40,
                      width: 40,
                    ),
                    onTap: () {
                      goBack(context);
                    }))),
        Center(
            child: Column(
              children: <Widget>[
                new Padding(
                  padding: const EdgeInsetsDirectional.only(top: 15.0, bottom: 0),
                  child: getTextWidget(title, 17, Colors.black, FontWeight.bold),
                ),
                getTextWidget(subTitle, 13, getThemeGreenColor(), FontWeight.w600),
              ],
            )),
        Positioned(
            right: 10,
            top: 10,
            child: Center(
                child: Container(
                    decoration: new BoxDecoration(
                      borderRadius: new BorderRadius.circular(4.0),
                      color: getThemeGreenColor(),
                    ),
                    child: InkWell(
                      child: Visibility(
                          visible: isPainFeelValuesLoaded ? (isValueSelected() ? true : false)  : false,
                          child: Padding(
                              padding: EdgeInsets.only(
                                  left: 8, top: 6, right: 8, bottom: 6),
                              child: getTextWidget(buttonLabel, 17,
                                  Colors.white, FontWeight.bold))),
                      onTap: () {
                        goToNext();
                      },
                    )))),
        SizedBox(
          width: 10,
        ),
      ],
    );
  }

  bool isValueSelected() {
    bool isSelected = false;
    for (int i = 0; i < painfeelItems.length; i++) {
      if (painfeelItems[i].isSelected) {
        isSelected = true;
        break;
      }
    }
    return isSelected;
  }

  void goToNext() {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return ReportYourPainQuestionFive();
    }));
  }

  Widget getQuestionFourLayout() {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          getTextWidget("How does your pain feel?", 20, getThemeBlackColor(),
              FontWeight.normal),
          SizedBox(
            height: 3,
          ),
          getTextWidget("Check all that apply.", 13, getThemeBlackColor(),
              FontWeight.w600),
          SizedBox(
            height: 43,
          ),
          new Expanded(
            child: GridView.count(
              shrinkWrap: true,
              crossAxisCount: 2,
              childAspectRatio: 4.0,
              children: List.generate(painfeelItems.length, (index) {
                return checkbox(painfeelItems[index].name,
                    painfeelItems[index].isSelected, index);
              }),
            ),
          )
        ]);
  }

  Widget checkbox(String title, bool boolValue, int index) {
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        CircularCheckBox(
          activeColor: getThemeGreenColor(),
          value: boolValue,
          materialTapTargetSize: MaterialTapTargetSize.padded,
          onChanged: (bool value) {
            setState(() {
              painfeelItems[index].isSelected = value;
            });
          },
        ),
        getTextWidget(
            title,
            15,
            painfeelItems[index].isSelected
                ? getThemeGreenColor()
                : Colors.black,
            painfeelItems[index].isSelected
                ? FontWeight.bold
                : FontWeight.normal)
      ],
    );
  }
}
