import 'dart:convert';

import 'package:circular_check_box/circular_check_box.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app/models/PainFeelItem.dart';
import 'package:flutter_app/screens/ChangePassword.dart';
import 'package:flutter_app/screens/ReportYourPain/ReportYourPainQuestionSix.dart';
import 'package:flutter_app/screens/ReportYourPain/ReportYourPainQuestionTwo.dart';
import 'package:flutter_app/utils/ColorUtils.dart';
import 'package:flutter_app/utils/CustomPopupMenu.dart';
import 'package:flutter_app/models/ReachBean.dart';
import 'package:flutter_app/screens/login/Signin.dart';
import 'package:flutter_app/utils/app_widgets.dart';
import 'package:flutter_app/utils/utils.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:toast/toast.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../models/UserBean.dart';
import '../../utils/UtilsImporter.dart';
import 'package:http/http.dart' as http;

import '../ReportSubmitted.dart';

class ReportYourPainQuestionFive extends StatefulWidget {
  @override
  _ReportYourPainState createState() => _ReportYourPainState();
}

class _ReportYourPainState extends State<ReportYourPainQuestionFive> {

  TextStyle style = TextStyle(fontFamily: getThemeFontName(), fontSize: 18.0);

  TextEditingController _textEditingController = new TextEditingController();
  bool isContentFilled = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final myTextField = Container(
      child: TextField(
        maxLines: null,
        expands: true,
        keyboardType: TextInputType.multiline,
        maxLength: 500,
        controller: _textEditingController,
        cursorColor: getThemeBlackColor(),
        onChanged: (text) {
          if (text.length > 0)
            setState(() => isContentFilled = true);
          else
            setState(() => isContentFilled = false);
        },
        decoration: InputDecoration(
          border: InputBorder.none,
          contentPadding: EdgeInsets.fromLTRB(10.0, 2.0, 10.0, 2.0),
          hintText: "Describe the event in here",
        ),
        obscureText: false,
        style: style,
      ),
      decoration: BoxDecoration(
        color: getInputBoxFillColor(),
        border: Border.all(
          color: getInputBoxFillColor(),
          width: 1,
        ),
        borderRadius: BorderRadius.circular(2),
      ),
    );

    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                createToolBar(true, 'Report your Pain', 'Question 5 of 5', 'Finish'),
            new Container(
              margin: const EdgeInsets.only(top: 10),
              height: 1,
              color: getColorFromHex('#CCCCCC'),
            ),


                Expanded(
                  child: Container(
                      margin: const EdgeInsets.all(20),
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            getTextWidget( "Did a specific event cause an increase in pain?",
                                20,
                                getThemeBlackColor(),
                                FontWeight.normal),
                            SizedBox(
                              height: 3,
                            ),
                            SizedBox(
                              height: 43,
                            ),
                            new Expanded(
                              child: myTextField,
                            )
                          ])),
                )
          ])),
    );
  }

  Widget createToolBar(  bool isBackButton, String title, String subTitle, String buttonLabel) {
    return Stack(
      children: [
        SizedBox(
          width: 8,
        ),
        Positioned(
            left: 10,
            top: isBackButton ? 20 :  10,
            child: Align(
                alignment: Alignment.center,
                child: InkWell(
                    child: isBackButton
                        ? Image.asset(
                      "assets/ic_back.png",
                      height: 25,
                      width: 40,
                    )
                        : Image.asset(
                      "assets/buttonsTopBarClose.png",
                      height: 40,
                      width: 40,
                    ),
                    onTap: () {
                      goBack(context);
                    }))),
        Center(
            child: Column(
              children: <Widget>[
                new Padding(
                  padding: const EdgeInsetsDirectional.only(top: 15.0, bottom: 0),
                  child: getTextWidget(title, 17, Colors.black, FontWeight.bold),
                ),
                getTextWidget(subTitle, 13, getThemeGreenColor(), FontWeight.w600),
              ],
            )),
        Positioned(
            right: 10,
            top: 10,
            child: Center(
                child: Container(
                    decoration: new BoxDecoration(
                      borderRadius: new BorderRadius.circular(4.0),
                      color: getThemeGreenColor(),
                    ),
                    child: InkWell(
                      child: Visibility(
                          visible:isContentFilled,
                          child: Padding(
                              padding: EdgeInsets.only(
                                  left: 8, top: 6, right: 8, bottom: 6),
                              child: getTextWidget(buttonLabel, 17,
                                  Colors.white, FontWeight.bold))),
                      onTap: () {
                        goToNext();
                      },
                    )))),
        SizedBox(
          width: 10,
        ),
      ],
    );
  }

  void goToNext() {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return ReportSubmitted();
    }));
  }
}
