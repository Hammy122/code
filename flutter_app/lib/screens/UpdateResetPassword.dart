


import 'package:flutter/material.dart';
import 'package:flutter_app/persistance/api_repository.dart';
import 'package:flutter_app/screens/login/Signin.dart';
import 'package:flutter_app/utils/ColorUtils.dart';
import 'package:flutter_app/utils/StringUtils.dart';
import 'package:flutter_app/utils/UtilsImporter.dart';
import 'package:flutter_app/utils/app_widgets.dart';
import 'package:flutter_app/utils/utils.dart';

class UpdateResetPassword extends StatefulWidget {
  UpdateResetPassword(this.email, {Key key, this.title}) : super(key: key);
  final String title;
  final String email;

  @override
  _ForgotPasswordPageState createState() => _ForgotPasswordPageState();
}

class _ForgotPasswordPageState extends State<UpdateResetPassword> {

  bool isLoading = false;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  bool isError = false;
  bool isPasswordAvailable = true;
  bool isConfirmPasswordAvailable = true;

  TextEditingController _textEditingControllerPassword = new TextEditingController();
  TextEditingController _textEditingControllerConfirmPassword = new TextEditingController();

  @override
  Widget build(BuildContext context) {

    final newPasswordField = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Expanded(
              child: TextField(
                cursorColor: getThemeBlackColor(),
                onChanged: (text) {
                  if (text.length > 0)
                    setState(() => isPasswordAvailable = true);
                  else
                    setState(() => isPasswordAvailable = false);
                },
                controller: _textEditingControllerPassword,
                decoration: InputDecoration(
                  hintText: "New Password",
                  border: InputBorder.none,
                  contentPadding: EdgeInsets.fromLTRB(10.0, 2.0, 10.0, 2.0),
                ),
                obscureText: true,
                style: style,
              )),
          // ClipOval(
          //   child: Material(
          //     child: InkWell(
          //       child: Image.asset(
          //         "assets/ic_info.png",
          //         width: 18,
          //         height: 18,
          //       ),
          //       onTap: () {
          //         UtilsImporter().commanUtils.showAlertDialogWithTitleOkButton(
          //             UtilsImporter().stringUtils.returnPasswordTitle,
          //             UtilsImporter().stringUtils.returnPasswordDescription,
          //             context);
          //       },
          //     ),
          //   ),
          // ),
          // SizedBox(
          //   width: 10,
          // ),
        ],
      ),
      decoration: BoxDecoration(
        color: getInputBoxFillColor(),
        border: isPasswordAvailable
            ? Border.all(
          color: getInputBoxFillColor(),
          width: 1,
        )
            : Border.all(
          color: getRedColor(),
          width: 1,
        ),
        borderRadius: BorderRadius.circular(2),
      ),
    );

    final confirmPasswordField = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Expanded(
              child: TextField(
                cursorColor: getThemeBlackColor(),
                onChanged: (text) {
                  if (text.length > 0)
                    setState(() => isConfirmPasswordAvailable = true);
                  else
                    setState(() => isConfirmPasswordAvailable = false);
                },
                controller: _textEditingControllerConfirmPassword,
                decoration: InputDecoration(
                  hintText: "Confirm Password",
                  border: InputBorder.none,
                  contentPadding: EdgeInsets.fromLTRB(10.0, 2.0, 10.0, 2.0),
                ),
                obscureText: true,
                style: style,
              )),
          // ClipOval(
          //   child: Material(
          //     child: InkWell(
          //       child: Image.asset(
          //         "assets/ic_info.png",
          //         width: 18,
          //         height: 18,
          //       ),
          //       onTap: () {
          //         UtilsImporter().commanUtils.showAlertDialogWithTitleOkButton(
          //             UtilsImporter().stringUtils.returnPasswordTitle,
          //             UtilsImporter().stringUtils.returnPasswordDescription,
          //             context);
          //       },
          //     ),
          //   ),
          // ),
          // SizedBox(
          //   width: 10,
          // ),
        ],
      ),
      decoration: BoxDecoration(
        color: getInputBoxFillColor(),
        border: isConfirmPasswordAvailable
            ? Border.all(
          color: getInputBoxFillColor(),
          width: 1,
        )
            : Border.all(
          color: getRedColor(),
          width: 1,
        ),
        borderRadius: BorderRadius.circular(2),
      ),
    );


    bool validatePassword(String value) {
      Pattern pattern = r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$';
      RegExp regex = new RegExp(pattern);
      print(value);
      if (value.isEmpty) {
         print("=======EMPTY==>");
        return true;
      }
      else  if (!regex.hasMatch(value)) {
        print("=======NOT MATCH==>");
        return true;
      } else {
        return false;
      }

    }


    final submitButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: getThemeGreenColor(),
      child: isLoading
          ? Center(
        child: SizedBox(
          child: Padding(
              padding: EdgeInsets.fromLTRB(15.0, 11.0, 15.0, 11.0),
              child: CircularProgressIndicator(valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),)),
          height: 50.0,
          width: 60.0,
        ),
      )
          : MaterialButton(
        padding: EdgeInsets.fromLTRB(30.0, 18.0, 30.0, 18.0),
        onPressed: () {
          print("Start");
          if (validatePassword(_textEditingControllerPassword.text)) {
            setState(() {
              isPasswordAvailable = false;
              isError = true;
            });
          }

          if (validatePassword(_textEditingControllerConfirmPassword.text)) {
            setState(() {
              isConfirmPasswordAvailable = false;
              isError = true;
            });
          }

          if (!validatePassword(_textEditingControllerPassword.text) && !validatePassword(_textEditingControllerConfirmPassword.text))
          {
            setState(() {
              isError = false;
            });
             ///  print("=======API CALL==>");

            if( _textEditingControllerPassword.text == _textEditingControllerConfirmPassword.text )
            {
              forgotPasswordApiCall();

            }else{
              setState(() {
                isConfirmPasswordAvailable = false;
                isError = true;
              });
            }
          }
        },
        child: Text("Submit",
            textAlign: TextAlign.center,
            style: style.copyWith(
                fontSize: 15,
                color: Colors.white,
                fontWeight: FontWeight.bold)),
      ),
    );

    return Scaffold(
      backgroundColor: Colors.white,
      key: _scaffoldKey,
      body: SafeArea(

        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[

            SizedBox(
              height: 15,
            ),

            Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [

                SizedBox(
                  width: 5,
                ),
                Align(
                    alignment: Alignment.center,
                    child: InkWell(
                        child: Image.asset(
                          "assets/ic_back.png",
                          height: 25,
                          width: 40,
                        ),
                        onTap: () {
                          goBack(context);
                        })),
                Expanded(
                    child: Text("Forgot password?",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: getThemeBlackColor(),
                            fontWeight: FontWeight.bold,
                            fontSize: 17,
                            fontFamily: getThemeFontName())))
              ],
            ),

            new Container(
              margin: const EdgeInsets.only(top: 20),
              height: 1,
              color: getColorFromHex('#CCCCCC'),
            ),

            new Expanded(
              child: SingleChildScrollView(
                child: Container(
                  decoration: BoxDecoration(
                    color: getColorFromHex("#FFFFFF"),
                  ),
                  margin: const EdgeInsets.only(top: 15.0),
                  child: Padding(
                    padding: const EdgeInsets.only(
                      top: 1,
                      left: 20,
                      right: 20,
                      bottom: 20,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[

                        Padding(
                            padding: const EdgeInsets.only(
                                left: 15, bottom: 4, top: 20),
                            child : Container(
                                child: Text(
                                    UtilsImporter().stringUtils.returnForgotPasswordDescription,
                                    style: TextStyle(
                                        color: getThemeBlackColor(),
                                        fontWeight: FontWeight.normal,
                                        fontSize: 17,
                                        fontFamily: getThemeFontName()))),
                        ),

                        Padding(
                            padding: const EdgeInsets.only(
                                left: 10, bottom: 4, top: 20),
                            child: Visibility(
                                maintainSize: true,
                                maintainAnimation: true,
                                maintainState: true,
                                visible: isError,
                                child: Text(
                                    "Please fill in the required fields*",
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                        color: getRedColor(),
                                        fontWeight: FontWeight.bold,
                                        fontSize: 13,
                                        fontFamily: getThemeFontName())))),
                        new Container(
                          margin: const EdgeInsets.only(
                              left: 1.0, right: 1.0, bottom: 10.0),
                          padding: const EdgeInsets.all(10.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[

                              SizedBox(height: 1.0),


                              Padding(
                                  padding: const EdgeInsets.only(
                                      left: 1, right: 1),
                                  child: newPasswordField),

                              SizedBox(height: 10.0),

                              Padding(
                                  padding: const EdgeInsets.only(
                                      left: 1, right: 1),
                                  child: confirmPasswordField),

                              SizedBox(
                                height: 55.0,
                              ),
                              Padding(
                                  padding: const EdgeInsets.only( left: 55, right: 55),
                                  child: submitButton),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void showThankYouDialog(BuildContext context) {
    Dialog dialogThankYou;
    TextStyle style = TextStyle(fontFamily: 'Orkney', fontSize: 14.0);
    final continueButon = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: getThemeGreenColor(),
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () {
          Navigator.pop(context, true);

          Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(builder: (context) => Signin()),
                  (Route<dynamic> route) => false);
        },
        child: Text("Continue",
            textAlign: TextAlign.center,
            style: style.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold)),
      ),
    );

    dialogThankYou = Dialog(
      child: Container(
        padding: EdgeInsets.only(bottom: 20),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(12),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(color: Colors.white),
                  child: Text(
                    "Password updated successfully.",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 18,
                        fontWeight: FontWeight.w600,
                        fontFamily: 'Orkney'),
                  ),
                ),
                SizedBox(
                  width: 30,
                ),
              ],
            ),
            Container(
                margin: const EdgeInsets.only(left: 15, right: 15, top: 6),
                child: continueButon),
          ],
        ),
      ),
    );
    showDialog(
        context: context,
        useRootNavigator: false,
        builder: (BuildContext context) => dialogThankYou);
  }



  void forgotPasswordApiCall() {
    setState(() => isLoading = true);
    var api = new APICallRepository();
    api.updateResetPassword(
      widget.email
    ,_textEditingControllerConfirmPassword.text.trim()
    ).then((user) {
      setState(() {
        setState(() => isLoading = false);
        print("response ==> " + user.toString());
        showThankYouDialog(context);

      });
    }, onError: (error) {
      print("ERROR FOUND FINALLY HERE ===> " + error.toString());
      setState(() => isLoading = false);
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text(error.toString(),
            textAlign: TextAlign.center,
            style: style.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold)),
        duration: Duration(seconds: 5),
      ));
    });
  }

}