import 'dart:io';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/screens/login/Signin.dart';
import 'package:flutter_app/models/UserBean.dart';
import 'package:flutter_app/utils/ColorUtils.dart';
import 'package:flutter_app/utils/utils.dart';
//import 'package:flutter_appavailability/flutter_appavailability.dart';
import 'package:toast/toast.dart';
import 'dart:convert';
 import 'package:http/http.dart' as http;
import '../utils/UtilsImporter.dart';
import 'package:progress_dialog/progress_dialog.dart';

class ResetPassword extends StatefulWidget {
  ResetPassword({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _ResetPasswordPageState createState() => _ResetPasswordPageState();
}

class _ResetPasswordPageState extends State<ResetPassword> {
  ProgressDialog pr;

  TextStyle style = TextStyle(fontFamily: 'Orkney', fontSize: 14.0);
  TextEditingController _textEditingControllerEmail =
      new TextEditingController();
  TextEditingController _textEditingControllerCode =
      new TextEditingController();
  TextEditingController _textEditingControllerNewPassword =
      new TextEditingController();
  TextEditingController _textEditingControllerNewConfirmPassword =
      new TextEditingController();

  TapGestureRecognizer recognizerCreateAccount;
  bool isLoading = false;
  GlobalKey _scaffold = GlobalKey();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      checkInternet();
    });
  }

  checkInternet() {
    setState(() {
      Future<bool> result = UtilsImporter().commanUtils.checkConnection();
      result.then((data) {
        isLoading = data;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context);
    pr.style(
        message: 'Please Wait...',
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: Center(
          child: Container(
            height: 30,
            width: 30,
            margin: EdgeInsets.all(5),
            child: CircularProgressIndicator(
              strokeWidth: 2.0,
              valueColor: AlwaysStoppedAnimation(Colors.black),
            ),
          ),
        ),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 15.0, fontWeight: FontWeight.w600));

    final emailField = TextField(
      controller: _textEditingControllerEmail,
      obscureText: false,
      style: style,
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
          filled: true,
          labelText: 'Verified Email, pls',
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Verified Email, pls 😃",
          fillColor: Colors.white,
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20.0),
            borderSide: BorderSide(
              color: Color.fromRGBO(78, 78, 85, 1),
            ),
          ),
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(20.0))),
    );

    final codeField = TextField(
      controller: _textEditingControllerCode,
      obscureText: false,
      style: style,
      decoration: InputDecoration(
          filled: true,
          labelText: 'Recovering code',
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Recovering code",
          fillColor: Colors.white,
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20.0),
            borderSide: BorderSide(
              color: Color.fromRGBO(78, 78, 85, 1),
            ),
          ),
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(20.0))),
    );
    final newPasswordField = TextField(
      controller: _textEditingControllerNewPassword,
      obscureText: true,
      style: style,
      decoration: InputDecoration(
          filled: true,
          fillColor: Colors.white,
          labelText: 'New Password',
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "New password",
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20.0),
            borderSide: BorderSide(
              color: Color.fromRGBO(78, 78, 85, 1),
            ),
          ),
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(20.0))),
    );

    final confirmPasswordField = TextField(
      controller: _textEditingControllerNewConfirmPassword,
      obscureText: true,
      style: style,
      decoration: InputDecoration(
          filled: true,
          fillColor: Colors.white,
          labelText: 'Confirm new Password',
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Confirm new password",
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20.0),
            borderSide: BorderSide(
              color: Color.fromRGBO(78, 78, 85, 1),
            ),
          ),
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(20.0))),
    );
    final submitButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(20.0),
      color: Color.fromRGBO(78, 78, 85, 1),
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 20.0),
        onPressed: () async {
          /////////////////
          // showThankYouDialog(context);

          if (!UtilsImporter()
              .commanUtils
              .validateTextField(_textEditingControllerEmail.text)) {
            UtilsImporter().commanUtils.showAlertDialogWithOkButton(
                "Please provide your Official Email.", context);
          } else if (!UtilsImporter()
              .commanUtils
              .validateTextField(_textEditingControllerCode.text)) {
            UtilsImporter().commanUtils.showAlertDialogWithOkButton(
                "Please provide recovery code which we have sent you via email.",
                context);
          } else if (!UtilsImporter()
              .commanUtils
              .validateTextField(_textEditingControllerNewPassword.text)) {
            UtilsImporter().commanUtils.showAlertDialogWithOkButton(
                "Please provide new password.", context);
          } else if (!UtilsImporter().commanUtils.validateTextField(
              _textEditingControllerNewConfirmPassword.text)) {
            UtilsImporter().commanUtils.showAlertDialogWithOkButton(
                "Please provide confirm password.", context);
          } else if (_textEditingControllerNewPassword.text !=
              _textEditingControllerNewConfirmPassword.text) {
            UtilsImporter().commanUtils.showAlertDialogWithOkButton(
                "New password and confirm password should be same.", context);
          } else {
            print("Start");
            //////////////////
            pr.show();
            callResetPasswordAPI(_textEditingControllerEmail.text,_textEditingControllerCode.text,_textEditingControllerNewPassword.text,_textEditingControllerNewConfirmPassword.text)
                .then((result) {
//              pr.dismiss();
              print(result);
              try {
                var success = result['success'];
                if (success == true) {
                  showThankYouDialog(context);
                } else {
                  var message = result['message'];
                  UtilsImporter()
                      .commanUtils
                      .showAlertDialogWithOkButton(message, context);
                }
              } on Exception catch (_) {
//                pr.dismiss();
                Toast.show("Something went wrong, please try again!!", context);
              }
            });
          }
        },
        child: Text("Change Password",
            textAlign: TextAlign.center,
            style: style.copyWith(
                color: getColorFromHex("#DBDDE2"),
                fontWeight: FontWeight.bold,
                fontFamily: 'Orkney')),
      ),
    );

    return Scaffold(
        key: _scaffold,
        backgroundColor: getColorFromHex("#E5E5E5"),
        body: SingleChildScrollView(
          child: Container(
            margin: const EdgeInsets.only(top: 15.0),
            child: Padding(
              padding: const EdgeInsets.only(
                  top: 40, bottom: 20, left: 10, right: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
//                  Padding(
//                    padding: const EdgeInsets.all(10),
//                    child: Center(
//                        child: new Text("ThyReach",
//                            style: TextStyle(
//                                fontWeight: FontWeight.bold,
//                                fontSize: 37,
//                                fontFamily: 'Orkney'))),
//                  ),
//                  Padding(
//                      padding: const EdgeInsets.only(top: 5),
//                      child: Center(
//                        child: new Text(
//                          "Your Reach is your Rich, expand your reach",
//                          style: TextStyle(
//                              fontWeight: FontWeight.bold,
//                              fontSize: 15,
//                              fontFamily: 'Orkney'),
//                        ),
//                      )),
                  new Container(
                    margin: const EdgeInsets.only(
                        left: 1.0, top: 35.0, right: 1.0, bottom: 10.0),
                    padding: const EdgeInsets.all(10.0),
                    decoration: BoxDecoration(
                        color: getColorFromHex("#E5E5E5"),
                        border: Border.all(
                            color: Color.fromRGBO(198, 200, 202, 1))),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(top: 15.0, left: 20),
                          child: new Text("Reset Password",
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  color: getColorFromHex("#717F8B"),
                                  fontWeight: FontWeight.normal,
                                  fontSize: 30,
                                  fontFamily: 'Orkney')),
                        ),
                        SizedBox(height: 35.0),
                        emailField,
                        SizedBox(
                          height: 35.0,
                        ),
                        codeField,
                        SizedBox(
                          height: 35.0,
                        ),
                        newPasswordField,
                        SizedBox(
                          height: 35.0,
                        ),
                        confirmPasswordField,
                        SizedBox(
                          height: 35.0,
                        ),
                        submitButton,
                        SizedBox(
                          height: 25.0,
                        ),
                        Wrap(children: <Widget>[
                          new Container(
                            margin: const EdgeInsets.all(15.0),
                            padding: const EdgeInsets.all(3.0),
                            decoration: myBoxDecoration(),
                            child: new GestureDetector(
                                child: Text("Log in",
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                        color: getColorFromHex("#717F8B"),
                                        fontWeight: FontWeight.normal,
                                        fontSize: 15,
                                        fontFamily: 'Orkney')),
                                onTap: () {
                                  Navigator.push(context,
                                      MaterialPageRoute(builder: (context) {
                                    return Signin();
                                  }));
                                }),
                          )
                        ]),
                        SizedBox(
                          height: 55.0,
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ));
  }

  void showThankYouDialog(BuildContext context) {
    Dialog dialogThankYou;
    TextStyle style = TextStyle(fontFamily: 'Orkney', fontSize: 14.0);
    final continueButon = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Color.fromRGBO(78, 78, 85, 1),
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () {
          Navigator.pop(context, true);
          Navigator.of(context)
              .pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
              Signin()), (Route<dynamic> route) => false);
          //openEmailApp(_scaffold.currentState.context);
        },
        child: Text("Go to log in page",
            textAlign: TextAlign.center,
            style: style.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold)),
      ),
    );

    dialogThankYou = Dialog(
      child: Container(
        padding: EdgeInsets.only(bottom: 20),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(
                      top: 18.0, left: 20.0, bottom: 10, right: 20),
                  child: Text(
                    "Password Changed",
                    style: TextStyle(
                        color: Color.fromRGBO(143, 233, 227, 1),
                        fontSize: 22,
                        fontWeight: FontWeight.normal,
                        fontFamily: 'Orkney'),
                  ),
                ),
                Container(
                  child: GestureDetector(
                    onTap: () {
                      Navigator.pop(context, true);
                      Navigator.of(context)
                          .pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
                          Signin()), (Route<dynamic> route) => false);
                    },
                    child: Align(
                      alignment: Alignment.topRight,
                      child: CircleAvatar(
                        radius: 25.0,
                        backgroundColor: Colors.white,
                        child: Icon(Icons.close, color: Colors.black),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            Container(
              margin: const EdgeInsets.only(top: 10.0, left: 15, right: 15),
              child: Text(
                "You have successfully changed reset your passoword, please login with with your new passowrd",
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 15,
                    fontWeight: FontWeight.normal,
                    fontFamily: 'Orkney'),
              ),
            ),

            Container(
                margin: const EdgeInsets.only(left: 15, right: 15, top: 16),
                child: continueButon),
          ],
        ),
      ),
    );
    showDialog(
        context: context,
        useRootNavigator: false,
        builder: (BuildContext context) => dialogThankYou);
  }
}

BoxDecoration myBoxDecoration() {
  return BoxDecoration(
    border: Border(
      bottom: BorderSide(
        color: Color.fromRGBO(113, 127, 139, 1),
        //                   <--- border color
        width: 1.0,
      ),
    ),
  );
}

//void openEmailApp(BuildContext context) {
//  try {
//    AppAvailability.launchApp(
//            Platform.isIOS ? "message://" : "com.google.android.gm")
//        .then((_) {
//      print("App Email launched!");
//    }).catchError((err) {
//      Scaffold.of(context)
//          .showSnackBar(SnackBar(content: Text("App Email not found!")));
//      print(err);
//    });
//  } catch (e) {
//    Scaffold.of(context)
//        .showSnackBar(SnackBar(content: Text("Email App not found!")));
//  }
//}

Future<dynamic> callResetPasswordAPI(
    String email, String code, String new_password, String re_password) async {
  final response = await http.post(
    UtilsImporter().apiUtils.baseURL + UtilsImporter().apiUtils.reset_password1,
    body: json.encode({
      "email": email,
      "code": code,
      "new_password": new_password,
      "re_password": re_password,
    }),
    headers: header(),
  );
  return json.decode(response.body);
}

header() => {"Content-Type": 'application/json'};
