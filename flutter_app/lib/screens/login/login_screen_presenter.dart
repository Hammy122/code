import 'package:flutter_app/data/rest_ds.dart';
import 'package:flutter_app/utils/app_exceptions.dart';

abstract class LoginScreenContract {
  void onLoginSuccess(dynamic response);

  void onLoginError(String errorTxt);
}

class LoginScreenPresenter {
  LoginScreenContract _view;
  RestDatasource api = new RestDatasource();

  LoginScreenPresenter(this._view);

  doLogin(String email, String password) {
//    api.login(email, password).then((dynamic response) {
//      _view.onLoginSuccess(response);
//    }).catchError((Object error) => _view.onLoginError(error.toString()));

    try {
      api.login(email, password).then((dynamic response) {
        _view.onLoginSuccess(response);
      });
    } on FetchDataException catch (e) {
      _view.onLoginError(e.toString());
    }
//    api.login(email, password).then((dynamic response) {
//      _view.onLoginSuccess(response);
//    }).catchError((Object error){
//      _view.onLoginError(error.toString());
//    });
  }
}
