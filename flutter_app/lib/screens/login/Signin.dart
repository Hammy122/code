import 'dart:async';
import 'dart:io';

import 'package:custom_switch/custom_switch.dart';
import 'package:device_info/device_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app/apiHelper/app_exceptions.dart';
import 'package:flutter_app/persistance/api_repository.dart';
import 'package:flutter_app/screens/ForgotPassword.dart';
import 'package:flutter_app/screens/Home.dart';
import 'package:flutter_app/screens/ActivateAccount.dart';
import 'package:flutter_app/screens/TransactionScreen.dart';
import 'package:flutter_app/screens/body/human_anatomy.dart';
import 'package:flutter_app/utils/ColorUtils.dart';
import 'package:flutter_app/utils/StringUtils.dart';
import 'package:flutter_app/utils/my_globals.dart';
import 'package:flutter_app/utils/session_management_timer.dart';
import 'package:flutter_app/utils/utils.dart';
import 'package:flutter_progress_button/flutter_progress_button.dart';
import 'package:local_auth/local_auth.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:toast/toast.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'dart:math' as math show sin, pi;
import '../../models/UserBean.dart';
import '../../utils/UtilsImporter.dart';
import '../GetStarted.dart';
import '../PrivacyPolicy.dart';
import '../Test.dart';
import 'auth.dart';
import 'login_screen_presenter.dart';
import 'package:local_auth/error_codes.dart' as auth_error;

class Signin extends StatefulWidget {
  Signin({Key key, this.title}) : super(key: key);
  final String title;

  @override
  LoginScreenState createState() => LoginScreenState();
}

class LoginScreenState extends State<Signin> {
  TextStyle style = TextStyle(fontFamily: getThemeFontName(), fontSize: 15.0);
  TextEditingController _textEditingControllerEmail =
      new TextEditingController();
  TextEditingController _textEditingControllerPassword =
      new TextEditingController();
  TapGestureRecognizer recognizerCreateAccount;
  bool isLogin = false;
  bool isLoading = false;

  // GlobalKey _scaffold = GlobalKey();
  // final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final formKey = new GlobalKey<FormState>();

  // final scaffoldKey = new GlobalKey<ScaffoldState>();
  bool switchControl = false;
  var textHolder = 'Switch is OFF';
  String deviceId = "12345678";

  String errorString = "Please fill in the required fields*";
  // SessionManagementTimer sessionManagementTimer;
  //MyGlobals myGlobals = MyGlobals();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      checkInternet();
    });
    // sessionManagementTimer = new SessionManagementTimer(myGlobals.scaffoldKey.currentContext);
    recognizerCreateAccount = TapGestureRecognizer()
      ..onTap = () {
//        Navigator.push(context, MaterialPageRoute(builder: (context) {
//          return CreateAccount();
//        }));
      };
    getDeviceID();
  }

  getDeviceID() async {
    deviceId = await getDeviceId();
    print("deviceId => " + deviceId);
  }

  checkInternet() {
    setState(() {
      Future<bool> result = UtilsImporter().commanUtils.checkConnection();
      result.then((data) {
        isLogin = data;
      });
    });
  }

  bool isEmailAvailable = true, isPasswordAvailable = true, isError = false;

  final LocalAuthentication _localAuthentication = LocalAuthentication();

  Future<bool> _isBiometricAvailable() async {
    bool isAvailable = false;
    try {
      isAvailable = await _localAuthentication.canCheckBiometrics;
    } on PlatformException catch (e) {
      print(e);
    }
    if (!mounted) return isAvailable;
    isAvailable
        ? print('Biometric is available!')
        : print('Biometric is unavailable.');
    return isAvailable;
  }

  Future<void> _getListOfBiometricTypes() async {
    List<BiometricType> listOfBiometrics;
    try {
      listOfBiometrics = await _localAuthentication.getAvailableBiometrics();
    } on PlatformException catch (e) {
      print(e);
    }
    if (!mounted) return;
    print(listOfBiometrics);
  }

  Future<void> _authenticateUser() async {
    bool isAuthenticated = false;
    String Error = "";
    String ErrorCode = "";

    try {
      isAuthenticated = await _localAuthentication.authenticateWithBiometrics(
        localizedReason: " ",
        useErrorDialogs: true,
        stickyAuth: true,
      );
    } on PlatformException catch (e) {
      print("==error occur==>" + e.toString());
      if (e.code == auth_error.lockedOut) {
        // Handle this exception here.
        ErrorCode = e.code;
        Error = e.message;
      }
      if (e.code == auth_error.permanentlyLockedOut) {
        // Handle this exception here.
        ErrorCode = e.code;
        Error = e.message;
      }
    }

    if (!mounted) return;
    isAuthenticated
        ? print('User is authenticated!')
        : print('User is not authenticated.');

    if (isAuthenticated) {
      login();
      //Navigator.of(context).push( MaterialPageRoute( builder: (context) => TransactionScreen(), ), );
    } else {
      setState(() {
        isError = true;
        if (Error.isNotEmpty) {
          if (ErrorCode == auth_error.lockedOut) {
            errorString = Error;
          }
          if (ErrorCode == auth_error.permanentlyLockedOut) {
            errorString =
                "The operation was canceled because ERROR_LOCKOUT occurred too many times. Biometric authentication is disabled until Strong authentication like PIN/Pattern/Password is required to unlock.";
          }
        } else {
          errorString = "Please authenticate using Touch ID to continue.";
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    myGlobals.myGlobleContext = context;
    void validateTextFields() {
      if (_textEditingControllerEmail.text.isEmpty ||
          _textEditingControllerPassword.text.isEmpty) {
        setState(() {
          isError = true;
          errorString = "Please fill in the required fields*";
        });
      } else {
        setState(() {
          isError = false;
        });
      }
    }

    final emailField = Container(
      child: TextField(
        controller: _textEditingControllerEmail,
        keyboardType: TextInputType.emailAddress,
        cursorColor: getThemeBlackColor(),
        onChanged: (text) {
          // handleUserInteraction(context);
          if (text.length > 0)
            setState(() => isEmailAvailable = true);
          else
            setState(() => isEmailAvailable = false);
        },
        decoration: InputDecoration(
          border: InputBorder.none,
          contentPadding: EdgeInsets.fromLTRB(10.0, 2.0, 10.0, 2.0),
          hintText: "Email address",
        ),
        obscureText: false,
        style: style,
      ),
      decoration: BoxDecoration(
        color: getInputBoxFillColor(),
        border: isEmailAvailable
            ? Border.all(
                color: getInputBoxFillColor(),
                width: 1,
              )
            : Border.all(
                color: getRedColor(),
                width: 1,
              ),
        borderRadius: BorderRadius.circular(2),
      ),
    );
    final passwordField = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Expanded(
              child: TextField(
            cursorColor: getThemeBlackColor(),
            onChanged: (text) {
              // handleUserInteraction(context);
              if (text.length > 0)
                setState(() => isPasswordAvailable = true);
              else
                setState(() => isPasswordAvailable = false);
            },
            controller: _textEditingControllerPassword,
            decoration: InputDecoration(
              hintText: "Password",
              border: InputBorder.none,
              contentPadding: EdgeInsets.fromLTRB(10.0, 2.0, 10.0, 2.0),
            ),
            obscureText: true,
            style: style,
          )),
          SizedBox(
            width: 10,
          ),
        ],
      ),
      decoration: BoxDecoration(
        color: getInputBoxFillColor(),
        border: isPasswordAvailable
            ? Border.all(
                color: getInputBoxFillColor(),
                width: 1,
              )
            : Border.all(
                color: getRedColor(),
                width: 1,
              ),
        borderRadius: BorderRadius.circular(2),
      ),
    );
    void toggleSwitch(bool value) {
      // handleUserInteraction(context);
      if (switchControl == false) {
        setState(() {
          switchControl = true;
        });
        print('Switch is ON');
        // Put your code here which you want to execute on Switch ON event.
      } else {
        setState(() {
          switchControl = false;
        });
        print('Switch is OFF');
        // Put your code here which you want to execute on Switch OFF event.
      }
    }

    final loginButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: getThemeGreenColor(),
      child: isLoading
          ? Center(
              child: SizedBox(
                child: Padding(
                    padding: EdgeInsets.fromLTRB(15.0, 11.0, 15.0, 11.0),
                    child: CircularProgressIndicator(
                        valueColor:
                            AlwaysStoppedAnimation<Color>(Colors.white))),
                height: 50.0,
                width: 60.0,
              ),
            )
          : MaterialButton(
              padding: EdgeInsets.fromLTRB(30.0, 18.0, 30.0, 18.0),
              onPressed: () async {
                // handleUserInteraction(context);
                validateTextFields();
                if (UtilsImporter()
                    .commanUtils
                    .isTextEmpty(_textEditingControllerEmail.text)) {
                  setState(() {
                    isEmailAvailable = false;
                    isError = true;
                    errorString = "Please fill in the required fields*";
                  });
                }
                if (UtilsImporter()
                    .commanUtils
                    .isTextEmpty(_textEditingControllerPassword.text)) {
                  setState(() {
                    isPasswordAvailable = false;
                    isError = true;
                    errorString = "Please fill in the required fields*";
                  });
                }
                if (isError) return;

                // if(switchControl) {
                //   // if is enable switch show enable toch id screen.
                //   if (await _isBiometricAvailable()) {
                //     await _getListOfBiometricTypes();
                //     await _authenticateUser();
                //   }
                //
                // }else{
                login();
                // }
                // Navigator.push(context, MaterialPageRoute(builder: (context) {
                //   return Home();
                // }));
              },
              child: Text("Sign in",
                  textAlign: TextAlign.center,
                  style: style.copyWith(
                      fontSize: 15,
                      color: Colors.white,
                      fontWeight: FontWeight.bold)),
            ),
    );

    return Scaffold(
      backgroundColor: Colors.white,
      key: myGlobals.scaffoldKey,
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            SizedBox(
              height: 15,
            ),
            Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SizedBox(
                  width: 5,
                ),
                Align(
                    alignment: Alignment.center,
                    child: InkWell(
                        child: Image.asset(
                          "assets/ic_back.png",
                          height: 25,
                          width: 40,
                        ),
                        onTap: () {
                          goBack(context);
                        })),
              ],
            ),
            new Container(
              margin: const EdgeInsets.only(top: 20),
              height: 1,
              color: getColorFromHex('#CCCCCC'),
            ),
            new Expanded(
              child: SingleChildScrollView(
                child: Container(
                  decoration: BoxDecoration(
                    color: getColorFromHex("#FFFFFF"),
                  ),
                  margin: const EdgeInsets.only(top: 15.0),
                  child: Padding(
                    padding: const EdgeInsets.only(
                      top: 1,
                      bottom: 20,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(30),
                          child: Center(
                              child: Image.asset(
                            UtilsImporter().stringUtils.logo,
                            height: 100,
                            fit: BoxFit.fitHeight,
                          )),
                        ),
                        Padding(
                            padding: const EdgeInsets.only(
                                left: 40, bottom: 4, top: 20),
                            child: Visibility(
                                maintainSize: true,
                                maintainAnimation: true,
                                maintainState: true,
                                visible: isError,
                                child: Text(errorString,
                                    //"Please fill in the required fields*",
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                        color: getRedColor(),
                                        fontWeight: FontWeight.bold,
                                        fontSize: 13,
                                        fontFamily: getThemeFontName())))),
                        new Container(
                          margin: const EdgeInsets.only(
                              left: 1.0, right: 1.0, bottom: 10.0),
                          padding: const EdgeInsets.all(10.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              SizedBox(height: 1.0),
                              Padding(
                                padding:
                                    const EdgeInsets.only(left: 25, right: 25),
                                child: Theme(
                                    data: Theme.of(context).copyWith(
                                        splashColor: Colors.transparent),
                                    child: emailField),
                              ),
                              SizedBox(height: 10.0),
                              Padding(
                                  padding: const EdgeInsets.only(
                                      left: 25, right: 25),
                                  child: passwordField),
                              SizedBox(
                                height: 15,
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  new Container(
                                    margin: const EdgeInsets.only(
                                        left: 30.0, top: 20),
                                    padding: const EdgeInsets.all(3.0),
                                    child: new GestureDetector(
                                        behavior: HitTestBehavior.translucent,
                                        child: Text("Enable Touch ID",
                                            textAlign: TextAlign.left,
                                            style: TextStyle(
                                                color: getThemeGrayColor(),
                                                fontWeight: FontWeight.w600,
                                                fontSize: 13,
                                                fontFamily:
                                                    getThemeFontName())),
                                        onTap: () {}),
                                  ),
                                  new Container(
                                    margin: const EdgeInsets.only(
                                        left: 30.0, right: 15, top: 5),
                                    padding: const EdgeInsets.all(3.0),
                                    child: CupertinoSwitch(
                                      activeColor: getThemeGreenColor(),
                                      onChanged: toggleSwitch,
                                      value: switchControl,
                                    ),
                                  )
                                ],
                              ),
                              SizedBox(
                                height: 35.0,
                              ),
                              Padding(
                                  padding: const EdgeInsets.only(
                                      left: 55, right: 55),
                                  child: loginButton),
                              SizedBox(
                                height: 25.0,
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  new Container(
                                    margin: const EdgeInsets.all(15.0),
                                    padding: const EdgeInsets.all(3.0),
                                    decoration: myBoxDecoration(),
                                    child: new GestureDetector(
                                        //child: Text("Forget password?",
                                        behavior: HitTestBehavior.translucent,
                                        child: Text(
                                            UtilsImporter()
                                                .stringUtils
                                                .txtForgetPassword,
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                color: getThemeBlueColor(),
                                                fontWeight: FontWeight.normal,
                                                fontSize: 13,
                                                fontFamily: 'Orkney')),
                                        onTap: () {
                                          Navigator.push(context,
                                              MaterialPageRoute(
                                                  builder: (context) {
                                            return ForgotPassword();
                                          }));
                                        }),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
            Container(
                padding: const EdgeInsets.all(8.0),
                alignment: Alignment.center,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Center(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text("Read our",
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                    color: getThemeGrayColor(),
                                    fontWeight: FontWeight.w600,
                                    fontSize: 15,
                                    fontFamily: getThemeFontName())),
                            SizedBox(
                              width: 5,
                            ),
                            new Container(
                              padding: const EdgeInsets.only(left: 2.0),
                              decoration: myBoxDecoration(),
                              child: new GestureDetector(
                                  behavior: HitTestBehavior.translucent,
                                  child: Text("Privacy Policy.",
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                          color: getColorFromHex("#1b76bb"),
                                          fontWeight: FontWeight.bold,
                                          fontSize: 15,
                                          fontFamily: getThemeFontName())),
                                  onTap: () {
                                    Navigator.push(context,
                                        MaterialPageRoute(builder: (context) {
                                      return PrivacyPolicy();
                                    }));
                                  }),
                            ),
                            SizedBox(
                              height: 40,
                            ),
                          ],
                        ),
                      )
                    ]))
          ],
        ),
      ),
    );
  }

  goBack(BuildContext context) {
    if (Navigator.canPop(context)) {
      Navigator.pop(context);
    } else {
      // SystemNavigator.pop();
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context) => GetStarted()),
          (Route<dynamic> route) => false);
    }
  }

  void login() {
    setState(() => isLoading = true);
    var api = new APICallRepository();
    api
        .signIn(_textEditingControllerEmail.text,
            _textEditingControllerPassword.text, deviceId)
        .then((loginResponse) {
      setState(() {
        setState(() => isLoading = false);
        print("SignIn Response===>>>" + loginResponse.response.toString());

        UtilsImporter().preferencesUtils.setisLogin();
        UtilsImporter().preferencesUtils.setisTouchEnable(switchControl);
        // UtilsImporter().preferencesUtils.setisUserVerified(user.isverified);
        UtilsImporter().preferencesUtils.setToken(loginResponse.response.token);
        // UtilsImporter().preferencesUtils.setUserData(listUserDate);
        UtilsImporter().commanUtils.setCurrentUser(loginResponse.response.user);
        setState(() {
          _textEditingControllerEmail.text = '';
          _textEditingControllerPassword.text = '';
        });

        setState(() {
          isLoading = false;
        });

        // Navigator.push(context, MaterialPageRoute(builder: (context) {
        //   return Home();
        // }));

        //myGlobals.initializeTimer();
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => Home()),
            (Route<dynamic> route) => false);
      });
    }, onError: (error) {
      setState(() {
        print("ERROR FOUND FINALLY HERE ===> " + error.toString());
        setState(() => isLoading = false);
        myGlobals.scaffoldKey.currentState.showSnackBar(SnackBar(
          content: Text(error.toString(),
              textAlign: TextAlign.center,
              style: style.copyWith(
                  color: Colors.white, fontWeight: FontWeight.bold)),
          duration: Duration(seconds: 5),
        ));
      });
    });
  }

  @override
  void onAuthStateChanged(AuthState state) {}

  void _showSnackBar(String text) {
    myGlobals.scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(text)));
  }

  @override
  void onLoginError(String errorTxt) {
    print(errorTxt);

    myGlobals.scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(errorTxt.toString(),
          textAlign: TextAlign.center,
          style:
              style.copyWith(color: Colors.white, fontWeight: FontWeight.bold)),
      duration: Duration(seconds: 5),
    ));
    setState(() => isLoading = false);
  }

  @override
  void onLoginSuccess(response) {
    _showSnackBar(response.toString());
    setState(() => isLoading = false);
  }
}

enum ConfirmAction { CANCEL, ACCEPT }

Future<ConfirmAction> _asyncConfirmDialog(BuildContext context) async {
  return showDialog<ConfirmAction>(
    context: context,
    barrierDismissible: false, // user must tap button for close dialog!
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text('Reset settings?'),
        content: const Text(
            'This will reset your device to its default factory settings.'),
        actions: <Widget>[
          FlatButton(
            child: const Text('CANCEL'),
            onPressed: () {
              Navigator.of(context).pop(ConfirmAction.CANCEL);
            },
          ),
          FlatButton(
            child: const Text('ACCEPT'),
            onPressed: () {
              Navigator.of(context).pop(ConfirmAction.ACCEPT);
            },
          )
        ],
      );
    },
  );
}

BoxDecoration myBoxDecoration() {
  return BoxDecoration(
    border: Border(
      bottom: BorderSide(
        color: getThemeBlueColor(),
        //                   <--- border color
        width: 1.0,
      ),
    ),
  );
}

class RatingBox extends StatefulWidget {
  @override
  _RatingBoxState createState() => _RatingBoxState();
}

class _RatingBoxState extends State<RatingBox> {
  int _rating = 0;

  void _setRatingAsOne() {
    setState(() {
      _rating = 1;
    });
  }

  void _setRatingAsTwo() {
    setState(() {
      _rating = 2;
    });
  }

  void _setRatingAsThree() {
    setState(() {
      _rating = 3;
    });
  }

  Widget build(BuildContext context) {
    double _size = 20;
    print(_rating);
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      crossAxisAlignment: CrossAxisAlignment.end,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        Container(
          padding: EdgeInsets.all(0),
          child: IconButton(
            icon: (_rating >= 1
                ? Icon(
                    Icons.star,
                    size: _size,
                  )
                : Icon(
                    Icons.star_border,
                    size: _size,
                  )),
            color: Colors.red[500],
            onPressed: _setRatingAsOne,
            iconSize: _size,
          ),
        ),
        Container(
          padding: EdgeInsets.all(0),
          child: IconButton(
            icon: (_rating >= 2
                ? Icon(
                    Icons.star,
                    size: _size,
                  )
                : Icon(
                    Icons.star_border,
                    size: _size,
                  )),
            color: Colors.red[500],
            onPressed: _setRatingAsTwo,
            iconSize: _size,
          ),
        ),
        Container(
          padding: EdgeInsets.all(0),
          child: IconButton(
            icon: (_rating >= 3
                ? Icon(
                    Icons.star,
                    size: _size,
                  )
                : Icon(
                    Icons.star_border,
                    size: _size,
                  )),
            color: Colors.red[500],
            onPressed: _setRatingAsThree,
            iconSize: _size,
          ),
        ),
      ],
    );
  }
}

Future<dynamic> callSignInAPI(String email, String password) async {
  final response = await http.post(
      UtilsImporter().apiUtils.baseURL + UtilsImporter().apiUtils.signin,
      headers: header(),
      body: json.encode({"email": email, "password": password}));

  print("sign in response : " + response.body);
  return json.decode(response.body);
}

header() => {"Content-Type": 'application/json'};

class ThreeSizeDot extends StatefulWidget {
  ThreeSizeDot(
      {Key key,
      this.shape = BoxShape.circle,
      this.duration = const Duration(milliseconds: 1000),
      this.size = 8.0,
      this.color_1,
      this.color_2,
      this.color_3,
      this.padding = const EdgeInsets.all(2)})
      : super(key: key);

  final BoxShape shape;
  final Duration duration;
  final double size;
  final Color color_1;
  final Color color_2;
  final Color color_3;
  final EdgeInsetsGeometry padding;

  @override
  _ThreeSizeDotState createState() => _ThreeSizeDotState();
}

class _ThreeSizeDotState extends State<ThreeSizeDot>
    with SingleTickerProviderStateMixin {
  AnimationController animationController;
  Animation<double> animation_1;
  Animation<double> animation_2;
  Animation<double> animation_3;

  @override
  void initState() {
    super.initState();
    animationController =
        AnimationController(vsync: this, duration: widget.duration);
    animation_1 = DelayTween(begin: 0.0, end: 1.0, delay: 0.0)
        .animate(animationController);
    animation_2 = DelayTween(begin: 0.0, end: 1.0, delay: 0.2)
        .animate(animationController);
    animation_3 = DelayTween(begin: 0.0, end: 1.0, delay: 0.4)
        .animate(animationController);
    animationController.repeat();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          ScaleTransition(
            scale: animation_1,
            child: Padding(
              padding: widget.padding,
              child: Dot(
                shape: widget.shape,
                size: widget.size,
                color: Colors.white,
              ),
            ),
          ),
          ScaleTransition(
            scale: animation_2,
            child: Padding(
              padding: widget.padding,
              child: Dot(
                shape: widget.shape,
                size: widget.size,
                color: Colors.white,
              ),
            ),
          ),
          ScaleTransition(
            scale: animation_3,
            child: Padding(
              padding: widget.padding,
              child: Dot(
                shape: widget.shape,
                size: widget.size,
                color: Colors.white,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class DelayTween extends Tween<double> {
  DelayTween({
    double begin,
    double end,
    this.delay,
  }) : super(begin: begin, end: end);

  final double delay;

  @override
  double lerp(double t) =>
      super.lerp((math.sin((t - delay) * 2 * math.pi) + 1) / 2);

  @override
  double evaluate(Animation<double> animation) => lerp(animation.value);
}

class Dot extends StatelessWidget {
  final BoxShape shape;
  final double size;
  final Color color;

  Dot({
    Key key,
    this.shape,
    this.size,
    this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        width: size,
        height: size,
        decoration: BoxDecoration(color: color, shape: shape),
      ),
    );
  }
}

const _inactivityTimeout = Duration(seconds: 10);
Timer _keepAliveTimer;

void _keepAlive(bool visible) {
  _keepAliveTimer?.cancel();
  if (visible) {
    _keepAliveTimer = null;
  } else {
    _keepAliveTimer = Timer(_inactivityTimeout, () => exit(0));
  }
}
