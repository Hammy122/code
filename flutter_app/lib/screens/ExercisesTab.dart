import 'dart:convert';

import 'package:circular_check_box/circular_check_box.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app/models/PainFeelItem.dart';
import 'package:flutter_app/screens/ChangePassword.dart';
import 'package:flutter_app/screens/ChatsScreen.dart';
import 'package:flutter_app/screens/Home.dart';
import 'package:flutter_app/screens/ReportYourPain/ReportYourPainQuestionSix.dart';
import 'package:flutter_app/screens/ReportYourPain/ReportYourPainQuestionTwo.dart';
import 'package:flutter_app/utils/ColorUtils.dart';
import 'package:flutter_app/utils/CustomPopupMenu.dart';
import 'package:flutter_app/models/ReachBean.dart';
import 'package:flutter_app/screens/login/Signin.dart';
import 'package:flutter_app/utils/app_widgets.dart';
import 'package:flutter_app/utils/utils.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:toast/toast.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:url_launcher/url_launcher.dart';
import '../models/UserBean.dart';
import '../utils/UtilsImporter.dart';
import 'package:http/http.dart' as http;

class ExercisesTab extends StatefulWidget {
  @override
  _ExercisesTabState createState() => _ExercisesTabState();
}

class _ExercisesTabState extends State<ExercisesTab> {
  TextStyle style = TextStyle(fontFamily: getThemeFontName(), fontSize: 18.0);
  List<ExercisesModel> array = new List();
  bool isComingSoon=true;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    array.add(new ExercisesModel("Exercise 1"));
    array.add(new ExercisesModel("Exercise 2"));
    array.add(new ExercisesModel("Exercise 3"));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
            new Padding(
              padding: const EdgeInsetsDirectional.only(top: 25.0, bottom: 10),
              child: getTextWidget(
                  "Exercises", 17, Colors.black, FontWeight.bold),
            ),
            new Container(
              margin: const EdgeInsets.only(top: 1),
              height: 0.5,
              color: getColorFromHex('#CCCCCC'),
            ),
            Expanded(
              child: isComingSoon ?   Center(child :  getTextWidget("COMING SOON", 22, Colors.black,
                  FontWeight.normal)) :
              new ListView.separated(
                physics: BouncingScrollPhysics(),
                itemCount: array.length,
                itemBuilder: _getItemCell,
                separatorBuilder: (context, index) {
                  return Divider(
                    height: 1,
                    color: getInputHintTextColor(),
                  );
                },
                padding: EdgeInsets.only(top: 1, bottom: 15),
              ),
            )
          ])),
    );
  }

  Widget _getItemCell(BuildContext context, int index) {
    return new Container(
        padding: const EdgeInsets.only(left: 0, right: 0, top: 10, bottom: 10),
        color: Colors.white,
        child: new Column(
          children: <Widget>[
            new ListTile(
              dense: false,
              trailing:
                  Icon(Icons.keyboard_arrow_right, color: getThemeGreenColor()),
              title: Align(
                  alignment: Alignment(-1.1, 5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      getTextWidget(array[index].title, 17, Colors.black,
                          FontWeight.normal),
                    ],
                  )),

              //trailing: ,
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return ChatsScreen();
                }));
              },
            )
          ],
        ));
  }
}

class ExercisesModel {
  String title;

  ExercisesModel(this.title);
}
