import 'dart:async';

import 'package:custom_switch/custom_switch.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app/screens/ForgotPassword.dart';
import 'package:flutter_app/screens/Home.dart';
import 'package:flutter_app/screens/ActivateAccount.dart';
import 'package:flutter_app/screens/body/human_anatomy.dart';
import 'package:flutter_app/utils/ColorUtils.dart';
import 'package:flutter_app/utils/UtilsImporter.dart';
import 'package:flutter_app/utils/utils.dart';
import 'package:flutter_progress_button/flutter_progress_button.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';
import 'package:toast/toast.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'dart:math' as math show sin, pi;
import 'dart:io';
import 'package:path_provider/path_provider.dart';

class PrivacyPolicy extends StatefulWidget {
  @override
  PrivacyPolicyState createState() => PrivacyPolicyState();
}

class PrivacyPolicyState extends State<PrivacyPolicy> {
  TextStyle style = TextStyle(fontFamily: 'Orkney', fontSize: 15.0);

  String pathPDF = "assets/Heartsend_Privacy_Policy.pdf";

  bool isLoading = false;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final formKey = new GlobalKey<FormState>();
  final scaffoldKey = new GlobalKey<ScaffoldState>();

  void _submit() {
    setState(() => isLoading = true);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      checkInternet();
    });
//    fromAsset('assets/Heartsend_Privacy_Policy.pdf',
//            'Heartsend_Privacy_Policy.pdf')
//        .then((f) {
//      setState(() {
//        pathPDF = f.path;
//        if (pathPDF != null || pathPDF.isNotEmpty) {
////          Navigator.push(
////            context,
////            MaterialPageRoute(
////              builder: (context) => PDFScreen(path: pathPDF),
////            ),
////          );
//        }
//      });
//    });
  }

  Future<File> fromAsset(String asset, String filename) async {
    // To open from assets, you can copy them to the app storage folder, and the access them "locally"
    Completer<File> completer = Completer();

    try {
      var dir = await getApplicationDocumentsDirectory();
      File file = File("${dir.path}/$filename");
      var data = await rootBundle.load(asset);
      var bytes = data.buffer.asUint8List();
      await file.writeAsBytes(bytes, flush: true);
      completer.complete(file);
    } catch (e) {
      throw Exception('Error parsing asset file!');
    }

    return completer.future;
  }

  checkInternet() {
    setState(() {
      Future<bool> result = UtilsImporter().commanUtils.checkConnection();
      result.then((data) {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      key: _scaffoldKey,
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            //Header Container
            SizedBox(
              height: 15,
            ),
            Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SizedBox(
                  width: 5,
                ),
                Align(
                    alignment: Alignment.center,
                    child: InkWell(
                        child: Image.asset(
                          "assets/ic_back.png",
                          height: 25,
                          width: 40,
                        ),
                        onTap: () {
                          goBack(context);
                        })),
              ],
            ),
            new Container(
              margin: const EdgeInsets.only(top: 20),
              height: 1,
              color: getColorFromHex('#CCCCCC'),
            ),
            SizedBox(
              height: 20,
            ),
            //Body Container
            Expanded(
              child: SingleChildScrollView(
                padding: const EdgeInsets.symmetric(horizontal: 5.0),
                child: Column(
                  children: <Widget>[
//                    Container(
//                      alignment: Alignment.topLeft,
//                      child: Text("Heartsend Privacy Policy",
//                          style: TextStyle(
//                              fontWeight: FontWeight.w600,
//                              fontSize: 30,
//                              fontFamily: getThemeFontName())),
//                    ),
                    SizedBox(
                      height: 6,
                    ),
//                    if (pathPDF != null || pathPDF.isNotEmpty)
                      Container(child: SfPdfViewer.asset(pathPDF)
//                    Text(UtilsImporter().stringUtils.privacy ,
//                        style: TextStyle(
//                          color: getThemeBlackColor(),
//                            fontWeight: FontWeight.normal,
//                            fontSize: 15,
//                            fontFamily: getThemeFontName()))
                          )
                    //TextField nearly at bottom
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

//class PDFScreen extends StatefulWidget {
//  final String path;
//
//  PDFScreen({Key key, this.path}) : super(key: key);
//
//  _PDFScreenState createState() => _PDFScreenState();
//}

//class _PDFScreenState extends State<PDFScreen> with WidgetsBindingObserver {
//  final Completer<PDFViewController> _controller =
//  Completer<PDFViewController>();
//  int pages = 0;
//  int currentPage = 0;
//  bool isReady = false;
//  String errorMessage = '';
//
//  @override
//  Widget build(BuildContext context) {
//    return Scaffold(
//      appBar: AppBar(
//        title: Text("Document"),
//        actions: <Widget>[
//          IconButton(
//            icon: Icon(Icons.share),
//            onPressed: () {},
//          ),
//        ],
//      ),
//      body: Stack(
//        children: <Widget>[
//          PDFView(
//            filePath: widget.path,
//            enableSwipe: true,
//            swipeHorizontal: true,
//            autoSpacing: false,
//            pageFling: true,
//            pageSnap: true,
//            defaultPage: currentPage,
//            fitPolicy: FitPolicy.BOTH,
//            preventLinkNavigation:
//            false, // if set to true the link is handled in flutter
//            onRender: (_pages) {
//              setState(() {
//                pages = _pages;
//                isReady = true;
//              });
//            },
//            onError: (error) {
//              setState(() {
//                errorMessage = error.toString();
//              });
//              print(error.toString());
//            },
//            onPageError: (page, error) {
//              setState(() {
//                errorMessage = '$page: ${error.toString()}';
//              });
//              print('$page: ${error.toString()}');
//            },
//            onViewCreated: (PDFViewController pdfViewController) {
//              _controller.complete(pdfViewController);
//            },
//            onLinkHandler: (String uri) {
//              print('goto uri: $uri');
//            },
//            onPageChanged: (int page, int total) {
//              print('page change: $page/$total');
//              setState(() {
//                currentPage = page;
//              });
//            },
//          ),
//          errorMessage.isEmpty
//              ? !isReady
//              ? Center(
//            child: CircularProgressIndicator(),
//          )
//              : Container()
//              : Center(
//            child: Text(errorMessage),
//          )
//        ],
//      ),
//      floatingActionButton: FutureBuilder<PDFViewController>(
//        future: _controller.future,
//        builder: (context, AsyncSnapshot<PDFViewController> snapshot) {
//          if (snapshot.hasData) {
//            return FloatingActionButton.extended(
//              label: Text("Go to ${pages ~/ 2}"),
//              onPressed: () async {
//                await snapshot.data.setPage(pages ~/ 2);
//              },
//            );
//          }
//
//          return Container();
//        },
//      ),
//    );
//  }
//}

enum ConfirmAction { CANCEL, ACCEPT }

Future<ConfirmAction> _asyncConfirmDialog(BuildContext context) async {
  return showDialog<ConfirmAction>(
    context: context,
    barrierDismissible: false, // user must tap button for close dialog!
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text('Reset settings?'),
        content: const Text(
            'This will reset your device to its default factory settings.'),
        actions: <Widget>[
          FlatButton(
            child: const Text('CANCEL'),
            onPressed: () {
              Navigator.of(context).pop(ConfirmAction.CANCEL);
            },
          ),
          FlatButton(
            child: const Text('ACCEPT'),
            onPressed: () {
              Navigator.of(context).pop(ConfirmAction.ACCEPT);
            },
          )
        ],
      );
    },
  );
}

BoxDecoration myBoxDecoration() {
  return BoxDecoration(
    border: Border(
      bottom: BorderSide(
        color: getThemeBlackColor(),
        //                   <--- border color
        width: 1.0,
      ),
    ),
  );
}

class RatingBox extends StatefulWidget {
  @override
  _RatingBoxState createState() => _RatingBoxState();
}

class _RatingBoxState extends State<RatingBox> {
  int _rating = 0;

  void _setRatingAsOne() {
    setState(() {
      _rating = 1;
    });
  }

  void _setRatingAsTwo() {
    setState(() {
      _rating = 2;
    });
  }

  void _setRatingAsThree() {
    setState(() {
      _rating = 3;
    });
  }

  Widget build(BuildContext context) {
    double _size = 20;
    print(_rating);
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      crossAxisAlignment: CrossAxisAlignment.end,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        Container(
          padding: EdgeInsets.all(0),
          child: IconButton(
            icon: (_rating >= 1
                ? Icon(
                    Icons.star,
                    size: _size,
                  )
                : Icon(
                    Icons.star_border,
                    size: _size,
                  )),
            color: Colors.red[500],
            onPressed: _setRatingAsOne,
            iconSize: _size,
          ),
        ),
        Container(
          padding: EdgeInsets.all(0),
          child: IconButton(
            icon: (_rating >= 2
                ? Icon(
                    Icons.star,
                    size: _size,
                  )
                : Icon(
                    Icons.star_border,
                    size: _size,
                  )),
            color: Colors.red[500],
            onPressed: _setRatingAsTwo,
            iconSize: _size,
          ),
        ),
        Container(
          padding: EdgeInsets.all(0),
          child: IconButton(
            icon: (_rating >= 3
                ? Icon(
                    Icons.star,
                    size: _size,
                  )
                : Icon(
                    Icons.star_border,
                    size: _size,
                  )),
            color: Colors.red[500],
            onPressed: _setRatingAsThree,
            iconSize: _size,
          ),
        ),
      ],
    );
  }
}

Future<dynamic> callSignInAPI(String email, String password) async {
  final response = await http.post(
      UtilsImporter().apiUtils.baseURL + UtilsImporter().apiUtils.signin,
      headers: header(),
      body: json.encode({"email": email, "password": password}));

  print("sign in response : " + response.body);
  return json.decode(response.body);
}

header() => {"Content-Type": 'application/json'};

class ThreeSizeDot extends StatefulWidget {
  ThreeSizeDot(
      {Key key,
      this.shape = BoxShape.circle,
      this.duration = const Duration(milliseconds: 1000),
      this.size = 8.0,
      this.color_1,
      this.color_2,
      this.color_3,
      this.padding = const EdgeInsets.all(2)})
      : super(key: key);

  final BoxShape shape;
  final Duration duration;
  final double size;
  final Color color_1;
  final Color color_2;
  final Color color_3;
  final EdgeInsetsGeometry padding;

  @override
  _ThreeSizeDotState createState() => _ThreeSizeDotState();
}

class _ThreeSizeDotState extends State<ThreeSizeDot>
    with SingleTickerProviderStateMixin {
  AnimationController animationController;
  Animation<double> animation_1;
  Animation<double> animation_2;
  Animation<double> animation_3;

  @override
  void initState() {
    super.initState();
    animationController =
        AnimationController(vsync: this, duration: widget.duration);
    animation_1 = DelayTween(begin: 0.0, end: 1.0, delay: 0.0)
        .animate(animationController);
    animation_2 = DelayTween(begin: 0.0, end: 1.0, delay: 0.2)
        .animate(animationController);
    animation_3 = DelayTween(begin: 0.0, end: 1.0, delay: 0.4)
        .animate(animationController);
    animationController.repeat();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          ScaleTransition(
            scale: animation_1,
            child: Padding(
              padding: widget.padding,
              child: Dot(
                shape: widget.shape,
                size: widget.size,
                color: Colors.white,
              ),
            ),
          ),
          ScaleTransition(
            scale: animation_2,
            child: Padding(
              padding: widget.padding,
              child: Dot(
                shape: widget.shape,
                size: widget.size,
                color: Colors.white,
              ),
            ),
          ),
          ScaleTransition(
            scale: animation_3,
            child: Padding(
              padding: widget.padding,
              child: Dot(
                shape: widget.shape,
                size: widget.size,
                color: Colors.white,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class DelayTween extends Tween<double> {
  DelayTween({
    double begin,
    double end,
    this.delay,
  }) : super(begin: begin, end: end);

  final double delay;

  @override
  double lerp(double t) =>
      super.lerp((math.sin((t - delay) * 2 * math.pi) + 1) / 2);

  @override
  double evaluate(Animation<double> animation) => lerp(animation.value);
}

class Dot extends StatelessWidget {
  final BoxShape shape;
  final double size;
  final Color color;

  Dot({
    Key key,
    this.shape,
    this.size,
    this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        width: size,
        height: size,
        decoration: BoxDecoration(color: color, shape: shape),
      ),
    );
  }
}
