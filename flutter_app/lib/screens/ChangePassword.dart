import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/screens/login/Signin.dart';
import 'package:flutter_app/models/UserBean.dart';
import 'package:flutter_app/utils/ColorUtils.dart';
import 'package:flutter_app/utils/utils.dart';
import 'package:toast/toast.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import '../utils/UtilsImporter.dart';
import 'package:progress_dialog/progress_dialog.dart';

//class ChangePassword extends StatelessWidget {
//  static var routeName = "ChangePassword";
//
//  @override
//  Widget build(BuildContext context) {
//    return MaterialApp(
//        color: Colors.white,
//        debugShowCheckedModeBanner: false,
//        title: 'ThyReach',
//        theme: ThemeData(
//          primarySwatch: Colors.grey,
//          backgroundColor: Color.fromRGBO(229, 229, 229, 1),
//        ),
//        home: ChangePasswordPage(title: 'Signup'));
//  }
//}

class ChangePassword extends StatefulWidget {
  ChangePassword({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _ChangePasswordPageState createState() => _ChangePasswordPageState();
}

void showThankYouDialog(BuildContext context) {
  Dialog dialogThankYou;
  TextStyle style = TextStyle(fontFamily: 'Orkney', fontSize: 14.0);
  final continueButon = Material(
    elevation: 5.0,
    borderRadius: BorderRadius.circular(30.0),
    color: Color.fromRGBO(78, 78, 85, 1),
    child: MaterialButton(
      minWidth: MediaQuery.of(context).size.width,
      padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
      onPressed: () {
        Navigator.pop(context, true);

       // Navigator.of(context)
       //     .pushNamedAndRemoveUntil('/Signin', (Route<dynamic> route) => false);

        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => Signin()),
            (Route<dynamic> route) => false);
      },
      child: Text("Continue",
          textAlign: TextAlign.center,
          style:
              style.copyWith(color: Colors.white, fontWeight: FontWeight.bold)),
    ),
  );

  dialogThankYou = Dialog(
    child: Container(
      padding: EdgeInsets.only(bottom: 20),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(12),
                alignment: Alignment.center,
                decoration: BoxDecoration(color: Colors.white),
                child: Text(
                  "Thanks for signing up!!!",
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 18,
                      fontWeight: FontWeight.w600,
                      fontFamily: 'Orkney'),
                ),
              ),
              SizedBox(
                width: 30,
              ),
              Container(
                child: GestureDetector(
                  onTap: () {
                    Navigator.pop(context, true);

//        Navigator.of(context)
//            .pushNamedAndRemoveUntil('/Signin', (Route<dynamic> route) => false);

                    Navigator.of(context).pushAndRemoveUntil(
                        MaterialPageRoute(builder: (context) => Signin()),
                        (Route<dynamic> route) => false);
                  },
                  child: Align(
                    alignment: Alignment.topRight,
                    child: CircleAvatar(
                      radius: 25.0,
                      backgroundColor: Colors.white,
                      child: Icon(Icons.close, color: Colors.black),
                    ),
                  ),
                ),
              ),
            ],
          ),
          Container(
            padding:
                EdgeInsets.only(top: 18.0, left: 20.0, bottom: 20, right: 20),
            child: Text(
              "Email link Sent",
              style: TextStyle(
                  color: Color.fromRGBO(143, 233, 227, 1),
                  fontSize: 32,
                  fontWeight: FontWeight.normal,
                  fontFamily: 'Orkney'),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(top: 10.0, left: 15, right: 15),
            child: Text(
              "We have sent you an email. Pls click the link in the email to verify your email.",
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 15,
                  fontWeight: FontWeight.normal,
                  fontFamily: 'Orkney'),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(top: 20.0, left: 15, right: 15),
            child: Text(
              "To set up your profile, pls  click below to",
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 15,
                  fontWeight: FontWeight.w600,
                  fontFamily: 'Orkney'),
            ),
          ),
          Container(
              margin: const EdgeInsets.only(left: 15, right: 15, top: 6),
              child: continueButon),
        ],
      ),
    ),
  );
  showDialog(
      context: context,
      useRootNavigator: false,
      builder: (BuildContext context) => dialogThankYou);
}

class _ChangePasswordPageState extends State<ChangePassword> {
  ProgressDialog pr;
  var _token;
  TextStyle style = TextStyle(fontFamily: 'Orkney', fontSize: 14.0);

  TextEditingController _textEditingControllerCurrentPassword =
      new TextEditingController();
  TextEditingController _textEditingControllerNewPassword =
      new TextEditingController();
  TextEditingController _textEditingControllerNewConfirmPassword =
      new TextEditingController();

  TapGestureRecognizer recognizerCreateAccount;
  bool isLoading = false;
  GlobalKey _scaffold = GlobalKey();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      checkInternet();
    });
    recognizerCreateAccount = TapGestureRecognizer()
      ..onTap = () {
//        Navigator.push(context, MaterialPageRoute(builder: (context) {
//          return CreateAccount();
//        }));
      };
  }

  checkInternet() {
    setState(() {
      Future<bool> result = UtilsImporter().commanUtils.checkConnection();
      result.then((data) {
        isLoading = data;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    Future<String> token = UtilsImporter().preferencesUtils.getToken();
    token.then((data) {
      _token = data;
    });
    pr = new ProgressDialog(context);
    pr.style(
        message: 'Please Wait...',
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: Center(
          child: Container(
            height: 30,
            width: 30,
            margin: EdgeInsets.all(5),
            child: CircularProgressIndicator(
              strokeWidth: 2.0,
              valueColor: AlwaysStoppedAnimation(Colors.black),
            ),
          ),
        ),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 15.0, fontWeight: FontWeight.w600));

//    final currentPasswordField = TextField(
//      controller: _textEditingControllerCurrentPassword,
//      obscureText: true,
//      style: style,
//      decoration: InputDecoration(
//          filled: true,
//          fillColor: Colors.white,
//          labelText: 'Current Password',
//          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
//          hintText: "Current password",
//          focusedBorder: OutlineInputBorder(
//            borderRadius: BorderRadius.circular(20.0),
//            borderSide: BorderSide(
//              color: Color.fromRGBO(78, 78, 85, 1),
//            ),
//          ),
//          border:
//              OutlineInputBorder(borderRadius: BorderRadius.circular(20.0))),
//    );
    final currentPasswordField = Container(
      child: TextField(
        controller: _textEditingControllerCurrentPassword,
        decoration: InputDecoration(
          labelText: 'Current Password',
          labelStyle: TextStyle(color: getColorFromHex("#717F8B")),
          border: InputBorder.none,
          contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),

        ),
        obscureText: true,
        style: style,
      ),
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(
          color: Colors.white,
          width: 1,
        ),
        borderRadius: BorderRadius.circular(20),
      ),
    );

//    final newPasswordField = TextField(
//      controller: _textEditingControllerNewPassword,
//      obscureText: true,
//      style: style,
//      decoration: InputDecoration(
//          filled: true,
//          fillColor: Colors.white,
//          labelText: 'New Password',
//          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
//          hintText: "New password",
//          focusedBorder: OutlineInputBorder(
//            borderRadius: BorderRadius.circular(20.0),
//            borderSide: BorderSide(
//              color: Color.fromRGBO(78, 78, 85, 1),
//            ),
//          ),
//          border:
//              OutlineInputBorder(borderRadius: BorderRadius.circular(20.0))),
//    );

    final newPasswordField = Container(
      child: TextField(
        controller: _textEditingControllerNewPassword,
        decoration: InputDecoration(
          labelText: 'New Password',
          labelStyle: TextStyle(color: getColorFromHex("#717F8B")),
          border: InputBorder.none,
          contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),

        ),
        obscureText: true,
        style: style,
      ),
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(
          color: Colors.white,
          width: 1,
        ),
        borderRadius: BorderRadius.circular(20),
      ),
    );

//    final confirmPasswordField = TextField(
//      controller: _textEditingControllerNewConfirmPassword,
//      obscureText: true,
//      style: style,
//      decoration: InputDecoration(
//          filled: true,
//          fillColor: Colors.white,
//          labelText: 'Confirm new Password',
//          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
//          hintText: "Confirm new password",
//          focusedBorder: OutlineInputBorder(
//            borderRadius: BorderRadius.circular(20.0),
//            borderSide: BorderSide(
//              color: Color.fromRGBO(78, 78, 85, 1),
//            ),
//          ),
//          border:
//              OutlineInputBorder(borderRadius: BorderRadius.circular(20.0))),
//    );

    final confirmPasswordField = Container(
      child: TextField(
        controller: _textEditingControllerNewConfirmPassword,
        decoration: InputDecoration(
          labelText: 'Confirm new Password',
          labelStyle: TextStyle(color: getColorFromHex("#717F8B")),
          border: InputBorder.none,
          contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),

        ),
        obscureText: true,
        style: style,
      ),
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(
          color: Colors.white,
          width: 1,
        ),
        borderRadius: BorderRadius.circular(20),
      ),
    );

    final submitButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(20.0),
      color: Color.fromRGBO(78, 78, 85, 1),
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 20.0),
        onPressed: () async {
          /////////////////
//          showThankYouDialog(context);
//
//          if (!UtilsImporter()
//              .commanUtils
//              .validateTextField(_textEditingControllerEmail.text)) {
//            UtilsImporter().commanUtils.showAlertDialogWithOkButton(
//                "Please provide your Official Email.", context);
//          } else

          if (!UtilsImporter()
              .commanUtils
              .validateTextField(_textEditingControllerCurrentPassword.text)) {
            UtilsImporter().commanUtils.showAlertDialogWithOkButton(
                "Please provide your current password.", context);
          } else if (!UtilsImporter()
              .commanUtils
              .validateTextField(_textEditingControllerNewPassword.text)) {
            UtilsImporter().commanUtils.showAlertDialogWithOkButton(
                "Please provide your new password.", context);
          } else if (!UtilsImporter().commanUtils.validatePassword(
              _textEditingControllerNewConfirmPassword.text)) {
            UtilsImporter().commanUtils.showAlertDialogWithOkButton(
                "Please provide your confirm new password.", context);
          } else if (_textEditingControllerNewPassword.text !=
              _textEditingControllerNewConfirmPassword.text) {
            UtilsImporter().commanUtils.showAlertDialogWithOkButton(
                "New password and confirm password should be same.", context);
          } else {
            List<String> listUserDate = [];
            print("Start");
            //////////////////
            pr.show();
            callChangePasswordAPI(
                    _textEditingControllerCurrentPassword.text,
                    _textEditingControllerNewPassword.text,
                    _textEditingControllerNewConfirmPassword.text,_token)
                .then((result) {
//              pr.dismiss();
              print(result);
              try {
                var success = result['success'];
                if (success == true) {
                  //showThankYouDialog(context);

                  Toast.show("Password has been changed.", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
                  Navigator.pop(context, true);
                } else {
                  var message = result['message'];
                  UtilsImporter()
                      .commanUtils
                      .showAlertDialogWithOkButton(message, context);
                }
              } on Exception catch (_) {
//                pr.dismiss();
                Toast.show("Something went wrong, please try again!!", context);
              }
            });
          }
        },
        child: Text("Update",
            textAlign: TextAlign.center,
            style: style.copyWith(
                color: getColorFromHex("#DBDDE2"),
                fontWeight: FontWeight.bold,
                fontFamily: 'Orkney')),
      ),
    );

    return Scaffold(
        key: _scaffold,
        backgroundColor: getColorFromHex("#E5E5E5"),
        body: SingleChildScrollView(
          child: Container(
            margin: const EdgeInsets.only(top: 15.0),
            child: Padding(
              padding: const EdgeInsets.only(
                  top: 40, bottom: 20, left: 10, right: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
//                  Padding(
//                    padding: const EdgeInsets.all(10),
//                    child: Center(
//                        child: new Text("ThyReach",
//                            style: TextStyle(
//                                fontWeight: FontWeight.bold,
//                                fontSize: 37,
//                                fontFamily: 'Orkney'))),
//                  ),
//                  Padding(
//                      padding: const EdgeInsets.only(top: 5),
//                      child: Center(
//                        child: new Text(
//                          "Your Reach is your Rich, expand your reach",
//                          style: TextStyle(
//                              fontWeight: FontWeight.bold,
//                              fontSize: 15,
//                              fontFamily: 'Orkney'),
//                        ),
//                      )),
                  new Container(
                    margin: const EdgeInsets.only(
                        left: 1.0, top: 35.0, right: 1.0, bottom: 10.0),
                    padding: const EdgeInsets.all(10.0),
                    decoration: new BoxDecoration(
                      image: new DecorationImage(
                        image: new AssetImage("assets/rectangle.png"),
                        fit: BoxFit.fill,
                      ),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(top: 15.0, left: 20),
                          child: new Text("Change Password",
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  color: getColorFromHex("#717F8B"),
                                  fontWeight: FontWeight.normal,
                                  fontSize: 30,
                                  fontFamily: 'Orkney')),
                        ),
                        SizedBox(
                          height: 35.0,
                        ),
            Padding(
              padding: const EdgeInsets.only(left: 25, right: 25),
              child:currentPasswordField),
                        SizedBox(
                          height: 25.0,
                        ),
            Padding(
              padding: const EdgeInsets.only(left: 25, right: 25),
              child:newPasswordField),
                        SizedBox(
                          height: 25.0,
                        ),
            Padding(
              padding: const EdgeInsets.only(left: 25, right: 25),
              child:confirmPasswordField),
                        SizedBox(height: 5.0),
                        Padding(
                            padding: const EdgeInsets.only(left: 25, top: 10,right: 25),
                            child: Text(
                                "Use 8 or more characters with a mix of letters, numbers & symbols",
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                    color: getColorFromHex("#717F8B"),
                                    fontWeight: FontWeight.normal,
                                    fontSize: 12,
                                    fontFamily: 'Orkney'))),
                        SizedBox(
                          height: 25.0,
                        ),
            Padding(
              padding: const EdgeInsets.only(left: 25, right: 25),
              child:submitButton),
                        SizedBox(
                          height: 55.0,
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ));
  }
}

BoxDecoration myBoxDecoration() {
  return BoxDecoration(
    border: Border(
      bottom: BorderSide(
        color: Color.fromRGBO(113, 127, 139, 1),
        //                   <--- border color
        width: 1.0,
      ),
    ),
  );
}

Future<dynamic> callChangePasswordAPI(
    String old_password, String new_password, String re_password,String token) async {
  final response = await http.post(
      UtilsImporter().apiUtils.baseURL + UtilsImporter().apiUtils.change_password,
      headers: header("Token " + token),
      body: json.encode({
        "old_password": old_password,
        "new_password": new_password,
        "re_password": re_password,
      }));
  return json.decode(response.body);
}

header(String token) => {"Authorization": token ?? "","Content-Type": 'application/json'};
