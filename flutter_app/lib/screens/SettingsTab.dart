import 'dart:convert';
import 'dart:io';

import 'package:circular_check_box/circular_check_box.dart';
import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app/models/PainFeelItem.dart';
import 'package:flutter_app/persistance/api_repository.dart';
import 'package:flutter_app/screens/ChangePassword.dart';
import 'package:flutter_app/screens/ChatsScreen.dart';
import 'package:flutter_app/screens/Home.dart';
import 'package:flutter_app/screens/ReportYourPain/ReportYourPainQuestionSix.dart';
import 'package:flutter_app/screens/ReportYourPain/ReportYourPainQuestionTwo.dart';
import 'package:flutter_app/utils/APIUtils.dart';
import 'package:flutter_app/utils/ColorUtils.dart';
import 'package:flutter_app/utils/CustomAlertDialog.dart';
import 'package:flutter_app/utils/CustomPopupMenu.dart';
import 'package:flutter_app/models/ReachBean.dart';
import 'package:flutter_app/screens/login/Signin.dart';
import 'package:flutter_app/utils/app_widgets.dart';
import 'package:flutter_app/utils/utils.dart';
import 'package:flutter_email_sender/flutter_email_sender.dart';
import 'package:package_info/package_info.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:toast/toast.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:url_launcher/url_launcher.dart';
import '../models/UserBean.dart';
import '../utils/UtilsImporter.dart';
import 'package:http/http.dart' as http;

import 'GetStarted.dart';
import 'PrivacyPolicy.dart';

class SettingsTab extends StatefulWidget {
  @override
  _SettingsTabState createState() => _SettingsTabState();
}

class _SettingsTabState extends State<SettingsTab> {
  TextStyle style = TextStyle(fontFamily: getThemeFontName(), fontSize: 18.0);
  List<SettingsModel> array = new List();
  bool isLoading = false;
  List<String> attachments = [];
  bool isHTML = false;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  String token = "";
  String virsion = "";
  static final DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
  Map<String, dynamic> _deviceData = <String, dynamic>{};
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    array.add(new SettingsModel("Contact Us"));
    array.add(new SettingsModel("Privacy policy"));
    array.add(new SettingsModel("Send Feedback"));
//    array.add(new SettingsModel("Report a problem"));
    Future<String> token = UtilsImporter().preferencesUtils.getToken();
    token.then((data) {
      print("TOKEN ==> " + data);
      this.token = data;
    });
    PackageInfo.fromPlatform().then((PackageInfo packageInfo) {
      // String appName = packageInfo.appName;
      // String packageName = packageInfo.packageName;
      // String buildNumber = packageInfo.buildNumber;
      setState(() {
        virsion = packageInfo.version;
      });
    });

    initPlatformState();

  }


  Future<void> initPlatformState() async {
    Map<String, dynamic> deviceData;

    try {
      if (Platform.isAndroid) {
        deviceData = _readAndroidBuildData(await deviceInfoPlugin.androidInfo);
      } else if (Platform.isIOS) {
        deviceData = _readIosDeviceInfo(await deviceInfoPlugin.iosInfo);
      }
    } on PlatformException {
      deviceData = <String, dynamic>{
        'Error:': 'Failed to get platform version.'
      };
    }

    if (!mounted) return;

    setState(() {
      _deviceData = deviceData;
    });
  }

  Map<String, dynamic> _readAndroidBuildData(AndroidDeviceInfo build) {
    return <String, dynamic>{
      'version.securityPatch': build.version.securityPatch,
      'version.sdkInt': build.version.sdkInt,
      'version.release': build.version.release,
      'version.previewSdkInt': build.version.previewSdkInt,
      'version.incremental': build.version.incremental,
      'version.codename': build.version.codename,
      'version.baseOS': build.version.baseOS,
      'board': build.board,
      'bootloader': build.bootloader,
      'brand': build.brand,
      'device': build.device,
      'display': build.display,
      'fingerprint': build.fingerprint,
      'hardware': build.hardware,
      'host': build.host,
      'id': build.id,
      'manufacturer': build.manufacturer,
      'model': build.model,
      'product': build.product,
      'supported32BitAbis': build.supported32BitAbis,
      'supported64BitAbis': build.supported64BitAbis,
      'supportedAbis': build.supportedAbis,
      'tags': build.tags,
      'type': build.type,
      'isPhysicalDevice': build.isPhysicalDevice,
      'androidId': build.androidId,
      'systemFeatures': build.systemFeatures,
    };
  }

  Map<String, dynamic> _readIosDeviceInfo(IosDeviceInfo data) {
    return <String, dynamic>{
      'name': data.name,
      'systemName': data.systemName,
      'systemVersion': data.systemVersion,
      'model': data.model,
      'localizedModel': data.localizedModel,
      'identifierForVendor': data.identifierForVendor,
      'isPhysicalDevice': data.isPhysicalDevice,
      'utsname.sysname:': data.utsname.sysname,
      'utsname.nodename:': data.utsname.nodename,
      'utsname.release:': data.utsname.release,
      'utsname.version:': data.utsname.version,
      'utsname.machine:': data.utsname.machine,
    };
  }

  Future<void> sendEmail(int type) async {
    final Email email = Email(
      body: type == 0 ? APIUtils.contactEmailBody : APIUtils.feedbackBody,
      subject:
          type == 0 ? APIUtils.contactEmailSubject : APIUtils.feedbackSubject,
      recipients: [type == 0 ? APIUtils.contactEmail : APIUtils.feedbackEmail],
      attachmentPaths: attachments,
      isHTML: isHTML,
    );

    String platformResponse;

    try {
      await FlutterEmailSender.send(email);
      platformResponse = 'success';
    } catch (error) {
      platformResponse = error.toString();
    }

    if (!mounted) return;

    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(platformResponse),
    ));
  }

  // final Uri params = Uri(
  //   scheme: 'mailto',
  //   path: 'email@example.com',
  //   query: 'subject=App Feedback&body=App Version 3.23', //add subject and body here
  // );
  //

  _launchURL(String toMailId, String subject, String body,bool isDeviceInfoNeedToSend) async {

    print("_deviceData==>"+_deviceData.toString());
    if(isDeviceInfoNeedToSend){
      if (Platform.isAndroid) {
        body=body+ " (" + "app version : "+ virsion+  ", manufacturer : " +_deviceData['manufacturer'].toString()+", model : "+ _deviceData['model'].toString() + ") Here's the issue :" ;
      }else{
        body=body+ " (" + "app version : "+ virsion+  ", systemName : " +_deviceData['systemName'].toString()+", systemVersion : "+ _deviceData['systemVersion'].toString() +", model : "+ _deviceData['model'].toString() + ") Here's the issue :" ;
      }
    }

    // subject=subject.replaceAll(" ", "%20");
    // body=body.replaceAll(" ", "%20");
   var url = 'mailto:$toMailId?subject=$subject&body=$body';

    if (await canLaunch(Uri.encodeFull(url))) {
      await launch(Uri.encodeFull(url));
    } else {
      throw 'Could not launch $url';
    }
  }

  _launchCaller() async {
    const url = "tel:6144903384";
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    final logOutButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: getThemeGreenColor(),
      child: isLoading
          ? Center(
              child: SizedBox(
                child: Padding(
                    padding: EdgeInsets.fromLTRB(15.0, 11.0, 15.0, 11.0),
                    child: CircularProgressIndicator(
                        valueColor:
                            AlwaysStoppedAnimation<Color>(Colors.white))),
                height: 50.0,
                width: 60.0,
              ),
            )
          : MaterialButton(
              padding: EdgeInsets.fromLTRB(30.0, 18.0, 30.0, 18.0),
              onPressed: () {
                print("Start");

                var dialog = CustomAlertDialog(
                    title: "Logout",
                    message: "Are you sure, do you want to logout?",
                    onPostivePressed: () {
                      Navigator.of(context, rootNavigator: true).pop('dialog');
                      // preference clear first and call signout api..
                      logout();
                    },
                    onNegativePressed: () {},
                    positiveBtnText: 'Yes',
                    negativeBtnText: 'No');
                showDialog(
                    context: context,
                    builder: (BuildContext context) => dialog);
              },
              child: Text("Log out",
                  textAlign: TextAlign.center,
                  style: style.copyWith(
                      fontSize: 15,
                      color: Colors.white,
                      fontWeight: FontWeight.bold)),
            ),
    );
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      body: SafeArea(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
            new Padding(
              padding: const EdgeInsetsDirectional.only(top: 25.0, bottom: 10),
              child:
                  getTextWidget("Settings", 17, Colors.black, FontWeight.bold),
            ),
            new Container(
              margin: const EdgeInsets.only(top: 1),
              height: 0.5,
              color: getColorFromHex('#CCCCCC'),
            ),
            _getItemCell(0),
            Divider(color: getColorFromHex('#c2c4ca')),
            _getItemCell(1),
            Divider(color: getColorFromHex('#c2c4ca')),
            _getItemCell(2),
            Divider(color: getColorFromHex('#c2c4ca')),
//            _getItemCell(3),
//            Divider(color: getColorFromHex('#c2c4ca')),
            Padding( padding: const EdgeInsets.only(left: 55, right: 55, top: 55),
                child: logOutButton),
//            Expanded(
//              child: new ListView.separated(
//                physics: BouncingScrollPhysics(),
//                itemCount: array.length,
//                itemBuilder: _getItemCell,
//                separatorBuilder: (context, index) {
//                  return Divider(
//                    height: 1,
//                    color: getInputHintTextColor(),
//                  );
//                },
//                padding: EdgeInsets.only(top: 1, bottom: 15),
//              ),
//              flex: 1,
//            ),

              SizedBox(height: 20,),

              Container(

                child : Text(  virsion == "" ? "" : "version : $virsion"
                    ,  textAlign: TextAlign.center
                    , style: style.copyWith( fontSize: 17,  color: Colors.black, fontWeight:  FontWeight.normal)
                )
              ),

          ])
      ),
    );
  }

  void logout() {
    setState(() => isLoading = true);
    var api = new APICallRepository();
    api.signout(token).then((response) {
      setState(() {
        setState(() => isLoading = false);
        print("Response===>>>" + response.toString());
        UtilsImporter()
            .preferencesUtils
            .clearSharedPreferences();
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => GetStarted()),
            (Route<dynamic> route) => false);
      });
    }, onError: (error) {
      setState(() {
        print("ERROR FOUND FINALLY HERE ===> " + error.toString());
        setState(() => isLoading = false);
        UtilsImporter()
            .preferencesUtils
            .clearSharedPreferences();
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => GetStarted()),
                (Route<dynamic> route) => false);
        // _scaffoldKey.currentState.showSnackBar(SnackBar(
        //   content: Text(error.toString(),
        //       textAlign: TextAlign.center,
        //       style: style.copyWith(
        //           color: Colors.white, fontWeight: FontWeight.bold)),
        //   duration: Duration(seconds: 5),
        // ));
      });
    });
  }

  Widget _getItemCell(int index) {
    return new Container(
        padding: const EdgeInsets.only(left: 0, right: 0, top: 5, bottom: 5),
        color: Colors.white,
        child: new Column(
          children: <Widget>[
            new ListTile(
              dense: false,
              trailing:
                  Icon(Icons.keyboard_arrow_right, color: getThemeGreenColor()),
              title: Align(
                  alignment: Alignment(-1.1, 5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      getTextWidget(array[index].title, 17, Colors.black,
                          FontWeight.normal),
                    ],
                  )),

              //trailing: ,
              onTap: () {
//                Navigator.push(context, MaterialPageRoute(builder: (context) {
//                  return ChatsScreen();
//                }));
                if (index == 0)
                  _settingModalBottomSheet(context);
                else if (index == 2) {
                  // sendEmail(1);
                  _launchURL(APIUtils.feedbackEmail, APIUtils.feedbackSubject,
                      APIUtils.feedbackBody,false);
                } else if (index == 1) {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return PrivacyPolicy();
                  }));
                }
              },
            )
          ],
        ));
  }

  void _settingModalBottomSheet(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(
            child: new Wrap(
              children: <Widget>[
                new ListTile(
                    leading: new Icon(Icons.email),
                    title: getTextWidgetWithAlign(
                        'Email Us',
                        18,
                        getThemeBlackColor(),
                        FontWeight.normal,
                        TextAlign.start),
                    onTap: () => {
                          Navigator.pop(context),
//                      sendEmail(0)
                          _launchURL(
                              APIUtils.contactEmail,
                              APIUtils.contactEmailSubject,
                              APIUtils.contactEmailBody, true)
                        }),
                new ListTile(
                  leading: new Icon(Icons.call),
                  title: getTextWidgetWithAlign('Call Us', 18,
                      getThemeBlackColor(), FontWeight.normal, TextAlign.start),
                  onTap: () => {Navigator.pop(context), _launchCaller()},
                ),
              ],
            ),
          );
        });
  }
}

class SettingsModel {
  String title;

  SettingsModel(this.title);
}
