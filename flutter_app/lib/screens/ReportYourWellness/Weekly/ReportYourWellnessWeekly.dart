import 'dart:convert';

import 'package:circular_check_box/circular_check_box.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app/apiHelper/allquestions_response.dart';
import 'package:flutter_app/apiHelper/app_exceptions.dart';
import 'package:flutter_app/models/AnswerItem.dart';
import 'package:flutter_app/models/PainFeelItem.dart';
import 'package:flutter_app/models/ReportPainMonthlyQuestionItem.dart';
import 'package:flutter_app/models/ReportWellnessQuestionItem.dart';
import 'package:flutter_app/persistance/api_repository.dart';
import 'package:flutter_app/screens/ChangePassword.dart';
import 'package:flutter_app/screens/ReportSubmitted.dart';
import 'package:flutter_app/screens/ReportYourPain/ReportYourPainQuestionTwo.dart';
import 'package:flutter_app/utils/ColorUtils.dart';
import 'package:flutter_app/utils/CustomPopupMenu.dart';
import 'package:flutter_app/models/ReachBean.dart';
import 'package:flutter_app/screens/login/Signin.dart';
import 'package:flutter_app/utils/UtilsImporter.dart';
import 'package:flutter_app/utils/app_widgets.dart';
import 'package:flutter_app/utils/utils.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:toast/toast.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:http/http.dart' as http;

class ReportYourWellnessWeekly extends StatefulWidget {
  @override
  _ReportYourWellnessWeeklyState createState() =>
      _ReportYourWellnessWeeklyState();
}

class _ReportYourWellnessWeeklyState extends State<ReportYourWellnessWeekly> {
  TextStyle style = TextStyle(fontFamily: getThemeFontName(), fontSize: 18.0);
  List<Datum> weeklyWellnessQuestions = new List();
  // List<ReportWellnessQuestionItem> weeklyWellnessQuestions = new List();
  int currentQuestionIndex = 0;
  bool isQuestionsLoaded = false;
  String token = "";
  String deviceId = "12345678";
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  // loadWeeklyWellnessQuestions() async {
  //   String data =
  //       await rootBundle.loadString('assets/weeklyWellnessQuestions.json');
  //   var tagObjsJson = jsonDecode(data)['response']['data'] as List;
  //   // print("tagObjsJson  ==>"+tagObjsJson.toString());
  //   weeklyWellnessQuestions = tagObjsJson
  //       .map((tagJson) => ReportWellnessQuestionItem.fromJson(tagJson))
  //       .toList();
  // }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // loadWeeklyWellnessQuestions().then((result) {
    //   setState(() {
    //     isQuestionsLoaded = true;
    //     print("SIZE ==> " + weeklyWellnessQuestions.length.toString());
    //   });
    // });

    Future<String> token = UtilsImporter().preferencesUtils.getToken();
    Future<String> device = getDeviceId();
    token.then((data) {
      print("token ==> " + data);
      this.token = data;
      device.then((deviceId) {
        print("deviceID ==> " + deviceId);
        this.deviceId = deviceId;
        getAllQuestions();
      });
    });
  }

  void getAllQuestions() {
    setState(() => isQuestionsLoaded = false);
    var api = new APICallRepository();
    api
        .allquestion(deviceId, UtilsImporter().apiUtils.WEEKLY,
        UtilsImporter().apiUtils.WELLNESS, token)
        .then((response) {
      setState(() {
        setState(() => isQuestionsLoaded = true);
        weeklyWellnessQuestions = response.response.data;
      });
    }, onError: (error) {
      setState(() {
        print("ERROR FOUND FINALLY HERE ===> " + error.toString());
        // setState(() => isLoading = false);
        if (error.toString().contains('Unauthorised')) {
          doLogout(context, error.toString());
        } else if (error is ProviderDeactivatedException) {
          doLogout(context, error.toString());
        } else if (error is PatientDeactivatedException) {
          doLogout(context, error.toString());
        } else {
          _scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text(error.toString(),
                textAlign: TextAlign.center,
                style: style.copyWith(
                    color: Colors.white, fontWeight: FontWeight.bold)),
            duration: Duration(seconds: 5),
          ));
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      body: SafeArea(
          child: isQuestionsLoaded
              ? Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                      createToolBar(
                          currentQuestionIndex >= 1,
                          'Report your Wellness',
                          "Question " +
                              (currentQuestionIndex + 1).toString() +
                              " of " +
                              weeklyWellnessQuestions.length.toString(),
                          currentQuestionIndex <
                                  (weeklyWellnessQuestions.length - 1)
                              ? 'Next'
                              : 'Finish'),
                      new Container(
                        margin: const EdgeInsets.only(top: 10),
                        height: 1,
                        color: getColorFromHex('#CCCCCC'),
                      ),
                      Expanded(
                        child: Container(
                            margin: const EdgeInsets.all(20),
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  getTextWidget(
                                      weeklyWellnessQuestions[
                                              currentQuestionIndex]
                                          .question,
                                      20,
                                      getThemeBlackColor(),
                                      FontWeight.normal),
                                  new Center(
                                      child: Visibility(
                                          visible: weeklyWellnessQuestions[
                                                      currentQuestionIndex]
                                                  .Questionsubtitle
                                                  .length >
                                              0,
                                          child: Html(
                                            data: weeklyWellnessQuestions[
                                                    currentQuestionIndex]
                                                .Questionsubtitle
                                                .toString(),
                                          ))),
                                  SizedBox(
                                    height: 45,
                                  ),
                                  new Expanded(
                                    child: getAnswersLayout(
                                        weeklyWellnessQuestions[
                                            currentQuestionIndex]),
                                  )
                                ])),
                      )
                    ])
              : Center(
            child: SizedBox(
              child: Padding(
                  padding: EdgeInsets.fromLTRB(15.0, 11.0, 15.0, 11.0),
                  child: CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation<Color>(
                          getThemeGreenColor()))),
              height: 50.0,
              width: 60.0,
            ),
          )),
    );
  }

  Widget createToolBar(
      bool isBackButton, String title, String subTitle, String buttonLabel) {
    return Stack(
      children: [
        SizedBox(
          width: 8,
        ),
        Positioned(
            left: 10,
            top: 10,
            child: Align(
                alignment: Alignment.center,
                child: InkWell(
                    child: isBackButton
                        ? Image.asset(
                            "assets/ic_back.png",
                            height: 25,
                            width: 40,
                          )
                        : Image.asset(
                            "assets/buttonsTopBarClose.png",
                            height: 40,
                            width: 40,
                          ),
                    onTap: () {
                      if (currentQuestionIndex <= 0)
                        goBack(context);
                      else {
                        setState(() {
                          if (currentQuestionIndex <= 0)
                            currentQuestionIndex = 0;
                          else
                            currentQuestionIndex--;
                        });
                      }
                    }))),
        Center(
            child: Column(
          children: <Widget>[
            new Padding(
              padding: const EdgeInsetsDirectional.only(top: 15.0, bottom: 0),
              child: getTextWidget(title, 17, Colors.black, FontWeight.bold),
            ),
            getTextWidget(subTitle, 13, getThemeGreenColor(), FontWeight.w600),
          ],
        )),
        Positioned(
            right: 10,
            top: 10,
            child: Center(
                child: Container(
                    decoration: new BoxDecoration(
                      borderRadius: new BorderRadius.circular(4.0),
                      color: getThemeGreenColor(),
                    ),
                    child: InkWell(
                      child: Visibility(
                          visible: getSelected(),
                          child: Padding(
                              padding: EdgeInsets.only(
                                  left: 8, top: 6, right: 8, bottom: 6),
                              child: getTextWidget(buttonLabel, 17,
                                  Colors.white, FontWeight.bold))),
                      onTap: () {
                        goToNext(buttonLabel);
                      },
                    )))),
        SizedBox(
          width: 10,
        ),
      ],
    );
  }

  Widget getAnswersLayout(Datum monthlyPainQuestion) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
              child: Container(
                  margin: const EdgeInsets.only(left: 15, right: 15),
                  color: Colors.white,
                  child: ListView.builder(
                    itemCount: monthlyPainQuestion.questionDetails.length,
                    itemBuilder: (context, index) {
                      return _getItemCell(
                          index: index,
                          title: monthlyPainQuestion.questionDetails[index].text,
                          isSelected:
                              monthlyPainQuestion.questionDetails[index].isSelected);
                    },
                    padding: EdgeInsets.all(0.0),
                  ))),
        ]);
  }

  void goToNext(String buttonLabel) {
    if (buttonLabel == 'Finish') {
      Navigator.push(context, MaterialPageRoute(builder: (context) {
        return ReportSubmitted();
      }));
    } else {
      setState(() {
        currentQuestionIndex++;
      });
    }
  }

  void unSelectAll() {
    for (int i = 0; i < weeklyWellnessQuestions.length; i++) {
      for (int j = 0; j < weeklyWellnessQuestions[i].questionDetails.length; j++) {
        weeklyWellnessQuestions[i].questionDetails[j].isSelected = false;
      }
    }
  }

  void setSelectedAnswer(int answerIndex) {
    weeklyWellnessQuestions[currentQuestionIndex]
        .questionDetails[answerIndex]
        .isSelected = true;
  }

  bool getSelected() {
    for (int j = 0;
        j < weeklyWellnessQuestions[currentQuestionIndex].questionDetails.length;
        j++) {
      if (weeklyWellnessQuestions[currentQuestionIndex].questionDetails[j].isSelected)
        return true;
    }
    return false;
  }

  Widget _getItemCell({int index, String title, bool isSelected}) {
    return new GestureDetector(
      child: Card(
          margin: EdgeInsets.symmetric(vertical: 10),
          color: isSelected
              ? getColorFromHex('#1a75ba')
              : getColorFromHex("#ededf2"),
          child: Padding(
              padding: const EdgeInsetsDirectional.only(
                top: 25.0,
                bottom: 25,
              ),
              child: getTextWidget(title, 15,
                  isSelected ? Colors.white : Colors.black, FontWeight.bold))),
      onTap: () {
        setState(() {
          unSelectAll();
          isSelected = true;
          setSelectedAnswer(index);
        });
      },
    );
  }
}
