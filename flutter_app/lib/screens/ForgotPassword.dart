import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/persistance/api_repository.dart';
import 'package:flutter_app/screens/ResetPassword.dart';
import 'package:flutter_app/screens/UpdateResetPassword.dart';
import 'package:flutter_app/utils/ColorUtils.dart';
import 'package:flutter_app/utils/app_widgets.dart';
import 'package:flutter_app/utils/utils.dart';
import 'package:flutter_verification_code/flutter_verification_code.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import '../utils/UtilsImporter.dart';
import 'ChangePassword.dart';

class ForgotPassword extends StatefulWidget {
  ForgotPassword({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _ForgotPasswordPageState createState() => _ForgotPasswordPageState();
}

class _ForgotPasswordPageState extends State<ForgotPassword> {
  TextStyle style = TextStyle(fontFamily: getThemeFontName(), fontSize: 14.0);
  TextEditingController _textEditingControllerEmail = new TextEditingController();
  TapGestureRecognizer recognizerCreateAccount;
  bool isLoading = false;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
    //  checkInternet();
    });
    recognizerCreateAccount = TapGestureRecognizer()
      ..onTap = () {
//        Navigator.push(context, MaterialPageRoute(builder: (context) {
//          return CreateAccount();
//        }));
      };
  }

//  checkInternet() {
//    setState(() {
//      Future<bool> result = UtilsImporter().commanUtils.checkConnection();
//      result.then((data) {
//        isLoading = data;
//      });
//    });
//  }

  bool isEmailAvailable = true, isError = false;

  @override
  Widget build(BuildContext context) {

    void validateTextFields() {
      if (_textEditingControllerEmail.text.isEmpty) {
        setState(() {
          isError = true;
        });
      } else {
        setState(() {
          isError = false;
        });
        showOTPDialog(context);
      }
    }

    bool validateEmail(String value) {
      String  pattern = r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+";
      RegExp regExp = new RegExp(pattern);
      if( value.isEmpty || !regExp.hasMatch(value)) {
        return true;
      } else {
        return false;
      }
    }


    final emailField = Container(
      child: TextField(
        controller: _textEditingControllerEmail,
        keyboardType: TextInputType.emailAddress,
        cursorColor: getThemeBlackColor(),
        onChanged: (text) {
          if (text.length > 0)
            setState(() => isEmailAvailable = true);
          else
            setState(() => isEmailAvailable = false);
        },
        decoration: InputDecoration(
          border: InputBorder.none,
          contentPadding: EdgeInsets.fromLTRB(10.0, 2.0, 10.0, 2.0),
          hintText: "Email address",
        ),
        obscureText: false,
        style: style,
      ),
      decoration: BoxDecoration(
        color: getInputBoxFillColor(),
        border: isEmailAvailable
            ? Border.all(
                color: getInputBoxFillColor(),
                width: 1,
              )
            : Border.all(
                color: getRedColor(),
                width: 1,
              ),
        borderRadius: BorderRadius.circular(2),
      ),
    );


    final submitButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: getThemeGreenColor(),
      child: isLoading
          ? Center(
              child: SizedBox(
                child: Padding(
                    padding: EdgeInsets.fromLTRB(15.0, 11.0, 15.0, 11.0),
                    child: CircularProgressIndicator(valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),)),
                height: 50.0,
                width: 60.0,
              ),
            )
          : MaterialButton(
              padding: EdgeInsets.fromLTRB(30.0, 18.0, 30.0, 18.0),
              onPressed: () {

                if(validateEmail(_textEditingControllerEmail.text))
                  {
                    setState(() {
                      isError = true;
                    });
                  }else{
                    setState(() {
                      isError = false;
                    });
                    //showOTPDialog(context);
                    forgotPasswordApiCall();
                  }

//                validateTextFields();
//                if (UtilsImporter()
//                    .commanUtils
//                    .isTextEmpty(_textEditingControllerEmail.text)) {
//                  setState(() {
//                    isEmailAvailable = false;
//                    isError = true;
//                  });
//                }
//                Navigator.push(context, MaterialPageRoute(builder: (context) {
//                  return ActivateAccount();
//                }));
              },
              child: Text("Submit",
                  textAlign: TextAlign.center,
                  style: style.copyWith(
                      fontSize: 15,
                      color: Colors.white,
                      fontWeight: FontWeight.bold)),
            ),
    );

    return Scaffold(
      backgroundColor: Colors.white,
      key: _scaffoldKey,
      body: SafeArea(
        child: Column(
          children: <Widget>[
            createToolBar(),
            new Container(
              margin: EdgeInsets.only(top: 5),
              height: 1,
              color: getColorFromHex('#CCCCCC'),
            ),
            new Expanded(
              child: SingleChildScrollView(
                child: Container(
                  decoration: BoxDecoration(
                    color: getColorFromHex("#FFFFFF"),
                  ),
                  margin: const EdgeInsets.only(top: 15.0),
                  child: Padding(
                    padding: const EdgeInsets.only(
                      top: 1,
                      left: 20,
                      right: 20,
                      bottom: 20,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Container(
                            child: Text(
                                "No Worries! Please provide the email address you used to sign up and we'll send you a link to reset your password.",
                                style: TextStyle(
                                    color: getThemeBlackColor(),
                                    fontWeight: FontWeight.normal,
                                    fontSize: 17,
                                    fontFamily: getThemeFontName()))),
                        Padding(
                            padding: const EdgeInsets.only(
                                left: 10, bottom: 4, top: 20),
                            child: Visibility(
                                maintainSize: true,
                                maintainAnimation: true,
                                maintainState: true,
                                visible: isError,
                                child: Text(
                                    "Please fill in the required fields*",
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                        color: getRedColor(),
                                        fontWeight: FontWeight.bold,
                                        fontSize: 13,
                                        fontFamily: getThemeFontName())))),
                        new Container(
                          margin: const EdgeInsets.only(
                              left: 1.0, right: 1.0, bottom: 10.0),
                          padding: const EdgeInsets.all(10.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              SizedBox(height: 1.0),
                              Padding(
                                padding:
                                    const EdgeInsets.only(left: 1, right: 1),
                                child: Theme(
                                    data: Theme.of(context).copyWith(
                                        splashColor: Colors.transparent),
                                    child: emailField),
                              ),
                              SizedBox(
                                height: 55.0,
                              ),
                              Padding(
                                  padding: const EdgeInsets.only(
                                      left: 55, right: 55),
                                  child: submitButton),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }



  Widget createToolBar() {
    return Stack(
      children: [
        SizedBox(
          width: 8,
        ),
        Positioned(
            left: 10,
            top:  15,
            child: Align(
                alignment: Alignment.center,
                child: InkWell(
                    child: Image.asset(
                      "assets/ic_back.png",
                      height: 25,
                      width: 40,
                    ),
                    onTap: () {
                      goBack(context);
                    }))),
        Center(
            child: Column(
              children: <Widget>[
                new Padding(
                  padding: const EdgeInsetsDirectional.only( top: 15.0, bottom: 15),
                  child: getTextWidget(  "Forgot password?", 17, Colors.black, FontWeight.bold), ),
              ],
            )),
      ],
    );
  }



  void showThankYouDialog(BuildContext context) {

    Dialog dialogThankYou;
    TextStyle style = TextStyle(fontFamily: 'Orkney', fontSize: 14.0);
    final continueButon = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: getThemeGreenColor(),
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () {
          Navigator.pop(context, true);
          setState(() {
            _textEditingControllerEmail.text = "";
          });

        },
        child: Text("Continue",
            textAlign: TextAlign.center,
            style: style.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold)),
      ),
    );

    dialogThankYou = Dialog(
      child: Container(
        padding: EdgeInsets.only(bottom: 20),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(12),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(color: Colors.white),
                  child: Text(
                    "We have sent you an email to \n reset your password",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 18,
                        fontWeight: FontWeight.w600,
                        fontFamily: 'Orkney'),
                  ),
                ),
                SizedBox(
                  width: 30,
                ),
              ],
            ),
            Container(
                margin: const EdgeInsets.only(left: 15, right: 15, top: 6),
                child: continueButon),
          ],
        ),
      ),
    );

    showDialog(  context: context, useRootNavigator: false, builder: (BuildContext context) => dialogThankYou);

  }


  void showOTPDialog(BuildContext context) {
    Dialog otpDialog;
    var _code;
    bool _onEditing = true;

    TextStyle style = TextStyle(fontFamily: 'Orkney', fontSize: 14.0);

    final continueButon = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: getThemeGreenColor(),
      child: isLoading
            ? Center(
          child: SizedBox(
            child: Padding(
                padding: EdgeInsets.fromLTRB(15.0, 11.0, 15.0, 11.0),
                child: CircularProgressIndicator(valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),)),
            height: 50.0,
            width: 60.0,
          ),
        )
            : MaterialButton(
          padding: EdgeInsets.fromLTRB(30.0, 18.0, 30.0, 18.0),
          onPressed: () {
            if(_onEditing != true)
            {
              //API CALL
              print("===API CALL==>");
              forgotPasswordOtpApiCall(_textEditingControllerEmail.text, _code );
            }
          },
          child: Text("Continue",
              textAlign: TextAlign.center,
              style: style.copyWith(
                  fontSize: 15,
                  color: Colors.white,
                  fontWeight: FontWeight.bold)),
        ),
      );

    otpDialog = Dialog(
      child: Container(
        padding: EdgeInsets.only(bottom: 20),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(
                      top: 18.0, left: 20.0, bottom: 10, right: 20),
                  child: Text(
                    "Verification Code",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 22,
                        fontWeight: FontWeight.normal,
                        fontFamily: 'Orkney'),
                  ),
                ),
                Container(
                  child: GestureDetector(
                    onTap: () {
                      Navigator.pop(context, true);
                    },
                    child: Align(
                      alignment: Alignment.topRight,
                      child: CircleAvatar(
                        radius: 25.0,
                        backgroundColor: Colors.white,
                        child: Icon(Icons.close, color: Colors.black),
                      ),
                    ),
                  ),
                ),
              ],
            ),

            Container(
              margin: const EdgeInsets.only(top: 20.0, left: 15, right: 15),
              child: VerificationCode(
                textStyle: TextStyle(fontSize: 20.0, color: Colors.red[900]),
                underlineColor: Colors.amber,
                keyboardType: TextInputType.number,
                length: 4,
                // clearAll is NOT required, you can delete it
                // takes any widget, so you can implement your design
                // clearAll: Padding(
                //   padding: const EdgeInsets.all(8.0),
                //   child: Text(
                //     'clear all',
                //     style: TextStyle(
                //         fontSize: 14.0,
                //         decoration: TextDecoration.underline,
                //         color: Colors.blue[700]),
                //   ),
                // ),

                onCompleted: (String value) {
                  setState(() {
                    _code = value;
                  });
                },
                onEditing: (bool value) {
                  setState(() {
                    _onEditing = value;
                  });
                },
              ),
            ),

            Container(
                margin: const EdgeInsets.only(left: 15, right: 15, top: 16),
                child: continueButon),

          ],
        ),
      ),
    );

    showDialog(  barrierDismissible: false,  context: context, useRootNavigator: false, builder: (BuildContext context) => otpDialog);
  }


  void forgotPasswordApiCall() {
    setState(() => isLoading = true);
    var api = new APICallRepository();
    api.forgotPassword(
        _textEditingControllerEmail.text.trim()
    ).then((user) {
          setState(() {
            setState(() => isLoading = false);
            print("response ==> " + user.toString());

            showThankYouDialog(context);
          ///  showOTPDialog(context);


          });
    }, onError: (error) {
          print("ERROR FOUND FINALLY HERE ===> " + error.toString());
          setState(() => isLoading = false);
          _scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text(error.toString(),
                textAlign: TextAlign.center,
                style: style.copyWith(
                    color: Colors.white, fontWeight: FontWeight.bold)),
            duration: Duration(seconds: 5),
          ));
    });
  }


  void forgotPasswordOtpApiCall(String email,String otp) {
    setState(() => isLoading = true);
    var api = new APICallRepository();
    api.forgotPasswordOtp(
        _textEditingControllerEmail.text.trim()
        ,otp
    ).then((user) {
          setState(() {
            setState(() => isLoading = false);
            print("response ==> " + user.toString());

            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => UpdateResetPassword(_textEditingControllerEmail.text.trim())),
                    (Route<dynamic> route) => false);
          });
    }, onError: (error) {

          print("ERROR FOUND FINALLY HERE ===> " + error.toString());
          setState(() => isLoading = false);
          _scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text(error.toString(),
                textAlign: TextAlign.center,
                style: style.copyWith(
                    color: Colors.white, fontWeight: FontWeight.bold)),
            duration: Duration(seconds: 5),
          ));
    });
  }



}

BoxDecoration myBoxDecoration() {
  return BoxDecoration(
    border: Border(
      bottom: BorderSide(
        color: Color.fromRGBO(113, 127, 139, 1),
        //                   <--- border color
        width: 1.0,
      ),
    ),
  );
}





//void openEmailApp(BuildContext context){
//  try{
//    AppAvailability.launchApp(Platform.isIOS ? "message://" : "com.google.android.gm").then((_) {
//      print("App Email launched!");
//    }).catchError((err) {
//      Scaffold.of(context).showSnackBar(SnackBar(
//          content: Text("App Email not found!")
//      ));
//      print(err);
//    });
//  } catch(e) {
//    Scaffold.of(context).showSnackBar(SnackBar(content: Text("Email App not found!")));
//  }
//}
Future<dynamic> callResetPasswordAPI(String email) async {
  final response = await http.get(
    UtilsImporter().apiUtils.baseURL +
        UtilsImporter().apiUtils.reset_password +
        email,
    headers: header(),
  );
  return json.decode(response.body);
}

header() => {"Content-Type": 'application/json'};
