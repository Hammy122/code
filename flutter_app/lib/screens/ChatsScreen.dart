import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app/screens/ChangePassword.dart';
import 'package:flutter_app/utils/ColorUtils.dart';
import 'package:flutter_app/utils/CustomPopupMenu.dart';
import 'package:flutter_app/screens/login/Signin.dart';
import 'package:flutter_app/utils/app_widgets.dart';
import 'package:flutter_app/utils/receivedmessagewidget.dart';
import 'package:flutter_app/utils/sentmessagewidget.dart';
import 'package:flutter_app/utils/utils.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:toast/toast.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:url_launcher/url_launcher.dart';
import '../models/ChatMessageItem.dart';
import '../models/UserBean.dart';
import '../utils/UtilsImporter.dart';
import 'package:http/http.dart' as http;

class ChatsScreen extends StatefulWidget {
  @override
  ChatsScreenState createState() => ChatsScreenState();
}

class ChatsScreenState extends State<ChatsScreen> {
  TextStyle style = TextStyle(fontFamily: getThemeFontName(), fontSize: 14.0);
  Future<List<String>> userProfile;
  bool isMessagesFound = true;
  bool _showBottom = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

//    setState(() {
//      userProfile = UtilsImporter().preferencesUtils.getUserData();
////      checkInternet();
//    });
  }

  @override
  Widget build(BuildContext context) {
    goBack(BuildContext context, bool isReachCreated) {
      if (Navigator.canPop(context)) {
        Navigator.pop(context, isReachCreated);
      } else {
        SystemNavigator.pop();
      }
    }

    return Scaffold(
        backgroundColor: Colors.white,
        body: SafeArea(
          child: Column(children: <Widget>[
            Container(
                padding:
                    const EdgeInsets.only(left: 5, right: 0, top: 5, bottom: 5),
                color: Colors.white,
                child: Stack(
                  children: [
                    Positioned(
                        left: 10,
                        child: Align(
                            alignment: Alignment.center,
                            child: InkWell(
                                child: Image.asset(
                                  "assets/buttonsTopBarClose.png",
                                  height: 40,
                                  width: 40,
                                ),
                                onTap: () {
                                  goBack(context, false);
                                }))),
                    Center(
                        child: Container(
                      margin: const EdgeInsets.only(top: 5, bottom: 5),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          getTextWidget(
                              "Dr. Smith", 17, Colors.black, FontWeight.bold),
                          SizedBox(
                            height: 2,
                            width: 0,
                          ),
                          getTextWidget(
                              "PCP", 13, getThemeGreenColor(), FontWeight.w600),
                        ],
                      ),
                    )),
                  ],
                )),
            Container(
              margin: const EdgeInsets.only(top: 2),
              height: 1,
              color: getInputHintTextColor(),
            ),
            Expanded(
                child: Stack(
              children: <Widget>[
                Positioned.fill(
                  child: Column(
                    children: <Widget>[
                      isMessagesFound
                          ? Expanded(
                              child: ListView.builder(
                                padding: const EdgeInsets.all(15),
                                itemCount: messages.length,
                                itemBuilder: (ctx, i) {
                                  if (messages[i]['status'] ==
                                      MessageType.received) {
                                    return ReceivedMessagesWidget(i: i);
                                  } else {
                                    return SentMessageWidget(i: i);
                                  }
                                },
                              ),
                            )
                          : Expanded(
                              child: Padding(
                                  padding: EdgeInsets.only(
                                      left: 50, right: 50, bottom: 20),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.stretch,
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      getTextWidget(
                                          "Start the conversation with your provider here!",
                                          20,
                                          Colors.black,
                                          FontWeight.normal),
                                      SizedBox(
                                        height: 15,
                                      ),
                                      Image.asset(
                                        "assets/arrowDown.png",
                                        height: 50.0,
                                        width: 45.0,
                                      )
                                    ],
                                  ))),
                      Container(
                        color: getColorFromHex('#ededf2'),
                        padding: EdgeInsets.all(5.0),
                        height: 61,
                        child: Row(
                          children: <Widget>[
                            SizedBox(
                              width: 8,
                            ),
                            Expanded(
                              child: Container(
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(5.0),
                                  boxShadow: [
                                    BoxShadow(
                                        offset: Offset(0, 3),
                                        blurRadius: 4,
                                        color: Colors.grey)
                                  ],
                                ),
                                child: Row(
                                  children: <Widget>[
                                    SizedBox(
                                      width: 15,
                                    ),
                                    Expanded(
                                      child: TextField(
                                        decoration: InputDecoration(
                                            hintText: "Type your message here",
                                            border: InputBorder.none),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(width: 5),
                            Container(
                              padding: const EdgeInsets.all(10.0),
                              child: InkWell(
                                child: Image.asset(
                                  "assets/ic_message_send.png",
                                  height: 25.0,
                                  width: 25.0,
                                ),
                                onTap: () {
                                  setState(() {
                                    _showBottom = true;
                                  });
                                },
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                Positioned.fill(
                  child: GestureDetector(
                    onTap: () {
                      setState(() {
                        _showBottom = false;
                      });
                    },
                  ),
                ),
                _showBottom
                    ? Positioned(
                        bottom: 90,
                        left: 25,
                        right: 25,
                        child: Container(
                          padding: EdgeInsets.all(25.0),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                  offset: Offset(0, 5),
                                  blurRadius: 15.0,
                                  color: Colors.grey)
                            ],
                          ),
                          child: GridView.count(
                            mainAxisSpacing: 21.0,
                            crossAxisSpacing: 21.0,
                            shrinkWrap: true,
                            crossAxisCount: 3,
                            children: List.generate(
                              icons.length,
                              (i) {
                                return Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(15.0),
                                    color: Colors.grey[200],
                                    border:
                                        Border.all(color: myGreen, width: 2),
                                  ),
                                  child: IconButton(
                                    icon: Icon(
                                      icons[i],
                                      color: myGreen,
                                    ),
                                    onPressed: () {},
                                  ),
                                );
                              },
                            ),
                          ),
                        ),
                      )
                    : Container(),
              ],
            ))
          ]),
        ));
  }
}

List<IconData> icons = [
  Icons.image,
  Icons.camera,
  Icons.file_upload,
  Icons.folder,
  Icons.gif
];
