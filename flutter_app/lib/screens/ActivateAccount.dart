import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/models/register_response_model.dart';
import 'package:flutter_app/persistance/api_repository.dart';
import 'package:flutter_app/screens/login/Signin.dart';
import 'package:flutter_app/models/UserBean.dart';
import 'package:flutter_app/utils/ColorUtils.dart';
import 'package:flutter_app/utils/utils.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:toast/toast.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import '../utils/UtilsImporter.dart';
import 'package:progress_dialog/progress_dialog.dart';

import 'PrivacyPolicy.dart';

class ActivateAccount extends StatefulWidget {
  static var routeName = "Signup";

  ActivateAccount({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MySignUpPageState createState() => _MySignUpPageState();
}

class _MySignUpPageState extends State<ActivateAccount> {
//  ProgressDialog pr;
  String _birthDay = "Date of Birth";
  var api = new APICallRepository();


  // Value that is shown in the date picker in dateAndTime mode.
  DateTime dateTime = DateTime.now();

  TextStyle style = TextStyle(fontFamily: getThemeFontName(), fontSize: 15.0);
  TextEditingController _textEditingControllerEmail =
      new TextEditingController();

  TextEditingController _textEditingControllerConfirmEmail =
      new TextEditingController();
  TextEditingController _textEditingControllerFirstName =
      new TextEditingController();
  TextEditingController _textEditingControllerLastName =
      new TextEditingController();
  TextEditingController _textEditingControllerProviderCode =
      new TextEditingController();
  TextEditingController _textEditingControllerPassword = new TextEditingController();
  TextEditingController _textEditingControllerConfirmPassword = new TextEditingController();

  TapGestureRecognizer recognizerCreateAccount;
  bool isLoading = false;
  //GlobalKey _scaffold = GlobalKey();
  final GlobalKey<ScaffoldState> _scaffold = new GlobalKey<ScaffoldState>();

  bool isEmailAvailable = true,
      isConfirmEmailAvailable = true,
      isFirstNameAvailable = true,
      isLastNameAvailable = true,
      isDateOfBirthAvailable = true,
      isPasswordAvailable = true,
      isConfirmPasswordAvailable = true,
      isProviderCodeAvailable = true,
      isError = false,
      isNotVisible = true ,
      isConfirmNotVisible = true ;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
//    setState(() {
//      checkInternet();
//    });
    recognizerCreateAccount = TapGestureRecognizer()
      ..onTap = () {
//        Navigator.push(context, MaterialPageRoute(builder: (context) {
//          return CreateAccount();
//        }));
      };
  }

  checkInternet() {
    setState(() {
      Future<bool> result = UtilsImporter().commanUtils.checkConnection();
      result.then((data) {
        //isLoading = data;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    void validateTextFields() {
      if (_textEditingControllerEmail.text.isEmpty ||
          _textEditingControllerConfirmEmail.text.isEmpty ||
          _textEditingControllerFirstName.text.isEmpty ||
          _textEditingControllerLastName.text.isEmpty ||
          _birthDay.toString() == "Date of Birth" ||
          _textEditingControllerPassword.text.isEmpty ||
          _textEditingControllerConfirmPassword.text.isEmpty ||
          _textEditingControllerProviderCode.text.isEmpty) {
        setState(() {
          isError = true;
        });
      } else {
        setState(() {
          isError = false;
        });
      }
    }

    final emailField = Container(
      child: TextField(
        controller: _textEditingControllerEmail,
        keyboardType: TextInputType.emailAddress,
        cursorColor: getThemeBlackColor(),
        onChanged: (text) {
          if (text.length > 0)
            setState(() => isEmailAvailable = true);
          else
            setState(() => isEmailAvailable = false);
        },
        decoration: InputDecoration(
          border: InputBorder.none,
          contentPadding: EdgeInsets.fromLTRB(10.0, 2.0, 10.0, 2.0),
          hintText: "Email address",
        ),
        obscureText: false,
        style: style,
      ),
      decoration: BoxDecoration(
        color: getInputBoxFillColor(),
        border: isEmailAvailable
            ? Border.all(
                color: getInputBoxFillColor(),
                width: 1,
              )
            : Border.all(
                color: getRedColor(),
                width: 1,
              ),
        borderRadius: BorderRadius.circular(2),
      ),
    );

    final confirmEmailField = Container(
      child: TextField(
        controller: _textEditingControllerConfirmEmail,
        keyboardType: TextInputType.emailAddress,
        cursorColor: getThemeBlackColor(),
        onChanged: (text) {
          if (text.length > 0)
            setState(() => isConfirmEmailAvailable = true);
          else
            setState(() => isConfirmEmailAvailable = false);
        },
        decoration: InputDecoration(
          border: InputBorder.none,
          contentPadding: EdgeInsets.fromLTRB(10.0, 2.0, 10.0, 2.0),
          hintText: "Confirm email address",
        ),
        obscureText: false,
        style: style,
      ),
      decoration: BoxDecoration(
        color: getInputBoxFillColor(),
        border: isConfirmEmailAvailable
            ? Border.all(
                color: getInputBoxFillColor(),
                width: 1,
              )
            : Border.all(
                color: getRedColor(),
                width: 1,
              ),
        borderRadius: BorderRadius.circular(2),
      ),
    );

    final firstNameField = Container(
      child: TextField(
        controller: _textEditingControllerFirstName,
        cursorColor: getThemeBlackColor(),
        onChanged: (text) {
          if (text.length > 0)
            setState(() => isFirstNameAvailable = true);
          else
            setState(() => isFirstNameAvailable = false);
        },
        decoration: InputDecoration(
          border: InputBorder.none,
          contentPadding: EdgeInsets.fromLTRB(10.0, 2.0, 10.0, 2.0),
          hintText: "First name (legal)",
        ),
        obscureText: false,
        style: style,
      ),
      decoration: BoxDecoration(
        color: getInputBoxFillColor(),
        border: isFirstNameAvailable
            ? Border.all(
                color: getInputBoxFillColor(),
                width: 1,
              )
            : Border.all(
                color: getRedColor(),
                width: 1,
              ),
        borderRadius: BorderRadius.circular(2),
      ),
    );

    final lastNameField = Container(
      child: TextField(
        controller: _textEditingControllerLastName,
        cursorColor: getThemeBlackColor(),
        onChanged: (text) {
          if (text.length > 0)
            setState(() => isLastNameAvailable = true);
          else
            setState(() => isLastNameAvailable = false);
        },
        decoration: InputDecoration(
          border: InputBorder.none,
          contentPadding: EdgeInsets.fromLTRB(10.0, 2.0, 10.0, 2.0),
          hintText: "Last name (legal)",
        ),
        obscureText: false,
        style: style,
      ),
      decoration: BoxDecoration(
        color: getInputBoxFillColor(),
        border: isLastNameAvailable
            ? Border.all(
                color: getInputBoxFillColor(),
                width: 1,
              )
            : Border.all(
                color: getRedColor(),
                width: 1,
              ),
        borderRadius: BorderRadius.circular(2),
      ),
    );


    final passwordField = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Expanded(
              child: TextField(
                cursorColor: getThemeBlackColor(),

                onChanged: (text) {
                  if (text.length > 0)
                    setState(() => isPasswordAvailable = true);
                  else
                    setState(() => isPasswordAvailable = false);
                },
                controller: _textEditingControllerPassword,

                decoration: InputDecoration(
                  hintText: "Password",
                  border: InputBorder.none,
                  contentPadding: EdgeInsets.fromLTRB(10.0, 2.0, 10.0, 2.0),
                ),

                obscureText: isNotVisible,

                style: style,
          )),

          InkWell(
            child: Icon( Icons.remove_red_eye_sharp , color: isNotVisible ?  getThemeGrayColor() :  getThemeGreenColor()  ),
            onTap: () {
              setState(() {
                if(isNotVisible) {
                  isNotVisible = false;
                }else {
                  isNotVisible = true;
                }
              });

            },
          ),

          SizedBox(width: 10,),

          ClipOval(
            child: Material(
              child: InkWell(
                child: Image.asset(
                  "assets/ic_info.png",
                  width: 18,
                  height: 18,
                ),
                onTap: () {
                  UtilsImporter().commanUtils.showAlertDialogWithTitleOkButton(
                      UtilsImporter().stringUtils.returnPasswordTitle,
                      UtilsImporter().stringUtils.returnPasswordDescription,
                      context);
                },
              ),
            ),
          ),

          SizedBox(
            width: 10,
          ),
        ],
      ),
      decoration: BoxDecoration(
        color: getInputBoxFillColor(),
        border: isPasswordAvailable
            ? Border.all(
                color: getInputBoxFillColor(),
                width: 1,
              )
            : Border.all(
                color: getRedColor(),
                width: 1,
              ),
        borderRadius: BorderRadius.circular(2),
      ),
    );


    final confirmPasswordField = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Expanded(
              child: TextField(
                cursorColor: getThemeBlackColor(),

                onChanged: (text) {
                  if (text.length > 0)
                    setState(() => isConfirmPasswordAvailable = true);
                  else
                    setState(() => isConfirmPasswordAvailable = false);
                },
                controller: _textEditingControllerConfirmPassword,

                decoration: InputDecoration(
                  hintText: "Confirm Password",
                  border: InputBorder.none,
                  contentPadding: EdgeInsets.fromLTRB(10.0, 2.0, 10.0, 2.0),
                ),

                obscureText: isConfirmNotVisible,

                style: style,
          )),

          InkWell(
            child: Icon( Icons.remove_red_eye_sharp , color: isConfirmNotVisible ?  getThemeGrayColor() :  getThemeGreenColor()  ),
            onTap: () {
              setState(() {
                if(isConfirmNotVisible) {
                  isConfirmNotVisible = false;
                }else {
                  isConfirmNotVisible = true;
                }
              });

            },
          ),

          SizedBox(width: 10,),

          ClipOval(
            child: Material(
              child: InkWell(
                child: Image.asset(
                  "assets/ic_info.png",
                  width: 18,
                  height: 18,
                ),
                onTap: () {
                  UtilsImporter().commanUtils.showAlertDialogWithTitleOkButton(
                      UtilsImporter().stringUtils.returnPasswordTitle,
                      UtilsImporter().stringUtils.returnPasswordDescription,
                      context);
                },
              ),
            ),
          ),

          SizedBox(
            width: 10,
          ),
        ],
      ),
      decoration: BoxDecoration(
        color: getInputBoxFillColor(),
        border: isConfirmPasswordAvailable
            ? Border.all(
                color: getInputBoxFillColor(),
                width: 1,
              )
            : Border.all(
                color: getRedColor(),
                width: 1,
              ),
        borderRadius: BorderRadius.circular(2),
      ),
    );




    final providerCodeField = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Expanded(
              child: TextField(
            controller: _textEditingControllerProviderCode,
            cursorColor: getThemeBlackColor(),
            onChanged: (text) {
              if (text.length > 0)
                setState(() => isProviderCodeAvailable = true);
              else
                setState(() => isProviderCodeAvailable = false);
            },
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.fromLTRB(10.0, 2.0, 10.0, 2.0),
              hintText: "Provider Code",
            ),
            obscureText: false,
            style: style,
          )),
          ClipOval(
            child: Material(
              child: InkWell(
                child: Image.asset(
                  "assets/ic_info.png",
                  width: 18,
                  height: 18,
                ),
                onTap: () {
                  UtilsImporter().commanUtils.showAlertDialogWithTitleOkButton(
                      UtilsImporter().stringUtils.providerCodeTitle,
                      UtilsImporter().stringUtils.ProviderCodeDescription,
                      context);
                },
              ),
            ),
          ),
          SizedBox(
            width: 10,
          ),
        ],
      ),
      decoration: BoxDecoration(
        color: getInputBoxFillColor(),
        border: isProviderCodeAvailable
            ? Border.all(
                color: getInputBoxFillColor(),
                width: 1,
              )
            : Border.all(
                color: getRedColor(),
                width: 1,
              ),
        borderRadius: BorderRadius.circular(2),
      ),
    );

    Future _chooseDate(BuildContext context, String initialDateString) async {
      var now = new DateTime.now();
      var initialDate = convertToDate(initialDateString) ?? now;
      initialDate = (initialDate.year >= now.year && initialDate.isAfter(now)
          ? initialDate
          : now);
      DatePicker.showDatePicker(context, showTitleActions: true, maxTime: now,
          onConfirm: (date) {
        setState(() {
          isDateOfBirthAvailable = true;
          DateTime dt = date;
          //String r = ftDateAsStr(dt);
          String r = symptomsftDateAsStr(dt);
          _birthDay = r;
        });
      }, currentTime: initialDate);
    }

    final birthDayField = GestureDetector(
        onTap: () {
          _chooseDate(context, _birthDay);
        },
        child: Container(
          padding: EdgeInsets.fromLTRB(10.0, 15.0, 10.0, 15.0),
          child: Text("$_birthDay",
              style: TextStyle(
                  color: _birthDay.toString() == "Date of Birth"
                      ? getInputBoxHintColor()
                      : getThemeBlackColor(),
                  fontSize: 14,
                  fontFamily: getThemeFontName())),
          decoration: BoxDecoration(
            color: getInputBoxFillColor(),
            border: isDateOfBirthAvailable
                ? Border.all(
                    color: getInputBoxFillColor(),
                    width: 1,
                  )
                : Border.all(
                    color: getRedColor(),
                    width: 1,
                  ),
            borderRadius: BorderRadius.circular(2),
          ),
        ));

    bool validatePassword(String value) {
      Pattern pattern = r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$';
      RegExp regex = new RegExp(pattern);
      print(value);
      if (value.isEmpty) {
       // print("=======EMPTY==>");
        return true;
      }
      else  if (!regex.hasMatch(value)) {
        //print("=======NOT MATCH==>");
        return true;
      } else {
        return false;
      }

    }

    final activateAccountButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: getThemeGreenColor(),
      child: isLoading
          ? Center(
              child: SizedBox(
                child: Padding(
                   // padding: EdgeInsets.fromLTRB(30.0, 18.0, 30.0, 18.0),
                    padding: EdgeInsets.fromLTRB(15.0, 11.0, 15.0, 11.0),
                    child: CircularProgressIndicator( valueColor:  AlwaysStoppedAnimation<Color>(Colors.white))),
                height: 50.0,
                width: 60.0,
              ),
            )
          : MaterialButton(
              minWidth: MediaQuery.of(context).size.width,
              padding: EdgeInsets.fromLTRB(30.0, 18.0, 30.0, 18.0),
              onPressed: () {
                print("Start");

                validateTextFields();
                if (UtilsImporter().commanUtils.isTextEmpty(_textEditingControllerEmail.text)) {
                  setState(() {
                    isEmailAvailable = false;
                    isError = true;
                  });
                }
                if (UtilsImporter().commanUtils.isTextEmpty(_textEditingControllerConfirmEmail.text)) {
                  setState(() {
                    isConfirmEmailAvailable = false;
                    isError = true;
                  });
                }
                if (UtilsImporter().commanUtils.isTextEmpty(_textEditingControllerFirstName.text)) {
                  setState(() {
                    isFirstNameAvailable = false;
                    isError = true;
                  });
                }
                if (UtilsImporter().commanUtils.isTextEmpty(_textEditingControllerLastName.text)) {
                  setState(() {
                    isLastNameAvailable = false;
                    isError = true;
                  });
                }
                if (_birthDay.toString() == "Date of Birth") {
                  setState(() {
                    isDateOfBirthAvailable = false;
                    isError = true;
                  });
                }

                // if (UtilsImporter() .commanUtils .isTextEmpty(_textEditingControllerPassword.text)) {
                //   setState(() {
                //     isPasswordAvailable = false;
                //     isError = true;
                //   });
                // }

                if (validatePassword(_textEditingControllerPassword.text)) {
                  setState(() {
                    isPasswordAvailable = false;
                    isError = true;
                  });

                  if (_textEditingControllerEmail.text.isNotEmpty && _textEditingControllerConfirmEmail.text.isNotEmpty &&
                      _textEditingControllerFirstName.text.isNotEmpty && _textEditingControllerLastName.text.isNotEmpty &&
                      _birthDay.toString() != "Date of Birth" && _textEditingControllerProviderCode.text.isNotEmpty)
                  {
                    UtilsImporter().commanUtils
                        .showAlertDialogWithTitleOkButton(
                          UtilsImporter().stringUtils.returnPasswordTitle
                          ,UtilsImporter().stringUtils.returnPasswordDescription,context );
                  }

                }


                if (validatePassword(_textEditingControllerConfirmPassword.text)) {
                  setState(() {
                    isConfirmPasswordAvailable = false;
                    isError = true;
                  });

                  if (_textEditingControllerEmail.text.isNotEmpty && _textEditingControllerConfirmEmail.text.isNotEmpty &&
                      _textEditingControllerFirstName.text.isNotEmpty && _textEditingControllerLastName.text.isNotEmpty &&
                      _birthDay.toString() != "Date of Birth" && _textEditingControllerProviderCode.text.isNotEmpty)
                  {
                    UtilsImporter().commanUtils
                        .showAlertDialogWithTitleOkButton(
                          UtilsImporter().stringUtils.returnPasswordTitle
                          ,UtilsImporter().stringUtils.returnPasswordDescription,context );
                  }

                }




                if (UtilsImporter().commanUtils.isTextEmpty(_textEditingControllerProviderCode.text))
                {
                  setState(() {
                    isProviderCodeAvailable = false;
                    isError = true;
                  });
                }

                if (_textEditingControllerEmail.text.isNotEmpty && _textEditingControllerConfirmEmail.text.isNotEmpty &&
                    _textEditingControllerFirstName.text.isNotEmpty && _textEditingControllerLastName.text.isNotEmpty &&
                    _birthDay.toString() != "Date of Birth" && !validatePassword(_textEditingControllerPassword.text) &&
                    !validatePassword(_textEditingControllerConfirmPassword.text) &&  _textEditingControllerProviderCode.text.isNotEmpty)
                {
                 // print("=======Success==>");
                  if( _textEditingControllerPassword.text == _textEditingControllerConfirmPassword.text )
                  {
                     doActivateAccount();

                  }else{
                    setState(() {
                      isConfirmPasswordAvailable = false;
                      isError = true;
                    });
                  }

                }
//                Navigator.push(context, MaterialPageRoute(builder: (context) {
//                  return ActivateAccount();
//                }));
              },
              child: Text("Activate account", textAlign: TextAlign.center, style: style.copyWith( fontSize: 15, color: Colors.white, fontWeight: FontWeight.bold)),
            ),
    );

    return Scaffold(
        backgroundColor: Colors.white,
        key: _scaffold,
        body: SafeArea(
            child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
              SizedBox(
                height: 15,
              ),
              Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SizedBox(
                    width: 5,
                  ),
                  Align(
                      alignment: Alignment.center,
                      child: InkWell(
                          child: Image.asset(
                            "assets/ic_back.png",
                            height: 25,
                            width: 40,
                          ),
                          onTap: () {
                            goBack(context);
                          })),
                ],
              ),
              new Container(
                margin: const EdgeInsets.only(top: 20),
                height: 1,
                color: getColorFromHex('#CCCCCC'),
              ),

              Expanded(
                  child: SingleChildScrollView(
                    child: Container(
                      child: Padding(
                        padding: const EdgeInsets.only(
                            top: 14, bottom: 20, left: 15, right: 15),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(10),
                              child: Text("Activate your account below!",
                                  style: TextStyle(
                                      color: getThemeBlackColor(),
                                      fontWeight: FontWeight.w600,
                                      fontSize: 30,
                                      fontFamily: getThemeFontName())),
                            ),
                            Padding(
                                padding: const EdgeInsets.only(left: 10, bottom: 4),
                                child: Visibility(
                                    maintainSize: true,
                                    maintainAnimation: true,
                                    maintainState: true,
                                    visible: isError,
                                    child: Text("Please fill in the required fields*",
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                            color: getRedColor(),
                                            fontWeight: FontWeight.bold,
                                            fontSize: 13,
                                            fontFamily: getThemeFontName())))),
                            new Align(
                                alignment: Alignment.center,
                                child: new Container(
                                  margin: const EdgeInsets.only(bottom: 10.0),
                                  padding:
                                      const EdgeInsets.only(left: 10.0, right: 10),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.stretch,
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: <Widget>[
                                      SizedBox(height: 10.0),
                                      Padding(
                                          padding: const EdgeInsets.only(
                                              left: 1, right: 1),
                                          child: emailField),
                                      SizedBox(height: 10.0),
                                      Padding(
                                          padding: const EdgeInsets.only(
                                              left: 1, right: 1),
                                          child: confirmEmailField),
                                      SizedBox(height: 10.0),
                                      Padding(
                                          padding: const EdgeInsets.only(
                                              left: 1, right: 1),
                                          child: firstNameField),
                                      SizedBox(height: 10.0),
                                      Padding(
                                          padding: const EdgeInsets.only(
                                              left: 1, right: 1),
                                          child: lastNameField),
                                      SizedBox(height: 10.0),
                                      Padding(
                                          padding: const EdgeInsets.only(
                                              left: 1, right: 1),
                                          child: birthDayField),
                                      SizedBox(height: 10.0),
                                      Padding(
                                          padding: const EdgeInsets.only( left: 1, right: 1),
                                          child: passwordField
                                      ),

                                      SizedBox(height: 10.0),

                                      Padding(
                                          padding: const EdgeInsets.only( left: 1, right: 1),
                                          child: confirmPasswordField
                                      ),

                                      SizedBox(height: 10.0),


                                      Padding(
                                          padding: const EdgeInsets.only(
                                              left: 1, right: 1),
                                          child: providerCodeField),
                                      SizedBox(height: 20.0),
                                      Center(
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: <Widget>[
                                            Text("By signing up you agree to ",
                                                textAlign: TextAlign.left,
                                                style: TextStyle(
                                                    color: getThemeBlackColor(),
                                                    fontWeight: FontWeight.w600,
                                                    fontSize: 13,
                                                    fontFamily: getThemeFontName())),
                                            SizedBox(
                                              width: 5,
                                            ),
                                            new Container(
                                              padding: const EdgeInsets.only(left: 0.0),
                                              decoration: myBoxDecorationTerms(),
                                              child: new GestureDetector(
                                                  child: Text("Terms of Service",
                                                      textAlign: TextAlign.left,
                                                      style: TextStyle(
                                                          color: getColorFromHex(
                                                              "#1b76bb"),
                                                          fontWeight: FontWeight.w600,
                                                          fontSize: 13,
                                                          fontFamily:
                                                              getThemeFontName())),
                                                  onTap: () {
                                                    Navigator.push(context,
                                                        MaterialPageRoute(
                                                            builder: (context) {
                                                      return PrivacyPolicy();
                                                    }));
                                                  }),
                                            ),
                                            SizedBox(
                                              height: 30,
                                            ),
                                          ],
                                        ),
                                      ),
                                      SizedBox(
                                        height: 50.0,
                                      ),
                                      Padding(
                                          padding: const EdgeInsets.only(
                                              left: 25, right: 25),
                                          child: activateAccountButton),
                                      SizedBox(
                                        height: 40.0,
                                      ),
                                      Center(
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: <Widget>[
                                            Text("Already have an account? ",
                                                textAlign: TextAlign.left,
                                                style: TextStyle(
                                                    color: getThemeGrayColor(),
                                                    fontWeight: FontWeight.w600,
                                                    fontSize: 13,
                                                    fontFamily: getThemeFontName())),
                                            SizedBox(
                                              width: 3,
                                            ),
                                            new Container(
                                              padding:
                                                  const EdgeInsets.only(left: 2.0),
                                              decoration: myBoxDecorationTerms(),
                                              child: new GestureDetector(
                                                  child: Text("Sign in",
                                                      textAlign: TextAlign.left,
                                                      style: TextStyle(
                                                          color: getColorFromHex(
                                                              "#1b76bb"),
                                                          fontWeight: FontWeight.w600,
                                                          fontSize: 13,
                                                          fontFamily:
                                                              getThemeFontName())),
                                                  onTap: () {
                                                    Navigator.push(context,
                                                        MaterialPageRoute(
                                                            builder: (context) {
                                                      return Signin();
                                                    }));
                                                  }),
                                            ),
                                            SizedBox(
                                              height: 30,
                                            ),
                                          ],
                                        ),
                                      ),
                                      SizedBox(
                                        height: 25.0,
                                      ),
                                    ],
                                  ),
                                ))
                          ],
                        ),
                      ),
                    ),
              ))
          ],
        )));
  }

  void doActivateAccount() {
    setState(() => isLoading = true);
    api
        .signUp(
            _textEditingControllerEmail.text.trim(),
            _textEditingControllerFirstName.text.trim(),
            _textEditingControllerLastName.text.trim(),
            _textEditingControllerPassword.text.trim(),
            _birthDay,
            _textEditingControllerProviderCode.text.trim())
        .then((user) {
      setState(() {
        setState(() => isLoading = false);
        print("response ==> " + user.toString());
        showThankYouDialog(context);
      });
    }, onError: (error) {
        // print("ERROR FOUND FINALLY HERE ===> " + error.toString());
        // setState(() {
        //   setState(() => isLoading = false);
        //   UtilsImporter()
        //       .commanUtils
        //       .showAlertDialogWithOkButton(error.toString(), context);
        // });

      print("ERROR FOUND FINALLY HERE ===> " + error.toString());
      setState(() => isLoading = false);

      _scaffold.currentState.showSnackBar(SnackBar(
        content: Text(error.toString(),
            textAlign: TextAlign.center,
            style: style.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold)),
        duration: Duration(seconds: 5),
      ));


    });
  }

  void showThankYouDialog(BuildContext context) {
    Dialog dialogThankYou;
    TextStyle style = TextStyle(fontFamily: 'Orkney', fontSize: 14.0);
    final continueButon = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: getThemeGreenColor(),
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () {
          Navigator.pop(context, true);

          Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(builder: (context) => Signin()),
              (Route<dynamic> route) => false);
        },
        child: Text("Continue",
            textAlign: TextAlign.center,
            style: style.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold)),
      ),
    );

    dialogThankYou = Dialog(
      child: Container(
        padding: EdgeInsets.only(bottom: 20),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(12),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(color: Colors.white),
                  child: Text(
                    "You account is now activated!",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                        fontFamily: 'Orkney'),
                  ),
                ),
                SizedBox(
                  width: 30,
                ),
              ],
            ),
            Container(
                margin: const EdgeInsets.only(left: 15, right: 15, top: 6),
                child: continueButon),
          ],
        ),
      ),
    );
    showDialog(
        context: context,
        useRootNavigator: false,
        builder: (BuildContext context) => dialogThankYou);
  }


}

BoxDecoration myBoxDecoration() {
  return BoxDecoration(
    border: Border(
      bottom: BorderSide(
        color: Color.fromRGBO(113, 127, 139, 1),
        //                   <--- border color
        width: 1.0,
      ),
    ),
  );
}

BoxDecoration myBoxDecorationTerms() {
  return BoxDecoration(
    border: Border(
      bottom: BorderSide(
        color: getColorFromHex("#1b76bb"),
        //                   <--- border color
        width: 1.0,
      ),
    ),
  );
}
