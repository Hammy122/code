import 'dart:convert';
import 'dart:io';

import 'package:circular_check_box/circular_check_box.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app/apiHelper/allquestions_response.dart';
import 'package:flutter_app/apiHelper/app_exceptions.dart';
import 'package:flutter_app/apiHelper/getSymptomlist_response.dart';
import 'package:flutter_app/persistance/api_repository.dart';
import 'package:flutter_app/screens/ChangePassword.dart';
import 'package:flutter_app/screens/ReportYourPain/ReportYourPainQuestionTwo.dart';
import 'package:flutter_app/screens/ReportYourSymptoms/ReportYourSymptomsTwo.dart';
import 'package:flutter_app/screens/ReportYourSymptoms/SymptomsList.dart';
import 'package:flutter_app/utils/ColorUtils.dart';
import 'package:flutter_app/utils/CustomAlertDialog.dart';
import 'package:flutter_app/utils/CustomPopupMenu.dart';
import 'package:flutter_app/models/ReachBean.dart';
import 'package:flutter_app/screens/login/Signin.dart';
import 'package:flutter_app/utils/app_widgets.dart';
import 'package:flutter_app/utils/utils.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:path_provider/path_provider.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:toast/toast.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../models/UserBean.dart';
import '../../utils/UtilsImporter.dart';
import 'package:http/http.dart' as http;

import '../ReportSubmitted.dart';

class ReportYourSymptoms extends StatefulWidget {
  static List<SymptomItemModel> symptomList = new List();
  static List<SymptomItemModel> filterSymptomList = new List();

  @override
  _ReportYourSymptomsState createState() => _ReportYourSymptomsState();
}

class _ReportYourSymptomsState extends State<ReportYourSymptoms> with WidgetsBindingObserver  {

  //ProgressDialog pr;
  List<SymptomItemModel> selectedList = new List();
  List<Datum> symptomsQuestions = new List();
  int currentQuestionIndex = 0;
  TextStyle style = TextStyle(fontFamily: getThemeFontName(), fontSize: 14.0);
  bool isLoading = true;
  bool isSymptomsLoaded = false;
  bool isQuestionsLoaded = false;
  String token = "";
  String deviceId = "12345678";
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool isDateAvailable = true;
  String _birthDay = "Select Date";
  bool isAnswerSubmited=false;
  bool isValuesLoaded = false;
  TextEditingController _textEditingSymptom = new TextEditingController();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    selectedList.clear();
    // ReportYourSymptoms.symptomList.clear();
    // ReportYourSymptoms.filterSymptomList.clear();
    // loadSymptoms().then((result) {
    //   setState(() {
    //     isSymptomsLoaded = true;
    //     print("SIZE ==> " + ReportYourSymptoms.symptomList.length.toString());
    //   });
    // });
    Future<String> token = UtilsImporter().preferencesUtils.getToken();
    Future<String> device = getDeviceId();
    token.then((data) {
      print("token ==> " + data);
      this.token = data;
      device.then((deviceId) {
        print("deviceID ==> " + deviceId);
        this.deviceId = deviceId;
        // getAllSymptoms();
        getAllQuestions();
      });
    });
  }

  void getAllQuestions() {
    setState(() => isQuestionsLoaded = false);
    var api = new APICallRepository();
    api.allquestion(deviceId, UtilsImporter().apiUtils.DAILY,
        UtilsImporter().apiUtils.SYMPTOMS, token)
        .then((response) {
      setState(() {
        setState(() => isQuestionsLoaded = true);
        symptomsQuestions = response.response.data;
        getAllSymptoms();
      });
    }, onError: (error) {
      setState(() {
        print("ERROR FOUND FINALLY HERE ===> " + error.toString());
        // setState(() => isLoading = false);
        if (error.toString().contains('Unauthorised')) {
          doLogout(context, error.toString());
        }
        if (error is ProviderDeactivatedException) {
          doLogout(context, error.toString());
        } else if (error is PatientDeactivatedException) {
          doLogout(context, error.toString());
        } else {
          _scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text(error.toString(),
                textAlign: TextAlign.center,
                style: style.copyWith(
                    color: Colors.white, fontWeight: FontWeight.bold)),
            duration: Duration(seconds: 5),
          ));
        }
      });
    });
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    print("didChangeAppLifecycleState");
    if (state == AppLifecycleState.resumed) {
      // user returned to our app
      print("resumed");
    } else if (state == AppLifecycleState.inactive) {
      // app is inactive
      print("inactive");
    } else if (state == AppLifecycleState.paused) {
      // user is about quit our app temporally
      print("paused");
    } else if (state == AppLifecycleState.detached) {
      // app suspended (not used in iOS)
      print("detached");
    } else {
      print("else");
    }
  }

  // loadSymptoms() async {
  //   String data = await rootBundle.loadString('assets/symptoms.json');
  //   var tagObjsJson = jsonDecode(data) as List;
  //   // print("tagObjsJson  ==>"+tagObjsJson.toString());
  //   ReportYourSymptoms.symptomList.clear();
  //   ReportYourSymptoms.symptomList =
  //       tagObjsJson.map((tagJson) => SymptomsItem.fromJson(tagJson)).toList();
  //   ReportYourSymptoms.filterSymptomList.clear();
  //   ReportYourSymptoms.filterSymptomList = ReportYourSymptoms.symptomList;
  //   print("total records  ==>" +
  //       ReportYourSymptoms.symptomList.length.toString());
  // }

  void getAllSymptoms() {
    setState(() => isSymptomsLoaded = false);
    var api = new APICallRepository();
    api.symptomslist(deviceId, token).then((response) {
      setState(() {
        setState(() => isSymptomsLoaded = true);
        setState(() => isLoading = false);
        // ReportYourSymptoms.symptomList.clear();
        ReportYourSymptoms.symptomList = response.response.data;

        // ReportYourSymptoms.filterSymptomList.clear();
        ReportYourSymptoms.filterSymptomList = response.response.data;
      });
    }, onError: (error) {
      setState(() {
        print("ERROR FOUND FINALLY HERE ===> " + error.toString());
        setState(() => isSymptomsLoaded = true);
        if (error.toString().contains('Unauthorised')) {
          doLogout(context, error.toString());
        } else if (error is ProviderDeactivatedException) {
          doLogout(context, error.toString());
        } else if (error is PatientDeactivatedException) {
          doLogout(context, error.toString());
        } else {
          _scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text(error.toString(),
                textAlign: TextAlign.center,
                style: style.copyWith(
                    color: Colors.white, fontWeight: FontWeight.bold)),
            duration: Duration(seconds: 5),
          ));
        }

      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
          child: isLoading
              ? Center(
            child: SizedBox(
              child: Padding(
                  padding: EdgeInsets.fromLTRB(15.0, 11.0, 15.0, 11.0),
                  child: CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation<Color>(
                          getThemeGreenColor()))),
              height: 50.0,
              width: 60.0,
            ),
          )
              : Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    // createToolBar(
                    //     false, 'Report your Symptoms', 'Question 1 of 4', 'Next'),

                    createToolBar( currentQuestionIndex >= 1, 'Report your Symptoms',
                        "Question " + (currentQuestionIndex + 1).toString() + " of " + symptomsQuestions.length.toString(),
                        currentQuestionIndex < (symptomsQuestions.length - 1) ? 'Next' : 'Finish'),

                    new Container(
                      margin: const EdgeInsets.only(top: 10),
                      height: 1,
                      color: getColorFromHex('#CCCCCC'),
                    ),

                    SizedBox(height: 20),

                    isSymptomsLoaded ?
                       Expanded(
                          child: Container(
                              //padding: const EdgeInsets.symmetric( horizontal: 30.0),
                              child: getLayout())
                       )
                          : SizedBox(
                              height: 30.0,
                              width: 30.0,
                              child: CircularProgressIndicator(
                                  valueColor:
                                  AlwaysStoppedAnimation(Colors.blue),
                                  strokeWidth: 2.0)
                              )
                  ]
          )
      ),
    );
  }

  Widget createToolBar(bool isBackButton, String title, String subTitle, String buttonLabel) {
    return Stack(
      children: [
        SizedBox(
          width: 8,
        ),
        Positioned(
            left: 10,
            top: isBackButton ? 20 : 10,
            child: Align(
                alignment: Alignment.center,
                child: InkWell(
                    child: isBackButton
                        ? Image.asset(
                      "assets/ic_back.png",
                      height: 25,
                      width: 40,
                    )
                        : Image.asset(
                      "assets/buttonsTopBarClose.png",
                      height: 40,
                      width: 40,
                    ),
                    onTap: () {
                     // goBack(context);

                      if (currentQuestionIndex <= 0)
                        goBack(context);
                      else {
                        setState(() {
                          if (currentQuestionIndex <= 0)
                            currentQuestionIndex = 0;
                          else
                            currentQuestionIndex--;
                        });
                      }


                    }))),
        Center(
            child: Column(
              children: <Widget>[
                new Padding(
                  padding: const EdgeInsetsDirectional.only(
                      top: 15.0, bottom: 0),
                  child: getTextWidget(
                      title, 17, Colors.black, FontWeight.bold),
                ),
                getTextWidget(
                    subTitle, 13, getThemeGreenColor(), FontWeight.w600),
              ],
            )),
        Positioned(
            right: 10,
            top: 10,
            child: Center(
                child:
                isSubmissionInProgress ?
                Center(
                  child: SizedBox(
                    child: Padding(
                        padding: EdgeInsets.fromLTRB(15.0, 11.0, 15.0, 11.0),
                        child: CircularProgressIndicator(
                            valueColor: AlwaysStoppedAnimation<Color>(
                                getThemeGreenColor()))),
                    height: 50.0,
                    width: 60.0,
                  ),
                ) :
                Container(
                    decoration: new BoxDecoration(
                      borderRadius: new BorderRadius.circular(4.0),
                      color: getThemeGreenColor(),
                    ),
                    child: InkWell(
                      child: Visibility(
                         // visible:  selectedList.length > 0,
                         /// visible:  isValuesLoaded ? isValueSelected() : false ,
                          visible:  isCheckVisibility() ,

                          child: Padding(
                              padding: EdgeInsets.only(
                                  left: 8, top: 6, right: 8, bottom: 6),
                              child: getTextWidget(buttonLabel, 17,
                                  Colors.white, FontWeight.bold))),
                      onTap: () {
                        goToNext(buttonLabel);
                      },
                    )))),
        SizedBox(
          width: 10,
        ),
      ],
    );
  }

  bool isCheckVisibility() {

    if (currentQuestionIndex == 0) {

      return isValuesLoaded ? isValueSelected() : false ;

    } else if (currentQuestionIndex == 1) {

      return isDateSelected();

    } else if (currentQuestionIndex == 2) {

      return isStartedSelected();
    } else {
      return isValuesLoaded ? isNormalValueSelected() : false ;
    }
  }

  bool getSelected() {
    for (int j = 0;
    j < symptomsQuestions[currentQuestionIndex].questionDetails.length;
    j++) {
      if (symptomsQuestions[currentQuestionIndex].questionDetails[j].isSelected)
        return true;
    }
    return false;
  }

  bool isValueSelected() {
    for (int j = 0; j < selectedList.length ; j++) {
      if (selectedList[j].isSelected)
        return true;
    }
    return false;
  }

  bool isDateSelected() {
    for (int j = 0; j < selectedList.length ; j++) {
      if (selectedList[j].symptomsDate == "Select")
        return false;
    }
    return true;
  }

  bool isStartedSelected() {

    for (int j = 0; j < selectedList.length ; j++) {
      if (selectedList[j].symptomsStarted == "Select")
        return false;
    }
    return true;
  }

  bool isNormalValueSelected() {
    for (int j = 0; j < selectedList.length ; j++) {
      if (selectedList[j].isNormalActivities)
        return true;
    }
    return false;
  }

  void goToNext(String buttonLabel) {
    // Navigator.push(context, MaterialPageRoute(builder: (context) {
    //   return ReportYourSymptomsTwo();
    // }));

    submitData(buttonLabel);

    // if (buttonLabel == 'Finish') {
    //   Navigator.push(context, MaterialPageRoute(builder: (context) {
    //     return ReportSubmitted();
    //   }));
    // } else {
    //   setState(() {
    //     currentQuestionIndex++;
    //   });
    // }
  }

  Widget getLayout() {

    final symptomField = Container(
      child: TextField(
        controller: _textEditingSymptom,
        cursorColor: getThemeBlackColor(),
        onChanged: (text) {
//          if (text.length > 0)
//            setState(() => isEmailAvailable = true);
//          else
//            setState(() => isEmailAvailable = false);
        },
        decoration: InputDecoration(
          border: InputBorder.none,
          contentPadding: EdgeInsets.fromLTRB(10.0, 2.0, 10.0, 2.0),
          hintText: "Write symptom",
        ),
        obscureText: false,
        style: style,
      ),
      decoration: BoxDecoration(
        color: getInputBoxFillColor(),
        border: Border.all(
          color: getInputBoxFillColor(),
          width: 1,
        ),
        borderRadius: BorderRadius.circular(2),
      ),
    );

    final searchButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: getThemeBlueColor(),
      child: isLoading
          ? Center(
        child: SizedBox(
          child: Padding(
              padding: EdgeInsets.fromLTRB(20.0, 18.0, 20.0, 18.0),
              child: CircularProgressIndicator()),
          height: 50.0,
          width: 60.0,
        ),
      )
          : MaterialButton(
        minWidth: MediaQuery
            .of(context)
            .size
            .width,
        padding: EdgeInsets.fromLTRB(20.0, 18.0, 20.0, 18.0),
        onPressed: () {
          print("Start");
//                Navigator.push(context, MaterialPageRoute(builder: (context) {
//                  return SymptomsList();
//                }));

          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => SymptomsList(selectedList),
            ),
          ).then((value) {
            //print("===added===>" + value.toString());
            if (value != null) {
              setState(() {
                isValuesLoaded = true;
                // selectedList.clear();
                //  selectedList.addAll(value);
              });
            }
          });

//                Navigator.push(
//                  context,
//                  MaterialPageRoute(
//                    builder: (context) => SymptomsList(),
//                  ),
//                ).then((value) {
////                  print("===selected===>" + value.toString());
////                  if (value != null) {
////                    setState(() {
////                      Symptom.symptomList.addAll(value);
////                    });
////                  }
//                });
        },
        child: Text("Search symptoms",
            textAlign: TextAlign.center,
            style: style.copyWith(
                fontSize: 15,
                color: Colors.white,
                fontWeight: FontWeight.bold)),
      ),
    );

   // if (symptomsQuestions[currentQuestionIndex].question.contains("What are your symptoms?"))
    if (symptomsQuestions[currentQuestionIndex].questionrange.contains("searchSymptoms"))
    {
      return Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[

            getTextWidget(symptomsQuestions[currentQuestionIndex].question, 20,  getThemeBlackColor(), FontWeight.normal),

            Padding(
                padding: const EdgeInsets.only(top: 52, left: 60, right: 60),
                child: searchButton
            ),

            SizedBox(height: 40.0,),

            InkWell(
                child: Container(
                    margin: const EdgeInsets.only(top: 10),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Align(
                            alignment: Alignment.center,
                            child: InkWell(
                              child: Image.asset(
                                "assets/ic_plus_green.png",
                                height: 35,
                                width: 35,
                              ),
                            )),

                        SizedBox(
                          width: 10,
                        ),

                        getTextWidgetWithAlign(
                            'My symptom is not here, I’ll add it manually',
                            15,
                            getThemeGreenColor(),
                            FontWeight.normal,
                            TextAlign.start),
//                      Expanded(child: addCustomSymptoms())
                      ],
                    )),
                onTap: () {

                  showDialog(
                      context: context,
                      builder: (context)
                      {
                        _textEditingSymptom.text = "";
                        bool isProgressLoading = false;
                        bool isDialogError = false;
                        String errorText = "";

                        return StatefulBuilder(
                        builder: (context,StateSetter dialogState) {
                          return AlertDialog(

                            title: Column(
                              children: [

                                getTextWidget("Add symptom manually ", 20, getThemeBlackColor(), FontWeight.bold),

                                getTextWidget("Write your current symptom", 15, getThemeBlackColor(), FontWeight.normal),
                                SizedBox(height: 5,),
                                Visibility(
                                    visible:  isDialogError,
                                    child: getTextWidget( errorText, 15, Colors.red , FontWeight.bold)),
                              ],

                            ),
                            content: symptomField,

                            actions: <Widget>[
                              TextButton(
                                child: const Text('CANCEL'),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                              isProgressLoading
                                  ? Center(
                                child: SizedBox(
                                  child: Padding(
                                      padding: EdgeInsets.fromLTRB(15.0, 11.0, 15.0, 11.0),
                                      child: CircularProgressIndicator(
                                          valueColor: AlwaysStoppedAnimation<Color>(
                                              Colors.blue))),
                                  height: 50.0,
                                  width: 60.0,
                                ), )
                                  : TextButton(
                                  child: const Text('SAVE'),
                                  onPressed: () {
                                    if (_textEditingSymptom.text.isEmpty) {
                                      return;
                                    }

                                    dialogState(() => isProgressLoading = true);
                                    var api = new APICallRepository();
                                    api.addSymptom(deviceId,_textEditingSymptom.text , token).then((response)
                                    {
                                      dialogState(() => isProgressLoading = false);
                                      setState(() {
                                        SymptomItemModel item = new SymptomItemModel();
                                        item.symptomsName = _textEditingSymptom.text;
                                        item.isSelected = true;
                                        item.isNormalActivities = false;
                                        item.symptomsDate = "Select";
                                        item.symptomsStarted = "Select";
                                        ReportYourSymptoms.symptomList.insert(0, item);
                                        selectedList.insert(0, item);
                                        _textEditingSymptom.text = "";
                                        Navigator.of(context).pop();
                                      });
                                    }, onError: (error) {
                                      dialogState(() {
                                        print("ERROR FOUND FINALLY HERE ===> " + error.toString());
                                        dialogState(() => isProgressLoading = false);
                                        if (error.toString().contains('Unauthorised')) {
                                          doLogout(context, error.toString());
                                        } else if (error is ProviderDeactivatedException) {
                                          doLogout(context, error.toString());
                                        } else if (error is PatientDeactivatedException) {
                                          doLogout(context, error.toString());
                                        } else {
                                          dialogState(() {

                                            isDialogError = true;
                                            errorText = error.toString().replaceAll("Error During Communication: ","");
                                            print("ERROR ===> $errorText" );
                                          });

                                          // _scaffoldKey.currentState.showSnackBar(SnackBar(
                                          //   content: Text(error.toString(),
                                          //       textAlign: TextAlign.center,
                                          //       style: style.copyWith(
                                          //           color: Colors.white, fontWeight: FontWeight.bold)),
                                          //   duration: Duration(seconds: 5),
                                          // ));
                                        }
                                      });
                                    });







                                  }),
                            ],
                          );
                        });


                      });
                }),

            new Container(
              margin: const EdgeInsets.only(top: 10),
              height: 0.5,
              color: getColorFromHex('#CCCCCC'),
            ),

            Expanded(
                child: Visibility(
                    visible: selectedList.length > 0,
                    child: Padding(
                        padding: const EdgeInsets.only(top: 30 , left: 20 , right: 20 ),
                        child: getAndSetSelectedSymptoms())))
          ]);
    }
    else if (symptomsQuestions[currentQuestionIndex].questionrange.isNotEmpty && symptomsQuestions[currentQuestionIndex].questionrange.contains("date"))
    {
      Future _chooseDate(BuildContext context, String initialDateString) async {
        var now = new DateTime.now();
        var initialDate = convertToDate(initialDateString) ?? now;
        initialDate = (initialDate.year >= now.year && initialDate.isAfter(now) ? initialDate : now);
            DatePicker.showDatePicker(context, showTitleActions: true, maxTime: now, onConfirm: (date) {
              setState(() {
                isDateAvailable = true;
                DateTime dt = date;
                String r = symptomsftDateAsStr(dt);
                _birthDay = r;
              });
            }, currentTime: initialDate);
      }

      final birthDayField = GestureDetector(
          onTap: () {
            _chooseDate(context, _birthDay);
          },
          child: Container(
            padding: EdgeInsets.fromLTRB(10.0, 15.0, 10.0, 15.0),
            child: Text("$_birthDay",
                style: TextStyle(
                    color: _birthDay.toString() == "Select Date"
                        ? getInputBoxHintColor()
                        : getThemeBlackColor(),
                    fontSize: 14,
                    fontFamily: getThemeFontName())),
            decoration: BoxDecoration(
              color: getInputBoxFillColor(),
              border: isDateAvailable
                  ? Border.all(
                color: getInputBoxFillColor(),
                width: 1,
              )
                  : Border.all(
                color: getRedColor(),
                width: 1,
              ),
              borderRadius: BorderRadius.circular(2),
            ),
          ));

/*      return Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            getTextWidget(symptomsQuestions[currentQuestionIndex].question, 20,
                getThemeBlackColor(), FontWeight.normal),
            Container(
                child: Padding(
                    padding:
                    const EdgeInsets.only(top: 52, left: 30, right: 30),
                    child: birthDayField))
          ]);
      */

      return getDateWidget();

    }
    else if(symptomsQuestions[currentQuestionIndex].questionrange.contains("dropdown"))
    {
      longDoYouList.clear();
      for (int i = 0; i < symptomsQuestions[currentQuestionIndex].questionDetails.length; i++) {
        longDoYouList.add(symptomsQuestions[currentQuestionIndex].questionDetails[i].text);
      }
      return getStartedSymptomsWidget();
    }
    else if(symptomsQuestions[currentQuestionIndex].questionrange.contains("selectApply"))
    {
      return getPreventNormalActivityWidget();
    }
    // else
    // {
    //   return getQuestionAnswerLayout(symptomsQuestions[currentQuestionIndex]);
    // }
  }

  Widget getDateWidget()
  {

    Future _chooseDate(BuildContext context, String initialDateString, int index) async {
      var now = new DateTime.now();

      var initialDate = convertToDate(initialDateString) ?? now;
      initialDate = (initialDate.year >= now.year && initialDate.isAfter(now) ? initialDate : now);

      DatePicker.showDatePicker(context, showTitleActions: true, maxTime: now, onConfirm: (date) {
        setState(() {
          DateTime dt = date;
          String r = symptomsDateAsStr(dt);
          selectedList[index].symptomsDate = r;
        });
      }, currentTime: initialDate);
    }

    return Center(

      child: Column(

        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [

          SizedBox(height: 10,),

          getTextWidget(symptomsQuestions[currentQuestionIndex].question, 20,
              getThemeBlackColor(), FontWeight.normal),

          SizedBox(height: 30,),

          Expanded(
              child: Padding(
                  padding: const EdgeInsets.only( left: 10 ),
                  child: ListView.separated(
                    separatorBuilder: (context, index) =>
                        Divider(
                          color: getInputHintTextColor(),
                          height: 0.5,
                        ),
                    itemCount: selectedList.length,
                    itemBuilder: (context, index) {

                      return ListTile(
                        title: getTextWidgetWithAlign(selectedList[index].symptomsName, 17, getThemeBlackColor(), FontWeight.normal, TextAlign.start),

                        subtitle: Visibility(
                          child:  getTextWidgetWithAlign(selectedList[index].symptomsDate != "Select" ? selectedList[index].symptomsDate : "", 17,  getThemeBlackColor(), FontWeight.normal, TextAlign.start),
                          visible: selectedList[index].symptomsDate != "Select" ? true : false,
                        ),

                        trailing: InkWell(
                          child: getTextWidgetWithAlign(selectedList[index].symptomsDate == "Select" ? "Select Date" : "Edit Date", 17, getThemeGreenColor(), FontWeight.normal, TextAlign.start),
                          onTap:(){
                            _chooseDate(context, "Select Date", index);
                          },
                        ),

                      );
                    },
                  )
              )
          )

        ],
      ),
    );
  }

  int _changedNumber = 0, _selectedNumber = 1;
  List<String> longDoYouList = <String>[];
  // static const List<String> longDoYouList = const <String>[
  //   'Comes and Goes',
  //   'Constant',
  //   'Seconds',
  //   'Minutes',
  //   'Hours',
  // ];


  Widget getStartedSymptomsWidget()
  {
    return Center(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,

        children: [

          SizedBox(height: 10,),

          getTextWidget(symptomsQuestions[currentQuestionIndex].question, 20,
              getThemeBlackColor(), FontWeight.normal),

          SizedBox(height: 30,),

          Expanded(
              child: Padding(
                  padding: const EdgeInsets.only( left: 10 ),
                  child: ListView.separated(
                    separatorBuilder: (context, index) =>
                        Divider(
                          color: getInputHintTextColor(),
                          height: 0.5,
                        ),
                    itemCount: selectedList.length,
                    itemBuilder: (context, index) {

                      return ListTile(
                        title: getTextWidgetWithAlign(selectedList[index].symptomsName, 17, getThemeBlackColor(), FontWeight.normal, TextAlign.start),

                        subtitle: Visibility(
                          child:  getTextWidgetWithAlign(selectedList[index].symptomsStarted != "Select" ? selectedList[index].symptomsStarted : "Select", 17, getThemeGrayColor(), FontWeight.normal, TextAlign.start),
                          visible: selectedList[index].symptomsStarted != "Select" ? true : false,
                        ),

                        trailing: InkWell(
                          child: getTextWidgetWithAlign(selectedList[index].symptomsStarted == "Select" ? "Select" : "Edit", 17, getThemeGreenColor(), FontWeight.normal, TextAlign.start),
                          onTap:(){


                            showModalBottomSheet(
                                context: context,
                                builder: (BuildContext context) {

                                  return Container(
                                    height: 200.0,
                                    color: Colors.white,

                                    child: Row(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: <Widget>[
                                        CupertinoButton(
                                          child: getTextWidgetWithAlign("Cancel", 17, getThemeGrayColor(), FontWeight.normal, TextAlign.start),
                                          onPressed: () {
                                            Navigator.pop(context);
                                          },
                                        ),

                                        Expanded(
                                          child: CupertinoPicker(
                                              scrollController: new FixedExtentScrollController(
                                                initialItem: _selectedNumber,
                                              ),
                                              itemExtent: 50.0,
                                              backgroundColor: Colors.white,
                                              onSelectedItemChanged: (int index) {
                                                _changedNumber = index;
                                              },
                                              children: new List<Widget>.generate(longDoYouList.length,
                                                      (int index) {
                                                    return new Center(
                                                      child: new Text(longDoYouList[index]),
                                                    );
                                                  })),
                                        ),

                                        CupertinoButton(
                                          child: getTextWidgetWithAlign("Done", 17, getThemeGreenColor(), FontWeight.normal, TextAlign.end),
                                          onPressed: () {
                                            setState(() {
                                             _selectedNumber = _changedNumber;
                                             selectedList[index].symptomsStarted = longDoYouList[_changedNumber];
                                            });
                                            Navigator.pop(context);
                                          },
                                        ),
                                      ],
                                    ),








                                  );
                                });
                          },
                        ),

                      );
                    },
                  )
              )
          )
        ],
      ),
    );

  }

  Widget getPreventNormalActivityWidget()
  {
    return Center(

      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,

        children: [
          SizedBox(height: 10,),

          getTextWidget(symptomsQuestions[currentQuestionIndex].question, 20, getThemeBlackColor(), FontWeight.normal),

          SizedBox(height: 30,),

          getTextWidget("Select all that apply", 16, getThemeGrayColor(), FontWeight.normal),

          SizedBox(height: 20,),

          Expanded(
              child:
              Padding(
                  padding: const EdgeInsets.only( left: 10 ),
                  child: ListView.separated(
                    separatorBuilder: (context, index) =>
                        Divider(
                          color: getInputHintTextColor(),
                          height: 0.5,
                        ),
                    itemCount: selectedList.length,
                    itemBuilder: (context, index) {
                      return InkWell(
                        child: checkbox(selectedList[index].symptomsName, selectedList[index].isNormalActivities, index , false),
                        onTap: () {
                          setState(() {
                            if (selectedList[index].isNormalActivities)
                              selectedList[index].isNormalActivities = false;
                            else
                              selectedList[index].isNormalActivities = true;
                          });
                        },
                      );
                    },
                  )
              )
          )
        ],
      ),
    );

  }

  Widget getQuestionAnswerLayout(Datum monthlyPainQuestion) {
    return  Container(
          margin: const EdgeInsets.all(20),
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                getTextWidget(
                    monthlyPainQuestion
                        .question,
                    20,
                    getThemeBlackColor(),
                    FontWeight.normal),
                new Center(
                    child: Visibility(
                        visible: monthlyPainQuestion
                            .Questionsubtitle
                            .length >
                            0,
                        child: Html(
                          data: monthlyPainQuestion
                              .Questionsubtitle
                              .toString(),
                        ))),
                SizedBox(
                  height: 45,
                ),
                new Expanded(
                  child: getAnswersLayout(
                      symptomsQuestions[
                      currentQuestionIndex]),
                )
              ]),
    );
  }

  Widget getAnswersLayout(Datum monthlyPainQuestion) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
              child: Container(
                  margin: const EdgeInsets.only(left: 15, right: 15),
                  color: Colors.white,
                  child: ListView.builder(
                    itemCount: monthlyPainQuestion.questionDetails.length,
                    itemBuilder: (context, index) {
                      return _getItemCell(
                          index: index,
                          title: monthlyPainQuestion.questionDetails[index].text,
                          isSelected:
                          monthlyPainQuestion.questionDetails[index].isSelected);
                    },
                    padding: EdgeInsets.all(0.0),
                  ))),
        ]);
  }

  Widget _getItemCell({int index, String title, bool isSelected}) {
    return new GestureDetector(
      child: Card(
          margin: EdgeInsets.symmetric(vertical: 10),
          color: isSelected
              ? getColorFromHex('#1a75ba')
              : getColorFromHex("#ededf2"),
          child: Padding(
              padding: const EdgeInsetsDirectional.only(
                top: 25.0,
                bottom: 25,
              ),
              child: getTextWidget(title, 15,
                  isSelected ? Colors.white : Colors.black, FontWeight.bold))),
      onTap: () {
        setState(() {
          unSelectAll();
          isSelected = true;
          setSelectedAnswer(index);
        });
      },
    );
  }

  void unSelectAll() {
    for (int i = 0; i < symptomsQuestions.length; i++) {
      for (int j = 0; j < symptomsQuestions[i].questionDetails.length; j++) {
        symptomsQuestions[i].questionDetails[j].isSelected = false;
      }
    }
  }

  void setSelectedAnswer(int answerIndex) {
    symptomsQuestions[currentQuestionIndex]
        .questionDetails[answerIndex]
        .isSelected = true;
  }

  Widget getAndSetSelectedSymptoms() {
    print("selected length==>" + selectedList.length.toString());
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          getTextWidget('Your symptoms added:', 16, getColorFromHex('#8c8c8c'),
              FontWeight.normal),

          SizedBox(height: 10,),

          Expanded(
              child: ListView.separated(
                separatorBuilder: (context, index) =>
                    Divider(
                      color: getInputHintTextColor(),
                      height: 0.5,
                    ),
                itemCount: selectedList.length,
                itemBuilder: (context, index) {
                  return InkWell(
                    child: checkbox(selectedList[index].symptomsName,
                        selectedList[index].isSelected, index , true),
//                  ListTile(
//                    title: getTextWidgetWithAlign(symptomList[index].name, 17,
//                        getThemeBlackColor(), FontWeight.normal, TextAlign.start),
//                    trailing: Visibility(
//                      child: Icon(Icons.check, color: getThemeGreenColor()),
//                      visible: symptomList[index].isSelected,
//                    ),
//                  ),
                    onTap: () {
                      setState(() {
                        if (selectedList[index].isSelected)
                          selectedList[index].isSelected = false;
                        else
                          selectedList[index].isSelected = true;
                      });
//            setSaveButtonVisibility();
                    },
                  );
                },
              ))
        ]);
  }

  Widget checkbox(String title, bool boolValue, int index , bool isSymptom) {
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        CircularCheckBox(
          activeColor: getThemeGreenColor(),
          value: boolValue,
          materialTapTargetSize: MaterialTapTargetSize.padded,
          onChanged: (bool value) {
              if(isSymptom)
                {
                  setState(() {
                    ReportYourSymptoms.symptomList[index].isSelected = value;
                  });
                }
          },
        ),
        getTextWidget(
            title,
            15,
            boolValue ? getThemeGreenColor() : Colors.black,
            boolValue ? FontWeight.bold : FontWeight.normal)
      ],
    );
  }

  var dailyQueAnsList = new List<Map<String, String>>();
  var selected_Question_details =  [];
  var selected_Question_value =  [];
  File file;
  bool isSubmissionInProgress = false;

  _writeJson(String text) async {
    final Directory directory = await getApplicationDocumentsDirectory();
    file = File('${directory.path}/report_data.json');
    await file.writeAsString(text);
  }

  void submitData(String buttonLabel)
  {
    selected_Question_details.clear();
    selected_Question_value.clear();

    if (symptomsQuestions[currentQuestionIndex].questionrange.contains("searchSymptoms"))
    {
      for (int j = 0; j < selectedList.length ; j++) {
        if(selectedList[j].isSelected) {
            selected_Question_value.add(selectedList[j].symptomsName);
            selected_Question_details.add(selectedList[j].symptomsName);
        }
      }
    }
    else if (symptomsQuestions[currentQuestionIndex].questionrange.isNotEmpty && symptomsQuestions[currentQuestionIndex].questionrange.contains("date"))
    {
      for (int j = 0; j < selectedList.length ; j++) {
        selected_Question_value.add(selectedList[j].symptomsName);
        var date = selectedList[j].symptomsDate.toString().replaceAll("/", "-");
        selected_Question_details.add(date);
      }

    }
    else if(symptomsQuestions[currentQuestionIndex].questionrange.contains("dropdown"))
    {
      for (int j = 0; j < selectedList.length ; j++) {
        selected_Question_value.add(selectedList[j].symptomsName);
        selected_Question_details.add(selectedList[j].symptomsStarted);
      }
    }
    else if(symptomsQuestions[currentQuestionIndex].questionrange.contains("selectApply"))
    {
      for (int j = 0; j < selectedList.length ; j++) {
        selected_Question_value.add(selectedList[j].symptomsName);
        if(selectedList[j].isNormalActivities) {
          selected_Question_details.add("yes");
        }else {
          selected_Question_details.add("no");
        }
      }
    }

    Map map = new Map<String, String>();
    map["Question_section"] = UtilsImporter().apiUtils.SYMPTOMS;
    map["Question"] =  symptomsQuestions[currentQuestionIndex].id.toString();
    map["Question_value"] = selected_Question_value.toString().replaceAll("[", "").replaceAll("]", "");
    map["report_type"] = UtilsImporter().apiUtils.DAILY;
    map["generalsymptoms"] = selected_Question_details.toString().replaceAll("[", "").replaceAll("]", "");
    dailyQueAnsList.add(map);

    if (buttonLabel == 'Finish')
    {
      setState(() {
        isSubmissionInProgress = true;
      });
      print("MapList==> " + json.encode(dailyQueAnsList));
      _writeJson(json.encode(dailyQueAnsList)).then((result)
      {
        var api = new APICallRepository();
        api.addWeeklyReport(token, deviceId, file).then((response) {
          setState(() {
            isSubmissionInProgress = false;
            print("Success ===> " + response.toString());
            Navigator.push(context, MaterialPageRoute(builder: (context) {
              return ReportSubmitted();
            }));
          });
        }, onError: (error) {
          setState(() {
            isSubmissionInProgress = false;
            print("ERROR FOUND FINALLY HERE ===> " + error.toString());
            if (error.toString().contains('Unauthorised')) {
              doLogout(context, error.toString());
            } else if (error is ProviderDeactivatedException) {
              doLogout(context, error.toString());
            } else if (error is PatientDeactivatedException) {
              doLogout(context, error.toString());
            } else {
              _scaffoldKey.currentState.showSnackBar(SnackBar(
                content: Text(error.toString(),
                    textAlign: TextAlign.center,
                    style: style.copyWith(
                        color: Colors.white, fontWeight: FontWeight.bold)),
                duration: Duration(seconds: 5),
              ));
            }
          });
        });
      });
    } else {
      setState(() {
        currentQuestionIndex++;
      });
    }
  }
}
