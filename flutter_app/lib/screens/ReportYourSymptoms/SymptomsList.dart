import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app/apiHelper/getSymptomlist_response.dart';
import 'package:flutter_app/models/SymptomsItem.dart';
import 'package:flutter_app/screens/ChangePassword.dart';
import 'package:flutter_app/screens/ReportYourPain/ReportYourPainQuestionTwo.dart';
import 'package:flutter_app/screens/ReportYourSymptoms/AddCustomSymptom.dart';
import 'package:flutter_app/utils/ColorUtils.dart';
import 'package:flutter_app/utils/CustomPopupMenu.dart';
import 'package:flutter_app/models/ReachBean.dart';
import 'package:flutter_app/screens/login/Signin.dart';
import 'package:flutter_app/utils/app_widgets.dart';
import 'package:flutter_app/utils/utils.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:sticky_headers/sticky_headers/widget.dart';
import 'package:toast/toast.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../models/UserBean.dart';
import '../../utils/UtilsImporter.dart';
import 'package:http/http.dart' as http;

import 'ReportYourSymptoms.dart';

class SymptomsList extends StatefulWidget {
  List<SymptomItemModel> selectedList;

  SymptomsList(this.selectedList);

  @override
  _SymptomsListState createState() => _SymptomsListState(this.selectedList);
}

class _SymptomsListState extends State<SymptomsList> {
  _SymptomsListState(this.selectedList);

  List<SymptomItemModel> selectedList;

  TextStyle style = TextStyle(fontFamily: getThemeFontName(), fontSize: 14.0);
  String selectedAnswer = "XYZ";
  bool isSymptomsLoaded = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    ReportYourSymptoms.filterSymptomList = ReportYourSymptoms.symptomList;

  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,

              children: <Widget>[

                createToolBar(false, 'Select all that applies', '', 'Finish'),

                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,

                  children: <Widget>[
                    Container(
                        padding: const EdgeInsets.only(left: 15, right: 0, top: 0),
                        child: GestureDetector(
                          child: getTextWidget("Cancel", 17,
                              getColorFromHex("#007bff"), FontWeight.normal),
                          onTap: () {
                            goBack(context);
                          },
                        )),
                    Visibility(
                      child: GestureDetector(
                          child: Container(
                              padding:
                                  const EdgeInsets.only(left: 0, right: 15, top: 0),
                              child: getTextWidget("Save", 17,
                                  getColorFromHex("#007bff"), FontWeight.normal)),
                          onTap: () {
                            Navigator.pop(context, selectedList);
                            //  goBack(context);
                          }),
                      visible: selectedList.length > 0,
                    )
                  ],
                ),

                SizedBox(height: 20),


                Expanded(child: Container(child: getLayout()))

//            isSymptomsLoaded
//                ? Expanded(child: Container(child: getLayout()))
//                : SizedBox(
//                    height: 30.0,
//                    width: 30.0,
//                    child: CircularProgressIndicator(
//                        valueColor: AlwaysStoppedAnimation(Colors.blue),
//                        strokeWidth: 2.0)),

              ]
          )
      ),
    );
  }

  Widget createToolBar(
      bool isBackButton, String title, String subTitle, String buttonLabel) {
    return Stack(
      children: [
        SizedBox(
          width: 8,
        ),
        Center(
          child: Padding(
            padding: const EdgeInsetsDirectional.only(top: 15.0, bottom: 0),
            child: getTextWidget(title, 15, Colors.black, FontWeight.normal),
          ),
        ),
        SizedBox(
          width: 10,
        ),
      ],
    );
  }

  void goToNext() {
//    Navigator.push(context, MaterialPageRoute(builder: (context) {
//      return ReportYourPainQuestionTwo();
//    }));
  }

  Widget getLayout() {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Padding(
              padding: const EdgeInsetsDirectional.only(
                  top: 11.0, start: 20, end: 20, bottom: 20),
              child: Center(
                  child: Container(
                child: TextField(
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.all(15.0),
                    prefixIcon: Padding(
                      padding: const EdgeInsetsDirectional.only(end: 0.0),
                      child:
                          Icon(Icons.search, color: getColorFromHex("#BFC5CC")),
                    ),
                    hintText: 'Search symptoms',
                    labelStyle: TextStyle(
                        color: getColorFromHex("#717F8B"),
                        fontWeight: FontWeight.normal,
                        fontSize: 18,
                        fontFamily: getThemeFontName()),
                    border: InputBorder.none,
                  ),
                  obscureText: false,
                  style: style,
                  onChanged: (string) {
                    setState(() {
                      ReportYourSymptoms.filterSymptomList = ReportYourSymptoms
                          .symptomList
                          .where((u) => u.symptomsName
                              .toLowerCase()
                              .contains(string.toLowerCase()))
                          .toList();
                    });
                  },
                ),
                decoration: BoxDecoration(
                  color: getColorFromHex("#F2F3F5"),
                  border: Border.all(
                    color: Colors.white,
                    width: 0,
                  ),
                  borderRadius: BorderRadius.circular(12),
                ),
              ))),
          new Container(
            height: 1,
            color: getColorFromHex('#CCCCCC'),
          ),

/*
          InkWell(
              child: Container(
                  margin: const EdgeInsets.only(top: 10),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Align(
                          alignment: Alignment.center,
                          child: InkWell(
                            child: Image.asset(
                              "assets/ic_plus_green.png",
                              height: 35,
                              width: 35,
                            ),
                          )),
                      SizedBox(
                        width: 10,
                      ),
                      getTextWidgetWithAlign(
                          'My symptom is not here, I’ll add it manually',
                          15,
                          getThemeGreenColor(),
                          FontWeight.normal,
                          TextAlign.start),
//                      Expanded(child: addCustomSymptoms())
                    ],
                  )),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => AddCustomSymptom(),
                  ),
                ).then((value) {
                  print("===added===>" + value.toString());
                  if(value != null )
                    {
                      if (value.toString().isNotEmpty) {
                        setState(() {
                          SymptomItemModel item = new SymptomItemModel();
                          item.symptomsName = value;
                          item.isSelected = true;
                          ReportYourSymptoms.symptomList.insert(0, item);
                          // ReportYourSymptoms.symptomList.add(item);
//                      selectedList.add(item);

                          selectedList.insert(0, item);
                          // ReportYourSymptoms.filterSymptomList.add(item);
                        });
                      }
                    }

                });
              }),

          new Container(
            margin: const EdgeInsets.only(top: 10),
            height: 1,
            color: getColorFromHex('#CCCCCC'),
          ),

*/

          Expanded(child: Container(child: getListView()))
        ]);
  }

//  Widget addCustomSymptoms() {
//    print("=== TOTAL ==> " + custom_symptomList.length.toString());
//    return Column(children: <Widget>[
//      for (var symptom in custom_symptomList)
//        InkWell(
//          child: ListTile(
//            title: getTextWidgetWithAlign(symptom.name, 17,
//                getThemeBlackColor(), FontWeight.normal, TextAlign.start),
//            trailing: Visibility(
//              child: Icon(Icons.check, color: getThemeGreenColor()),
//              visible: symptom.isSelected,
//            ),
//          ),
//          onTap: () {
//            setState(() {
//              if (symptom.isSelected)
//                symptom.isSelected = false;
//              else
//                symptom.isSelected = true;
//            });
//            setSaveButtonVisibility();
//          },
//        )
//    ]);
//  }

//  Widget getListView() {
//    return ListView.builder(itemBuilder: (context, index) {
//      return StickyHeader(
//        header: Container(
//          height: 50.0,
//          color: getColorFromHex("#F2F3F5"),
//          padding: EdgeInsets.symmetric(horizontal: 16.0),
//          alignment: Alignment.centerLeft,
//          child: getTextWidgetWithAlign('Header #$index', 15,
//              getThemeBlackColor(), FontWeight.bold, TextAlign.start),
//        ),
//        content: Container(
//          child: Image.network(
//              "https://cdn.pixabay.com/photo/2015/12/01/20/28/road-1072823__340.jpg",
//              fit: BoxFit.cover,
//              width: double.infinity,
//              height: 200.0),
//        ),
//      );
//    });
//  }
  Widget getListView() {
    return ListView.separated(
        physics: const AlwaysScrollableScrollPhysics(),
      separatorBuilder: (context, index) => Divider(
        color: getInputHintTextColor(),
        height: 0.5,
      ),
      itemCount: ReportYourSymptoms.filterSymptomList.length,
      itemBuilder: (context, index) {
        return InkWell(
          child: ListTile(
            title: getTextWidgetWithAlign(
                ReportYourSymptoms.filterSymptomList[index].symptomsName,
                17,
                getThemeBlackColor(),
                FontWeight.normal,
                TextAlign.start),
            trailing: Visibility(
              child: Icon(Icons.check, color: getThemeGreenColor()),
              visible: ReportYourSymptoms.filterSymptomList[index].isSelected,
            ),
          ),
          onTap: () {
            setState(() {
              if (ReportYourSymptoms.filterSymptomList[index].isSelected) {
                ReportYourSymptoms.filterSymptomList[index].isSelected = false;
                ReportYourSymptoms.symptomList[index].isSelected = false;

//                SymptomsItem item = new SymptomsItem();
//                item.name = ReportYourSymptoms.filterSymptomList[index].name;
//                item.isSelected = false;
//                if(selectedList.length>0)
//                selectedList.remove(item);
                removeThisItem(  ReportYourSymptoms.filterSymptomList[index].symptomsName);
              } else {
                SymptomItemModel item = new SymptomItemModel();
                ReportYourSymptoms.filterSymptomList[index].isSelected = true;
                ReportYourSymptoms.symptomList[index].isSelected = true;
                item.symptomsName = ReportYourSymptoms.filterSymptomList[index].symptomsName;
                item.isSelected = true;
                item.isNormalActivities = false;
                item.symptomsDate = "Select";
                item.symptomsStarted = "Select";
                selectedList.add(item);
              }

              //  setSaveButtonVisibility();
            });
//            setState(() {
//              if (ReportYourSymptoms.filterSymptomList[index].isSelected) {
//                ReportYourSymptoms.filterSymptomList[index].isSelected = false;
//                ReportYourSymptoms.symptomList[index].isSelected = false;
//              } else {
//                ReportYourSymptoms.filterSymptomList[index].isSelected = true;
//                ReportYourSymptoms.symptomList[index].isSelected = true;
//                print("==ADD it ==> "+ReportYourSymptoms.symptomList[index].name);
//               // ReportYourSymptoms.selectedList.add(ReportYourSymptoms.symptomList[index]);
////                addThis(ReportYourSymptoms.symptomList[index]);
//              }
//            });
            // setSaveButtonVisibility();
          },
        );
      },
    );
  }

  removeThisItem(String name) {
    int index = 0;
    for (int i = 0; i < selectedList.length; i++) {
      if (selectedList[i].symptomsName == name) {
        index = i;
        break;
      }
    }
    selectedList.removeAt(index);
  }

//  void addThis(SymptomsItem item) async {
//    try {
//      ReportYourSymptoms.selectedList.add(item);
//    } catch (exception) {
//      print(exception.toString());
//    }
//  }

//  void setSaveButtonVisibility() {
//    setState(() {
//       selectedList.clear();
//    });
////      ReportYourSymptoms.selectedList.clear();
//    for (int i = 0; i < ReportYourSymptoms.filterSymptomList.length; i++) {
//      if (ReportYourSymptoms.filterSymptomList[i].isSelected) {
//        print(
//            "===> selected ==>" + ReportYourSymptoms.filterSymptomList[i].name);
//        setState(() {
//          ReportYourSymptoms.selectedList
//              .add(ReportYourSymptoms.filterSymptomList[i]);
//        });
////          ReportYourSymptoms.selectedList.add(ReportYourSymptoms.filterSymptomList[i]);
//      }
//    }

//    for (int i = 0; i < custom_symptomList.length; i++) {
//      if (custom_symptomList[i].isSelected)
//        setState(() {
//          Symptom.selectedList.add(Symptom.filterSymptomList[i]);
//        });
//      break;
//    }
//  }
}
