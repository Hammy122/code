import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_app/apiHelper/app_exceptions.dart';
import 'package:flutter_app/persistance/api_repository.dart';
import 'package:flutter_app/screens/ChangePassword.dart';
import 'package:flutter_app/screens/ReportYourPain/ReportYourPainQuestionTwo.dart';
import 'package:flutter_app/screens/ReportYourSymptoms/SymptomsList.dart';
import 'package:flutter_app/utils/ColorUtils.dart';
import 'package:flutter_app/utils/CustomPopupMenu.dart';
import 'package:flutter_app/models/ReachBean.dart';
import 'package:flutter_app/screens/login/Signin.dart';
import 'package:flutter_app/utils/app_widgets.dart';
import 'package:flutter_app/utils/utils.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:toast/toast.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../models/UserBean.dart';
import '../../utils/UtilsImporter.dart';
import 'package:http/http.dart' as http;

class AddCustomSymptom extends StatefulWidget {
  @override
  _AddCustomSymptomState createState() => _AddCustomSymptomState();
}

class _AddCustomSymptomState extends State<AddCustomSymptom> {
  //ProgressDialog pr;
  TextStyle style = TextStyle(fontFamily: getThemeFontName(), fontSize: 14.0);
  String selectedAnswer = "XYZ";
  bool isLoading = false;
  String token = "";
  String deviceId = "12345678";
  TextEditingController _textEditingSymptom = new TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future<String> token = UtilsImporter().preferencesUtils.getToken();
    Future<String> device = getDeviceId();
    token.then((data) {
      print("token ==> " + data);
      this.token = data;
      device.then((deviceId) {
        print("deviceID ==> " + deviceId);
        this.deviceId = deviceId;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      body: SafeArea(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
            createToolBar(true, 'Add Symptom', '', 'Finish'),
            new Container(
              margin: const EdgeInsets.only(top: 10),
              height: 1,
              color: getColorFromHex('#CCCCCC'),
            ),
            SizedBox(height: 20),
            Expanded(
                child: SingleChildScrollView(
                    padding: const EdgeInsets.symmetric(horizontal: 30.0),
                    child: getLayout()))
          ])),
    );
  }

  Widget createToolBar(
      bool isBackButton, String title, String subTitle, String buttonLabel) {
    return Stack(
      children: [
        SizedBox(
          width: 8,
        ),
        Positioned(
            left: 10,
            top: isBackButton ? 20 : 10,
            child: Align(
                alignment: Alignment.center,
                child: InkWell(
                    child: isBackButton
                        ? Image.asset(
                            "assets/ic_back.png",
                            height: 25,
                            width: 40,
                          )
                        : Image.asset(
                            "assets/buttonsTopBarClose.png",
                            height: 40,
                            width: 40,
                          ),
                    onTap: () {
                      goBack(context);
                    }))),
        Center(
            child: Column(
          children: <Widget>[
            new Padding(
              padding: const EdgeInsetsDirectional.only(top: 15.0, bottom: 0),
              child: getTextWidget(title, 17, Colors.black, FontWeight.bold),
            ),
            getTextWidget(subTitle, 13, getThemeGreenColor(), FontWeight.w600),
          ],
        )),
        Positioned(
            right: 10,
            top: 10,
            child: Center(
                child: Container(
                    decoration: new BoxDecoration(
                      borderRadius: new BorderRadius.circular(4.0),
                      color: getThemeGreenColor(),
                    ),
                    child: InkWell(
                      child: Visibility(
                          visible: selectedAnswer != "XYZ",
                          child: Padding(
                              padding: EdgeInsets.only(
                                  left: 8, top: 6, right: 8, bottom: 6),
                              child: getTextWidget(buttonLabel, 17,
                                  Colors.white, FontWeight.bold))),
                      onTap: () {
                        goToNext();
                      },
                    )))),
        SizedBox(
          width: 10,
        ),
      ],
    );
  }

  void goToNext() {
//    Navigator.push(context, MaterialPageRoute(builder: (context) {
//      return ReportYourPainQuestionTwo();
//    }));
  }

  Widget getLayout() {
    final symptomField = Container(
      child: TextField(
        controller: _textEditingSymptom,
        cursorColor: getThemeBlackColor(),
        onChanged: (text) {
//          if (text.length > 0)
//            setState(() => isEmailAvailable = true);
//          else
//            setState(() => isEmailAvailable = false);
        },
        decoration: InputDecoration(
          border: InputBorder.none,
          contentPadding: EdgeInsets.fromLTRB(10.0, 2.0, 10.0, 2.0),
          hintText: "Write symptom",
        ),
        obscureText: false,
        style: style,
      ),
      decoration: BoxDecoration(
        color: getInputBoxFillColor(),
        border: Border.all(
          color: getInputBoxFillColor(),
          width: 1,
        ),
        borderRadius: BorderRadius.circular(2),
      ),
    );

    final addButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: getThemeBlueColor(),
      child: isLoading
          ? Center(
              child: SizedBox(
                child: Padding(
                    padding: EdgeInsets.fromLTRB(15.0, 11.0, 15.0, 11.0),
                    child: CircularProgressIndicator(
                        valueColor:
                            AlwaysStoppedAnimation<Color>(Colors.white))),
                height: 50.0,
                width: 60.0,
              ),
            )
          : MaterialButton(
              minWidth: MediaQuery.of(context).size.width,
              padding: EdgeInsets.fromLTRB(20.0, 18.0, 20.0, 18.0),
              onPressed: () {
//                 Navigator.push(context, MaterialPageRoute(builder: (context) {
//                  return SymptomsList();
////                        return HumanAnatomy();
//                }));
                if (_textEditingSymptom.text.isEmpty) {
                  return;
                }
                // Navigator.pop(context, _textEditingSymptom.text);
                addSymptom(_textEditingSymptom.text);
              },
              child: Text("Add symptom",
                  textAlign: TextAlign.center,
                  style: style.copyWith(
                      fontSize: 15,
                      color: Colors.white,
                      fontWeight: FontWeight.bold)),
            ),
    );

    return Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          symptomField,
          Padding(
              padding: const EdgeInsets.only(top: 52, left: 30, right: 30),
              child: addButton)
        ]);
  }

  void addSymptom(String name) {
    setState(() => isLoading = true);
    var api = new APICallRepository();
    api
        .addSymptom(deviceId,name  , token)
        .then((response) {
      setState(() {
        setState(() => isLoading = false);
        Navigator.pop(context, _textEditingSymptom.text);
      });
    }, onError: (error) {
      setState(() {
        print("ERROR FOUND FINALLY HERE ===> " + error.toString());
        // setState(() => isLoading = false);
        setState(() => isLoading = false);
        if (error.toString().contains('Unauthorised')) {
          doLogout(context, error.toString());
        } else if (error is ProviderDeactivatedException) {
          doLogout(context, error.toString());
        } else if (error is PatientDeactivatedException) {
          doLogout(context, error.toString());
        } else {
          _scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text(error.toString(),
                textAlign: TextAlign.center,
                style: style.copyWith(
                    color: Colors.white, fontWeight: FontWeight.bold)),
            duration: Duration(seconds: 5),
          ));
        }
      });
    });
  }
}
