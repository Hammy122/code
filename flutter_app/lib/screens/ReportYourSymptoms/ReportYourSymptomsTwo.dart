import 'dart:convert';

import 'package:circular_check_box/circular_check_box.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app/models/SymptomsItem.dart';
import 'package:flutter_app/screens/ChangePassword.dart';
import 'package:flutter_app/screens/ReportYourPain/ReportYourPainQuestionTwo.dart';
import 'package:flutter_app/screens/ReportYourSymptoms/SymptomsList.dart';
import 'package:flutter_app/utils/ColorUtils.dart';
import 'package:flutter_app/utils/CustomPopupMenu.dart';
import 'package:flutter_app/models/ReachBean.dart';
import 'package:flutter_app/screens/login/Signin.dart';
import 'package:flutter_app/utils/app_widgets.dart';
import 'package:flutter_app/utils/utils.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:toast/toast.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../models/UserBean.dart';
import '../../utils/UtilsImporter.dart';
import 'package:http/http.dart' as http;

import 'ReportYourSymptomsThree.dart';

class ReportYourSymptomsTwo extends StatefulWidget {

  @override
  _ReportYourSymptomsTwoState createState() => _ReportYourSymptomsTwoState();
}

class _ReportYourSymptomsTwoState extends State<ReportYourSymptomsTwo> {
  bool isDateOfBirthAvailable = true;
  String _birthDay = "Select Date";
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
            createToolBar(
                true, 'Report your Symptoms', 'Question 2 of 4', 'Next'),
            new Container(
              margin: const EdgeInsets.only(top: 10),
              height: 1,
              color: getColorFromHex('#CCCCCC'),
            ),
            SizedBox(height: 20),
            Expanded(
                child: Container(
                    padding: const EdgeInsets.symmetric(horizontal: 30.0),
                    child: getLayout()))
          ])),
    );
  }

  Widget createToolBar(
      bool isBackButton, String title, String subTitle, String buttonLabel) {
    return Stack(
      children: [
        SizedBox(
          width: 8,
        ),
        Positioned(
            left: 10,
            top: isBackButton ? 20 : 10,
            child: Align(
                alignment: Alignment.center,
                child: InkWell(
                    child: isBackButton
                        ? Image.asset(
                            "assets/ic_back.png",
                            height: 25,
                            width: 40,
                          )
                        : Image.asset(
                            "assets/buttonsTopBarClose.png",
                            height: 40,
                            width: 40,
                          ),
                    onTap: () {
                      goBack(context);
                    }))),
        Center(
            child: Column(
          children: <Widget>[
            new Padding(
              padding: const EdgeInsetsDirectional.only(top: 15.0, bottom: 0),
              child: getTextWidget(title, 17, Colors.black, FontWeight.bold),
            ),
            getTextWidget(subTitle, 13, getThemeGreenColor(), FontWeight.w600),
          ],
        )),
        Positioned(
            right: 10,
            top: 10,
            child: Center(
                child: Container(
                    decoration: new BoxDecoration(
                      borderRadius: new BorderRadius.circular(4.0),
                      color: getThemeGreenColor(),
                    ),
                    child: InkWell(
                      child: Visibility(
                          visible: true,
                          child: Padding(
                              padding: EdgeInsets.only(
                                  left: 8, top: 6, right: 8, bottom: 6),
                              child: getTextWidget(buttonLabel, 17,
                                  Colors.white, FontWeight.bold))),
                      onTap: () {
                        goToNext();
                      },
                    )))),
        SizedBox(
          width: 10,
        ),
      ],
    );
  }

  void goToNext() {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return ReportYourSymptomsThree();
    }));
  }

  Widget getLayout() {
//    final searchButton = Material(
//      elevation: 5.0,
//      borderRadius: BorderRadius.circular(30.0),
//      color: getThemeBlueColor(),
//      child: isLoading
//          ? Center(
//              child: SizedBox(
//                child: Padding(
//                    padding: EdgeInsets.fromLTRB(20.0, 18.0, 20.0, 18.0),
//                    child: CircularProgressIndicator()),
//                height: 50.0,
//                width: 60.0,
//              ),
//            )
//          : MaterialButton(
//              minWidth: MediaQuery.of(context).size.width,
//              padding: EdgeInsets.fromLTRB(20.0, 18.0, 20.0, 18.0),
//              onPressed: () {
//                print("Start");
////                Navigator.push(context, MaterialPageRoute(builder: (context) {
////                  return SymptomsList();
////                }));
//
//                Navigator.push(
//                  context,
//                  MaterialPageRoute(
//                    builder: (context) => SymptomsList(selectedList),
//                  ),
//                ).then((value) {
//                  //print("===added===>" + value.toString());
//                  if (value != null) {
//                    setState(() {
//                      // selectedList.clear();
//                      //  selectedList.addAll(value);
//                    });
//                  }
//                });
//
////                Navigator.push(
////                  context,
////                  MaterialPageRoute(
////                    builder: (context) => SymptomsList(),
////                  ),
////                ).then((value) {
//////                  print("===selected===>" + value.toString());
//////                  if (value != null) {
//////                    setState(() {
//////                      Symptom.symptomList.addAll(value);
//////                    });
//////                  }
////                });
//              },
//              child: Text("Search symptoms",
//                  textAlign: TextAlign.center,
//                  style: style.copyWith(
//                      fontSize: 15,
//                      color: Colors.white,
//                      fontWeight: FontWeight.bold)),
//            ),
//    );

    Future _chooseDate(BuildContext context, String initialDateString) async {
      var now = new DateTime.now();
      var initialDate = convertToDate(initialDateString) ?? now;
      initialDate = (initialDate.year >= now.year && initialDate.isAfter(now)
          ? initialDate
          : now);
      DatePicker.showDatePicker(context, showTitleActions: true, maxTime: now,
          onConfirm: (date) {
        setState(() {
          isDateOfBirthAvailable = true;
          DateTime dt = date;
          String r = ftDateAsStr(dt);
          _birthDay = r;
        });
      }, currentTime: initialDate);
    }

    final birthDayField = GestureDetector(
        onTap: () {
          _chooseDate(context, _birthDay);
        },
        child: Container(
          padding: EdgeInsets.fromLTRB(10.0, 15.0, 10.0, 15.0),
          child: Text("$_birthDay",
              style: TextStyle(
                  color: _birthDay.toString() == "Select Date"
                      ? getInputBoxHintColor()
                      : getThemeBlackColor(),
                  fontSize: 14,
                  fontFamily: getThemeFontName())),
          decoration: BoxDecoration(
            color: getInputBoxFillColor(),
            border: isDateOfBirthAvailable
                ? Border.all(
                    color: getInputBoxFillColor(),
                    width: 1,
                  )
                : Border.all(
                    color: getRedColor(),
                    width: 1,
                  ),
            borderRadius: BorderRadius.circular(2),
          ),
        ));
    return Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          getTextWidget("When did they start? ", 20, getThemeBlackColor(),
              FontWeight.normal),
          Padding(
              padding: const EdgeInsets.only(top: 52, left: 30, right: 30),
              child: birthDayField),
//          Expanded(
//              child: Visibility(
//                  visible: selectedList.length > 0,
//                  child: Padding(
//                      padding: const EdgeInsets.only(top: 52),
//                      child: getAndSetSelectedSymptoms())))
        ]);
  }
}
