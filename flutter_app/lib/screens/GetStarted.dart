import 'package:custom_switch/custom_switch.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/screens/ForgotPassword.dart';
import 'package:flutter_app/screens/Home.dart';
import 'package:flutter_app/screens/ActivateAccount.dart';
import 'package:flutter_app/screens/body/human_anatomy.dart';

import 'package:flutter_app/screens/login/Signin.dart';
import 'package:flutter_app/utils/ColorUtils.dart';
import 'package:flutter_app/utils/UtilsImporter.dart';
import 'package:flutter_app/utils/utils.dart';
import 'package:flutter_progress_button/flutter_progress_button.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:toast/toast.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'dart:math' as math show sin, pi;

import 'PrivacyPolicy.dart';


class GetStarted extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        color: Colors.white,
        debugShowCheckedModeBanner: false,
        title: 'Heartsend',
        theme: ThemeData(
          primarySwatch: Colors.grey,
          backgroundColor: Color.fromRGBO(229, 229, 229, 1),
        ),
        home: GetStartedPage(title: 'GetStarted'));
  }
}

class GetStartedPage extends StatefulWidget {
  GetStartedPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  GetStartedScreenState createState() => GetStartedScreenState();
}

class GetStartedScreenState extends State<GetStartedPage> {
   TextStyle style = TextStyle(fontFamily: getThemeFontName(), fontSize: 15.0);


  TapGestureRecognizer recognizerCreateAccount;
  bool isLogin = false;
  bool isLoading = false;
   final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
   final formKey = new GlobalKey<FormState>();
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  bool switchControl = false;
  var textHolder = 'Switch is OFF';


  void _submit() {
    setState(() => isLoading = true);

  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      checkInternet();
    });
    recognizerCreateAccount = TapGestureRecognizer()
      ..onTap = () {

      };
  }

  checkInternet() {
    setState(() {
      Future<bool> result = UtilsImporter().commanUtils.checkConnection();
      result.then((data) {
        isLogin = data;
      });
    });
  }

  @override
  Widget build(BuildContext context) {

    final activateButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: getThemeGreenColor(),
      child: isLoading
          ? Center(
              child: SizedBox(
                child: Padding(
                    padding: EdgeInsets.fromLTRB(30.0, 18.0, 30.0, 18.0),
                    child: CircularProgressIndicator()),
                height: 50.0,
                width: 60.0,
              ),
            )
          : MaterialButton(
              minWidth: MediaQuery.of(context).size.width,
              padding: EdgeInsets.fromLTRB(30.0, 18.0, 30.0, 18.0),
              onPressed: () {
                  print("Start");
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) {
                        return ActivateAccount();
//                        return HumanAnatomy();
                      }));
              },
              child: Text("Activate account",
                  textAlign: TextAlign.center,
                  style: style.copyWith( fontSize: 15,
                      color: Colors.white, fontWeight: FontWeight.bold)),
            ),
    );
    final signInButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: getThemeBlueColor(),
      child: isLoading
          ? Center(
        child: SizedBox(
          child: Padding(
              padding: EdgeInsets.fromLTRB(20.0, 18.0, 20.0, 18.0),
              child: CircularProgressIndicator()),
          height: 50.0,
          width: 60.0,
        ),
      )
          : MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 18.0, 20.0, 18.0),
        onPressed: () {

            print("Start");

            Navigator.push(context,
                MaterialPageRoute(builder: (context) {
                  return Signin();
//                        return HumanAnatomy();
                }));

        },
        child: Text("Sign in",
            textAlign: TextAlign.center,
            style: style.copyWith(fontSize: 15,
                color: Colors.white, fontWeight: FontWeight.bold)),
      ),
    );

    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            //Header Container
            Container(
              padding: const EdgeInsets.only(top:25.0),
              alignment: Alignment.center,
              child:
              Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(30),
                      child: Center(
                          child: Image.asset(
                            UtilsImporter().stringUtils.logo,
                            height: 135,
                            fit: BoxFit.fitHeight,
                          )),
                    ),
                    Padding(
                        padding: const EdgeInsets.only(left:30,right:30,top: 20),
                        child: Center(
                          child: new Text(
                            "We want to ensure simple, safe and secure 24hr monitoring.",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: getThemeBlackColor(),
                                fontWeight: FontWeight.normal,
                                fontSize: 20,
                                fontFamily: getThemeFontName()),
                          ),
                        )),
                  ])

            ),

            //Body Container
            Expanded(
              child: Center(child : SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    Center(
                        child: Image.asset(
                          UtilsImporter().stringUtils.getStartedMiddleLogo,
                          height: 180,
                          fit: BoxFit.fitHeight,
                        )),
                  ],
                ),
              )),
            ),

            //Footer Container
            //Here you will get unexpected behaviour when keyboard pops-up.
            //So its better to use `bottomNavigationBar` to avoid this.
            Container(
              padding: const EdgeInsets.all(8.0),
              alignment: Alignment.center,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Padding(
                      padding:
                      const EdgeInsets.only(left: 30, right: 30),
                      child: activateButton),
                  Padding(
                      padding:
                      const EdgeInsets.only(top:20,left: 30, right: 30),
                      child: signInButton),
                  SizedBox(height: 30,),
                  Center(child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text("Read our",textAlign: TextAlign.left,
                          style: TextStyle(
                              color: getThemeGrayColor(),
                              fontWeight: FontWeight.w600,
                              fontSize: 15,
                              fontFamily: getThemeFontName())),
                      SizedBox(width: 5,),
                      new Container(
                        padding: const EdgeInsets.only(left:2.0),
                        decoration: myBoxDecoration(),
                        child: new GestureDetector(
                            child: Text("Privacy Policy.",
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                    color: getColorFromHex("#1b76bb"),
                                    fontWeight: FontWeight.bold,
                                    fontSize: 15,
                                    fontFamily: getThemeFontName())),
                            onTap: () {
                              Navigator.push(context,
                                  MaterialPageRoute(builder: (context) {
                                    return PrivacyPolicy();
                                  }));
                            }),
                      ),
                      SizedBox(height: 40,),
                    ],
                  ),),],),
            ),
          ],
        ),
      ),
    );



  }




}

BoxDecoration myBoxDecoration() {
  return BoxDecoration(
    border: Border(
      bottom: BorderSide(
        color: getColorFromHex("#1b76bb"),
        //                   <--- border color
        width: 1.0,
      ),
    ),
  );
}


Future<dynamic> callSignInAPI(String email, String password) async {
  final response = await http.post(
      UtilsImporter().apiUtils.baseURL + UtilsImporter().apiUtils.signin,
      headers: header(),
      body: json.encode({"email": email, "password": password}));

  print("sign in response : " + response.body);
  return json.decode(response.body);
}

header() => {"Content-Type": 'application/json'};

class ThreeSizeDot extends StatefulWidget {
  ThreeSizeDot(
      {Key key,
      this.shape = BoxShape.circle,
      this.duration = const Duration(milliseconds: 1000),
      this.size = 8.0,
      this.color_1,
      this.color_2,
      this.color_3,
      this.padding = const EdgeInsets.all(2)})
      : super(key: key);

  final BoxShape shape;
  final Duration duration;
  final double size;
  final Color color_1;
  final Color color_2;
  final Color color_3;
  final EdgeInsetsGeometry padding;

  @override
  _ThreeSizeDotState createState() => _ThreeSizeDotState();
}

class _ThreeSizeDotState extends State<ThreeSizeDot>
    with SingleTickerProviderStateMixin {
  AnimationController animationController;
  Animation<double> animation_1;
  Animation<double> animation_2;
  Animation<double> animation_3;

  @override
  void initState() {
    super.initState();
    animationController =
        AnimationController(vsync: this, duration: widget.duration);
    animation_1 = DelayTween(begin: 0.0, end: 1.0, delay: 0.0)
        .animate(animationController);
    animation_2 = DelayTween(begin: 0.0, end: 1.0, delay: 0.2)
        .animate(animationController);
    animation_3 = DelayTween(begin: 0.0, end: 1.0, delay: 0.4)
        .animate(animationController);
    animationController.repeat();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          ScaleTransition(
            scale: animation_1,
            child: Padding(
              padding: widget.padding,
              child: Dot(
                shape: widget.shape,
                size: widget.size,
                color: Colors.white,
              ),
            ),
          ),
          ScaleTransition(
            scale: animation_2,
            child: Padding(
              padding: widget.padding,
              child: Dot(
                shape: widget.shape,
                size: widget.size,
                color: Colors.white,
              ),
            ),
          ),
          ScaleTransition(
            scale: animation_3,
            child: Padding(
              padding: widget.padding,
              child: Dot(
                shape: widget.shape,
                size: widget.size,
                color: Colors.white,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class DelayTween extends Tween<double> {
  DelayTween({
    double begin,
    double end,
    this.delay,
  }) : super(begin: begin, end: end);

  final double delay;

  @override
  double lerp(double t) =>
      super.lerp((math.sin((t - delay) * 2 * math.pi) + 1) / 2);

  @override
  double evaluate(Animation<double> animation) => lerp(animation.value);
}

class Dot extends StatelessWidget {
  final BoxShape shape;
  final double size;
  final Color color;

  Dot({
    Key key,
    this.shape,
    this.size,
    this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        width: size,
        height: size,
        decoration: BoxDecoration(color: color, shape: shape),
      ),
    );
  }
}
