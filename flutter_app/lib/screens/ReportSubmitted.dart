import 'dart:convert';

import 'package:circular_check_box/circular_check_box.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app/models/PainFeelItem.dart';
import 'package:flutter_app/screens/ChangePassword.dart';
import 'package:flutter_app/screens/Home.dart';
import 'package:flutter_app/screens/ReportYourPain/ReportYourPainQuestionSix.dart';
import 'package:flutter_app/screens/ReportYourPain/ReportYourPainQuestionTwo.dart';
import 'package:flutter_app/utils/ColorUtils.dart';
import 'package:flutter_app/utils/CustomPopupMenu.dart';
import 'package:flutter_app/models/ReachBean.dart';
import 'package:flutter_app/screens/login/Signin.dart';
import 'package:flutter_app/utils/app_widgets.dart';
import 'package:flutter_app/utils/utils.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:toast/toast.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:url_launcher/url_launcher.dart';
import '../models/UserBean.dart';
import '../utils/UtilsImporter.dart';
import 'package:http/http.dart' as http;

class ReportSubmitted extends StatefulWidget {
  @override
  _ReportYourPainState createState() => _ReportYourPainState();
}

class _ReportYourPainState extends State<ReportSubmitted> {
  TextStyle style = TextStyle(fontFamily: getThemeFontName(), fontSize: 18.0);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(const Duration(milliseconds: 4000), () {
// Here you can write your code
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context) => Home()),
          (Route<dynamic> route) => false);
      setState(() {
        // Here you can write your code for open new view
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
            Expanded(
              child: Container(
                  margin: const EdgeInsets.all(20),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Image.asset(
                          UtilsImporter().stringUtils.reportSubmittedIcon,
                          width: 70,
                          height: 70,
                        ),
                        SizedBox(
                          height: 50,
                        ),
                        getTextWidget(
                            "Your information was sent to your provider! Report as often as you need to!",
                            20,
                            getThemeBlackColor(),
                            FontWeight.normal),
                      ])),
            )
          ])),
    );
  }

  void goToNext() {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return ReportYourPainQuestionSix();
    }));
  }
}
