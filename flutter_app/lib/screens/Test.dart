import 'dart:convert';

import 'package:circular_check_box/circular_check_box.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app/models/PainFeelItem.dart';
import 'package:flutter_app/screens/ChangePassword.dart';
import 'package:flutter_app/screens/ChatsScreen.dart';
import 'package:flutter_app/screens/Home.dart';
import 'package:flutter_app/screens/ReportYourPain/ReportYourPainQuestionSix.dart';
import 'package:flutter_app/screens/ReportYourPain/ReportYourPainQuestionTwo.dart';
import 'package:flutter_app/utils/ColorUtils.dart';
import 'package:flutter_app/utils/CustomPopupMenu.dart';
import 'package:flutter_app/models/ReachBean.dart';
import 'package:flutter_app/screens/login/Signin.dart';
import 'package:flutter_app/utils/app_widgets.dart';
import 'package:flutter_app/utils/utils.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:toast/toast.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:url_launcher/url_launcher.dart';
import '../models/UserBean.dart';
import '../utils/UtilsImporter.dart';
import 'package:http/http.dart' as http;

class Test extends StatefulWidget {
  @override
  _TestState createState() => _TestState();
}

class _TestState extends State<Test> with SingleTickerProviderStateMixin {
  TabController tabController;

//  final myImageAndCaption = [
//  ["assets/1.jpg", "Watch 1", "Rs. 100"],
//  ["assets/2.jpg", "Watch 2", "Rs.200"],
//  ["assets/4.jpg", "Watch 3", "Rs. 300"],
//  ["assets/5.jpg", "Watch 4", "Rs. 400"],
//   ];

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    tabController.dispose();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    tabController = TabController(length: 2, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    final searchField = Container(
      child: TextField(
        keyboardType: TextInputType.emailAddress,
        cursorColor: getThemeBlackColor(),
        onChanged: (text) {},
        decoration: InputDecoration(
          border: InputBorder.none,
          contentPadding: EdgeInsets.fromLTRB(10.0, 2.0, 10.0, 2.0),
          hintText: "Search",
        ),
        obscureText: false,
        style: style,
      ),
      decoration: BoxDecoration(
        color: getInputBoxFillColor(),
        border: Border.all(
          color: getInputBoxFillColor(),
          width: 1,
        ),
        borderRadius: BorderRadius.circular(2),
      ),
    );

    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
            body: SafeArea(
                child: Column(
          children: <Widget>[
            Container(
                padding: const EdgeInsets.only(
                    left: 15, right: 5, top: 15, bottom: 5),
                color: Colors.white,
                child: Stack(
                  children: [
                    Positioned(
                        left: 10,
                        child: Align(
                            alignment: Alignment.center,
                            child: InkWell(
                                child: Image.asset(
                                  "assets/buttonsTopBarClose.png",
                                  height: 40,
                                  width: 40,
                                ),
                                onTap: () {}))),
                    Center(
                        child: Container(
                      margin: const EdgeInsets.only(top: 5, bottom: 5),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Expanded(child: searchField),
                          InkWell(
                              child: Image.asset(
                                "assets/user_avatar.png",
                                height: 50,
                                width: 50,
                              ),
                              onTap: () {})
                        ],
                      ),
                    )),
                  ],
                )),
            Container(
              margin: const EdgeInsets.only(top: 0),
              height: 1,
              color: getInputHintTextColor(),
            ),
            Align(alignment: Alignment.centerLeft, child: _getTabBar()),
            Expanded(
                child: Container(
              child: _getTabBarView(
                <Widget>[
                  MyPage(),
                  MyPage(),
                ],
              ),
            )),

//            Expanded(
//                child: new GridView.count(
//              primary: true,
//              crossAxisCount: 2,
//              children: List.generate(array.length, (index) {
//                return getStructuredGridCell(array[index]);
//              }),
//            ))
//          Expanded(
//              child: GridView.count(
//                shrinkWrap: true,
//                  crossAxisCount: 3,
//                  children: [
//                    ...array.map(
//                          (i) => Column(
//                        mainAxisSize: MainAxisSize.max,
//                        children: [
//                          SizedBox(height: 5,),
//                          Material(
//                            elevation: 3.0,
//                            child: Image.asset(
//                              i.image,
//                              fit: BoxFit.fitWidth,
//                              height: 100,
//                              width: 100,
//                            ),
//                          ),
//                          SizedBox(height: 5,),
//                          Text(i.title), Text(i.price),
//                        ],
//                      ),)
//        ],),
//      ),
          ],
        ))));
  }

  TabBar _getTabBar() {
    return TabBar(
      isScrollable: false,
      labelStyle: TextStyle(
          fontSize: 16.0,
          fontWeight: FontWeight.w600,
          color: getColorFromHex("#000000"),
          fontFamily: 'Roboto'),
      //For Selected tab
      unselectedLabelStyle: TextStyle(
          fontSize: 16.0,
          color: getColorFromHex("#A0A1A2"),
          fontFamily: 'Roboto'),
      indicatorColor: getThemeBlackColor(),

      unselectedLabelColor: getColorFromHex("#A0A1A2"),
      labelColor: getThemeBlackColor(),
      indicatorSize: TabBarIndicatorSize.tab,
      tabs: <Widget>[
        Tab(
          text: "One Page",
        ),
        Tab(text: "Two page"),
      ],
      controller: tabController,
    );
  }

  TabBarView _getTabBarView(tabs) {
    return TabBarView(
      children: tabs,
      controller: tabController,
    );
  }
}

class MyPage extends StatelessWidget {
  List<ProductModel> array = new List();

  @override
  Widget build(BuildContext context) {
    array.add(new ProductModel("Watch 1", "Rs. 100", "assets/logo.png"));
    array.add(new ProductModel("Watch 2", "Rs. 200", "assets/logo.png"));
    array.add(new ProductModel("Watch 3", "Rs. 300", "assets/logo.png"));
    array.add(new ProductModel("Watch 4", "Rs. 400", "assets/logo.png"));

    Card getStructuredGridCell(ProductModel product) {
      return new Card(
          elevation: 1.5,
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisSize: MainAxisSize.min,
            verticalDirection: VerticalDirection.down,
            children: <Widget>[
              new Image.asset(
                product.image,
                height: 130.0,
                width: 100.0,
              ),
              new Center(
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      height: 10,
                    ),
                    new Text(product.title),
                    SizedBox(
                      height: 5,
                    ),
                    new Text(product.price),
                  ],
                ),
              )
            ],
          ));
    }

    Widget _getContactItemCell(BuildContext context, int index) {
      return new Container(
          padding:
              const EdgeInsets.only(left: 0, right: 0, top: 10, bottom: 10),
          color: Colors.white,
          child: Expanded(
              child: new GridView.count(
            primary: true,
            crossAxisCount: 2,
            children: List.generate(array.length, (index) {
              return getStructuredGridCell(array[index]);
            }),
          )));
    }

    return Scaffold(
        body: Container(child :    new GridView.count(
      primary: true,
      crossAxisCount: 2,
      children: List.generate(array.length, (index) {
        return getStructuredGridCell(array[index]);
      }),
    )));
  }
}

class ProductModel {
  String title;
  String price;
  String image;

  ProductModel(this.title, this.price, this.image);
}
