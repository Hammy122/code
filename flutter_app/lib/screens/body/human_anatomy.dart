library human_anatomy;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/utils/ColorUtils.dart';
import 'package:flutter_app/utils/utils.dart';
import 'package:flutter_svg/flutter_svg.dart';

class HumanAnatomy extends StatefulWidget {
  final ValueChanged<List> onChanged;

  const HumanAnatomy({Key key, this.onChanged}) : super(key: key);

  @override
  _HumanAnatomyState createState() => new _HumanAnatomyState();
}

class _HumanAnatomyState extends State<HumanAnatomy> {
  var _bodyPartList = [];
  bool isLogoutLoading = false;
  bool sideSwitch = false;

  @override
  void initState() {
    super.initState();
    if (mounted) {
      _publishSelection(_bodyPartList);
    }
  }

  void toggleSwitch(bool value) {
    if (sideSwitch == false) {
      setState(() {
        sideSwitch = true;
      });
      print('Switch is ON');
      // Put your code here which you want to execute on Switch ON event.
    } else {
      setState(() {
        sideSwitch = false;
      });
      print('Switch is OFF');
      // Put your code here which you want to execute on Switch OFF event.
    }
  }

  @override
  Widget build(BuildContext context) {
    Widget _settingsPopup() => PopupMenuButton<int>(
          itemBuilder: (context) => [
            PopupMenuItem(
              value: 1,
              child: ListTile(
                dense: true,
                contentPadding: EdgeInsets.only(left: 0.0, right: 0.0),
                leading: IconButton(
                  icon: Icon(
                    Icons.settings,
                    color: Colors.black,
                  ),
                ),
                title: Text('Settings',
                    style: TextStyle(
                        fontWeight: FontWeight.normal,
                        fontSize: 16,
                        fontFamily: 'Orkney')),
                onTap: () {
                  print("Settings");
                },
              ),
            ),
            PopupMenuItem(
              value: 2,
              child: ListTile(
                dense: true,
                contentPadding: EdgeInsets.only(left: 0.0, right: 0.0),
                leading: IconButton(
                  icon: Icon(
                    Icons.update,
                    color: Colors.black,
                  ),
                ),
                title: Text('Change your password',
                    style: TextStyle(
                        fontWeight: FontWeight.normal,
                        fontSize: 16,
                        fontFamily: 'Orkney')),
                onTap: () {
                  print("Hi");
                },
              ),
            ),
            PopupMenuItem(
              value: 3,
              child: isLogoutLoading
                  ? Center(
                      child: SizedBox(
                        child: Padding(
                            padding:
                                EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                            child: CircularProgressIndicator()),
                        height: 40.0,
                        width: 40.0,
                      ),
                    )
                  : ListTile(
                      dense: true,
                      contentPadding: EdgeInsets.only(left: 0.0, right: 0.0),
                      leading: IconButton(
                        icon: Icon(
                          Icons.exit_to_app,
                          color: Colors.black,
                        ),
                        onPressed: () {},
                      ),
                      title: Text('Log Out',
                          style: TextStyle(
                              fontWeight: FontWeight.normal,
                              fontSize: 16,
                              fontFamily: 'Orkney')),
                      onTap: () {
                        print("Hi");

//              setState(() {
//                isLogoutLoading=true;
//              });
                      },
                    ),
            )
          ],
        );
    return Scaffold(
        backgroundColor: Colors.white,
        body: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Container(
                  padding: const EdgeInsets.only(left: 0, right: 0, top: 40),
                  color: Colors.white,
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      new Padding(
                        padding: const EdgeInsets.fromLTRB(21.0, 0, 0.0, 0.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text('Where is your pain?',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20,
                                    fontFamily: 'Orkney')),
                            Text('select all that apply',
                                style: TextStyle(
                                    fontWeight: FontWeight.normal,
                                    fontSize: 15,
                                    fontFamily: 'Orkney')),
                          ],
                        ),
                      ),
                      new Container(
                        margin: const EdgeInsets.only(right: 1),
                        child: _settingsPopup(),
                      ),
                    ],
                  )),
              Expanded(
                child: SingleChildScrollView(child: humanAnatomy()),
              ),
              new Container(
                margin: const EdgeInsets.only(left: 30.0, right: 30, top: 5,bottom: 20),
                padding: const EdgeInsets.all(3.0),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text('Front',
                        style: TextStyle(
                            fontWeight: FontWeight.normal,
                            fontSize: 15,
                            fontFamily: 'Orkney')),
                    SizedBox(width: 5,),
                    CupertinoSwitch(
                      activeColor: getColorFromHex("#2e67b8"),
                      onChanged: toggleSwitch,
                      value: sideSwitch,
                    ),
                    SizedBox(width: 5,),
                    Text('Back',
                        style: TextStyle(
                            fontWeight: FontWeight.normal,
                            fontSize: 15,
                            fontFamily: 'Orkney')),
                  ],
                ),
              )
            ]));
  }

  Widget humanAnatomy() {
    return Container(
      margin: EdgeInsets.only(top: 50),
      color: Colors.white,
      width: 340.0,
      height: 557,
      child: SizedBox(
        child: Stack(
          children: <Widget>[
//            bodyPart("assets/head.svg", "Head", 0.0, 0.0, 0.0, 70.0),
//            bodyPart("assets/left_ear.svg", "leftEar", 32.0, 50.0, 0.0, 20.0),
//            bodyPart("assets/right_ear.svg", "rightEar", 32.0, 0.0, 50.0, 20.0),
//            bodyPart("assets/left_eye.svg", "LeftEye", 22.0, 20.0, 0.0, 10.0),
//            bodyPart("assets/right_eye.svg", "rightEye", 22.0, 0.0, 20.0, 10.0),
//            bodyPart("assets/nose.svg", "Nose", 25.0, 0.0, 0.0, 20.0),
//            bodyPart("assets/mouth.svg", "Mouth", 48.0, 0.0, 0.0, 10.0),
//            bodyPart("assets/neck.svg", "Neck", 58.0, 0.0, 0.0, 40.0),
//            bodyPart("assets/chest.svg", "Chest", 93.0, 0.0, 0.0, 95.0),
//            bodyPart("assets/abdomin.svg", "Abdomin", 175.0, 0.0, 0.0, 65.0),
//            bodyPart("assets/pelvis.svg", "Pelvis", 225.0, 0.0, 0.0, 50.0),
//            bodyPart("assets/publs.svg", "Publs", 275.0, 0.0, 0.0, 15.0),
//            bodyPart("assets/left_soulder.svg", "leftSoulder", 90.0, 105.0, 0.0,
//                50.0),
//            bodyPart("assets/right_soulder.svg", "rightSoulder", 89.0, 0.0,
//                110.0, 50.0),
//            bodyPart("assets/left_arm.svg", "leftArm", 120.0, 145.0, 0.0, 70.0),
//            bodyPart(
//                "assets/right_arm.svg", "rightArm", 118.0, 0.0, 142.0, 70.0),
//            bodyPart(
//                "assets/left_elbow.svg", "leftElbow", 172.0, 172.0, 0.0, 39.0),
//            bodyPart("assets/right_elbow.svg", "rightElbow", 170.0, 0.0, 170.0,
//                40.0),
//            bodyPart("assets/left_forearm.svg", "leftForearm", 195.0, 190.0,
//                0.0, 54.0),
//            bodyPart("assets/right_forearm.svg", "rightForearm", 195.0, 0.0,
//                190.0, 54.0),
//            bodyPart(
//                "assets/left_wrist.svg", "leftWrist", 238.0, 220.0, 0.0, 23.0),
//            bodyPart("assets/right_wrist.svg", "rightWrist", 238.0, 0.0, 220.0,
//                23.0),
//            bodyPart(
//                "assets/left_hand.svg", "leftHand", 250.0, 250.0, 0.0, 60.0),
//            bodyPart(
//                "assets/right_hand.svg", "rightHand", 250.0, 0.0, 250.0, 60.0),
//            bodyPart(
//                "assets/left_thigh.svg", "leftThigh", 242.0, 63.0, 0.0, 138.0),
//            bodyPart("assets/right_thigh.svg", "rightThigh", 242.0, 0.0, 63.0,
//                138.0),
//            bodyPart(
//                "assets/left_knee.svg", "leftKnee", 360.0, 68.0, 0.0, 48.0),
//            bodyPart(
//                "assets/right_knee.svg", "rightKnee", 360.0, 0.0, 68.0, 48.0),
//            bodyPart("assets/left_leg.svg", "leftLeg", 395.0, 64.0, 0.0, 105.0),
//            bodyPart(
//                "assets/right_leg.svg", "rightLeg", 393.0, 0.0, 65.0, 106.0),
//            bodyPart(
//                "assets/left_ankle.svg", "leftAnkle", 495.0, 64.0, 0.0, 25.0),
//            bodyPart(
//                "assets/right_ankle.svg", "rightAnkle", 493.0, 0.0, 68.0, 25.0),
//            bodyPart(
//                "assets/left_foot.svg", "leftFoot", 510.0, 80.0, 0.0, 60.0),
//            bodyPart(
//                "assets/right_foot.svg", "rightFoot", 508.0, 0.0, 83.0, 56.0),
          ],
        ),
      ),
    );
  }

  Widget bodyPart(String svgPath, String svgName, double marginTop,
      double marginRight, double marginLeft, double height) {
    Color _svgColor =
        _bodyPartList.contains(svgName) ? getColorFromHex("#2e67b8") : null;

    final Widget bodyPartSvg = new SvgPicture.asset(svgPath,
        semanticsLabel: svgName, color: _svgColor);
    return Container(
      margin:
          EdgeInsets.only(top: marginTop, right: marginRight, left: marginLeft),
      height: height,
      alignment: Alignment.topCenter,
      child: GestureDetector(
          onTap: () {
            setState(() {
              if (_bodyPartList.contains(svgName)) {
                _bodyPartList.remove(svgName);
              } else {
                _bodyPartList.add(svgName);
                // _bodyPartList.forEach((e) => print(e));
              }
            });
          },
          child: bodyPartSvg),
    );
  }

  void _publishSelection(List _bodyPartList) {
    if (widget.onChanged != null) {
      widget.onChanged(_bodyPartList);
    }
  }
}
