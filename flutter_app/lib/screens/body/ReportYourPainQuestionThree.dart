library human_anatomy;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/screens/ReportYourPain/ReportYourPainQuestionFour.dart';
import 'package:flutter_app/utils/ColorUtils.dart';
import 'package:flutter_app/utils/UtilsImporter.dart';
import 'package:flutter_app/utils/app_widgets.dart';
import 'package:flutter_app/utils/utils.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ReportYourPainQuestionThree extends StatefulWidget {
  final ValueChanged<List> onChanged;

  const ReportYourPainQuestionThree({Key key, this.onChanged}) : super(key: key);

  @override
  _HumanAnatomyState createState() => new _HumanAnatomyState();
}

class _HumanAnatomyState extends State<ReportYourPainQuestionThree> {
  var _bodyPartList = [];
  bool isLogoutLoading = false;
  int segmentedControlValue = 0;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  PersistentBottomSheetController controller;
  @override
  void initState() {
    super.initState();
    if (mounted) {
      _publishSelection(_bodyPartList);
    }
  }

  @override
  Widget build(BuildContext context) {

    Widget _settingsPopup() => PopupMenuButton<int>(
          itemBuilder: (context) => [
            PopupMenuItem(
              value: 1,
              child: ListTile(
                dense: true,
                contentPadding: EdgeInsets.only(left: 0.0, right: 0.0),
                leading: IconButton(
                  icon: Icon(
                    Icons.settings,
                    color: Colors.black,
                  ),
                ),
                title: Text('Settings',
                    style: TextStyle(
                        fontWeight: FontWeight.normal,
                        fontSize: 16,
                        fontFamily: 'Orkney')),
                onTap: () {
                  print("Settings");
                },
              ),
            ),
            PopupMenuItem(
              value: 2,
              child: ListTile(
                dense: true,
                contentPadding: EdgeInsets.only(left: 0.0, right: 0.0),
                leading: IconButton(
                  icon: Icon(
                    Icons.update,
                    color: Colors.black,
                  ),
                ),
                title: Text('Change your password',
                    style: TextStyle(
                        fontWeight: FontWeight.normal,
                        fontSize: 16,
                        fontFamily: 'Orkney')),
                onTap: () {
                  print("Hi");
                },
              ),
            ),
            PopupMenuItem(
              value: 3,
              child: isLogoutLoading
                  ? Center(
                      child: SizedBox(
                        child: Padding(
                            padding:
                                EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                            child: CircularProgressIndicator()),
                        height: 40.0,
                        width: 40.0,
                      ),
                    )
                  : ListTile(
                      dense: true,
                      contentPadding: EdgeInsets.only(left: 0.0, right: 0.0),
                      leading: IconButton(
                        icon: Icon(
                          Icons.exit_to_app,
                          color: Colors.black,
                        ),
                        onPressed: () {},
                      ),
                      title: Text('Log Out',
                          style: TextStyle(
                              fontWeight: FontWeight.normal,
                              fontSize: 16,
                              fontFamily: 'Orkney')),
                      onTap: () {
                        print("Hi");

//              setState(() {
//                isLogoutLoading=true;
//              });
                      },
                    ),
            )
          ],
        );

    return Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.white,
        body: SafeArea(
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
              Container(
                padding: const EdgeInsets.only(left: 0, right: 0, top: 0),
                color: Colors.white,
                child: createToolBar( true, 'Report your Pain', 'Question 3 of 5', 'Next'),
              ),
              new
              Container(
                margin: const EdgeInsets.only(top: 10),
                height: 1,
                color: getColorFromHex('#CCCCCC'),
              ),
              SizedBox(height: 20),



              getTextWidget("Where is your pain located?", 20,
                  getThemeBlackColor(), FontWeight.normal),
              SizedBox(
                height: 3,
              ),
              getTextWidget("Select all that apply", 13, getThemeBlackColor(),
                  FontWeight.w600),
              SizedBox(
                height: 20,
              ),
              Container(
                  margin: const EdgeInsets.only(left: 20.0, right: 20),
                  child: CupertinoSlidingSegmentedControl(
                      children: {
                        0: Container(
                            padding: EdgeInsets.symmetric(vertical: 9.0),
                            child: getTextWidget(
                                "FRONT", 13, Colors.white, FontWeight.bold)),
                        1: Container(
                            padding: EdgeInsets.symmetric(vertical: 9.0),
                            child: getTextWidget(
                                "BACK", 13, Colors.white, FontWeight.bold)),
                      },
                      backgroundColor: getInputBoxFillColor(),
                      thumbColor: getThemeGreenColor(),
                      groupValue: segmentedControlValue,
                      onValueChanged: (newValue) {
                        setState(() {
                          segmentedControlValue = newValue;
                        });
                      }
                      )
              ),
              segmentedControlValue == 1 ?
                    Expanded(
                      child: SingleChildScrollView(child: humanAnatomyBack()),
                    )
                  : Expanded(
                      child: SingleChildScrollView(child: humanAnatomyFront()),
                    ),
              new Container(
                margin: const EdgeInsets.only(
                    left: 30.0, right: 30, top: 5, bottom: 20),
                padding: const EdgeInsets.all(3.0),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    getTextWidget(
                        'Spots selected: ' + _bodyPartList.length.toString(),
                        16,
                        getThemeGrayColor(),
                        FontWeight.w600),
                  ],
                ),
              )


            ]
            )
        )
    );
  }

  Widget createToolBar( bool isBackButton, String title, String subTitle, String buttonLabel) {
    return Stack(
      children: [
        SizedBox(
          width: 8,
        ),
        Positioned(
            left: 10,
            top: isBackButton ? 20 : 10,
            child: Align(
                alignment: Alignment.center,
                child: InkWell(
                    child: isBackButton
                        ? Image.asset(
                            "assets/ic_back.png",
                            height: 25,
                            width: 40,
                          )
                        : Image.asset(
                            "assets/buttonsTopBarClose.png",
                            height: 40,
                            width: 40,
                          ),
                    onTap: () {
                      goBack(context);
                    }))),
        Center(
            child: Column(
          children: <Widget>[
            new Padding(
              padding: const EdgeInsetsDirectional.only(top: 15.0, bottom: 0),
              child: getTextWidget(title, 17, Colors.black, FontWeight.bold),
            ),
            getTextWidget(subTitle, 13, getThemeGreenColor(), FontWeight.w600),
          ],
        )),
        Positioned(
            right: 10,
            top: 10,
            child: Center(
                child: Container(
                    decoration: new BoxDecoration(
                      borderRadius: new BorderRadius.circular(4.0),
                      color: getThemeGreenColor(),
                    ),
                    child: InkWell(
                      child: Visibility(
                          visible: _bodyPartList.length > 0,
                          child: Padding(
                              padding: EdgeInsets.only(
                                  left: 8, top: 6, right: 8, bottom: 6),
                              child: getTextWidget(buttonLabel, 17,
                                  Colors.white, FontWeight.bold))),
                      onTap: () {
                        goToNext();
                      },
                    )))),
        SizedBox(
          width: 10,
        ),
      ],
    );
  }




  void goToNext() {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return ReportYourPainQuestionFour();
    }));
  }

  Widget humanAnatomyFront() {
    return Container(
      margin: EdgeInsets.only(top: 50),
      color: Colors.white,
      width: 400.0,
      height: 750,
      child: SizedBox(
        child: Stack(
          children: <Widget>[
            bodyPart(UtilsImporter().stringUtils.front_head, "Head", 0.0, 0.0,
                0.0, 100.0),
            bodyPart(UtilsImporter().stringUtils.front_throat, "Throat", 91.0,
                0.0, 0.0, 38.0),
            bodyPart(UtilsImporter().stringUtils.front_chest, "Chest", 102.0,
                0.0, 0.0, 135.0),
            bodyPart(UtilsImporter().stringUtils.front_abdomen, "Abdomen",
                229.0, 0.0, 0.0, 119.0),
            bodyPart(UtilsImporter().stringUtils.front_pelvis, "Pelvis", 342.0,
                0.0, 0.0, 70.0),
            bodyPart(UtilsImporter().stringUtils.front_left_hip, "Left Hip",
                330.0, 95.0, 0.0, 62.0),
            bodyPart(UtilsImporter().stringUtils.front_right_hip, "Right Hip",
                330.0, 0.0, 95.0, 62.0),
            bodyPart(UtilsImporter().stringUtils.front_left_soulder,
                "Left Shoulder", 122.0, 175.0, 0.0, 96.0),
            bodyPart(UtilsImporter().stringUtils.front_right_soulder,
                "Right Shoulder", 122.0, 0.0, 174.0, 96.0),
            bodyPart(UtilsImporter().stringUtils.front_left_upper_arm,
                "Left Arm", 203.0, 192.0, 0.0, 85.0),
            bodyPart(UtilsImporter().stringUtils.front_right_upper_arm,
                "Right Arm", 206.0, 0.0, 191.0, 80.0),
            bodyPart(UtilsImporter().stringUtils.front_left_elbow_one,
                "LeftElbow_One", 283.0, 230.0, 0.0, 9.0),
            bodyPart(UtilsImporter().stringUtils.front_left_elbow_two,
                "LeftElbow_Two", 289.0, 177.0, 0.0, 9.0),
            bodyPart(UtilsImporter().stringUtils.front_right_elbow_one,
                "RightElbow_One", 285.0, 0.0, 177.0, 9.0),
            bodyPart(UtilsImporter().stringUtils.front_right_elbow_two,
                "RightElbow_Two", 285.0, 0.0, 225.0, 9.0),
            bodyPart(UtilsImporter().stringUtils.front_left_forearm,
                "Left Forearm", 292.0, 210.0, 0.0, 105.0),
            bodyPart(UtilsImporter().stringUtils.front_right_forearm,
                "Right Forearm", 290.0, 0.0, 200.0, 100.0),
            bodyPart(UtilsImporter().stringUtils.front_left_hand, "Left Hand",
                395.0, 210.0, 0.0, 55.0),
            showLeftRightButton("Right", 485.0, 260.0, 0.0, 55.0),
            bodyPart(UtilsImporter().stringUtils.front_right_hand, "Right Hand",
                388.0, 0.0, 200.0, 55.0),
            showLeftRightButton(" Left ", 485.0, 0.0, 260.0, 55.0),
            bodyPart(UtilsImporter().stringUtils.front_left_upper_leg,
                "Left Upper Leg", 390.0, 68.0, 0.0, 156.0),
            bodyPart(UtilsImporter().stringUtils.front_right_upper_leg,
                "Right Upper Leg", 390.0, 0.0, 68.0, 156.0),
            bodyPart(UtilsImporter().stringUtils.front_left_knee, "Left Knee",
                528.0, 57.0, 0.0, 36.0),
            bodyPart(UtilsImporter().stringUtils.front_right_knee, "Right Knee",
                528.0, 0.0, 57.0, 36.0),
            bodyPart(UtilsImporter().stringUtils.front_left_lower_leg,
                "Left Lower Leg", 548.0, 62.0, 0.0, 160.0),
            bodyPart(UtilsImporter().stringUtils.front_right_lower_leg,
                "Right Lower Leg", 548.0, 0.0, 62.0, 160.0),
            bodyPart(UtilsImporter().stringUtils.front_left_foot, "Left Foot",
                705.0, 55.0, 0.0, 32.0),
            bodyPart(UtilsImporter().stringUtils.front_right_foot, "Right Foot",
                705.0, 0.0, 55.0, 32.0),
          ],
        ),
      ),
    );
  }

  Widget humanAnatomyBack() {
    return Container(
      margin: EdgeInsets.only(top: 50),
      color: Colors.white,
      width: 400.0,
      height: 820,
      child: SizedBox(
        child: Stack(
          children: <Widget>[
            bodyPart(UtilsImporter().stringUtils.back_head, "Back of Head", 0.0,
                0.0, 0.0, 85.0),
            bodyPart(UtilsImporter().stringUtils.back_neck, "Neck", 59.0,
                0.0, 0.0, 57.0),
            bodyPart(UtilsImporter().stringUtils.back_upper_back,
                "Upper Back", 100.0, 0.0, 0.0, 142.0),
            bodyPart(UtilsImporter().stringUtils.back_lower_back,
                "Lower Back", 233.0, 0.0, 0.0, 105.0),
            bodyPart(UtilsImporter().stringUtils.back_right_buttock,
                "Right Buttock", 330.0, 0.0, 78.0, 94.0),
            bodyPart(UtilsImporter().stringUtils.back_left_buttock,
                "Left Buttock", 330.0, 78.0, 0.0, 94.0),
            bodyPart(UtilsImporter().stringUtils.back_left_upper_leg,
                "Left Upper Leg Back", 413.0, 78.0, 0.0, 134.0),
            bodyPart(UtilsImporter().stringUtils.back_right_upper_leg,
                "Right Upper Leg Back", 413.0, 0.0, 78.0, 134.0),
            bodyPart(UtilsImporter().stringUtils.back_left_upper_arm,
                "Left Arm Back", 122.0, 179.0, 0.0, 150.0),
            bodyPart(UtilsImporter().stringUtils.back_right_upper_arm,
                "Right Arm Back", 122.0, 0.0, 179.0, 150.0),
            bodyPart(UtilsImporter().stringUtils.back_left_elbow,
                "LeftElbow_back", 255.0, 190.0, 0.0, 32.0),
            bodyPart(UtilsImporter().stringUtils.back_right_elbow,
                "RightElbow_back", 255.0, 0, 190.0, 32.0),
            bodyPart(UtilsImporter().stringUtils.back_left_lower_arm,
                "Left Lower Arm", 270.0, 197.0, 0.0, 115.0),
            bodyPart(UtilsImporter().stringUtils.back_right_lower_arm,
                "Right Forearm Back", 270.0, 0.0, 197.0, 115.0),
            bodyPart(UtilsImporter().stringUtils.back_left_hand,
                "Left Hand Back", 382.0, 196.0, 0.0, 55.0),
            showLeftRightButton(" Left ", 485.0, 260.0, 0.0, 55.0),
            showLeftRightButton("Right", 485.0, 0.0, 260.0, 55.0),
            bodyPart(UtilsImporter().stringUtils.front_right_hand,
                "Right Hand Back", 382.0, 0.0, 196.0, 55.0),
            bodyPart(UtilsImporter().stringUtils.back_left_knee,
                "Left Knee Back", 542.0, 70.0, 0.0, 44.0),
            bodyPart(UtilsImporter().stringUtils.back_right_knee,
                "Right Knee Back", 542.0, 0.0, 70.0, 44.0),
            bodyPart(UtilsImporter().stringUtils.back_left_lower_leg,
                "Left Leg Back", 572.0, 74.0, 0.0, 180.0),
            bodyPart(UtilsImporter().stringUtils.back_right_lower_leg,
                "Right Leg Back", 572.0, 0.0, 74.0, 180.0),
            bodyPart(UtilsImporter().stringUtils.back_left_foot,
                "Left Foot Back", 750.0, 60.0, 0.0, 30.0),
            bodyPart(UtilsImporter().stringUtils.back_right_foot,
                "Right Foot Back", 750.0, 0.0, 60.0, 30.0),
          ],
        ),
      ),
    );
  }

  Widget bodyPart(String svgPath, String svgName, double marginTop, double marginRight, double marginLeft, double height) {
    Color _svgColor =  _bodyPartList.contains(svgName) ? getAnatomicalColor() : null;

//    final Widget bodyPartSvg = new SvgPicture.asset(svgPath,
//        semanticsLabel: svgName, color: _svgColor,allowDrawingOutsideViewBox: true);
    final Widget bodyPartSvg = new Image.asset(svgPath, color: _svgColor);
    return Container(
      margin:
          EdgeInsets.only(top: marginTop, right: marginRight, left: marginLeft),
      height: height,
      alignment: Alignment.topCenter,
      child: GestureDetector(
          onTap: () {
            setState(() {
              if (_bodyPartList.contains(svgName)) {
                _bodyPartList.remove(svgName);
              } else {
                _settingModalBottomSheet(context);
                _bodyPartList.add(svgName);
                // _bodyPartList.forEach((e) => print(e));
              }
            });
          },
          child: bodyPartSvg),
    );
  }

  Widget showLeftRightButton(String label, double marginTop, double marginRight,
      double marginLeft, double height) {
    return Container(
      margin:
          EdgeInsets.only(top: marginTop, right: marginRight, left: marginLeft),
      height: height,
      alignment: Alignment.topCenter,
      child: GestureDetector(onTap: () {}, child: addButton(label)),
    );
  }

  Widget addButton(String label) {
    return Container(
        padding: EdgeInsets.only(left: 15.0, right: 15.0, top: 8, bottom: 8),
        color: getThemeGrayColor(),
        child: getTextWidget(label, 20, Colors.white, FontWeight.normal));
  }

  void _publishSelection(List _bodyPartList) {
    if (widget.onChanged != null) {
      widget.onChanged(_bodyPartList);
    }
  }

  void _settingModalBottomSheet(context) {
    _closeModalBottomSheet();
    controller = _scaffoldKey.currentState.showBottomSheet((BuildContext context) {
      return Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(10),
              topRight: Radius.circular(10),
              bottomLeft: Radius.circular(10),
              bottomRight: Radius.circular(10)),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 8,
              blurRadius: 7,
              offset: Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        height: 300,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Stack(
                children: [
                  SizedBox(
                    width: 8,
                  ),
                  Center(
                      child: Column(
                    children: <Widget>[
                      new Padding(
                        padding: const EdgeInsetsDirectional.only(
                            top: 15.0, bottom: 20),
                        child: getTextWidget(
                            'Spots selected: ' +
                                _bodyPartList.length.toString(),
                            17,
                            Colors.black,
                            FontWeight.bold),
                      ),
                      Container()
                    ],
                  )),
                  Positioned(
                      right: 10,
                      top: 10,
                      child: Center(
                          child: Container(
                              decoration: new BoxDecoration(
                                borderRadius: new BorderRadius.circular(4.0),
                                color: getThemeGreenColor(),
                              ),
                              child: InkWell(
                                child: Visibility(
                                    visible: true,
                                    child: Padding(
                                        padding: EdgeInsets.only(
                                            left: 8,
                                            top: 6,
                                            right: 8,
                                            bottom: 6),
                                        child: getTextWidget('Done', 17,
                                            Colors.white, FontWeight.bold))),
                                onTap: () {
                                  _closeModalBottomSheet();
                                },
                              )))),
                  SizedBox(
                    width: 10,
                  ),
                ],
              ),
              Expanded(
                  child: SingleChildScrollView(
                      child: Container(
                          padding: const EdgeInsets.only(
                              left: 20, right: 20, bottom: 20, top: 20),
                          child: getTextWidget(
                              _bodyPartList
                                  .toString()
                                  .replaceAll("[", "")
                                  .replaceAll("]", ""),
                              18,
                              getThemeBlackColor(),
                              FontWeight.normal))))
            ],
          ),
        ),
      );
    });
  }

  void _closeModalBottomSheet() {
    if (controller != null) {
      controller.close();
      controller = null;
    }
  }
}
